"""
Watches the filelock in TSD (or any similar place) for new samples/analyses.

In order to make sure they are transferred completely, we create a hash for each sample/analysis with all
file names and file sizes. After a given interval, we traverse the same path again and create the hash again.
If the hash is the same, we can be pretty sure the copying has completed.

The files are then copied into the repository.
For samples, the MD5 sum is checked for the read (fastq.gz) files.
For analysis, we check that the analysis file exists.
If everything it good, a READY file is touched in the sample/analysis folder, marking it ready for the
automation system to start processing.

Log files are written to predefined log path.

A killfile is checked between every file. If present, stops the exporter.
"""

import os
import subprocess
import md5
import time
import json
import shutil
import glob
import logging
import datetime

# Don't set too low, as that might cause partially transferred files to be copied.
CHANGE_INTERVAL = 30

FILE_LOCK_PATH = '/tsd/p22/fx/import_alt/production/hts/'
REPO_PATH = [
    '/cluster/projects/p22/production/'
    ]
LOG_PATH = '/cluster/projects/p22/production/sw/logs/tsd-import/filelock-exporter/'

GROUP = 'p22-member-group'

KILLFILE_PATH = '/cluster/projects/p22/production/sw/killfilelock'

logFormatter = logging.Formatter("%(asctime)s %(levelname)s - %(message)s")
log = logging.getLogger(__name__)

try:
    os.makedirs(LOG_PATH)
except OSError:
    pass
fileHandler = logging.FileHandler("{0}/{1}.log".format(LOG_PATH, datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')))
fileHandler.setFormatter(logFormatter)
log.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
log.addHandler(consoleHandler)

log.setLevel(logging.INFO)
log.info("Logging setup done")

def get_path_hash(path):
    """
    Creates a hash for all files existing within path (inc subdirectories).
    The hash is generating by the file path and file size.

    Idea is that you can watch a certain path at regular intervals to observe if any files have changes recently.
    """
    path_md5 = md5.md5()
    for path, _, files in sorted(os.walk(path), key=lambda x: x[0]):
        if files:
            for f in files:
                p = os.path.join(path, f)
                size = os.stat(p).st_size
                path_md5.update(p)
                path_md5.update(str(size))
    return path_md5.hexdigest()


def copy_path(src, dest):
    subprocess.check_output('rsync -rlpD --perms --chmod=u=rwX,g=rwX,o= {} {}'.format(src, dest), shell=True)


def set_group(group, dest):
    try:
        subprocess.check_output('chgrp -R {} {}'.format(group, dest), shell=True)
    except IOError as e:
        log.error("Cannot set group of {}".format(dest))


def check_not_exists(src, dest):
    """
    Checks whether src should be processed again.
    For now just a simple check: if READY exists in dest, it is processed already
    """
    return not os.path.exists(os.path.join(dest, 'READY'))


def validate_sample(target, dest):
    """
    Searches for a sample metadata file and validates the data by checking
    the MD5 sums of the reads so we know the files are correctly copied.
    Finally touches a READY file in the directory.
    """

    for path, _, files in os.walk(dest):
        for f in files:
            if f.endswith(".sample"):
                with open(os.path.join(path, f)) as fd:
                    sample = json.load(fd)
                if sample.get('taqman'):
                    if not os.path.exists(os.path.join(path, sample['taqman'])):
                        raise RuntimeError("Missing taqman file.")
                for read in sample['reads']:
                    # Use md5sum as it's much faster than calculating md5 in python
                    if not os.path.exists(os.path.join(path, read['path'])):
                        raise RuntimeError("Missing read file {}".format(read['path']))
                    md5sum = subprocess.check_output(
                        'md5sum {}'.format(os.path.join(path, read['path'])),
                        shell=True
                    ).split(' ', 1)[0]

                    if md5sum != read['md5']:
                        with open(os.path.join(path, 'MD5_FAILED'), 'w') as fd:
                            msg = 'MD5 for file {} failed, expected {}, got {}'.format(
                                read['path'],
                                read['md5'],
                                md5sum
                            )
                            fd.write(msg)
                            log.error(msg)
                            return

                # set group of parent folder
                set_group(GROUP, path)

                # Touch READY file and validate
                try:
                    with open(os.path.join(path, 'READY'), 'w'):
                        log.info("Sample {} validated.".format(sample['name']))
                except IOError as e:
                    log.error("Cannot touch READY file in {}".format(path))

                break


def validate_analysis(target, dest):
    """
    Only checks for analysis file, if so it creates READY file.
    """
    if glob.glob(os.path.join(dest, '*.analysis')):
        # set group of parent folder
        set_group(GROUP, dest)

        # Touch READY file and validate
        try:
            with open(os.path.join(dest, 'READY'), 'w'):
                log.info("Analysis validated.")
        except IOError as e:
            log.error("Cannot touch READY file in {}".format(dest))


def poll_changes():
    targets = [
        {
            'path': os.path.join(FILE_LOCK_PATH, 'analyses'),
            'hashes': dict(),
            'condition': check_not_exists,
            'repo_target': 'analyses',
            'postprocess': validate_analysis,
            'filelock_glob_criteria': [
                '*.analysis'
            ]
        },
        {
            'path': os.path.join(FILE_LOCK_PATH, 'samples'),
            'hashes': dict(),
            'condition': check_not_exists,
            'postprocess': validate_sample,
            'repo_target': 'samples',
            'filelock_glob_criteria': [
                '*R1_001.fastq.gz',
                '*R1_001_fastqc.tar',
                '*R2_001.fastq.gz',
                '*R2_001_fastqc.tar',
                '*.sample'
            ]
        }
    ]

    while True:
        for repo_path in REPO_PATH:
            for target in targets:
                for name in os.listdir(target['path']):
                    if os.path.exists(KILLFILE_PATH):
                        raise Exception("Killfile at {} detected. Breaking loop!".format(KILLFILE_PATH))
                    try:
                        src = os.path.join(target['path'], name)
                        dest = os.path.join(repo_path, target['repo_target'])

                        if not all([glob.glob(os.path.join(src, c)) for c in target['filelock_glob_criteria']]):
                            log.debug("{} is missing files, probably still copying...".format(src))
                            continue

                        if not target['condition'](src, os.path.join(dest, name)):
                            log.debug('{} already processed, skipping...'.format(src))
                            continue

                        curr_hash = get_path_hash(src)
                        # If hash matches, we consider the path ready for copying
                        if curr_hash == target['hashes'].get(name):
                            log.info('{} is ready, processing...'.format(src))
                            copy_path(src, dest)
                            del target['hashes'][name]
                            if 'postprocess' in target:
                                target['postprocess'](target, os.path.join(dest, name))
                        else:
                            target['hashes'][name] = curr_hash

                        # TODO: There's no use removing files from the file lock yet, as they will just get synced again.
                        # shutil.rmtree(src)
                    except Exception:
                        log.exception("Got exception while checking for file changes. Maybe sample is not on the file lock yet. Will try again next round.")

        # Interval to wait before rechecking for changes
        time.sleep(CHANGE_INTERVAL)


if __name__ == '__main__':
    poll_changes()

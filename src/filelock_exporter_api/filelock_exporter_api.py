"""
Watches the filelock in TSD (or any similar place) for new samples/analyses.

In order to make sure they are transferred completely, we create a hash for each sample/analysis with all
file names and file sizes. After a given interval, we traverse the same path again and create the hash again.
If the hash is the same, we can be pretty sure the copying has completed.

The files are then copied into the repository.
For samples, the MD5 sum is checked for the read (fastq.gz) files.
For analysis, we check that the analysis file exists.
If everything it good, a READY file is touched in the sample/analysis folder, marking it ready for the
automation system to start processing.

Log files are written to predefined log path.

A killfile is checked between every file. If present, stops the exporter.
"""

import os
import subprocess
import hashlib
import json
import glob
import logging
import datetime
import time
import logging.handlers

# Don't set too low, as that might cause partially transferred files to be copied.
CHANGE_INTERVAL = 30


FILE_LOCK_PATH = "/ess/p22/data/durable/s3-api/production"

PROD_DATA_REPO_PATH = "/ess/p22/data/durable/production/data"

ELLA_PROD_ANALYSES_REPO_PATH = "/ess/p22/data/durable/production/ella/ella-prod/data/analyses"

ARCHIVE_REPO_PATH = "/ess/p22/archive/production"

LOG_PATH = "/ess/p22/data/durable/production/logs/tsd-import/filelock-exporter"

KILLFILE_PATH = "/ess/p22/data/durable/production/sw/killfilelock"

PRI_URGENT = "urgent"
PRI_HIGH = "high"
PRI_NORMAL = "normal"

logFormatter = logging.Formatter("%(asctime)s %(levelname)s - %(message)s")
log = logging.getLogger(__name__)

try:
    os.makedirs(LOG_PATH)
except OSError:
    pass

fileHandler = logging.handlers.TimedRotatingFileHandler(
    os.path.join(LOG_PATH, "filelock_exporter.log"), when="W0"
)
fileHandler.setFormatter(logFormatter)
log.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
log.addHandler(consoleHandler)

log.setLevel(logging.INFO)
log.info("Logging setup done")


def get_path_hash(path):
    """
    Creates a hash for all files existing within path (inc subdirectories).
    The hash is generating by the file path and file size.

    Idea is that you can watch a certain path at regular intervals to observe if any files have changes recently.
    """
    path_md5 = hashlib.md5()
    for dirpath, _, files in sorted(os.walk(path), key=lambda x: x[0]):
        if files:
            for f in files:
                file_path = os.path.join(dirpath, f)
                size = os.stat(file_path).st_size
                path_md5.update(file_path.encode())
                path_md5.update(str(size).encode())
    return path_md5.hexdigest()


def get_dir_hash(path):
    """
    Creates a hash for all files existing within the deepest dir given by path (inc subdirectories).
    The hash is generating by the file path and file size

    """
    path_md5 = hashlib.md5()
    for path, _, files in sorted(os.walk(path), key=lambda x: x[0]):
        if files:
            for f in sorted(files):
                path_md5.update(f)
                p = os.path.join(path, f)
                size = os.stat(p).st_size
                path_md5.update(str(size).encode())
    return path_md5.hexdigest()


def get_file_hash(path):
    """
    Creates a hash for the input file.
    The hash is generating by the file path and file size

    """
    file_md5 = hashlib.md5()
    file_md5.update(path)
    file_md5.update(str(os.stat(path).st_size).encode())
    return file_md5.hexdigest()


def get_content_md5(path):
    """
    get file content md5
    """
    with open(path, "rb") as f:
        md5_hash = hashlib.md5()
        for chunk in iter(lambda: f.read(4096), b""):
            md5_hash.update(chunk)

        return md5_hash.hexdigest()


def mv_path(src, dest, archive_dest, duplicate_handling_policy=None):
    """
    mv src to dest folder
    check for already existing analyses-results folders, move to archive if exists
    """

    # archive the analyses-results if the same analyses-work folder is coming
    if dest.endswith('analyses-work'):
        timestamp = datetime.datetime.now().strftime("%Y-%m-%d")
        a_name = os.path.basename(src)
        if a_name.endswith('TRIO'):
            result_dest = os.path.join(PROD_DATA_REPO_PATH, "analyses-results", "trios", a_name)
            archive_path = os.path.join(ARCHIVE_REPO_PATH, "analyses-results", "trios", "{}-{}".format(a_name, timestamp))
        else:
            result_dest = os.path.join(PROD_DATA_REPO_PATH, "analyses-results", "singles", a_name)
            archive_path = os.path.join(ARCHIVE_REPO_PATH, "analyses-results", "singles", "{}-{}".format(a_name, timestamp))

        if os.path.exists(result_dest):
            log.warning("Moving %s to %s since the original results are existed.", result_dest, archive_path)
            subprocess.check_call(["cp", "-rp", result_dest, archive_path])
            subprocess.check_call(["rm", "-rf", result_dest])
            log.info("Finished moving %s to %s", result_dest, archive_path)

    # check if a analyses-results folder already exists at dest
    src_folder = os.path.basename(src)
    dest_path = os.path.join(dest, src_folder)
    if os.path.exists(dest_path):
        log.warning("%s folder already exists in %s", src_folder, dest_path)

        # crash if duplicate handling policy is not set and duplicate is found
        if not duplicate_handling_policy:
            raise RuntimeError(
                "Duplicate of {} found at {}, but duplicate handling policy is not set.".format(
                    src, dest_path
                )
            )

        # archive
        if duplicate_handling_policy == "archive":
            # move the existing analyses-results folder to archive with current date appended to the folder name
            # keep the original timestamp of the analyses-results folders and files
            timestamp = datetime.datetime.now().strftime("%Y-%m-%d")
            archive_path = os.path.join(
                archive_dest, "{}-{}".format(src_folder, timestamp)
            )
            log.warning(
                "Moving %s to %s given that duplicate handling policy is %s",
                dest_path,
                archive_path,
                duplicate_handling_policy,
            )
            subprocess.check_call(["cp", "-rp", dest_path, archive_path])
            subprocess.check_call(["rm", "-rf", dest_path])
            log.info("Finished moving %s to %s", dest_path, archive_path)
        # overwrite
        elif duplicate_handling_policy == "overwrite":
            log.warning(
                "Removing %s given that duplicate handling policy is %s",
                dest_path,
                duplicate_handling_policy,
            )
            subprocess.check_call(["rm", "-rf", dest_path])
            log.info("Finished removing %s.", dest_path)
        # skip
        elif duplicate_handling_policy == "skip":
            log.warning(
                "Skipping %s given that duplicate handling policy is %s",
                src,
                duplicate_handling_policy,
            )
            return
        # unknown
        else:
            raise RuntimeError(
                "Unknown duplicate handling policy: {}".format(
                    duplicate_handling_policy
                )
            )

    # move src to dest
    log.info("Moving %s to %s", src, dest)
    subprocess.check_call(["mv", src, "-t", dest])
    log.info("Finished moving %s to %s", src, dest)


def check_ready_and_md5(src, dest, checksum_files=None):
    """
    Checks whether src should be processed again.
    Firstly, check if READY file exists, if not exist, return True
    Secondly, compare md5 of each file in checksum_files, if any difference, return True
    """

    no_ready = not os.path.exists(os.path.join(dest, "READY"))
    if no_ready:
        return no_ready

    md5_match = True
    if checksum_files:
        for f in checksum_files:
            src_path = os.path.join(src, f)
            dest_path = os.path.join(dest, f)
            if os.path.exists(src_path) and os.path.exists(dest_path):
                src_md5 = get_content_md5(src_path)
                dest_md5 = get_content_md5(dest_path)
                if src_md5 != dest_md5:
                    log.debug(
                        "%s in %s and %s have different md5 checksums", f, src, dest
                    )
                    md5_match = False
                    break
                else:  # same taqman, remove from src so as to accept new one if any
                    try:
                        os.remove(src_path)
                        log.info("%s removed", src_path)
                    except OSError:
                        pass

    return not md5_match


def check_dot_ready_exists(src, dest, dummy=None):
    """
    Checks whether src should be processed again.
    For now just a simple check: if name.tar.READY exists in dest, it is processed already
    """
    if dest.endswith(".tar"):
        return not os.path.exists(".".join([dest, "READY"]))
    else:
        file_name = os.path.splitext(dest)[0]
        return not os.path.exists(".".join([file_name, "READY"]))


def check_hash_equal(src, dest):
    """
    all file names and sizes match
    """

    src_hash = get_dir_hash(src)

    if not os.path.exists(dest):  # empty dir should still exist on dest
        dest_hash = None
    else:
        dest_hash = get_dir_hash(dest)

    return not src_hash == dest_hash


def touch_ready(target, dest):
    """
    rsync finished, touch READY file
    """

    try:
        with open(os.path.join(dest, "READY"), "w"):
            log.info("Analysis validated.")
    except IOError:
        log.error("Cannot touch READY file in {}".format(dest))


def validate_sample(target, dest):
    """
    Searches for a sample metadata file and validates the data by checking
    the MD5 sums of the reads so we know the files are correctly copied.
    Finally touches a READY file in the directory.
    """

    for path, _, files in os.walk(dest):
        for f in files:
            if f.endswith(".sample"):
                with open(os.path.join(path, f)) as fd:
                    sample = json.load(fd)
                if sample.get("taqman"):
                    if not os.path.exists(os.path.join(path, sample["taqman"])):
                        raise RuntimeError("Missing taqman file.")
                for read in sample["reads"]:
                    # Use md5sum as it's much faster than calculating md5 in python
                    if not os.path.exists(os.path.join(path, read["path"])):
                        raise RuntimeError("Missing read file {}".format(read["path"]))
                    md5sum = subprocess.check_output(
                        "md5sum {}".format(os.path.join(path, read["path"])), shell=True
                    ).split(" ".encode(), 1)[0]

                    if md5sum != read["md5"]:
                        with open(os.path.join(path, "MD5_FAILED"), "w") as fd:
                            msg = "MD5 for file {} failed, expected {}, got {}".format(
                                read["path"], read["md5"], md5sum
                            )
                            fd.write(msg)
                            log.error(msg)
                            return

                # Touch READY file and validate
                try:
                    with open(os.path.join(path, "READY"), "w"):
                        log.info("Sample {} validated.".format(sample["name"]))
                except IOError:
                    log.error("Cannot touch READY file in {}".format(path))

                break


def validate_md5(target, dest):
    """
    This is mainly for NIPT related files.
    Searches for a md5 file and validates the data by checking
    the MD5 of the corresponding file so we know the files are correctly copied.
    Finally touches a file.tar.READY file in the directory.
    """
    if dest.endswith("md5"):
        file_name = os.path.splitext(dest)[0]

        if os.path.exists(dest) and os.path.exists(
            file_name
        ):  # if both md5 file and its corresponding tar file exists
            current_dir = os.getcwd()
            os.chdir(os.path.dirname(dest))
            try:
                check = subprocess.check_call(["md5sum", "-c", os.path.basename(dest)])
                try:
                    with open(".".join([file_name, "READY"]), "w"):
                        log.info("File {} validated.".format(file_name))
                except IOError:
                    log.error(
                        "Cannot touch {} file in {}".format(
                            ".".join([file_name, "READY"]), os.path.dirname(dest)
                        )
                    )
            except subprocess.CalledProcessError:
                msg = "MD5 for file {} failed.".format(file_name)
                log.error(msg)

            os.chdir(current_dir)


def validate_analysis(target, dest):
    """
    Only checks for analysis file, if so it creates READY file.
    """
    if glob.glob(os.path.join(dest, "*.analysis")):
        # Touch READY file and validate
        try:
            with open(os.path.join(dest, "READY"), "w"):
                log.info("Analysis validated.")
        except IOError:
            log.error("Cannot touch READY file in {}".format(dest))


def poll_changes():
    """
    export in order of priority
    within the same priority, export in order of definition in targets
    """
    IS_SLEEPING = False
    priority_groups = [PRI_URGENT, PRI_HIGH, PRI_NORMAL]
    targets = [
        {
            "sub_paths": ["ella-incoming"],
            "hashes": dict(),
            "condition": None,
            "repo_target": os.path.join(ELLA_PROD_ANALYSES_REPO_PATH, "incoming"),
            "duplicate_handling": "skip",
            "postprocess": touch_ready,
            "filelock_glob_criteria": [
                ["report.txt"],
                ["*.analysis"],
                ["*.vcf"],
                ["attachments/*.HTS.vedlegg.pdf"],
                ["tracks/*.vcf.gz"],
                ["tracks/*.vcf.gz.json"],
                ["tracks/*.vcf.gz.tbi"],
            ]
        },
        {
            "sub_paths": ["analyses-results", "singles"],
            "hashes": dict(),
            "condition": None,
            "repo_target": os.path.join(
                PROD_DATA_REPO_PATH, "analyses-results", "singles"
            ),
            "duplicate_handling": "archive",
            "postprocess": touch_ready,
            "filelock_glob_criteria": [
                ["data/variantcalling/*.final.vcf"],
                ["data/mapping/*.bam", "data/mapping/*.cram"],
                ["data/mapping/*.bai", "data/mapping/*.crai"],
                ["data/qc/*.qc_result.json"],
                ["logs"],
            ]
        },
        {
            "sub_paths": ["analyses-results", "trios"],
            "hashes": dict(),
            "condition": None,
            "repo_target": os.path.join(
                PROD_DATA_REPO_PATH, "analyses-results", "trios"
            ),
            "duplicate_handling": "archive",
            "postprocess": touch_ready,
            "filelock_glob_criteria": [
                ["*.analysis"],
                ["data/*.ped"],
                ["data/variantcalling/*.all.final.vcf"],
                ["data/qc/*.qc_result.json"],
                ["logs"],
            ]
        },
        {
            "sub_paths": ["analyses-work"],
            "hashes": dict(),
            "condition": None,
            "repo_target": os.path.join(PROD_DATA_REPO_PATH, "analyses-work"),
            "duplicate_handling": "overwrite",
            "postprocess": validate_analysis,
            "filelock_glob_criteria": [
                ["*.analysis"]
            ]
        },
        {
            "sub_paths": ["samples"],
            "hashes": dict(),
            "condition": None,
            "repo_target": os.path.join(PROD_DATA_REPO_PATH, "samples"),
            "duplicate_handling": "overwrite",
            "postprocess": validate_sample,
            "filelock_glob_criteria": [
                ["*R1_001.fastq.gz", "*R1_001.fastq.ora"],
                ["*R2_001.fastq.gz", "*R2_001.fastq.ora"],
                ["*.sample"]
            ]
        },
        {
            "sub_paths": ["nipt", "sequencingRuns"],
            "hashes": dict(),
            "condition": None,
            "repo_target": os.path.join(PROD_DATA_REPO_PATH, "nipt", "sequencingRuns"),
            "duplicate_handling": "skip",
            "postprocess": validate_md5,
            "filelock_glob_criteria": [],
        },
        {
            "sub_paths": ["nipt", "veriseqOutput"],
            "hashes": dict(),
            "condition": None,
            "repo_target": os.path.join(PROD_DATA_REPO_PATH, "nipt", "veriseqOutput"),
            "duplicate_handling": "skip",
            "postprocess": validate_md5,
            "filelock_glob_criteria": [],
        },
        {
            "sub_paths": ["nipt", "spectroMax"],
            "hashes": dict(),
            "condition": None,
            "repo_target": os.path.join(PROD_DATA_REPO_PATH, "nipt", "spectroMax"),
            "duplicate_handling": "skip",
            "postprocess": validate_md5,
            "filelock_glob_criteria": [],
        },
    ]

    while True:
        if os.path.exists(KILLFILE_PATH):
            raise Exception(
                "Killfile at {} detected. Breaking loop!".format(KILLFILE_PATH)
            )

        for priority in priority_groups:
            for target in targets:
                # skip nipt folder for priority urgent, high
                if "nipt" in target["sub_paths"] and priority != "normal":
                    continue

                target_path = os.path.join(
                    FILE_LOCK_PATH, priority, *target["sub_paths"]
                )
                if not os.path.exists(target_path):
                    log.warning("%s doesn't exist, create it now.", target_path)
                    subprocess.check_call("mkdir -p {}".format(target_path))

                for name in os.listdir(target_path):
                    try:
                        src = os.path.join(target_path, name)

                        _missing_files = []
                        for c in target["filelock_glob_criteria"]:  # checking whether all required files are under src
                            if not [f for p in c for f in glob.glob(os.path.join(src, p))]:
                                _missing_files.append(" or ".join(c))
                        if _missing_files:
                            log.debug(
                                "{} is missing file(s): [{}], probably still copying...".format(
                                    src, ", ".join(_missing_files)
                                )
                            )
                            continue

                        dest = target["repo_target"]
                        archive_dest = os.path.join(
                            ARCHIVE_REPO_PATH, *target["sub_paths"]
                        )

                        if target["sub_paths"] == ["samples"]:
                            extra_arg = [name + ".taqman"]
                        else:
                            extra_arg = None

                        if (
                            target["condition"]
                            and not target["condition"](
                                src, os.path.join(dest, name), extra_arg
                            )
                        ):  # checking whether the file has been processed from src to dest
                            log.debug("{} already processed, skipping...".format(src))
                            continue

                        if os.path.isfile(src):
                            curr_hash = get_file_hash(src)
                        else:
                            curr_hash = get_path_hash(src)

                        # If hash matches, we consider the path ready for copying
                        if curr_hash == target["hashes"].get(name):
                            log.info("{} is ready, processing...".format(src))
                            IS_SLEEPING = False

                            mv_path(src, dest, archive_dest, duplicate_handling_policy=target["duplicate_handling"])
                            del target["hashes"][name]
                            if "postprocess" in target:
                                target["postprocess"](target, os.path.join(dest, name))
                        else:
                            target["hashes"][name] = curr_hash

                    except Exception:
                        log.exception(
                            "Got exception while checking for file changes. "
                            "Maybe sample is not on the file lock yet. Will try again next round."
                        )

        # Interval to wait before rechecking for changes
        if not IS_SLEEPING:
            log.info("sleeping...")
            IS_SLEEPING = True
        time.sleep(CHANGE_INTERVAL)


if __name__ == "__main__":
    poll_changes()

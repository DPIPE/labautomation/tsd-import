#!/usr/bin/perl -w 
use strict;

my ($probandFile, $allSampleName, $output, $trio) = @ARGV;

# $probandFile: the file contains proband IDs in the format:
#               123456789-reanalyse-123456PM-TrioPU 
#               or 123456789-reanalyse-MigraForst
#               one line per sample and its project on Clarity
# The file name is the project name.

my %oldIDs;
open OLD, $allSampleName or die;
while(my $old = <OLD>){
    
    chomp $old;
    my $oldID = (split /-/, $old)[2];

    my @o_digitals = split //, $oldID;
    pop @o_digitals;
    pop @o_digitals;
    $oldID = join '', @o_digitals;

    push @{$oldIDs{$oldID}}, $old;
    
}
close OLD;



my %ids;
my @notFound;
my @singles;
open PROBAND, $probandFile or die;
open OUTPUT, ">$output" or die;
while(my $proband = <PROBAND>){

    chomp $proband;

    my @info = split /-/, $proband;

    if($trio){
	# if only need to process trio sample, then skip single samples
	if(scalar(@info) == 3){
	    push @singles, $proband;
	    next;
	}
    }


    # find proband ID
    my $probandID = (split /-|_/, $proband)[0];
    my @p_digitals = split //, $probandID;

    if(scalar(@p_digitals) == 11){
	pop @p_digitals;
	pop @p_digitals;
	$probandID = join '', @p_digitals;
    }

    my $oldProband;
    if($oldIDs{$probandID}){

	if(scalar(@{$oldIDs{$probandID}}) > 1){

	    print "\nWhich one to choose (print index):\n";
	    print join("\n", @{$oldIDs{$probandID}}), "\n";
	    print "The first index is 0\n";
	    
	    my $response = <STDIN>;
	    chomp $response;
	    print OUTPUT "$proband\t", $oldIDs{$probandID}->[$response], "\n";

	}else{
	    print OUTPUT "$proband\t", $oldIDs{$probandID}->[0], "\n";
	}

    }else{
	push @notFound, $proband;
	next;
    }

}
close PROBAND;

if(scalar(@notFound) > 0){
    print "Not found:\n", join("\n", @notFound), "\n";
}

if(scalar(@singles) > 0){
    print "\nOnly process trio samples, the following samples are skipped\n";
    print join("\n", @singles), "\n";
}

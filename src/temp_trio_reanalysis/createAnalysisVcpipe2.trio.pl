#!/usr/bin/perl -w                                                                                                            
use strict;
use lib qw(...);
use JSON qw( );

# export PERL5LIB=$PERL5LIB:/boston/diag/diagInternal/samples/trioReanalysis/script/JSON-2.97001/lib
# need to do afterwards                                                                                                       
# find ./ -type f -exec sed -i -e 's/ :/:/g' {} \;                                                                            

# samplesToTransfer.bash used under /tsd/p22/data/durable/production/samples and /tsd/p22/data/durable2/production/samples
# triosToTransfer.bash used under /tsd/p22/data/durable2/production/preprocessed/trios
# singlesToTransfer.bash used under /tsd/p22/data/durable2/production/preprocessed/singles

my ($sampleFile, $preprocessedAvail, $outputDir) = @ARGV;

# $sampleFile format, tab seperated file, copied from sample-sheet from column B to Q

my %preprocessedAvail;
open PA, $preprocessedAvail or die;
while(my $pa = <PA>){

    chomp $pa;
    $preprocessedAvail{$pa} = 1;
    
}


# The default $outputDir is "/boston/diag/diagInternal/samples/reanalysis/analyses" on vali
if(!$outputDir){
    $outputDir = "/boston/diag/diagInternal/samples/reanalysis/analyses";
}

# Find the latest gene panel version                                                                                          
my $bundle_file = do {
    open(my $json_fh, "<:encoding(UTF-8)", "/boston/diag/production/sw/vcpipe/vcpipe-bundle/bundle.json") or die;
    local $/;
    <$json_fh>
};

my $json = JSON->new;
my $bundleGP = $json->decode($bundle_file);

my %gp; # the format will be $gp{TrioPU} = '4'                                                                                
foreach my $eachGP(keys %{$bundleGP->{'clinicalGenePanels'}}){

    my %underKey = map { $_ => 1 } keys %{$bundleGP->{'clinicalGenePanels'}->{$eachGP}};

    if(!$underKey{'inactive'}){
	my ($tempN, $tempV) = split /_/, $eachGP;
	$tempV =~ s/v0+(.*)/$1/;
        $gp{$tempN} = $tempV;
    }

}

my $currDir = `pwd`;
chomp $currDir;

# Create analysis files for each sample                                                                                       
# Create transfer script
# Only process the samples with family ID in the "Sample/Name" in the sample sheet, since those samples are treated as trios

my @notFound;
open TRANSFERSAMPLE, ">$outputDir/samplesToTransfer.bash" or die;
open TRANSFERTRIO, ">$outputDir/triosToTransfer.bash" or die;
open TRANSFERSINGLE, ">$outputDir/singlesToTransfer.bash" or die;
open SAMPLE, $sampleFile or die;
while(my $entry = <SAMPLE>){

    chomp $entry; # format: originalEntry(sampleID-reanalyse-famID-genePanel)"\t"tsdEntry
    my %jsonDict; # store information for json  

    my ($sample, $fullName) = split /\t/, $entry;

    my @infos = split /-/, $sample;
    my $excapNo = $fullName;
    $excapNo =~ s/.*-excap(\d+)-.*/$1/i;

    # single samples will not go further
    my $isTrio;
    if(scalar(@infos) == 4){
        $isTrio = 1;
    }else{
	next;
    }

    my $genePanelName = $infos[-1];

    if(!$gp{$genePanelName}){
        print "Gene panel is wrong for $entry\n";
        next;
    }

    my $genePanel = join '_', $genePanelName, join('', 'v0', $gp{$genePanelName});
    my $genePanel_dir = join '-', $genePanelName, join('', 'v0', $gp{$genePanelName});


    $jsonDict{"params"}{"taqman"} = "false";
    $jsonDict{"samples"} = [$fullName];

    # construct params -> pedigree
    # for proband
    $jsonDict{"params"}{"pedigree"}{"proband"}{"sample"} = $fullName;
    my $probandGender = $infos[2];
    $probandGender =~ s/^\d\d\d\d\d\dP(.*)$/$1/;
    if($probandGender eq 'K'){
	$jsonDict{"params"}{"pedigree"}{"proband"}{"gender"} = "female";
    }elsif($probandGender eq 'M'){
	$jsonDict{"params"}{"pedigree"}{"proband"}{"gender"} = "male";
    }

    # construct params -> pedigree
    # find parent samples
    my $famID = $infos[2];
    $famID =~ s/^(\d\d\d\d\d\d).*/$1/;

    my %parents;
    my $searchFamID = join '', '*', $famID, '*Av*';

    my $runString = `ls -d /boston/diag/nscDelivery/*/$searchFamID`;
    chomp $runString;

    my %sampleDir;
    foreach my $eachSample (split /\n+/, $runString){

	if(-d $eachSample){
	    $sampleDir{$eachSample} = 1;
	}elsif($eachSample =~ /fastq.gz/){
	    $sampleDir{join '_', (split /_/, $eachSample)[0,1,2,3]} = 1;
	}

    }

    my @sampleDirs = sort keys %sampleDir;
    if(scalar(@sampleDirs) > 3){

	print "\nChoose which one do you want by index (comma seperated, start with index 0)\n\n", join("\n", @sampleDirs), "\n";
	
	my $index = <STDIN>;
	chomp $index;
	
	my @index = split /,/, $index;
	
	my @tempDirs;
	for(my $x=0; $x<scalar(@sampleDirs); $x++){
	    
	    if(grep(/^$x$/, @index)){
		push @tempDirs, $sampleDirs[$x];
	    }
	    
	}
	
	if(scalar(@tempDirs) >= 2){
	    @sampleDirs = @tempDirs;
	}else{
	    push @notFound, $sample;
	    system "rm -r $outputDir/$fullName";
	    next;
	}

    }elsif(scalar(@sampleDirs) < 2){
	push @notFound, $sample;
	system "rm -r $outputDir/$fullName";
	next;
    }

    # construct params -> pedigree   
    # for parents
    my $probandAstrio;
    foreach my $eachP(@sampleDirs){

	my $samplePID = $eachP;
	$samplePID =~ s/.*\/(.*)$/$1/;
	my $projectID = $eachP;
	$projectID =~ s/.*Project_Diag-(.*)-.*$/$1/;
	$projectID = (split /-/, $projectID)[0];

	my @sampleInfo = split /-/, $samplePID;

	if($sampleInfo[1] =~ /MK/){
	    my $motherID = join '-', "Diag", $projectID, $sampleInfo[0], "MK";
	    $motherID =~ s/Sample_//;
	    $parents{$motherID} = $eachP;
	    $jsonDict{"params"}{"pedigree"}{"mother"}{"sample"} = $motherID;
	    $jsonDict{"params"}{"pedigree"}{"mother"}{"gender"} = "female";
#               push @{$jsonDict{'samples'}}, $motherID;                                                                                                                                
	}elsif($sampleInfo[1] =~ /FM/){
	    my $fatherID = join '-', "Diag", $projectID, $sampleInfo[0], "FM";
	    $fatherID =~ s/Sample_//;
	    $parents{$fatherID} = $eachP;
	    $jsonDict{"params"}{"pedigree"}{"father"}{"sample"} = $fatherID;
	    $jsonDict{"params"}{"pedigree"}{"father"}{"gender"} = "male";
#               push @{$jsonDict{'samples'}}, $fatherID;                                                                                                                                
	}elsif($sampleInfo[1] =~ /P/){
	    $probandAstrio = 1;
	}
	
    }

    if(scalar(keys %parents) != 2){
	print "Something wrong with the parents: $fullName\n";
    }

    # correct proband for sample name
    if($probandAstrio){
	$fullName = join '-', $fullName, join('', 'P', $probandGender);
	$jsonDict{"params"}{"pedigree"}{"proband"}{"sample"} = $fullName;
	pop @{$jsonDict{"samples"}};
	push @{$jsonDict{"samples"}}, $fullName;
    }

    # put the parent names into json samples
    foreach my $eachkey(keys %parents){
	push @{$jsonDict{"samples"}}, $eachkey;
    }

    # create basepipes if necessary
    foreach my $baseSample(@{$jsonDict{"samples"}}){

	if($preprocessedAvail{$baseSample}){
	    # if the basepipe results available, transfer
	    print TRANSFERSINGLE "rsync -rRuaP $baseSample /cluster/projects/p22/production/preprocessed/singles\n";
	    print TRANSFERSAMPLE "rsync -rRuaP $baseSample/*.sample /cluster/projects/p22/production/samples/\n"; # annopipe needs .sample file
	}else{
	    # if the basepipe results NOT available
	    
            # transfer sample folder 
	    print TRANSFERSAMPLE "rsync -rRuaP $baseSample /cluster/projects/p22/production/samples/\n";
	    
            #create basepipe folder
	    chdir($outputDir);
	    createBasepipe($baseSample);
	    chdir($currDir);
	}
    }

    my $trioName = join '-', $fullName, "TRIO";
    if(!$preprocessedAvail{$trioName}){
	# create triopipe if necessary
	chdir($outputDir);
	createTriopipe(\%jsonDict, $trioName);
	chdir($currDir);
    }else{
	# copy triopipe results if available
	print TRANSFERTRIO "rsync -rRuaP $trioName /cluster/projects/p22/production/preprocessed/trios/\n";
    }

    # create annopipe for all samples
    $jsonDict{"params"}{"genepanel"} = $genePanel;
    my $trioAnnoName = join '-', $trioName, $genePanel_dir;
    chdir($outputDir);
    createAnnopipe(\%jsonDict, $trioAnnoName);
    chdir($currDir);

}
close SAMPLE;
close TRANSFERSAMPLE;
close TRANSFERSINGLE;
close TRANSFERTRIO;

sub createBasepipe{

    # input->params->taqman = false                                                                                                                                             
    # input->samples = [proband]                                                                                                                            # There will be no: gender, date_analysis_requested and priority

    my $name = shift;
    my %output;
    $output{"type"} = "basepipe";
    $output{"params"}{"taqman"} = "false";
    $output{"name"} = $name;
    $output{"samples"} = [$name];
    system "mkdir $name";
    my $analysisFile = join '.', $name, "analysis";

    open OUT, ">$name/$analysisFile" or die;
    print OUT JSON->new->pretty->encode(\%output);
    close OUT;
    print "Analysis folder for $name is created\n";

}

sub createAnnopipe{

    # input->params->genepanel = TrioPU_v04                                                                                                                                     
    # input->samples = [proband]                                                                                                                                                

    my ($input, $name) = @_;
    delete $input->{"params"}->{"taqman"};
    $input->{"type"} = "annopipe";
    $input->{"name"} = $name;
    system "mkdir $name";
    my $analysisFile = join '.', $name, "analysis";

    open OUT, ">$name/$analysisFile" or die;
    print OUT JSON->new->pretty->encode($input);
    close OUT;
    print "Analysis folder for $name is created\n";

}

sub createTriopipe{

    # input->params->pedigree->father                                                                                                                                           
    # input->params->pedigree->mother                                                                                                                                           
    # input->params->pedigree->proband                                                                                                                                          
    # input->samples = [proband, father, mother]                                                                                                                                

    my ($input, $name) = @_;
    $input->{"type"} = "triopipe";
    $input->{"name"} = $name;
    system "mkdir $name";
    my $analysisFile = join '.', $name, "analysis";

    delete $input->{"params"}->{"taqman"};
    open OUT, ">$name/$analysisFile" or die;
    print OUT JSON->new->pretty->encode($input);
    close OUT;
    print "Analysis folder for $name is created\n";

}

#!/usr/bin/perl -w
use strict;

my @runFolders = glob "/boston/diag/nscDelivery/*";

my $currDir = `pwd`;
chomp $currDir;


#my @runFolders = ('/boston/diag/nscDelivery/180928_E00401.A.Project_Diag-excap126-2018-09-17/');

my @failed;
foreach my $eachFolder(@runFolders){

    if(-d $eachFolder){

	my $eachFolder_modified = lc $eachFolder;

	if($eachFolder_modified =~ /excap/){

	    my $project = $eachFolder;
	    $project =~ s/.*(Project_.*)$/$1/;
	    $project = (split /-|_/, $project)[2];

	    chdir($eachFolder);

	    my @samples = glob "*";

	    my $eachSample_modified;
	    foreach my $eachSample(@samples){
		
		if(-d $eachSample and $eachSample =~ /Sample/){
		    
		    $eachSample_modified = $eachSample;
		    $eachSample_modified =~ s/.*(\d\d\d\d\d\d\d\d\d\d\d)-.*/$1/;

		    if($eachSample_modified =~ /[[:alpha:]]/){
			push @failed, $eachSample_modified;
		    }else{
			my $finalID = join('-', 'Diag', $project, $eachSample_modified);
			print "$finalID\n";
		    }
		    
		}elsif($eachSample =~ /fastq.gz/ and $eachSample =~ /R1/){
		    $eachSample_modified = (split /-/, $eachSample)[0];

                   if($eachSample_modified =~ /[[:alpha:]]/){
                        push @failed, $eachSample_modified;
		   }else{
			my $finalID = join('-', 'Diag', $project, $eachSample_modified);
			print "$finalID\n";
		   }

		}

	    }

	    chdir($currDir);

	}

    }

}

open OP, ">$currDir/notFound.txt" or die;
foreach my $failed (@failed){

    print OP "$failed not found\n";

}
close OP;


#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

export PERL5LIB=$PERL5LIB:${DIR}/JSON-2.97001/lib
perl ${DIR}/createAnalysisVcpipe2.trio.pl $1 $2 $3
find $3/ -type f -exec sed -i -e 's/ :/:/g' {} \;
########
# Find and save the TaqMan results for a particular Id sample, optionally save a copy of the original TaqMan file
#
# This script takes as argument the id sample, the location where the TaqMan results should be located
# where the results should be written and the defintion file.
# The definition file is needed in order get the position of the rs in order to compare it with the HTS file
#
########

import shutil
import argparse
import os
import time
import sys

from collections import defaultdict


DEFINITION_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'taqman_snp_definitions.txt')


class MultipleTaqmanResultsError(Exception):
    pass


class TaqmanParser(object):

    def __init__(self, source_path):
        self.source_path = source_path
        self.definition_file = DEFINITION_PATH

    def exists(self, sample_id):
        return bool(self._retrieve_taqman_file(sample_id))

    def create(self, sample_id, target_path, copy_source_target=None):
        taqman_file = self._retrieve_taqman_file(sample_id)
        sorted_taqman = []
        with open(taqman_file) as fd:
            taqman_data = self._read_taqman_file(sample_id, fd)
            if not taqman_data:
                raise RuntimeError("Could not find the TaqMan results for ID {} in location {}"
                                   "".format(sample_id, self.source_path))
            if not (len(taqman_data) == 23 or len(taqman_data) == 16):
                raise RuntimeError("23 or 16 taqman SNPs are required, but {} SNPs found in {}"
                                   "".format(len(taqman_data), taqman_file))

            with open(target_path, 'w') as fd_out:
                for taqman_line in taqman_data:
                    self._add_position_taqman_data(taqman_line)
                    sorted_taqman.append(taqman_line)
                for li in sorted(sorted_taqman, key=lambda x: (int(x[0]), int(x[1]))):
                    fd_out.write('\t'.join(li) + '\n')

        if copy_source_target:
            shutil.copy(taqman_file, copy_source_target)

    def _retrieve_taqman_file(self, sample_id):
        list_of_files = []
        for dirname, dirnames, filenames in os.walk(self.source_path):
            for filename in filenames:
                file_path = os.path.join(dirname, filename)
                if ".txt" in file_path:
                    try:
                        f = open(file_path, 'r')
                    except IOError:  # ignore permisson denied files
                        continue
                    for line in f:
                        fields = self._parse_line(line)
                        if fields and fields['sample_id'] == sample_id:
                            if f.name not in list_of_files:
                                list_of_files.append(f.name)

        # list_of_files.sort(key=lambda x: os.path.getctime(x),reverse=True)
        list_of_files.sort(key=lambda x: os.path.getmtime(x), reverse=True)

        if len(list_of_files) > 0:
            return list_of_files[0]
        else:
            raise RuntimeError("TaqMan file not found for {}. If you are sure the file exist, "
                               "make sure you have read permisson on it.".format(sample_id))

    def _parse_line(self, line):
        """
        parse a taqman file line
        """
        # ignore comment lines and empty lines
        if line.startswith("*") or line in ['', '\n', '\r\n', '[Results]\r\n'] or 'TE X1' in line or 'Assay ID' in line:
            return None

        parts = line.strip().split("\t")
        # Old format
        if len(parts) > 20:
            indices = {
                'genotype': 22,
                'sample_id': 3,
                'assay_id': 4,
                'rs': 9
            }
        # New format
        elif len(parts) == 4:
            indices = {
                'genotype': 3,
                'sample_id': 2,
                'assay_id': 0,
                'rs': 1
            }
        else:
            return None

        return {'sample_id': parts[indices['sample_id']],
                'assay_id': parts[indices['assay_id']],
                'genotype': parts[indices['genotype']],
                'rs': parts[indices['rs']]}

    def _parse_taqman_lines(self, sample_id, taqman_fp):
        dico_of_assayID = defaultdict(set)
        for line in taqman_fp:
            fields = self._parse_line(line)
            if fields and fields['sample_id'] == sample_id:
                dico_of_assayID[fields['assay_id']].add((fields['genotype'], fields['rs']))

        if not dico_of_assayID:
            raise RuntimeError("not enough(or too many) columns in taqman file {} for {}".format(
                taqman_fp.name, sample_id))

        return dico_of_assayID

    def _read_taqman_file(self, sample_id, taqman_fp):
        """Returns selected fields from a taqman file per assay_id

        A taqman file can have multiple (typically 2) calls
        per assay_id for the same sample
        """

        dico_of_assayID = self._parse_taqman_lines(sample_id, taqman_fp)

        result = list()
        for snp_list in sorted(dico_of_assayID.values()):
            if not snp_list:
                raise RuntimeError("wrongly formated taqman file {} for {}".format(
                    taqman_fp.name, sample_id))

            rs_set = set([x[1] for x in snp_list])
            if len(rs_set) != 1:  # all dbsnp should be equal
                raise RuntimeError("Same Assay ID with multiple rs '{}' found in taqman file {} "
                                   "for {}".format(",".join(rs_set), taqman_fp.name, sample_id))

            dbsnp = list(snp_list)[0][1]
            # Remove all undetermined (old format: ned, new format: UND)
            # 28.06.2016 new software introduced a "NOAMP" tag.
            snp_list = [x for x in snp_list if x[0] not in ['ned', 'UND','NOAMP','INV']]
            if not snp_list:
                # If empty, all were undetermined
                result.append([dbsnp] + ['Undetermined'] * 2)
            else:
                # If multiple, they can be considered of equal validity -> just pick one
                alleles = snp_list[0][0].split('/')
                if not (len(alleles) == 2 or len(alleles) == 1):
                    raise RuntimeError("1 or 2 alleles required, but {} found from {} in "
                                       "taqman file {} for {}".format(
                                           len(alleles), snp_list[0][0],
                                           taqman_fp.name, sample_id))
                if not all([a in ['A', 'G', 'C', 'T'] for a in alleles]):
                    raise RuntimeError(
                        "Unrecognized Allele nucleotide(s) '{}' in taqman file {} for {}".format(
                            ','.join([a for a in alleles if a not in ['A', 'G', 'C', 'T']]),
                            taqman_fp.name,
                            sample_id)
                    )
                result.append([dbsnp] + alleles)

        return result

    def _add_position_taqman_data(self, taqman_line):

        with open(self.definition_file, 'r') as fd_def:
            for line in fd_def:
                parts = line.strip().split("\t")

                if taqman_line[0].strip() == parts[7]:
                    taqman_line.insert(0, parts[3])
                    taqman_line.insert(0, parts[2])
                    return taqman_line
            else:
                raise RuntimeError(" Unrecognized snp reference '{}', is there a typo?".format(
                    taqman_line[0].strip()))


if __name__ == '__main__':
    parser = argparse.ArgumentParser("Search TaqMan files for specific sample id and outputs a simple tsv with data.")
    parser.add_argument('-searchPath', help='Current directory', required=True, type=str, action="store")
    parser.add_argument('-id', help='SampleID', required=True, type=str, action="store")
    parser.add_argument('-copySource', help='Target path for copying taqman source file', required=False, type=str, action="store")
    parser.add_argument('-o', help='Output file', required=True, type=str, action="store")

    args = parser.parse_args()

    tp = TaqmanParser(args.searchPath)
    tp.create(args.id, args.o, args.copySource)

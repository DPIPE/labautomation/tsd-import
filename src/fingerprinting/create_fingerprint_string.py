import argparse
import vcf

from operator import itemgetter

def getUIAC_letter(string):
    if string.split("/")[0] == string.split("/")[1]:
        return string.split("/")[1]
    else:
        f = itemgetter(1)
        try:
            ind = map(f, purines_pirimidines).index(string)
            return purines_pirimidines[ind][0]
        except:
            f = itemgetter(2)
            ind = map(f, purines_pirimidines).index(string)
            return purines_pirimidines[ind][0]

purines_pirimidines = [('R','A/G','G/A'),
                       ('Y','C/T','T/C'),
                       ('S','G/T','T/G'),
                       ('W','A/T','T/A'),
                       ('K','G/T','T/G'),
                       ('M','A/C','C/A'),
                       ('0','0','0')]

def create_taq_string(taq_file):
    string = ''
    with open(taq_file) as fh:
        for line in fh:
            parts = line.strip().split("\t")
            if parts[3] == 'Undetermined' or parts[4] == 'Undetermined':
                gt ='0/0'
            else:
                gt = "/".join(parts[3:5])
            string += getUIAC_letter(gt)
    return string

def GetVcfRecords(vcf_file):
    '''Get the vcf file and return an handle (vcf_reader) as well as an object (vcf_records) representing the variants
    '''
    vcf_records= []
    vcf_reader = vcf.Reader(open(vcf_file, 'r'))
    for record in vcf_reader:
        vcf_records.append(record)
    return (vcf_records,vcf_reader)

def create_hts_string(gt):
    pass

if __name__ == '__main__':
    parser = argparse.ArgumentParser("Read VCF file for specific sample id and outputs a simple tsv with data.")
    parser.add_argument('-vcf', help='HTS produces vcf for SNP-ID', required=True, type=str, action="store")
    parser.add_argument('-taq',help='Taqman GT calls',required=True, type=str, action="store")

    args = parser.parse_args()

    taq_string = create_taq_string(args.taq)
    print "TAQ : ",taq_string

    vcf_records,vcf_reader = GetVcfRecords(args.vcf)
    sample_name = vcf_reader.samples[0]
    hts_string = ''
    for record in vcf_records:
        if record.genotype(sample_name).gt_bases == None:
            tmp_gt = getUIAC_letter('0/0')
        else:
            tmp_gt = getUIAC_letter(record.genotype(sample_name).gt_bases)

        hts_string += tmp_gt

    print "HTS : ",hts_string
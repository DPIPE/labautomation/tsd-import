import pytest
from gettaqman import TaqmanParser

tp = TaqmanParser('.')


class TestRaise(object):
    """
    Test all raised exceptions used as Clarity Manager Review user message
    """
    def test_raise_taqman_not_found(self):
        """
        Taqman not found for a sample_id
        """
        with pytest.raises(RuntimeError,
                           match='TaqMan file not found for NonExistenceID'):
            tp.create('NonExistenceID', '/dev/null')

    def test_raise_not_enough_number_of_SNPs_case1(self):
        """
        Excactly 23(old) or 16(new) SNPs are required, not less, case1
        """
        with pytest.raises(RuntimeError,
                           match='23 or 16 taqman SNPs are required, but 15 SNPs found'):
            tp.create('NotEnoughSNPs1', '/dev/null')


    def test_raise_not_enough_number_of_SNPs_case2(self):
        """
        Excactly 23(old) or 16(new) SNPs are required, not less, case2
        """
        with pytest.raises(RuntimeError,
                           match='23 or 16 taqman SNPs are required, but 15 SNPs found'):
            tp.create('NotEnoughSNPs2', '/dev/null')

    def test_raise_too_many_SNPs(self):
        """
        Excactly 23(old) or 16(new) SNPs are required, not more
        """
        with pytest.raises(RuntimeError,
                           match='23 or 16 taqman SNPs are required, but 17 SNPs found'):
            tp.create('TooManySNPs', '/dev/null')

    def test_raise_extra_columns(self):
        """
        more than 20 or exactly 4 columns(tab sepatated) required
        """
        with pytest.raises(RuntimeError,
                           match='not enough\(or too many\) columns in taqman file \./test_taqman.txt'
                           ' for ExtraColumn'):
            tp.create('ExtraColumn', '/dev/null')

    def test_raise_not_unique_rs(self):
        """
        each Assay ID has one and only one rs
        """
        with pytest.raises(RuntimeError,
                           match="Same Assay ID with multiple rs 'rs970714[45],rs970714[45]' found "
                           "in taqman file \./test_taqman.txt for NotUniqueRs"):
            tp.create('NotUniqueRs', '/dev/null')

    def test_do_not_raise_allele_error(self):
        """
        one allele is allowd(this is what the old code base says)
        """
        tp.create('AllelesNotError1', '/dev/null')

    def test_raise_allele_error_case1(self):
        """
        1 or 2 alleles required, not more than 2
        """
        with pytest.raises(RuntimeError,
                           match='1 or 2 alleles required, but 3 found from T/G/C in taqman file '
                           '\./test_taqman.txt for AllelesError1'):
            tp.create('AllelesError1', '/dev/null')

    def test_raise_allels_nucleotide_wrong_case1(self):
        """
        unrecognized allele nucleotide(missing one allele, allel is empty string '')
        """
        with pytest.raises(RuntimeError,
                           match="Unrecognized Allele nucleotide\(s\) '' in taqman file "
                           "./test_taqman.txt for NotATGC1"):
            tp.create('NotATGC1', '/dev/null')

    def test_raise_allels_nucleotide_wrong_case2(self):
        """
        unrecognized allele nucleotide, e.g. 'N'
        """
        with pytest.raises(RuntimeError,
                           match="Unrecognized Allele nucleotide\(s\) 'N' in taqman file "
                           "\./test_taqman.txt for NotATGC2"):
            tp.create('NotATGC2', '/dev/null')

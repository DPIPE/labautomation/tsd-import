import pytest
import cStringIO
from gettaqman import TaqmanParser


def test_parse_taqman_lines_determined():
    sample_id = "NA12878K4"
    taqman_file = cStringIO.StringIO("""
337\tO1\tfalse\tNA12878K4\tC___1689240_10\tC\tT\tVIC-NFQ-MGB\tFAM-NFQ-MGB\trs786906\t\t0.000\tC___1689240_10\tUNKNOWN\t0.461\t2.341\t100.000\t\t0.884\t1.269\t0.893\t0.982\tHomozygous T/T\tManual\t40\ttrue\t0.200\ttrue\t3\t15\ttrue\t0.200\ttrue\t3\t15\t23.070\t22.850\tN\tN\tN\tN
338\tO2\tfalse\tNA12878K4\tC___3127590_1_\tA\tC\tVIC-NFQ-MGB\tFAM-NFQ-MGB\trs2296292\t\t0.000\tC___3127590_1_\tUNKNOWN\t1.883\t1.194\t97.382\t\t1.236\t1.093\t0.986\t0.976\tHeterozygous A/C\tAuto\t40\ttrue\t0.200\ttrue\t3\t15\ttrue\t0.200\ttrue\t3\t15\t23.896\t23.812\tN\tN\tN\tN
362\tP2\tfalse\tNA12878K4\tC___3127590_1_\tA\tC\tVIC-NFQ-MGB\tFAM-NFQ-MGB\trs2296292\t\t0.000\tC___3127590_1_\tUNKNOWN\t2.113\t1.326\t97.382\t\t1.256\t1.115\t0.990\t0.979\tHeterozygous A/C\tAuto\t40\ttrue\t0.200\ttrue\t3\t15\ttrue\t0.200\ttrue\t3\t15\t23.847\t23.788\tN\tN\tN\tN""")
    assert TaqmanParser('.')._parse_taqman_lines(sample_id, taqman_file) == {"C___3127590_1_": set([('A/C', 'rs2296292')]),"C___1689240_10":set([('T/T', 'rs786906')])}


def test_parse_taqman_lines_undetermined():
    sample_id = "NA12878K4"
    taqman_file = cStringIO.StringIO("""357\tO21\tfalse\tNA12878K4\tC___8730041_1_\tA\tC\tVIC-NFQ-MGB\tFAM-NFQ-MGB\trs3851\t\t0.000\tC___8730041_1_\tUNKNOWN\t0.647\t0.383\t100.000\t\t0.936\t0.834\t0.873\t0.860\tUndetermined\tManual\t40\ttrue\t0.200\ttrue\t3\t15\ttrue\t0.200\ttrue\t3\t15\t23.754\t23.678\tN\tN\tN\tN""")
    assert TaqmanParser('.')._parse_taqman_lines(sample_id, taqman_file) == {"C___8730041_1_": set([('ned', 'rs3851')])}


def test_parse_taqman_lines_skip_unrelated():
    sample_id = "NA12878K4"
    taqman_file = cStringIO.StringIO("""* Signal Smoothing On = true
* Stage/ Cycle where Analysis is performed = Stage 3, Step 2
* User Name = HTS Diagnostikk

[Results]\r\nWell\tWell Position\tOmit\tSample Name\tAssay ID\tAllele1 Name\tAllele2 Name\tAllele1 Dyes\tAllele2 Dyes\tNCBI SNP Reference\tContext Sequence\tQuality Value\tSNP Assay Name\tTask\tAllele1 Delta Rn\tAllele2 Delta Rn\tPass.Ref\tQuality(%)\tAllele1 Amp Score\tAllele2 Amp Score\tAllele1 Cq Conf\tAllele2 Cq Conf\tCall\tMethod\tCall Cycle\tAllele1 Automatic Ct Threshold\tAllele1 Ct Threshold\tAllele1 Automatic Baseline\tAllele1 Baseline Start\tAllele1 Baseline End\tAllele2 Automatic Ct Threshold\tAllele2 Ct Threshold\tAllele2 Automatic Baseline\tAllele2 Baseline Start\tAllele2 Baseline End\tComments\tAllele1 Ct\tAllele2 Ct\tAMPSCORE\tALLELE2CRTNOISE\tALLELE1CRTNOISE\tALLELE1CRTAMPLITUDE\tALLELE2CRTAMPLITUDE
1\tA1\tfalse\tNA12878K4\tC___1689240_10\tC\tT\tVIC-NFQ-MGB\tFAM-NFQ-MGB\trs786906\t\t0.000\tC___1689240_10\tUNKNOWN\t1.465\t1.551\t\t100.000\t1.176\t1.179\t0.988\t0.977\tHeterozygous C/T\tManual\t40\ttrue\t0.200\ttrue\t3\t15\ttrue\t0.200\ttrue\t3\t15\t\t25.562\t25.112\t\tN\tN\tN\tN""")
    assert TaqmanParser('.')._parse_taqman_lines(sample_id, taqman_file) == {"C___1689240_10": set([('C/T', 'rs786906')])}


def test_parse_taqman_lines_skip_unrelated_new_format():
    sample_id = "00000000000"
    taqman_file = cStringIO.StringIO("""# Template Created By User ID : GUEST
# Template Software Version Number :  1.3

Assay ID\tNCBI\tSNP Reference\tSample ID\tCall
C__11495689_10\trs9958735\t00000000000\tC/C
C__11495689_10\trs9958735\t00000000000\tC/C
C__11495689_10\trs9958735\t11111111111\tC/T
TE X1\t1111111111\tNOAMP

Assay ID\tAssay Name\tPopulation\tAllele 1 Freq\tAllele 2 Freq\t1/1 Freq\t1/2 Freq\t2/2 Freq\tChi-Squared P-Value
C__11495689_10\tC__11495689_10\tAll\t45.8%\t54.2%\t20.8%\t50%\t29.2%\t0.002\t0.964""")
    assert TaqmanParser('.')._parse_taqman_lines(sample_id, taqman_file) == {"C__11495689_10": set([('C/C', 'rs9958735')])}

def test_read_taqman_file_single_all_valid():
    sample_id = "NA12878K4"
    taqman_file = cStringIO.StringIO("""337\tO1\tfalse\tNA12878K4\tC___1689240_10\tC\tT\tVIC-NFQ-MGB\tFAM-NFQ-MGB\trs786906\t\t0.000\tC___1689240_10\tUNKNOWN\t0.461\t2.341\t100.000\t\t0.884\t1.269\t0.893\t0.982\tHomozygous T/T\tManual\t40\ttrue\t0.200\ttrue\t3\t15\ttrue\t0.200\ttrue\t3\t15\t23.070\t22.850\tN\tN\tN\tN
338\tO2\tfalse\tNA12878K4\tC___3127590_1_\tA\tC\tVIC-NFQ-MGB\tFAM-NFQ-MGB\trs2296292\t\t0.000\tC___3127590_1_\tUNKNOWN\t1.883\t1.194\t97.382\t\t1.236\t1.093\t0.986\t0.976\tHeterozygous A/C\tAuto\t40\ttrue\t0.200\ttrue\t3\t15\ttrue\t0.200\ttrue\t3\t15\t23.896\t23.812\tN\tN\tN\tN
362\tP2\tfalse\tNA12878K4\tC___3127590_1_\tA\tC\tVIC-NFQ-MGB\tFAM-NFQ-MGB\trs2296292\t\t0.000\tC___3127590_1_\tUNKNOWN\t2.113\t1.326\t97.382\t\t1.256\t1.115\t0.990\t0.979\tHeterozygous A/C\tAuto\t40\ttrue\t0.200\ttrue\t3\t15\ttrue\t0.200\ttrue\t3\t15\t23.847\t23.788\tN\tN\tN\tN""")

    assert TaqmanParser('.')._read_taqman_file(sample_id, taqman_file) == [['rs786906', 'T', 'T'], ['rs2296292', 'A', 'C']]


def test_read_taqman_file_multiple_all_valid():
    sample_id = "NA12878K4"
    taqman_file = cStringIO.StringIO("""337\tO1\tfalse\tNA12878K4\tC___1689240_10\tC\tT\tVIC-NFQ-MGB\tFAM-NFQ-MGB\trs786906\t\t0.000\tC___1689240_10\tUNKNOWN\t0.461\t2.341\t100.000\t\t0.884\t1.269\t0.893\t0.982\tHomozygous T/T\tManual\t40\ttrue\t0.200\ttrue\t3\t15\ttrue\t0.200\ttrue\t3\t15\t23.070\t22.850\tN\tN\tN\tN
338\tO2\tfalse\tNA12878K4\tC___3127590_1_\tA\tC\tVIC-NFQ-MGB\tFAM-NFQ-MGB\trs2296292\t\t0.000\tC___3127590_1_\tUNKNOWN\t1.883\t1.194\t97.382\t\t1.236\t1.093\t0.986\t0.976\tHeterozygous A/T\tAuto\t40\ttrue\t0.200\ttrue\t3\t15\ttrue\t0.200\ttrue\t3\t15\t23.896\t23.812\tN\tN\tN\tN
362\tP2\tfalse\tNA12878K4\tC___3127590_1_\tA\tC\tVIC-NFQ-MGB\tFAM-NFQ-MGB\trs2296292\t\t0.000\tC___3127590_1_\tUNKNOWN\t2.113\t1.326\t97.382\t\t1.256\t1.115\t0.990\t0.979\tHeterozygous A/C\tAuto\t40\ttrue\t0.200\ttrue\t3\t15\ttrue\t0.200\ttrue\t3\t15\t23.847\t23.788\tN\tN\tN\tN""")

    assert TaqmanParser('.')._read_taqman_file(sample_id, taqman_file) == [['rs786906', 'T', 'T'], ['rs2296292', 'A', 'C']]


def test_read_taqman_file_multiple_one_undetermined():
    sample_id = "NA12878K4"
    taqman_file = cStringIO.StringIO("""337\tO1\tfalse\tNA12878K4\tC___1689240_10\tC\tT\tVIC-NFQ-MGB\tFAM-NFQ-MGB\trs786906\t\t0.000\tC___1689240_10\tUNKNOWN\t0.461\t2.341\t100.000\t\t0.884\t1.269\t0.893\t0.982\tHomozygous T/T\tManual\t40\ttrue\t0.200\ttrue\t3\t15\ttrue\t0.200\ttrue\t3\t15\t23.070\t22.850\tN\tN\tN\tN
338\tO2\tfalse\tNA12878K4\tC___3127590_1_\tA\tC\tVIC-NFQ-MGB\tFAM-NFQ-MGB\trs2296292\t\t0.000\tC___3127590_1_\tUNKNOWN\t1.883\t1.194\t97.382\t\t1.236\t1.093\t0.986\t0.976\tHeterozygous A/T\tAuto\t40\ttrue\t0.200\ttrue\t3\t15\ttrue\t0.200\ttrue\t3\t15\t23.896\t23.812\tN\tN\tN\tN
362\tP2\tfalse\tNA12878K4\tC___3127590_1_\tA\tC\tVIC-NFQ-MGB\tFAM-NFQ-MGB\trs2296292\t\t0.000\tC___3127590_1_\tUNKNOWN\t2.113\t1.326\t97.382\t\t1.256\t1.115\t0.990\t0.979\tUndetermined\tAuto\t40\ttrue\t0.200\ttrue\t3\t15\ttrue\t0.200\ttrue\t3\t15\t23.847\t23.788\tN\tN\tN\tN""")

    assert TaqmanParser('.')._read_taqman_file(sample_id, taqman_file) == [['rs786906', 'T', 'T'], ['rs2296292', 'A', 'T']]


def test_read_taqman_file_multiple_one_undetermined_new_format():
    sample_id = "00000000000"
    taqman_file = cStringIO.StringIO("""Assay ID\tNCBI\tSNP Reference\tSample ID\tCall
C__11495689_10\trs9958735\t00000000000\tC/C
C__11495689_10\trs9958735\t00000000000\tUND
C__11495699_10\trs2296292\t00000000000\tUND
C__11495699_10\trs2296292\t00000000000\tUND
C__11495689_10\trs9958735\t11111111111\tC/T""")

    assert TaqmanParser('.')._read_taqman_file(sample_id, taqman_file) == [['rs9958735', 'C', 'C'], ['rs2296292', 'Undetermined', 'Undetermined']]

import os
import sys
import logging
import argparse
import getpass
import paramiko
import datetime
from stat import S_ISDIR, S_ISREG


LOG_PATH = '/boston/diag/transfer/sw/logs/'
logFormatter = logging.Formatter("%(asctime)s %(levelname)s - %(message)s")
log = logging.getLogger(__name__)

try:
    os.makedirs(LOG_PATH)
except OSError:
    pass

fileHandler = logging.FileHandler("{0}/importer-{1}.log".format(LOG_PATH, datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')))
fileHandler.setFormatter(logFormatter)
log.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
log.addHandler(consoleHandler)

log.setLevel(logging.INFO)
log.info("Logging setup done")


def check_connected(func):
    def wrapper(self, *args, **kwargs):
        if not self.connected:
            raise RuntimeError("SFTP is not connected. Call connect() before use.")
        return func(self, *args, **kwargs)
    return wrapper


progress_print_counter = 0


class SFTPWrapper(object):

    def __init__(self):
        self.sftp = None
        self.connected = False
        self.gid = None
        self.dir_mode = None
        self.file_mode = None

    def set_default_gid(self, gid):
        self.gid = gid

    def set_default_mode(self, dir_mode, file_mode):
        self.dir_mode = dir_mode
        self.file_mode = file_mode

    @staticmethod
    def _progress(count, total, suffix=''):
        global progress_print_counter
        # Avoid printing too often, causes issues on some systems
        progress_print_counter += 1
        if progress_print_counter < 1000:
            return
        progress_print_counter = 0

        if total == 0:
            count = total = 100
        bar_len = 40
        filled_len = int(round(bar_len * count / float(total)))

        percents = round(100.0 * count / float(total), 1)
        bar = '=' * filled_len + '-' * (bar_len - filled_len)

        sys.stdout.write('[%s] %s%s    %s\r' % (bar, percents, '%', suffix))
        sys.stdout.flush()

    @staticmethod
    def _auth_handler(title, instructions, prompt_list):
        answers = list()
        for prompt in prompt_list:
            print prompt[0]
            if prompt[1]:
                answer = raw_input()
            else:
                answer = getpass.getpass('')
            answers.append(answer)
        return answers

    def connect(self, user, hostname='tsd-fx03', port=22):
        transport = paramiko.Transport((hostname, port))
        transport.start_client()
        transport.auth_interactive(user, SFTPWrapper._auth_handler)
        self.sftp = paramiko.SFTPClient.from_transport(transport)
        self.connected = True

    @check_connected
    def is_dir(self, path):
        try:
            return S_ISDIR(self.sftp.stat(path).st_mode)
        except IOError:
            return False

    @check_connected
    def is_file(self, path):
        try:
            return S_ISREG(self.sftp.stat(path).st_mode)
        except IOError:
            return False

    @check_connected
    def exists(self, path):
        try:
            self.sftp.stat(path)
            return True
        except IOError:
            return False

    @check_connected
    def compare_size(self, local, target):
        if not self.exists(target):
            return False
        try:
            t = self.sftp.stat(target)
        except OSError:
            return False
        l = os.stat(local)
        return t.st_size == l.st_size

    @check_connected
    def makedirs(self, path):
        if self.is_dir(path):
            return

        if self.is_file(path):
            raise OSError("A file with the requested path {} already exists.".format(path))

        base, name = os.path.split(path)
        if base and not self.is_dir(base):
            self.makedirs(base)
        if name:
            try:
                self.sftp.mkdir(path)
            except IOError:
                log.error("Error mkdir: {}".format(path))
                raise
            if self.dir_mode:
                self.chmod(path, self.dir_mode)
            if self.gid:
                self.chgrp(path, self.gid)

    @check_connected
    def chgrp(self, path, gid):
        # First we need to get the current uid
        attrs = self.sftp.stat(path)
        uid = attrs.st_uid
        self.sftp.chown(path, uid, gid)

    @check_connected
    def chmod(self, path, mode):
        self.sftp.chmod(path, mode)

    @check_connected
    def put(self, path, target):
        base, filename = os.path.split(target)
        self.makedirs(base)
        attrs = self.sftp.put(
            path,
            target,
            callback=lambda x, y: SFTPWrapper._progress(x, y, suffix=filename)
        )
        if self.file_mode:
            self.chmod(target, self.file_mode)
        if self.gid:
            self.chgrp(target, self.gid)
        return attrs

    @check_connected
    def touch(self, path):
        s = self.sftp.open(path, 'a')
        s.close()

    @check_connected
    def rm(self, path):
        files = self.sftp.listdir(path)

        for f in files:
            filepath = os.path.join(path, f)
            try:
                self.sftp.remove(filepath)
            except IOError:
                self.rm(filepath)

        self.sftp.rmdir(path)


class Importer(object):

    def __init__(self, remote):
        self.remote = remote

    def import_path(self, import_path, remote_base):
        # Get first dir (should be either sample name or analysis name)
        for base_dir in os.listdir(import_path):
            # Upload all files within
            for path, _, files in os.walk(os.path.join(import_path, base_dir)):
                if files:
                    remote_path = os.path.relpath(path, os.path.join(import_path))
                    log.info("Found new directory for upload: {}".format(path))

                    for file in files:
                        local = os.path.join(path, file)
                        target = os.path.join(remote_base, remote_path, file)
                        log.info('Checking file: {}'.format(file))
                        if not self.remote.compare_size(local, target):
                            log.info("File doesn't exist or file sizes differ. Uploading...")
                            self.remote.put(local, target)
                        else:
                            log.info("File already exists and size matches, skipping")
                            continue

            log.info("Done uploading.")

    def cleanup(self, paths):
        """
        Cleanup remote by deleting all files in paths.
        """
        if not isinstance(paths, list):
            paths = [paths]
        log.info("Cleanup requested: Will delete all files on remote in {}".format(', '.join(paths)))
        answer = raw_input("Continue? (Type 'y' to continue)")
        if answer in ['y', 'Y']:
            for path in paths:
                self.remote.rm(path)
                # Remake folder as we only want to clean the content of it
                self.remote.makedirs(path)
        else:
            log.info("Aborted by user")
            sys.exit(1)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="""Transfers new samples/analyses into TSD file lock.""")
    parser.add_argument("--host", action="store", dest="host", required=False, help="Hostname of remote.")
    parser.add_argument("--cleanup", action="store_true", dest="cleanup", required=False, help="Cleanup the remote dir by deleting all the files.")
    parser.add_argument("--user", action="store", dest="user", required=True, help="Your TSD username.")
    parser.add_argument("--repo-sample", action="store", dest="repo_sample", required=False, help="Path to sample repository to import.")
    parser.add_argument("--repo-analysis", action="store", dest="repo_analysis", required=False, help="Path to analysis repository to import.")
    parser.add_argument("--repo-custom", action="store", dest="repo_custom", required=False, help="Path to folder whose subfolders will be recursively transferred to TSD")

    args = parser.parse_args()

    sftp = SFTPWrapper()
    kwargs = {}
    if args.host:
        kwargs['hostname'] = args.host
    sftp.connect(args.user, **kwargs)

    # p22-import-group
    PROJECT_GID = 1081

    sftp.set_default_gid(PROJECT_GID)
    sftp.set_default_mode(0775, 0664)

    SAMPLE_REMOTE = '/p22/import_alt/production/hts/samples/'
    ANALYSIS_REMOTE = '/p22/import_alt/production/hts/analyses/'
    CUSTOM_REMOTE = '/p22/import_alt/custom/'


    importer = Importer(sftp)
    if args.cleanup:
        importer.cleanup([SAMPLE_REMOTE,
                          ANALYSIS_REMOTE])

    if args.repo_analysis:
        importer.import_path(args.repo_analysis, ANALYSIS_REMOTE)
    if args.repo_sample:
        importer.import_path(args.repo_sample, SAMPLE_REMOTE)
    if args.repo_custom:
        importer.import_path(args.repo_custom, CUSTOM_REMOTE)

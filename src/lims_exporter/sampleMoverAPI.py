# -*- coding: utf-8 -*-
from collections import Counter
import datetime
import logging
import pdb
import time
import re
from collections import defaultdict

from genologics.entities import Protocol, Queue, Step
from genologics.descriptors import QC_FAIL, QC_PASS
import xml.etree.ElementTree as ET

logger = logging.getLogger('lims_exporterAPI.sampleMoverAPI')

# input samples format (from LimsQueue.failed_samples):
# {
#   "$sample_name": {
#     "artifacts": [ Artifact(), ... ],
#     "comment": "reason for escalation or None"
#   },
#   ...
# ]
# Where "artifacts" are the specific Analytes from Queue.artifacts

MANAGERS = {
    'EKG': 'https://ous-lims.sequencing.uio.no/api/v2/researchers/504',  # Lisa Redford
    # 'EKG': 'https://ous-lims.sequencing.uio.no/api/v2/researchers/606',  # Sean Richardson
    'EHG': 'https://ous-lims.sequencing.uio.no/api/v2/researchers/656',  # Morten Melgård
    'excap': 'https://ous-lims.sequencing.uio.no/api/v2/researchers/15',  # Marte Kirkevold
    'wgs': 'https://ous-lims.sequencing.uio.no/api/v2/researchers/17',  # Veronica Boger
    'Reanalyse' : 'https://ous-lims.sequencing.uio.no/api/v2/researchers/54',  # Hilde Johnsen
    'Super': 'https://ous-lims.sequencing.uio.no/api/v2/researchers/15',  # Marte Kirkevold
}
DEPARTMATCHER = re.compile(r"Diag-(wgs|excap|EKG|EHG|Reanalyse).*-\d+-\d+-\d+$")


class SampleMover(object):
    def __init__(self, lims, samples=None, advance_action="nextstep"):
        self.lims = lims
        if samples is None:
            samples = dict()
        self.samples = samples
        self.advance_action = advance_action

    def load(self, samples):
        self.samples.update(samples)
        return self

    def escalate(self, queueid, samps=None):
        if samps is None:
            samps = self.samples
        queue = Queue(self.lims, id=queueid)
        grouped = self.group_arts_by_department([a for s in samps.values() for a in s['artifacts']])

        steps = []
        for depart, arts in grouped.items():
            comment = "\n\n".join([
                "{}: {}".format(s['artifacts'][0].name, s.get('comment'))
                for s
                in samps.values()
                if any([a in arts for a in s['artifacts']])
                and s.get('comment')
            ])
            # import pdb; pdb.set_trace()
            new_step = Step.create(self.lims, queue.protocol_step_uri, arts)
            new_step.actions.get(force=True)

            # set QC_FAIL for all output artifacts and advance the step to escalation
            # update_arts = []
            # # only need to QC_FAIL artifacts if writing against the bioinf QC step instead of LIMS Exporter
            # for in_map, out_map in new_step.details.input_output_maps:
            #     if out_map is not None:
            #         out_map["uri"].qc_flag = QC_FAIL
            #         update_arts.append(out_map["uri"])
            # if update_arts:
            #     self.lims.put_batch(update_arts)
            new_step.advance()  # Record Details -> Next Steps
            new_step.get(force=True)
            new_step.actions.set_escalation(arts, comment, MANAGERS[depart])
            new_step.advance()  # Next Steps -> Manager Review
            steps.append(new_step)

        return steps

    def escalate_to_submitter(self, queueid, samps=None):
        if samps is None:
            samps = self.samples
        queue = Queue(self.lims, id=queueid)
        grouped = self.group_arts_by_submitter([a for s in samps.values() for a in s['artifacts']])

        steps = []
        for reviewer, arts in grouped.items():
            comment = "\n\n".join([
                "{}: {}".format(s['artifacts'][0].name, s.get('comment'))
                for s
                in samps.values()
                if any([a in arts for a in s['artifacts']])
                and s.get('comment')
            ])
            # import pdb; pdb.set_trace()
            new_step = Step.create(self.lims, queue.protocol_step_uri, arts)
            new_step.actions.get(force=True)

            # set QC_FAIL for all output artifacts and advance the step to escalation
            # update_arts = []
            # # only need to QC_FAIL artifacts if writing against the bioinf QC step instead of LIMS Exporter
            # for in_map, out_map in new_step.details.input_output_maps:
            #     if out_map is not None:
            #         out_map["uri"].qc_flag = QC_FAIL
            #         update_arts.append(out_map["uri"])
            # if update_arts:
            #     self.lims.put_batch(update_arts)
            new_step.advance() # Record Details -> Next Steps
            new_step.get(force=True)
            new_step.actions.set_escalation(arts, comment, reviewer)
            new_step.advance() # Next Steps -> Manager Review
            steps.append(new_step)

        return steps

    def advance(self, queueid, samps=None):
        if samps is None:
            samps = self.samples
        grouped = self.group_arts_by_project([a for s in samps.values() for a in s['artifacts']])

        # Find next step in protocol, or else mark the protocol as finished
        finish_protocol = False
        next_ps = None
        queue = Queue(self.lims, id=queueid)
        prot_uri = queue.protocol_step_uri[:queue.protocol_step_uri.rindex('/steps')]
        prot = Protocol(self.lims, uri=prot_uri)
        for i in range(len(prot.steps)):
            if prot.steps[i].uri == queue.protocol_step_uri:
                if i < len(prot.steps):
                    next_ps = prot.steps[i+1]
                    break
                else:
                    finish_protocol = True
        if next_ps is None:
            raise RuntimeError("Could not find protocol uri {} in protocol.steps {}".format(queue.protocol_step_uri, prot.steps))


        steps = []
        for proj_name, arts in grouped.items():
            new_step = Step.create(self.lims, queue.protocol_step_uri, arts)
            new_step.actions.get()
            new_step.actions.next_actions = [{"action": self.advance_action, "artifact": a, "step-uri": next_ps.uri} for a in arts]
            iters = 0
            while new_step.current_state != 'Completed':
                iters += 1
                old_state = new_step.current_state
                new_step.advance() # Record Details -> Next Steps
                new_step.get(force=True)
                logger.info("Advanced from {} to {} on iter {}".format(old_state, new_step.current_state, iters))
                if iters >= 5:
                    raise RuntimeError("Advanced step too many times without completion:\n{}".format(ET.tostring(new_step.root)))
            steps.append(new_step)

        return steps

    def group_arts_by_submitter(self, artifacts):
        missing_sub = 'missing-submitter'
        subs = {s.name: s.submitter.uri for s in self.lims.get_batch([s for a in artifacts for s in a.samples])}
        groups = {}
        for art in artifacts:
            sub = subs.get(art.name, missing_sub)
            if sub not in groups:
                groups[sub] = []
            groups[sub].append(art)

        bad_arts = groups.pop(missing_sub, None)
        if bad_arts:
            if len(groups) == 0:
                raise RuntimeError("No submitter to escalate bad artifacts to: {}".format(', '.join([a.id for a in artifacts])))
            else:
                logger.warn("Found {} artifacts with no submitter: {}".format(len(bad_arts), ", ".join([a.id for a in bad_arts])))

        return groups

    def group_arts_by_department(self, artifacts):
        """
        "Role-Based Permissions" of "ReviewEscalatedSample" works in Web Interface, but not through
        API, i.e. Manager Review can be only sent to Managers through API
        So we have an Manager for each department, e.g. EKG, EHG, EGG, WGS etc. who will recieve
        all Manager Reviews of his/her department's samples and is responsible for sovling the
        problem
        """
        groups = defaultdict(list)  # same keys as MANAGERS.keys
        for art in artifacts:
            try:
                pjname = art.samples[0].project.name
                dp = DEPARTMATCHER.match(pjname).group(1)
                groups[dp].append(art)
            except (AttributeError, TypeError, IndexError):
                groups['Super'].append(art)

        return groups

    def group_arts_by_project(self, artifacts):
        fallback_cat = 'mult_or_missing'
        groups = {}
        for art in artifacts:
            projs = [s.project.name for s in art.samples]
            if len(projs) != 1:
                pname = fallback_cat
            else:
                pname = projs[0]
            if pname not in groups:
                groups[pname] = []
            groups[pname].append(art)
        return groups

# keep this here until functionality has been put into the main class
#
# def resume_escalation(lims, api_fn='Tor', api_ln='Solli-Nowlan'):
#     open_procs = [
#         p for p
#         in set(lims.get_processes(techfirstname=api_fn, techlastname=api_ln))
#         if p.step.current_state != "Completed"
#     ]
#     if len(open_procs) != 1:
#         raise RuntimeError("Got unexpected number of open processes back: {}".format(open_procs))
#
#     new_step = open_procs[0].step
#     new_step.actions.get()
#     arts = open_procs[0].all_inputs()
#     reviewer = lims.get_samples(name=arts[0].name)[0].submitter.uri
#     actions = [{"artifact": a, "action": "review"} for a in arts]
#     pdb.set_trace()
#     new_step.actions.next_actions = actions
#
#     escalation = ET.SubElement(new_step.actions.root, 'escalation')
#     esc_arts = ET.SubElement(escalation, 'escalated-artifacts')
#     for a in arts:
#         ET.SubElement(esc_arts, 'escalated-artifact', {"uri": a.uri})
#     request = ET.SubElement(escalation, 'request')
#     ET.SubElement(request, 'comment').text = "\n".join(["{}: {}".format(a.name, "Error processing export, escalation interrupted") for a in arts])
#     ET.SubElement(request, 'reviewer', {"uri": reviewer})
#     pdb.set_trace()
#     new_step.actions.put()

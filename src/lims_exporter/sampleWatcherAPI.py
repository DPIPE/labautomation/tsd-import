"""
- Sample watcher using LIMS API, instead of walking the nscDelivery folder
- Samples ready to be exported are queued in "Lims Exporter" step of "Tolkning av HTS-data Diag"
Protocol, no need to touch READY files
- Get all metadata from LIMS API (Entity attributes or UDFs), instead of parsing delivery
folder/file names or parsing Demultiplexing.htm file
- md5sum is read from file "md5sum.txt" to make sure files untouched after NSC delivery
"""

import os
import re
import logging
import collections
import pprint
import glob
from datetime import datetime
from dateutil import parser as dtparser
from dateutil.relativedelta import relativedelta
from collections import defaultdict

from lims_exporterAPI import (EXPORTER_DONE_FILE, FAM_MEMBERD, GENDERD, MD5SUM_NAME, FALLBACK,
                              DEBUG, PROJECT_RE, QUALITY_CONTROL_DIR, CUSTOM_GENEPANEL_NO_ANNOPIPE,
                              TYPE1_REANA_PROJ_RE)

from genologics.entities import Process as GlsProcess, Queue
from genologics.lims import *
from genologics.config import BASEURI, USERNAME, PASSWORD

import genologics_sql.utils
from genologics_sql.tables import *
from genologics_sql.queries import *
session = genologics_sql.utils.get_session()


import preclarity_sql.utils
from preclarity_sql.tables import Sample as PreClaritySample
from preclarity_sql.queries import get_reanalysis_old_sample_name as get_preclarity_old_sample_name
session_sqlite = preclarity_sql.utils.get_session()


from fingerprinting.gettaqman import TaqmanParser

if FALLBACK:
    import glob


PROJ_MATCHER = re.compile(
    r'^Diag-(?P<proj_type>\w+?)(?P<proj_number>\d+)'
    r'(-(?P<project_date>\d+-\d+-\d+))?$', re.I
)

SAMPLE_NAME_MATCHER = re.compile(
    r'^(?P<sample_id>\w+?)'
    r'(-reanalyse)?'
    r'(-(?P<famid>[0-9]+)'
    r'(?P<fam_member>[PMF])+(?P<gender>[KM]+))?'
    r'-(?P<genepanel_name>\w+?)'
    r'(-(?P<genepanel_version>[vV]\w+?))?'
    r'(-KIT-(?P<capturekit>\w+))?$'
)

LAB_QC_CLUSTER_GEN = {
    'UDFs': [
        'Cluster Density (K/mm^2) R1',
        'Cluster Density (K/mm^2) R2',
        '% Aligned R1',
        '% Aligned R2',
        '% Bases >=Q30 R1',
        '% Bases >=Q30 R2',
        '%PF R1',
        '%PF R2',
        '% Sequencing Duplicates',
    ],
    'UDFs_rename': {
        'Cluster Density (K/mm^2) R1': 'cluster_density_R1',
        'Cluster Density (K/mm^2) R2': 'cluster_density_R2',
        '% Aligned R1': 'ratio_aligned_R1',
        '% Aligned R2': 'ratio_aligned_R2',
        '% Bases >=Q30 R1': 'ratio_above_Q30_R1',
        '% Bases >=Q30 R2': 'ratio_above_Q30_R2',
        '%PF R1': 'ratio_PF_R1',
        '%PF R2': 'ratio_PF_R2',
        '% Sequencing Duplicates': 'ratio_duplicates',
    }
}

METADATA = {
    'Sample' :  {  # {{{1
        'attrs': [
            'name',
            'date_received',
        ],
        'attrs_rename': {
            'date_received': 'date_clarity'
        },
        'UDFs' : [
            'Gene panel Diag',
            'Panel version Diag',  # non exist means latest should be used
            'Kit version Diag',                 # Kit name and Kit version together
            'Family number Diag',
            'Relation Diag',
            'Sex Diag',
            'Analysis type Diag',
            'Prioritet',
            'Enhet received Diag',
            'Reanalysis old sample ID Diag',
            'Index requested/used',
            'Specialized pipeline Diag',
        ],
        'UDFs_rename': {
            'Gene panel Diag'    : 'genepanel_name',
            'Panel version Diag' : 'genepanel_version',
            'Kit version Diag'                : 'capturekit',
            'Family number Diag'      : 'famid',
            'Relation Diag'           : 'fam_member',
            'Sex Diag'                : 'gender',
            'Analysis type Diag'      : 'joint_analysis_type',
            'Prioritet'               : 'priority',
            'Enhet received Diag'     : 'date_analysis_requested',
            'Reanalysis old sample ID Diag' :  'reana_oldid',
            'Index requested/used'          :  'requested_index',
            'Specialized pipeline Diag'     :  'specialized_pipeline',
        }
    },
    'Project': {  # {{{1
        'attrs': ['name'],
        'attrs_rename': {'name': 'projectname_with_date'},
        # ['HiSeq X', 'HiSeq high output', 'MiSeq', 'NextSeq high output']
        'UDFs' : ['Sequencing instrument requested'],
        'UDFs_rename': {'sequencing_instrument_requested'}
    },
    'Process': {  # {{{1
        # template: supported genologics entities # {{{2
        # 'process_name': {
        #     'self': {
        #         'attrs': [],
        #         'attrs_rename': dict(),
        #         'UDFs': [],
        #         'UDFs_rename': dict()
        #      },
        #     'input_Analyte': {
        #         'attrs': [],
        #         'attrs_rename': dict(),
        #         'UDFs': [],
        #         'UDFs_rename': dict()
        #     },
        #     'output_Analyte': {
        #         'attrs': [],
        #         'attrs_rename': dict(),
        #         'UDFs': [],
        #         'UDFs_rename': dict()
        #     },
        #     'output_ResultFile': {
        #         'attrs': [],
        #         'attrs_rename': dict(),
        #         'UDFs': [],
        #         'UDFs_rename': dict()
        #     }
        # }

        # sequencing_date, sequencer_id, flowcell, flowcell_id # {{{2
        # HiSeq # {{{3 excap/trio
        'Illumina Sequencing (HiSeq 3000/4000)'   : {
            'self': {
                'attrs': [],
                'attrs_rename': dict(),
                'UDFs': ['Run ID',
                         'Flow Cell Position',
                         'Flow Cell ID'],
                'UDFs_rename': {'Run ID'             : 'sequencing_run_id',
                                'Flow Cell Position' : 'flowcell',
                                'Flow Cell ID'       : 'flowcell_id'},
            }
        },
        # MiSeq # {{{3 Target, EKG, EHG
        'MiSeq Run (MiSeq)'                    : {
            'self': {
                'attrs': [],
                'attrs_rename': dict(),
                'UDFs': ['Run ID',
                         'Flow Cell ID'],
                'UDFs_rename': {'Run ID'             : 'sequencing_run_id',
                                'Flow Cell ID'       : 'flowcell_id'},
            }
        },
        # NextSeq # {{{3 Trio
        'NextSeq Run (NextSeq)'                : {
            'self': {
                'attrs': [],
                'attrs_rename': dict(),
                'UDFs': ['Run ID',
                         'Flow Cell ID'],
                'UDFs_rename': {'Run ID'             : 'sequencing_run_id',
                                'Flow Cell ID'       : 'flowcell_id'},
            }
        },
        # NextSeq # {{{3 new NextSeq workflow
        'NextSeq 500/550 Run'                : {
            'self': {
                'attrs': [],
                'attrs_rename': dict(),
                'UDFs': ['Run ID',
                         'Flow Cell ID'],
                'UDFs_rename': {'Run ID'             : 'sequencing_run_id',
                                'Flow Cell ID'       : 'flowcell_id'},
            }
        },
        # HiSeq # {{{3 WGS
        'Illumina Sequencing (HiSeq X)'   : {
            'self': {
                'attrs': [],
                'attrs_rename': dict(),
                'UDFs': ['Run ID',
                         'Flow Cell Position',
                         'Flow Cell ID'],
                'UDFs_rename': {'Run ID'             : 'sequencing_run_id',
                                'Flow Cell Position' : 'flowcell',
                                'Flow Cell ID'       : 'flowcell_id'},
            }
        },
        # HiSeq 2500  {{{3 Excap
        'Illumina Sequencing (Illumina SBS)'   : {
            'self': {
                'attrs': [],
                'attrs_rename': dict(),
                'UDFs': ['Run ID',
                         'Flow Cell Position',
                         'Flow Cell ID'],
                'UDFs_rename': {'Run ID'             : 'sequencing_run_id',
                                'Flow Cell Position' : 'flowcell',
                                'Flow Cell ID'       : 'flowcell_id'},
            }
        },

        # Novaseq {{{3 Excap WGS
        'AUTOMATED - NovaSeq Run (NovaSeq 6000 v3.0)'   : {
            'self': {
                'attrs': [],
                'attrs_rename': dict(),
                'UDFs': ['Run ID',
                         'Flow Cell Side',
                         'Flow Cell ID'],
                'UDFs_rename': {'Run ID'             : 'sequencing_run_id',
                                'Flow Cell Side'     : 'flowcell',
                                'Flow Cell ID'       : 'flowcell_id'},
            }
        },

        # Novaseq {{{3 Excap WGS; New Workflow
        'AUTOMATED - NovaSeq Run'   : {
            'self': {
                'attrs': [],
                'attrs_rename': dict(),
                'UDFs': ['Run ID',
                         'Flow Cell Side',
                         'Flow Cell ID'],
                'UDFs_rename': {'Run ID'             : 'sequencing_run_id',
                                'Flow Cell Side'     : 'flowcell',
                                'Flow Cell ID'       : 'flowcell_id'},
            }
        },

        # index # {{{2
        # Novaseq WGS {{{3
        'Adenylate ends & Ligate Adapters (TruSeq DNA)': {
            'output_Analyte': {
                'attrs'        : ['reagent_labels'],  # a SET of all reagent_labels in xml
                'attrs_rename' : dict(),
                'UDFs'         : [],
                'UDFs_rename'  : dict()
            }
        },
        # excap {{{3
        'Amplify Captured Libraries to add index tags': {
            'output_Analyte': {
                'attrs'        : ['reagent_labels'],  # a SET of all reagent_labels in xml
                'attrs_rename' : dict(),
                'UDFs'         : [],
                'UDFs_rename'  : dict()
            }
        },
        # Target a) {{{3 protocol: Diag_Nextera Hamilton (EKG)
        'Step 1: Tagmentation, Clean-up, and PCR' : {
            'output_Analyte': {
                # <reagent-label name="48 N712-E505 (GTAGAGGA-GTAAGGAG)"/>
                'attrs'        : ['reagent_labels'],  # a SET of all reagent_labels in xml
                'attrs_rename' : dict(),
                'UDFs'         : [],
                'UDFs_rename'  : dict()
            }
        },
        # Target b) {{{3 protocol: Diag_Nextera Rapid Capture (EHG)
        'First PCR Clean-up (Nextera Rapid Capture)': {
            'output_Analyte': {
                # <reagent-label name="48 N712-E505 (GTAGAGGA-GTAAGGAG)"/>
                'attrs'        : ['reagent_labels'],  # a SET of all reagent_labels in xml
                'attrs_rename' : dict(),
                'UDFs'         : [],
                'UDFs_rename'  : dict()
            }
        },
        # Target c) {{{3 protocol: EKG New
        'Tagment Genomic DNA, Clean-up and Amplify (Nextera Flex)': {
            'output_Analyte': {
                'attrs'        : ['reagent_labels'],  # a SET of all reagent_labels in xml
                'attrs_rename' : dict(),
                'UDFs'         : [],
                'UDFs_rename'  : dict()
            }
        },
        # Trio {{{3
        #    NextSeq: 'Diag_Nextera Rapid Capture' - 'Tagment DNA and clean-up Tagmented DNA'
        #    Hiseq: same as Exome
        # lane, cluster density, % Aligned, % Bases >= Q30, %PF {{{2
        #   Excap {{{3
        'Cluster Generation (HiSeq 3000/4000)': {
            'output_Analyte': {
                'attrs': ['location'],
                'attrs_rename': {
                    'location': 'flowcell_lane_coord',
                },
                'UDFs':  LAB_QC_CLUSTER_GEN['UDFs'],
                'UDFs_rename': LAB_QC_CLUSTER_GEN['UDFs_rename']
            }
        },
        # Excap {{{3
        'Cluster Generation (Illumina SBS)'            : {
            'output_Analyte': {
                'attrs'        : ['location'],
                'attrs_rename' : {'location'   : 'flowcell_lane_coord'},
                'UDFs':  LAB_QC_CLUSTER_GEN['UDFs'],
                'UDFs_rename': LAB_QC_CLUSTER_GEN['UDFs_rename']
            }
        },
        # Excap {{{3
        'Load to Flowcell (NovaSeq 6000 v3.0)'            : {
            'output_Analyte': {
                'attrs'        : ['location'],
                'attrs_rename' : {'location'   : 'flowcell_lane_coord'},
                'UDFs'         : [],
                'UDFs_rename'  : dict()
            }
        },
        # Target {{{3
        #   lane: Miseq has only one lane, so lane === 1
        #   cluster density, % Aligned, % Bases >= Q30, %PF:
        'Denature, Dilute and Load Sample (MiSeq)': {
            'output_Analyte': {
                'attrs': [],
                'attrs_rename' : dict(),
                'UDFs':  LAB_QC_CLUSTER_GEN['UDFs'],
                'UDFs_rename': LAB_QC_CLUSTER_GEN['UDFs_rename']
            }
        },

        # Trio {{{3
        #   NextSeq: lane === 'X'
        #   cluster density, % Aligned, % Bases >= Q30, %PF:
        'Denature, Dilute and Load Sample (NextSeq)': {
            'output_Analyte': {
                'attrs': [],
                'attrs_rename' : dict(),
                'UDFs':  LAB_QC_CLUSTER_GEN['UDFs'],
                'UDFs_rename': LAB_QC_CLUSTER_GEN['UDFs_rename']
            }
        },
        #   Hiseq: same as Exome; Not in use any longer

        # lane and cluster density, % Aligned, % Bases >= Q30, %PF {{{2
        # WGS {{{3
        'Cluster Generation (HiSeq X)'            : {
            'output_Analyte': {
                'attrs'        : ['location'],
                'attrs_rename' : {'location'   : 'flowcell_lane_coord'},
                'UDFs':  LAB_QC_CLUSTER_GEN['UDFs'],
                'UDFs_rename': LAB_QC_CLUSTER_GEN['UDFs_rename']
            }
        },
        # perfect_index_reads_pct, one_mismatch_index_pct, q30_bases_pct, mean_qual_score {{{2
        'Demultiplexing and QC': {
            'output_ResultFile': {
                'attrs'           : [],
                'attrs_rename'    : dict(),
                'UDFs'            : ['# Reads PF', '% Perfect Index Read',
                                     '% One Mismatch Reads (Index)', '% Bases >=Q30',
                                     'Ave Q Score', 'Sample sheet position'],
                'UDFs_rename'     : {'# Reads PF': 'readsCount',
                                     '% Perfect Index Read': 'perfect_index_reads_pct',
                                     '% One Mismatch Reads (Index)': 'one_mismatch_index_pct',
                                     '% Bases >=Q30': 'q30_bases_pct',
                                     'Ave Q Score' : 'mean_qual_score',
                                     'Sample sheet position': 'demultiplexing_samplesheet_index'}
            }
        },  # }}}
        # average fragment size {{{2
        # Excap: needs 2 of them {{{
        # instead of overwriting, pick step4 ( not step 10) from protocol 'SureSelect XT -
        # PreCapture' as the first fragment length and that(the only one) from protocol 'Diag
        # Normalization and pooling' as the last fragment length; rename as
        # 'first_avg_fragment_len' and 'last_avg_fragment_len'

        # EKG: only 1 from protocol 'Diag Normalization and pooling', rename as
        # 'last_avg_fragment_len'

        # UPDATE! TO MAKE IT SIMPLE, GET THE LAST AVERAGE FRAGMENT SIZE (last run of Tapestation
        # QC) step

        'Tapestation QC': {
            'output_ResultFile': {
                'attrs': [],
                'attrs_rename': dict(),
                'UDFs': ['Average Fragment Size'],
                'UDFs_rename': {'Average Fragment Size': 'TapeStation_average_fragment_size'}
            }
        },  # }}}
        # }}}
        # molarity {{{2
        'qPCR QC': {
            'output_ResultFile': {
                'attrs': [],
                'attrs_rename': dict(),
                'UDFs': ['Molarity'],
                'UDFs_rename': {'Molarity': 'qPCR_molarity'}
            }
        },  # }}}
        # }}}
    }  # }}}
}

BLANK_MAX_READS = 30000

def group_procs_by_usage():
    """
    multiple processes fro the same piece of metadata, due to different Project type
    """
    grouped = defaultdict(list)
    for proc in METADATA['Process']:
        for domain in METADATA['Process'][proc]:
            for region in ['attrs', 'UDFs']:
                if 'Run ID' in METADATA['Process'][proc][domain][region]:
                    grouped['sequenceing'].append(proc)
                if 'reagent_labels' in METADATA['Process'][proc][domain][region]:
                    grouped['for_reagent_labels'].append(proc)
                if 'location' in METADATA['Process'][proc][domain][region]:
                    grouped['for_location'].append(proc)
                if '# Reads PF' in METADATA['Process'][proc][domain][region]:
                    grouped['for_statistics'].append(proc)
    return grouped


USAGE_PROCS = group_procs_by_usage()


# sequence_date, sequencer_id parsed from "Run ID":
# <udf:field type="String" name="Run ID">160901_7001448_0419_AC9MV1ANXX</udf:field> sequencer_id
# project_date parsed from project[name] due to 'run_date' of project may differ from date portion
# of a project names {{{
#   PROJECT-NAME                              RUN_DATE
#   'Diag-excap43-2015-05-20'                 '2015-06-02'
#   'Diag-excap44-2015-06-01'                 '2015-06-09'
#   'Diag-excap45-2015-06-04'                 '2015-06-09'
#   'Diag-excap46-2015-06-26'                 '2015-07-06'
#   'Diag-excap57-2015-12-09'                 '2015-12-08'
#   'Diag-excap58-2015-12-17'                 '2015-12-16'
#   'Diag-excap77-2016-11-08'                 '2016-11-09'
#   'Diag-Excap80-20161219'                   '2016-12-19'
#   'Diag-excap90_2017-06-01'                 '2017-06-01'
#   'Diag-Reanalyse-2016-11-17'               '2016-11-18'
#   'Diag-Reanalyse-2016-12-19'               '2016-12-21'
#   'Diag-Reanalyse-2016-12-20'               '2016-12-21'
#   'Diag-Reanalyse-2017-02-18'               '2017-02-16'
#   'Diag-TargetS7-CuCa3-2015-11-06'          '2015-11-06'
#   'Diag-TargetS14-2016-08-29'               '2016-08-31'
#   'Diag-TargetS19-2017-01-09'               '2017-01-06'
#   'Diag-TargetS20-2017-01-30'               '2017-01-31'
#   'Diag-TargetS22-2017-03-27'               '2017-04-03'
#   'Diag-TargetS23-2017-03-27'               '2017-04-03'
#   'Diag-TargetS26-2017-05-04'               '2017-05-12'
#   'Diag-Trio6-27-10-2016'                   '2016-10-27'
#   'Diag-Trio8-2017-01-23'                   '2017-01-24'
#   'Diag-Trio9a-2017-02-08'                  '2017-02-09'
#   'Diag-Trio14-2017-06-16'                  '2017-06-15'
# }}}


class LimsQueue(object):
    """
    A class representing Queue of QC step of Bioinformatics Processing protocol
    in Clarity LIMS, this Queue has a fixed ID
    """
    def __init__(self, queueid, latest_panel_versions, source_path, repo_old_trio_analyses,
                 taqman_source, projects=None, samples=None):
        """
        queueid: Queue ID for 'lims exporter' process

        NOTE! assuming one Artificat has only one Sample
        """
        self.lims = Lims(BASEURI, USERNAME, PASSWORD)
        self.logger = logging.getLogger('lims_exporterAPI.sampleWatcherAPI.LimsQueue')
        self.queueid = queueid
        self.latest_panel_versions = latest_panel_versions
        self.source_path = source_path
        self.repo_old_trio_analyses = repo_old_trio_analyses
        self.taqman_source = taqman_source
        if projects:
            self.specified_projects = projects.split(',')
            self.logger.info('user specified projects:\n\t%s',
                             '\n\t'.join(self.specified_projects))
        else:
            self.specified_projects = None

        if samples:
            self.specified_samples = samples.split(',')
            self.logger.info('user specified samples:\n\t%s',
                             '\n\t'.join(self.specified_samples))
        else:
            self.specified_samples = None

        # list of lims sample name together with its project name, for skipping samples that are
        # already added from another process. Not quite needed when changed to get samples directly
        # from queue instead of from Processes
        self._target_samples = list()
        # list of ResultSamples in Bioinformatics Queue
        self.result_samples = list()
        # list of ResultSamples to be exported
        self.export_samples = list()
        # 'lims exporter' Queue
        self._queue = Queue(self.lims, id=self.queueid)
        # list of samples that failed for some reason. escalate, don't export
        self.failed_samples = dict()
        self._exclude_from_rss = []

        self.samples, self.projects = self.get_samples_in_Queue()
        # self.sample_names = [smp.name for smp in self.samples]
        # self.proj_and_smp_names = [s.project.name + '_' + s.name for s in self.samples]
        # Projects in 'lims exporter' queue

        # Names of Projects in 'lims exporter' queue
        if self.specified_projects:
            self.project_names = self.specified_projects
        else:
            self.project_names = set(p.name for p in self.projects)
        # Processes of all Samples in all Projects in Lims Exporter queue
        if len(self.project_names) == 0:
            self.processes = []
            self.logger.debug("Queue is empty.")
        else:
            self.get_queue_processes()

    @staticmethod
    def trim_processtype_ver(ptname):
        """ remove version number of a ProcessType

        :ptname: ProcessType name
        :returns: ProcessType name with version number removed

        """
        trimVerStr = r'(\s+([Dd]iag|NSC))?\s+\d{1,2}\.\d{1,2}$'
        return re.sub(trimVerStr, '', ptname)

    def get_samples_in_Queue(self):
        """
        collect sample and project related info
        """
        samples = list()
        projects = list()

        for art in self._queue.artifacts:
            for smp_in in art.samples:

                smp_in_name = smp_in.name

                if smp_in.project:
                    smp_proj = smp_in.project
                    smp_proj_name = smp_proj.name

                    # skip non-user-specified projects
                    if self.specified_projects and smp_proj_name not in self.specified_projects:
                        continue
                    # skip non-user-specified samples
                    # if the same sample in multiple projects, all of these samples will be
                    # exported
                    if self.specified_samples and smp_in_name not in self.specified_samples:
                        continue

                    if smp_in not in samples:
                        samples.append(smp_in)
                    if smp_proj not in projects:
                        projects.append(smp_proj)

                    smp_combine_proj = smp_in_name + '_' + smp_proj_name

                    if smp_combine_proj not in self._target_samples:
                        result_smp = ResultSample(smp_in_name, self.source_path,
                                                  self.repo_old_trio_analyses,
                                                  self.taqman_source, self.lims)
                        result_smp.metadata['artifact'] = art
                        result_smp.receive_lims_sample_metadata(smp_in)
                        result_smp.receive_lims_project_metadata(smp_proj)
                        self._target_samples.append(smp_combine_proj)
                        self.result_samples.append(result_smp)
                        self.logger.info('added new ResultSample: %s in %s %s',
                                         smp_in_name, smp_proj, smp_proj_name)
                else:
                    # sample has no project
                    self.logger.warning('%s %s has no project', smp_in, smp_in_name)

        return (samples, projects)

    def get_queue_processes(self):
        """
        get unique, targeted processes; sort processes by date_completed

        TODO:
            remove processes (Sample specific) run later than sample is queued in 'Lims Exporter'
        """
        all_queue_procs = self.get_all_procs_for_queued_projects()
        if len(all_queue_procs) == 0:
            self.processes = []
            return

        uniq_procs = self.get_unique_processes(all_queue_procs)
        if len(uniq_procs) == 0:
            self.processes = []
            return

        uniq_target_procs = self.get_targeted_processes(uniq_procs)
        if len(uniq_target_procs) == 0:
            self.processes = []
            return

        sorted_uniq_target_procs = self.sort_processes_by_date_started(uniq_target_procs)
        if len(sorted_uniq_target_procs) == 0:
            self.processes = []
            return

        self.processes = sorted_uniq_target_procs

    def get_all_procs_for_queued_projects(self):
        """all processes belonging to Projects of queued samples
        :returns: TODO

        """
        # all_queue_procs = self.lims.get_processes(projectname=self.project_names)

        from_dt = datetime.now() + relativedelta(days=-70)
        from_dt = from_dt.replace(microsecond=0).isoformat() + '+02:00'
        all_queue_procs = self.lims.get_processes(last_modified=from_dt)

        return all_queue_procs

    def get_unique_processes(self, all_queue_procs):
        """
        self.lims.get_processes(projectname='Foo') returns a list of Process with some Processes
        repeated many times (same limsid), remove such obvious duplicates
        """

        ids = [proc.id for proc in all_queue_procs]
        ids = list(frozenset(ids))
        uniq_procs = [GlsProcess(self.lims, id=id) for id in ids if id is not None]
        self.logger.debug("Following unique processes %s remaining for projects %s",
                          [(pc.type.name, pc.id) for pc in uniq_procs],
                          [(pj.name, pj.id) for pj in self.projects])

        return uniq_procs

    def get_targeted_processes(self, uniq_procs):
        """
        remove processes that not used for metatdata extraction
        """

        uniq_target_procs = []

        for proc in uniq_procs:
            ptn_nover = self.trim_processtype_ver(proc.type.name)
            if ptn_nover in METADATA['Process']:
                uniq_target_procs.append(proc)
            else:
                self.logger.debug("remove not used Process %s", ptn_nover)
        self.logger.debug("Following targeted processes %s remaining for projects %s",
                          [(pc.type.name, pc.id) for pc in uniq_target_procs],
                          [(pj.name, pj.id) for pj in self.projects])

        return uniq_target_procs

    def sort_processes_by_date_completed(self, uniq_target_procs):
        """
        A process may be run more than once(thus different Process ids), e.g.
        'Demultiplexing and QC' after re-sequencing;
        Several similar processes may be run for the same sample, e.g.
        'Illumina Sequencing (HiSeq 3000/4000)' and then 'NextSeq Run (NextSeq)' (re-sequencing
        with different sequencer)
        """

        dates_procs_completed = []

        for prc in uniq_target_procs[:]:
            try:
                date = prc.step.date_completed
                if date:
                    dates_procs_completed.append(date)
                else:
                    self.logger.warning("Process %s has no date-completed, removing it from list",
                                        prc)
                    uniq_target_procs.remove(prc)
            except AttributeError:
                self.logger.warning("Process %s has no date-completed, removing it from list",
                                    prc)
                uniq_target_procs.remove(prc)

        if len(uniq_target_procs) == 0:
            return []

        # double check correct order
        for p, d in zip(uniq_target_procs, dates_procs_completed):
            assert p.step.date_completed == d

        # sort by date-completed
        _, sorted_uniq_target_procs = zip(*sorted(zip(dates_procs_completed, uniq_target_procs)))

        return sorted_uniq_target_procs

    def sort_processes_by_date_started(self, uniq_target_procs):
        """
        A process may be run more than once(thus different Process ids), e.g.
        'Demultiplexing and QC' after re-sequencing;
        Several similar processes may be run for the same sample, e.g.
        'Illumina Sequencing (HiSeq 3000/4000)' and then 'NextSeq Run (NextSeq)' (re-sequencing
        with different sequencer)

        !!!sort by start date is more reliable than complete date? A re-run definitely starts later
        than the initial run (BUT requires that step complete date exists to exclude
        uncompleted/aborted steps)

        """

        dates_procs_started = []

        for prc in uniq_target_procs[:]:
            try:
                date_cmp = prc.step.date_completed  # complete date must available
                date_sta = prc.step.date_started
                if date_cmp:
                    dates_procs_started.append(date_sta)
                else:
                    self.logger.warning("Process %s has no date-completed, removing it from list",
                                        prc)
                    uniq_target_procs.remove(prc)
            except AttributeError:
                self.logger.warning("Process %s has no date-completed, removing it from list",
                                    prc)
                uniq_target_procs.remove(prc)

        if len(uniq_target_procs) == 0:
            return []

        # double check correct order
        for p, d in zip(uniq_target_procs, dates_procs_started):
            assert p.step.date_started == d

        # sort by date-started
        _, sorted_uniq_target_procs = zip(*sorted(zip(dates_procs_started, uniq_target_procs)))

        self.logger.debug("Following sorted targeted processes %s will be processed for projects"
                          " %s",
                          [(pc.type.name, pc.id) for pc in sorted_uniq_target_procs],
                          [(pj.name, pj.id) for pj in self.projects])

        return sorted_uniq_target_procs

    def get_all_similar_procs(self, proc, usage):
        """
        :proc:  LIMS Process
        get all similar processes among self.processes
        """
        # first check
        all_similar_procs = []

        for prc in self.processes:
            ptn_nover = self.trim_processtype_ver(prc.type.name)
            if ptn_nover in USAGE_PROCS[usage]:
                all_similar_procs.append(prc)

        return all_similar_procs

    def get_proc_usage(self, proc):
        """
        doc
        """
        ptn_nover = self.trim_processtype_ver(proc.type.name)
        for usage in USAGE_PROCS:
            if ptn_nover in USAGE_PROCS[usage]:
                return usage

    def get_latest_run_deprecated(self, smp, proc, domain):
        """
        :smp:   LIMS Sample
        :proc:  LIMS Process
        :domain: one process may be for multiple usages or domains
        Need to consider the case that same/similar runs for different batches of the same project,
        e.g. first run for sample A, B, C, second run for sample D, E, F; then both runs are unique
        Need to consider mixed case, e.g. first run for sample A, B, C, second run for sample B, C,
        M; then both second run is duplicate for only sample B, C
        """
        usage = self.get_proc_usage(proc)
        all_similar_procs = self.get_all_similar_procs(proc, usage)
        if len(all_similar_procs) <= 1:
            raise RuntimeError("We found no duplicate runs for {}, {}, {}"
                               "".format(smp, proc, usage))
        similar_procs_for_smp = []
        for proc in all_similar_procs:
            if domain in ['self', 'input_Analyte']:
                if smp in [art.samples for art in proc.all_inputs()]:
                    similar_procs_for_smp.append(proc)
            elif domain in ['output_Analyte']:
                if smp in [art.samples for art in proc.analytes()[0]]:
                    similar_procs_for_smp.append(proc)
            elif domain in ['output_ResultFile']:
                if smp in [art.samples for art in proc.result_files()]:
                    similar_procs_for_smp.append(proc)
            else:
                raise RuntimeError("unknown domain {}".format(domain))
        if len(similar_procs_for_smp) <= 1:
            raise RuntimeError("did not observe multiple similar runs for {} {} {}".format(
                smp, proc, domain))
        # keep only the latest
        latest_run = similar_procs_for_smp[0]
        latest_time = latest_run.step.date_completed

        for p_sim in similar_procs_for_smp:
            p_time = p_sim.step.date_completed
            if p_time > latest_time:
                self.logger.debug("A newer run of processes %s for %s, %s, %s",
                                  proc, smp, usage, domain)
                latest_run = p_sim
                latest_time = p_time

        return latest_run

    def processing_Artifact(self, proc, art_in, domain):
        """

        :art_in: LIIMS Artifact
        :domain: ['self', 'input_Analyte', 'output_Analyte', 'output_ResultFile']

        """

        assert domain in ['self', 'input_Analyte', 'output_Analyte', 'output_ResultFile']

        for smp_in in art_in.samples:

            smp_in_name = smp_in.name

            if smp_in.project:

                smp_proj = smp_in.project
                smp_proj_name = smp_proj.name

                for smp in self.result_samples:
                    if (smp.metadata['name'] == smp_in_name and
                            smp.metadata['projectname_with_date'] == smp_proj_name):
                        if domain == 'self':
                            smp.receive_lims_process_metadata(proc)
                        else:
                            pt_name_nover = self.trim_processtype_ver(proc.type.name)
                            smp.receive_lims_artifact_metadata(art_in, pt_name_nover, domain)
            else:
                # sample has no project
                self.logger.warning('%s %s has no project', smp_in, smp_in_name)

    @staticmethod
    def proc_has_out_analyte(proc):
        """if a LIMS Process has output of type Analyte, True/False
        proc.analytes() returns input Analyte if there is no output Analytes
        make sure proc.analytes() returns output Analyte

        :proc: LIMS Process
        :returns: True: proc has output Analyte; False: has not

        """
        for proc_output in proc.all_outputs():
            if proc_output.type == 'Analyte':
                return True

        return False

    def initial_query(self):
        """initial query API

        :returns: update self.result_samples

        """
        for proc in self.processes:

            self.logger.debug('Processing %s', proc)
            pt_name_nover = self.trim_processtype_ver(proc.type.name)

            if 'self' in METADATA['Process'][pt_name_nover]:
                for art_in in proc.all_inputs(unique=True):  # Analyte's Sample(s)
                    self.processing_Artifact(proc, art_in, 'self')
            if 'input_Analyte' in METADATA['Process'][pt_name_nover]:
                for art_in in proc.all_inputs(unique=True):
                    self.processing_Artifact(proc, art_in, 'input_Analyte')
            if ('output_Analyte' in METADATA['Process'][pt_name_nover] and
                    LimsQueue.proc_has_out_analyte(proc)):
                for art_in in proc.analytes()[0]:
                    self.processing_Artifact(proc, art_in, 'output_Analyte')
            if 'output_ResultFile' in METADATA['Process'][pt_name_nover]:
                for art_in in proc.result_files():
                    self.processing_Artifact(proc, art_in, 'output_ResultFile')

    def get_latest_genepanel_version(self, panelname):
        """
        if genepanel version not availbe from API (left empty in sample sheet), then use latest
        version available

        :returns: latest version nubmer of panelname

        """
        if panelname in ['TrioMor', 'TrioFar'] or panelname in CUSTOM_GENEPANEL_NO_ANNOPIPE:
            return '0'
        elif panelname in self.latest_panel_versions:
            ver = self.latest_panel_versions[panelname]
            ver = '{:02d}'.format(int(ver))
            ver = 'v'+str(ver)
            return ver
        else:
            raise RuntimeError('No active genepanel {0} found in bundle.json, :'.format(panelname))

    def secondary_parse(self):
        """parse/construct required meta info from initial query API

        :returns: update self.result_samples

        """
        self.logger.debug("start secondary parsing %s ResultSamples", len(self.result_samples))
        for smp in self.result_samples:
            try:
                sname = smp.metadata['name']
                # Set reanalysis values before doing more field extraction
                # add attribute whether a reanalysis sample
                smp.add_reanalyse_setting()
                # add reanalysis old sample id, name, old project, capturekit etc.
                smp.add_reanalyse_old_id_proj()
                # add type 1 reanalsis
                smp.add_type1_trio_reanalysis()
                # add reanalysis old capturekit
                # add projet name without date
                smp.add_project()
                # numeric sample id parsed from sample name
                smp.add_sample_id()
                smp.add_specialized_pipeline()

                if FALLBACK:  # metadata parsed from sample name when FALLBACK
                    if not smp.parsed_name:
                        raise RuntimeError('sample name parsing error: {0} (Reana: {1})'.format(
                            sname, smp.metadata.get('ReanaOldName')
                        ))

                    for k in ['genepanel_name',
                              'genepanel_version',  # can be 'None', handled later
                              'capturekit',
                              'famid',
                              'fam_member',
                              'gender']:
                        if k not in smp.metadata:  # not available in API
                            smp.metadata[k] = smp.parsed_name.get(k)
                        # Updated Sample Name but forgot to update UDF
                        elif smp.parsed_name.get(k) and smp.metadata[k] != smp.parsed_name.get(k):
                            raise RuntimeError("UDF for {} doesn't match Sample Name".format(k))
                smp.add_genderF()
                # update reanalyse capturekit
                smp.update_reanalyse_old_capturekit_pedigree()

                # handle gene panel version
                # when genepanel version left empty in Sample Sheet, thus UDF doesn't exit, and
                # genepanel_version part missing in sample name; use latest version of that panel
                if ('genepanel_version' not in smp.metadata
                        or smp.metadata['genepanel_version'] == ''
                        or smp.metadata['genepanel_version'] is None):

                    # get latest version
                    latest_pversion = self.get_latest_genepanel_version(
                        smp.metadata['genepanel_name']
                    )
                    smp.metadata['genepanel_version'] = latest_pversion

                smp.add_pedigree()
                # use taqman
                smp.add_uses_taqman()
                # secondary parse metadata appliable only to new samples
                if not smp.metadata['Reanalyse']:
                    smp.add_sequence_date()
                    smp.add_sequencer_id()
                    smp.add_sequencer_type()
                    smp.update_flowcell()
                    smp.add_project_date()
                    smp.add_path_of_project_dir()
                    smp.add_path_of_sample_dir()
                    smp.add_lane()
                    smp.add_index()
                    smp.add_stats()
                    smp.add_sample_reads()
                    smp.add_fastqc_dirname()
                    smp.add_whether_exported()
                    smp.add_path_of_qc_reports()
                    smp.check_sources()
                    smp.blankprove_check()

            except Exception as e:
                smp.logger.error("Error processing {}".format(sname), exc_info=1)
                self.fail_sample(smp, str(e))

    def handle_blanksamples(self):
        """
        Blankprove reads count based handling of EKG project.
        If reads count of a blankprove inside a project >= BLANK_MAX_READS:
            export the blankprove as normal
            send all other samples in the project to MR
        Else:
            send blankprove to finish protocol without creating any analysis and sample folders
            export all other samples normally
        hanle multiple EKG projects with blankprove
        """

        blankproves_not_blank = [s for s in self.result_samples
                                if s.metadata.get('blankprove_not_blank')]

        for bnb in blankproves_not_blank:
            for smp in self.result_samples:
                if (smp.metadata['project'] == bnb.metadata['project'] and
                        smp.metadata['name'] != bnb.metadata['name']):
                    self.fail_sample(smp, 'Too many reads of BlankProve in this project')


    def exclude_failed_samples(self):
        """
        remove failed_samples from result_samples
        """
        for fs in self._exclude_from_rss:
            for smp in self.result_samples[:]:
                if smp == fs:
                    self.result_samples.remove(smp)
                    self.logger.debug("Removed failed sample %s from result_samples",
                                      smp.metadata['name'])

    def check_taqman(self):
        """
        verify taqman can be found
        """

    def choose_export_smps(self):
        """filter out samples already exported

        :returns: update self.result_samples

        """
        for smp in self.result_samples:
            if DEBUG:
                self.export_samples.append(smp)
            else:
                if not smp.metadata.get('already_exported') \
                        and '-'.join([smp.metadata['projectname_with_date'],
                                      smp.metadata['name']]) not in self.failed_samples:
                    self.export_samples.append(smp)

    def poll(self):
        """
        top level monitor
        """
        self.initial_query()
        self.secondary_parse()
        self.handle_blanksamples()
        self.exclude_failed_samples()
        self.choose_export_smps()
        self.logger.debug("%s To Be Exported Samples:\n\t%s",
                          len(self.result_samples),
                          "\n\t".join([s.metadata['name'] for s in self.export_samples]))
        for export_smp in self.export_samples:
            self.logger.debug('exporting sample\n%s', pprint.pformat(vars(export_smp)))
            if export_smp.metadata.get('Reanalyse'):
                prjdir = None
                smpdir = None
            else:
                prjdir = export_smp.metadata['path_of_project_dir']
                smpdir = export_smp.metadata['path_of_sample_dir']
            yield {
                'sample': export_smp.metadata,
                'project_dir': prjdir,
                'sample_dir': smpdir
            }

    def fail_sample(self, rs, comment):
        # input string <project_name>-<sample_name>
        if not isinstance(rs, ResultSample):
            rs_objs = [rs_obj for rs_obj in self.result_samples
                       if '-'.join([rs_obj.metadata['projectname_with_date'],
                                    rs_obj.metadata['name']]) == rs]
            if len(rs_objs) != 1:
                raise ValueError("Could not find ResultSample or found too many for rs "
                                 "'{}': found {}".format(rs, len(rs_objs)))
            rs = rs_objs[0]

        # make string <project_name>-<sample_name>
        p_s = '-'.join([rs.metadata['projectname_with_date'], rs.metadata['name']])

        if p_s not in self.failed_samples:
            self.failed_samples[p_s] = {
                'artifacts': [a for a in self._queue.artifacts
                              if a.samples[0].name == rs.metadata['name'] and
                              a.samples[0].project.name == rs.metadata['projectname_with_date']],
                'comment': comment
            }

            self._exclude_from_rss.append(rs)

        return self.failed_samples[p_s]


class ResultSample(object):
    """
    restructure lims sample
    """

    def __init__(self, name, source_path, repo_old_trio_analyses, taqman_source, lims):
        """
        :name: should be the same as lims Sample.name
        """
        self.logger = logging.getLogger('lims_exporterAPI.sampleWatcherAPI.ResultSample')
        self.metadata = dict()
        self.metadata['name'] = name
        self.metadata['delivery_basepath'] = source_path
        self.metadata['repo_old_trio_analyses'] = repo_old_trio_analyses
        self.metadata['taqman_source'] = taqman_source
        self._parsed_name = None
        self.lims = lims

    def __eq__(self, theOther):
        """
        same sample has the same "Sample" and  "Project"
        """
        if isinstance(theOther, self.__class__):
            return self.metadata['name'] == theOther.metadata['name'] \
                and self.metadata['projectname_with_date'] == \
                theOther.metadata['projectname_with_date']

        return False

    def _retrieve_attr(self, attr, notEmpty=True, optional=False):
        """
        retrieve attribute of ResultSample
        when KeyError, give context

        """

        value = self.metadata.get(attr)
        if (not optional and
            notEmpty and (
                value is None or
                value == "" or
                (isinstance(value, collections.Sized) and len(value) == 0))):
            raise RuntimeError('ResultSample: {0} has no attribute "{1}"'
                               ''.format(self.metadata['name'], attr))

        return value

    def receive_lims_sample_metadata(self, lims_sample):
        """ add attributes/udfs of a LIMS Sample to ResultSample

        :lims_sample: a LIMS Sample
        :return: update self

        """
        if self.metadata['name'] != lims_sample.name:
            return

        # get Sample attributes
        if METADATA['Sample']['attrs']:
            self._add_attributes_of_lims_entity(lims_sample, METADATA['Sample']['attrs'],
                                                renaming=METADATA['Sample']['attrs_rename'])
        # get Sample UDFs
        if METADATA['Sample']['UDFs']:
            self._add_udfs_of_lims_entity(lims_sample, METADATA['Sample']['UDFs'],
                                          renaming=METADATA['Sample']['UDFs_rename'])

    def receive_lims_project_metadata(self, lims_project):
        """ add attributes/udfs of a LIMS Projec to ResultSample

        :lims_project: a LIMS Projec
        :return: update self

        """
        # get Project attributes
        if METADATA['Project']['attrs']:
            self._add_attributes_of_lims_entity(lims_project, METADATA['Project']['attrs'],
                                                renaming=METADATA['Project']['attrs_rename'])
        # get Project UDFs
        if METADATA['Project']['UDFs']:
            self._add_udfs_of_lims_entity(lims_project, METADATA['Project']['UDFs'],
                                          renaming=METADATA['Project']['UDFs_rename'])

    def receive_lims_process_metadata(self, limsProcess):
        """ add attributes or udfs of a LIMS Process itself to ResultSample
        add Process input/output artifact's attributes/udfs wiht receive_lims_artifact_metadata()

        :limsProcess: a LIMS Process
        :return: update self

        """
        pt_name_nover = LimsQueue.trim_processtype_ver(limsProcess.type.name)

        # get Process attributes
        if METADATA['Process'][pt_name_nover]['self']['attrs']:
            self._add_attributes_of_lims_entity(
                limsProcess,
                METADATA['Process'][pt_name_nover]['self']['attrs'],
                METADATA['Process'][pt_name_nover]['self']['attrs_rename'])

        # get Process UDFs
        if METADATA['Process'][pt_name_nover]['self']['UDFs']:
            self._add_udfs_of_lims_entity(
                limsProcess,
                METADATA['Process'][pt_name_nover]['self']['UDFs'],
                METADATA['Process'][pt_name_nover]['self']['UDFs_rename'])

    def receive_lims_artifact_metadata(self, limsArtifact, pt_name_nover, domain):
        """ artifact attribute or UDF

        :limsArtifact: Artifact
        :pt_name_nover: ProcessType name without version number
        :domain: ['self', 'input_Analyte', 'output_Analyte', 'output_ResultFile']
        :returns: update self

        """
        assert limsArtifact.type

        if METADATA['Process'][pt_name_nover][domain]['attrs']:
            self._add_attributes_of_lims_entity(
                limsArtifact,
                METADATA['Process'][pt_name_nover][domain]['attrs'],
                METADATA['Process'][pt_name_nover][domain]['attrs_rename']
            )
        if METADATA['Process'][pt_name_nover][domain]['UDFs']:
            self._add_udfs_of_lims_entity(
                limsArtifact,
                METADATA['Process'][pt_name_nover][domain]['UDFs'],
                METADATA['Process'][pt_name_nover][domain]['UDFs_rename']
            )

    def _add_attributes_of_lims_entity(self, lims_entity, attributes_wanted, renaming=None):
        """Add attributes of a LIMS entity to ResultSample

        : lims_entity       : a lims entity, e.g. Sample, Project, Artifact, ResultFile
        : attributes_wanted : a list of target attributes names
        : renaming          : rename the attributes of lims_entity before adding to ResultSample
        : returns           : update self

        """
        if renaming is None:
            renaming = dict()
        for attr in attributes_wanted:
            new_attr = attr if attr not in renaming else renaming[attr]
            try:
                v = getattr(lims_entity, attr)
                if isinstance(v, float):
                    v = float('{0:.2f}'.format(v))  # keep two digits after decimal point
                self.metadata[new_attr] = v
                self.logger.debug('added %s from %s for %s', new_attr, lims_entity,
                                  self.metadata['name'])
            except AttributeError:
                self.logger.debug('%s has no attribute %s for %s', lims_entity, attr,
                                  self.metadata['name'])

    def _add_udfs_of_lims_entity(self, lims_entity, udfs_wanted, renaming=None, overwrite=True):
        """Add UDFs of a LIMS entity to ResultSample

        : lims_entity : a lims entity, e.g. Sample, Project, Artifact, ResultFile
        : udfs_wanted : a list of target UDF names
        : renaming    : rename the UDF of lims_entity before adding to ResultSample
        : overwrite   : if attribute already added, overwrite it
        : returns     : update self

        """
        if renaming is None:
            renaming = dict()
        for k, v in lims_entity.udf.items():
            if k in udfs_wanted:
                new_k = k if k not in renaming else renaming[k]
                if overwrite or not hasattr(self, new_k):
                    if isinstance(v, float):
                        v = float('{0:.2f}'.format(v))  # keep two digits after decimal point
                    self.metadata[new_k] = v
                    self.logger.debug('added UDF %s from %s for %s', new_k, lims_entity,
                                      self.metadata['name'])

    def add_sample_id(self):
        """get sample_id from sample name

        :returns: add self.metadata['sample_id'] parsed from self.metadata['name']

        """
        # name = self.metadata['name']
        # unittest_001 import re
        # unittest_001 name = '123456789'
        # matched = re.match(r'^(?P<sample_id>\w+)'
        #                    r'(-(?P<famid>[0-9]+)(?P<fam_member>[PMF])(?P<gender>[KM]))?'
        #                    r'((-(?P<genepanel_name>\w+)(-(?P<genepanel_version>[vV]\w+))?)?'
        #                    r'-KIT-(?P<capturekit>\w+))?', name)
        # unittest_001 print(matched.group('sample_id'))
        # unittest_001 print(matched.group('famid'))
        # unittest_001 print(matched.group('fam_member'))
        # unittest_001 print(matched.group('gender'))
        # unittest_001 print(matched.group('genepanel_name'))
        # unittest_001 print(matched.group('genepanel_version'))
        # unittest_001 print(matched.group('capturekit'))

        if self.parsed_name:
            self.metadata['sample_id'] = self.parsed_name.get('sample_id')
            self.logger.debug("parsed sample_id (%s) for %s",
                              self.metadata['sample_id'],
                              self.metadata['name'])
        else:
            raise RuntimeError("Can't get sample ID from sample name: '{}'".format(
                self.metadata['name']))

    def add_sequence_date(self):
        """get sequence_date of a ResultSample

        :returns: add self.metadata['sequence_date'] parsed from sequencing_run_id

        """
        run_id = self._retrieve_attr('sequencing_run_id')
        dt = run_id.split('_')[0]
        date_date = datetime.strptime(dt, '%y%m%d')
        date_str = datetime.strftime(date_date, '%Y-%m-%d')

        self.metadata['sequence_date'] = date_str
        self.logger.debug("parsed sequence_date (%s) for %s",
                          self.metadata['sequence_date'],
                          self.metadata['name'])

    def add_sequencer_id(self):
        """get sequencer_id of a ResultSample

        :returns: add self.metadata['sequencer_id'] parsed from sequencing_run_id

        """

        run_id = self._retrieve_attr('sequencing_run_id')

        self.metadata['sequencer_id'] = run_id.split('_')[1]
        self.logger.debug("parsed sequencer_id (%s) for %s",
                          self.metadata['sequencer_id'],
                          self.metadata['name'])

    def add_sequencer_type(self):
        """get sequencer_type of a ResultSamples

        :returns: add self.metadata['sequencer_type'] parsed from sequencer_id

        """
        sequencer_id = self._retrieve_attr('sequencer_id')

        if sequencer_id.startswith('M'):
            sequencer_type = 'MiSeq'
        elif sequencer_id.startswith('N'):
            sequencer_type = 'NextSeq'
        elif sequencer_id.startswith('D'):
            sequencer_type = 'HiSeq 2500'
        elif sequencer_id.startswith('J'):
            sequencer_type = 'HiSeq 3000'
        elif sequencer_id.startswith('E'):
            sequencer_type = 'HiSeq X'
        elif sequencer_id.startswith('A'):
            sequencer_type = 'NovaSeq'
        else:
            raise RuntimeError("unknown sequencer id: '{}'".format(sequencer_id))

        self.metadata['sequencer_type'] = sequencer_type
        self.logger.debug("parsed sequencer_type (%s) for %s",
                          self.metadata['sequencer_type'],
                          self.metadata['name'])

    def update_flowcell(self):
        """
        (1) NovaSeq use "Left" in place of 'A' and "Right" in place of "B"
        (2) When sequenced with Hiseq/NovaSeq, then re-sequenced with Nextseq/MiSeq
            flowcell needs to be deleted, otherwise path_of_sample_dir will be wrong
        """
        sequencer_type = self._retrieve_attr('sequencer_type')

        if sequencer_type.startswith('NovaSeq'):
            nova_flowcell = self._retrieve_attr('flowcell')
            self.metadata['flowcell'] = {'Left': 'A', 'Right': 'B'}[nova_flowcell]

        if not (sequencer_type.startswith('HiSeq') or sequencer_type.startswith('NovaSeq')):
            try:
                del self.metadata['flowcell']
            except KeyError:
                pass

    def add_project(self):
        """add project (without date vs projectname_with_date) of a ResultSample e.g. Diag-excap99
        parsed from Sample.Project.name

        :returns: add self.metadata['project']

        """
        fullname = self._retrieve_attr('projectname_with_date')

        matched = re.match(r'(?P<project>([dD]iag-)?[^-]*)(-\d+)?', fullname, re.I)
        if matched:
            project = matched.group('project')
        else:
            raise RuntimeError('badly formatted project name: {0}'.format(fullname))

        self.metadata['project'] = project
        self.logger.debug("parsed project (%s) for %s",
                          self.metadata['project'],
                          self.metadata['name'])

    def add_specialized_pipeline(self):
        """ add Specialized pipeline Diag

        :returns: add self.metadata['specialized_pipeline']

        """

        sp_pipeline = self._retrieve_attr('specialized_pipeline', optional=True)

        if sp_pipeline:
            self.metadata['specialized_pipeline'] = sp_pipeline
            self.logger.debug("retrieved specialized_pipeline %s for %s",
                              self.metadata['specialized_pipeline'],
                              self.metadata['name'])

    def add_project_date(self):
        """get project_date of a ResultSample

        :returns: add self.metadata['project_date']

        """
        # fullname = 'Diag-EKG170818-2017-08-18' #pass, 2017-08-18
        # fullname = 'Diag-EKG170818' #pass, 2017-08-18
        # fullname = 'Diag-EKG170818-A' # x
        # fullname = 'EKG170818-2017-08-18' #pass, 2017-08-18
        # fullname = 'EKG170818-20170818' #pass, 2017-08-18
        # fullname = 'EKG170818add-081817' #pass, 2017-08-18
        # fullname = 'EKG170818add-181117' # x, *2018-11-17
        # fullname = 'EKG170818-170818' #pass, 2017-08-18
        # fullname = 'EKG170818' #pass 2017-08-18

        fullname = self._retrieve_attr('projectname_with_date')

        matched = re.match(r'(?P<project>([dD]iag-)?[^-]*)(-(?P<proj_date>\d+.*))?', fullname)
        if matched:
            if matched.group('proj_date'):
                try:
                    date = dtparser.parse(matched.group('proj_date'), yearfirst=True)
                except ValueError:
                    try:
                        # '2017-30-01'
                        # date = dtparser.parse(matched.group('proj_date'), dayfirst=True)
                        match_date = matched.group('proj_date')
                        m = re.match(r'(\d{4})[-/]?(\d{2})[-/]?(\d{2})', match_date)
                        if m:
                            repl_date = str(m.group(1)) + str(m.group(3)) + str(m.group(2))
                            date = dtparser.parse(repl_date, yearfirst=True)
                    except ValueError as err:
                        if not err.args:
                            err.args = ('', )
                        err.args += ('badly formatted project date ({0}) in project name({1}))'
                                     ''.format(matched.group('proj_date'), fullname),)
                        raise
            else:
                try:
                    dt = fullname[-6:]
                    date = dtparser.parse(dt, yearfirst=True)
                except ValueError as err:
                    if not err.args:
                        err.args = ('', )
                    err.args += ('badly formatted project date in project name({0}))'
                                 ''.format(fullname),)
                    raise
        else:
            raise RuntimeError('badly formatted project name: {0}'.format(fullname))

        self.metadata['project_date'] = date.strftime('%Y-%m-%d')
        self.logger.debug("parsed project_date (%s) for %s",
                          self.metadata['project_date'],
                          self.metadata['name'])

    def add_genderF(self):
        """
        translate gender
        """

        gender = self._retrieve_attr('gender', notEmpty=False, optional=True)  # EKG no gender info

        if gender:
            self.metadata['genderF'] = GENDERD[gender]

            self.logger.debug("parsed genderF (%s) for %s",
                              self.metadata['genderF'],
                              self.metadata['name'])

    def add_pedigree(self):
        """get pedigree of a Trio ResultSample

        :returns: add self.metadata['pedigree']

        """
        if 'joint_analysis_type' in self.metadata:
            if self.metadata['joint_analysis_type'].lower() == 'trio':  # a Trio ResultSample
                self.metadata['pedigree'] = {
                    FAM_MEMBERD[self.metadata['fam_member']]: {
                        "gender": self.metadata["genderF"]
                    }
                }
                self.logger.debug("added pedigree(%s) for %s",
                                  self.metadata['pedigree'],
                                  self.metadata['name'])
        else:
            if FALLBACK:
                try:
                    if self.metadata['famid']:
                        _gender = self._retrieve_attr("genderF")
                        self.metadata['pedigree'] = {
                            FAM_MEMBERD[self.metadata['fam_member']]: {
                                "gender": _gender
                            }
                        }
                        self.logger.debug("parsed pedigree(%s) for %s",
                                          self.metadata['pedigree'],
                                          self.metadata['name'])
                except Exception as e:
                    if not e.args:
                        e.args = ('',)
                    e.args += ('add_pedigree error {}'.format(self.metadata['name']),)
                    raise

    def _get_sample_read_filename(self, read_num):
        """compose fastq.gz filename of a ResultSample

        :read_num: "R1" or "R2"
        :returns: read fastq.gz file name

        """
        smp_name = self.metadata['name']
        sequencer_type = self._retrieve_attr('sequencer_type')
        lane = self._retrieve_attr('lane', optional=True)
        idx = self._retrieve_attr('index')
        samplesheet_idx = self._retrieve_attr('demultiplexing_samplesheet_index')

        # lane identifier in fastq file name
        if not lane:
            lane_identifier = None  # not lane identifier for NextSeq in read filename
        else:
            lane_identifier = 'L'+'{:03d}'.format(int(lane))

        # sample identifier(either index or S number) in fastq file name
        if sequencer_type == 'HiSeq 2500':
            smp_identifier = idx
        else:
            smp_identifier = 'S' + str(samplesheet_idx)

        filename_parts = [smp_name, smp_identifier, lane_identifier, read_num, '001']
        filename_parts_removed_none = [p for p in filename_parts if p is not None]

        read_file = '_'.join(filename_parts_removed_none) + '.fastq.gz'

        return read_file

    def _get_fastqc_dirname(self, read_num):
        """compose _fastqc dirname a ResultSample

        :read_num: "R1" or "R2"
        :returns: fastqc dirname

        """
        smp_name = self.metadata['name']
        sequencer_type = self._retrieve_attr('sequencer_type')
        lane = self._retrieve_attr('lane', optional=True)
        idx = self._retrieve_attr('index')
        samplesheet_idx = self._retrieve_attr('demultiplexing_samplesheet_index')

        # lane identifier in fastq file name
        if not lane:
            lane_identifier = None  # not lane identifier for NextSeq in read filename
        else:
            lane_identifier = 'L'+'{:03d}'.format(int(lane))

        # sample identifier(either index or S number) in fastq file name
        if sequencer_type == 'HiSeq 2500':
            smp_identifier = idx
        else:
            smp_identifier = 'S' + str(samplesheet_idx)

        dirname_parts = [smp_name, smp_identifier, lane_identifier, read_num, '001', 'fastqc']
        dirname_parts_clean = [p for p in dirname_parts if p is not None]

        dirname = '_'.join(dirname_parts_clean)

        return dirname

    def _get_qc_report_filename(self, read_num):
        """ get qc report pdf filename for read_num

        :read_num: 'R1' or 'R2'
        :returns: qc report filename

        """
        smp_name = self.metadata['name']
        run_id = self._retrieve_attr('sequencing_run_id')
        sequencer_type = self._retrieve_attr('sequencer_type')
        lane = self._retrieve_attr('lane', optional=True)
        idx = self._retrieve_attr('index')
        samplesheet_idx = self._retrieve_attr('demultiplexing_samplesheet_index')

        # lane identifier in fastq file name
        if not lane:
            lane_number = None
            lane_identifier = None  # not lane identifier for NextSeq in read filename
        elif sequencer_type == 'MiSeq':
            lane_number = None
            lane_identifier = 'L'+'{:03d}'.format(int(lane))
        else:
            lane_number = lane
            lane_identifier = 'L'+'{:03d}'.format(int(lane))

        # sample identifier(either index or S number) in fastq file name
        if sequencer_type == 'HiSeq 2500':
            smp_identifier = idx
        else:
            smp_identifier = 'S' + str(samplesheet_idx)

        head = [run_id, lane_number]
        head_clean = [p for p in head if p is not None]
        middle_parts = [smp_name, smp_identifier, lane_identifier, read_num, '001']
        middle_parts_clean = [p for p in middle_parts if p is not None]
        middle_clean = ['_'.join(middle_parts_clean)]
        tail = ['qc', 'pdf']

        report_name = '.'.join(head_clean + middle_clean + tail)

        return report_name

    def add_path_of_project_dir(self):
        """compose path_of_project_dir of a ResultSample

        :returns: add self.metadata['path_of_project_dir']

        """
        sequencing_run_id = self._retrieve_attr('sequencing_run_id')
        seq_date_id = '_'.join(sequencing_run_id.split('_')[0:2])
        projectname_with_date = self._retrieve_attr('projectname_with_date')

        if 'flowcell' in self.metadata:
            flowcell = self._retrieve_attr('flowcell', optional=True)
            relative_path = (seq_date_id + '.' +
                             flowcell + '.' +
                             'Project' + '_' +
                             projectname_with_date)

        else:
            relative_path = (seq_date_id + '.' +
                             'Project' + '_' +
                             projectname_with_date)

        self.metadata['path_of_project_dir'] = os.path.join(self.metadata['delivery_basepath'],
                                                            relative_path)

        self.logger.debug("parsed path_of_project_dir for %s", self.metadata['name'])

    def add_path_of_sample_dir(self):
        """add real_sample_dir which is the path to folder containing R1 R2 fastq.gz files
        HiSeq - each sample has a subdir in project dir containing fastq.gz files

        :returns: add self.metadata['path_of_sample_dir']

        """
        smp_name = self.metadata['name']
        sequencer_type = self._retrieve_attr('sequencer_type')
        project_dir = self._retrieve_attr('path_of_project_dir')

        # set {'HiSeq X', 'HiSeq high output', 'MiSeq', 'NextSeq high output'}
        if sequencer_type in ['HiSeq 2500', 'HiSeq 3000', 'HiSeq X', 'NovaSeq']:
            subdir = 'Sample_' + smp_name
            self.metadata['path_of_sample_dir'] = os.path.join(self.metadata['delivery_basepath'],
                                                               project_dir, subdir)
        elif sequencer_type in ['MiSeq', 'NextSeq']:
            self.metadata['path_of_sample_dir'] = os.path.join(self.metadata['delivery_basepath'],
                                                               project_dir)

        self.logger.debug("parsed path_of_sample_dir (%s) for %s",
                            self.metadata['path_of_sample_dir'],
                            self.metadata['name'])

    def add_reanalyse_setting(self):
        """whether Reanalyse

        :returns: add self.metadata['Reanalyse']
        None             : not reanalysis sample ,
        *                : reanalysis sample     , search old id/name, old project automatically
                                                   by matching leading 9 digits of this sample id
        11 digits number : reanalysis sample     , search old id/name, old project automatically
                                                   by matching leading 9 digits of this 11 digits
                                                   number

        """
        if FALLBACK:
            projectname_with_date = self._retrieve_attr('projectname_with_date')

            matched = re.match(r'(diag[-_])?Reanalyse', projectname_with_date, re.I)

            self.metadata['Reanalyse'] = bool(matched)
        else:
            reanalysis_old_sampleid = self._retrieve_attr('reana_oldid', optional=True)
            self.metadata['Reanalyse'] = reanalysis_old_sampleid

        self.logger.debug("add Reanalyse (%s) for %s",
                          self.metadata['Reanalyse'],
                          self.metadata['name'])

    def _querydb_reana_oldname_oldproj(self):
        """
        """

        # get old  id/name, old project
        if FALLBACK:
            oldname_oldproj_clarity = get_reanalysis_old_sample_name(
                session,
                self.metadata['name']
            )
            oldname_oldproj_preclarity = get_preclarity_old_sample_name(
                session_sqlite,
                self.metadata['name']
            )

            return oldname_oldproj_clarity + oldname_oldproj_preclarity

        else:
            _reana = self.metadata['Reanalyse']
            if _reana.strip() == '?':
                oldname_oldproj_clarity = get_reanalysis_old_sample_name(
                    session,
                    self.metadata['name']
                )
                oldname_oldproj_preclarity = get_preclarity_old_sample_name(
                    session_sqlite,
                    self.metadata['name']
                )

                return oldname_oldproj_clarity + oldname_oldproj_preclarity

            else:
                # 11 digits id/fullname/project given in excel sheet
                match = re.match(r'(?P<sample_name>(?P<sample_id>\d{11})(-[\w-]*)?)'
                                 r'([, ]+(?P<project>[\w-]+))?$',
                                 _reana.strip())
                if not match:
                    raise RuntimeError("Wrong 'Reanalysis old sample id': '{}' in lims sheet."
                                       "".format(_reana))

                the_name, the_id, the_project = match.group('sample_name', 'sample_id', 'project')

                oldname_oldproj_clarity = get_reanalysis_old_sample_name(
                    session, the_id
                )
                oldname_oldproj_preclarity = get_preclarity_old_sample_name(
                    session_sqlite, the_id
                )

                all_queried = oldname_oldproj_clarity + oldname_oldproj_preclarity
                match_specified = []

                if the_name == the_id and not the_project:  # only id is specified
                    match_specified = all_queried
                else:  # more filtering with sample_name and/or project
                    for smp, proj in all_queried:
                        if the_name != the_id:  # sample_name is given
                            if the_project:
                                if smp == the_name and proj == the_project:  # match both
                                    match_specified.append((smp, proj))
                            elif smp == the_name:  # match sample_name only
                                match_specified.append((smp, proj))
                        elif proj == the_project:  # "id,project" specified, match project
                            match_specified.append((smp, proj))

                return match_specified

    def add_reanalyse_old_id_proj(self):
        """
        if reanalysis sample, add old sample id/name, old project
        add self.metadata['ReanaOldName']
        add self.metadata['ReanaOldID']
        add self.metadata['ReanaOldProject']
        """

        if not self.metadata['Reanalyse']:
            return

        oldname_oldproj = self._querydb_reana_oldname_oldproj()

        # update metadata
        if len(oldname_oldproj) == 0:   # no match
            # if DEBUG:
            #     self.logger.debug('Skip export reanalysis %s due to 0 old id found',
            #                       self.metadata['name'])
            #     self.metadata['skip_export'] = True
            # else:
            raise RuntimeError("Could not find reanalysis old sample id for {0}"
                               "".format(self.metadata['name']))

        elif len(oldname_oldproj) == 1:  # ideally single match
            self._update_reana_metadata(oldname_oldproj[0])

        else:  # multiple matches
            # try to rescue by further filtering
            diags = []
            for smp, proj in oldname_oldproj:
                Pmatch = PROJ_MATCHER.match(proj)
                Nmatch = SAMPLE_NAME_MATCHER.match(smp)
                if not Pmatch:
                    continue
                if not Nmatch:
                    continue
                if Pmatch.group('proj_type').lower() not in ['excap']:
                    continue
                if not Pmatch.group('proj_number'):
                    continue
                if Nmatch.group('capturekit') not in ['Av5', 'TWHCV1']:
                    continue

                diags.append([int(Pmatch.group('proj_number')), (smp, proj)])

            if len(diags) == 0:
                raise RuntimeError(
                    "Found 0 old id from multiple candidate old sample ids:\n{}".format(
                        '\n'.join([', '.join([n, p]) for n, p in oldname_oldproj])
                    )
                )
            elif len(diags) == 1:
                self._update_reana_metadata(diags[0][1])
            else:
                latest_one = sorted(diags, reverse=True)[0][1]
                self._update_reana_metadata(latest_one)

    def _update_reana_metadata(self, old_name_proj):
        """
        update reanalysis metadata
        :old_name_proj: tuple of old sample name, old project name
        """
        fullname_smp, fullname_prj = old_name_proj
        matched = re.match(r'(?P<project>([dD]iag-)?[^-]*)(-\d+)?', fullname_prj)
        if matched:
            project = matched.group('project')
        else:
            raise RuntimeError('badly formatted project name: {0}'.format(fullname_prj))

        self.metadata['ReanaOldName'] = fullname_smp
        self.metadata['ReanaOldID'] = self.metadata['ReanaOldName'][:11]
        self.metadata['ReanaOldProjectWithDate'] = fullname_prj
        self.metadata['ReanaOldProject'] = project

        self.logger.debug("parsed ReanaOldName (%s) for %s",
                          self.metadata['ReanaOldName'],
                          self.metadata['name'])
        self.logger.debug("parsed ReanaOldID (%s) for %s",
                          self.metadata['ReanaOldID'],
                          self.metadata['name'])
        self.logger.debug("parsed ReanaOldProjectWithDate(%s) for %s",
                          self.metadata['ReanaOldProjectWithDate'],
                          self.metadata['name'])
        self.logger.debug("parsed ReanaOldProject(%s) for %s",
                          self.metadata['ReanaOldProject'],
                          self.metadata['name'])

    def add_type1_trio_reanalysis(self):
        """
        type1 trio reanalysis: not hybrid trio reanalysis where parents are newly sequenced
        self.metadata['old_trio_analysis_path'] added
        self.metadata['old_triopipe_json_path'] added
        """

        if not all([self.metadata['Reanalyse'],  # reanalysis
                    self.metadata['joint_analysis_type'].lower() == 'trio',  # requested trio
                    TYPE1_REANA_PROJ_RE.match(self.metadata['projectname_with_date'])  # type1
                    ]):
            return

        old_trio_analysis_path = None

        # match with the newest
        matched_dirs = glob.glob(
            os.path.join(
                self.metadata['repo_old_trio_analyses'],
                '-'.join([self.metadata['ReanaOldProject'],
                            self.metadata['ReanaOldID']]
                            ) + '-P*'
            )
        )

        if len(matched_dirs) == 1:
            old_trio_analysis_path = matched_dirs[0]
        else:
            # try match first 9 digits of old sample id only
            matched_dirs = glob.glob(
                os.path.join(
                    self.metadata['repo_old_trio_analyses'],
                    ''.join(['*', self.metadata['ReanaOldID'][:9], '*'])
                )
            )

            if len(matched_dirs) == 1:
                old_trio_analysis_path = matched_dirs[0]
            elif len(matched_dirs) > 1:
                old_trio_analysis_path = matched_dirs[0]
                self.logger.warnning("multiple old trio found for %s: %s; randomly chose %s",
                                     self.metadata['ReanaOldID'][:9],
                                     matched_dirs,
                                     matched_dirs[0])
            else:
                raise RuntimeError("There is no folder in {} for sample {}"
                                   "".format(self.metadata['repo_old_trio_analyses'],
                                             self.metadata['ReanaOldID'][:9]))

        # check triopipe json file can be found
        old_trio_dot_analysis_pttn = os.path.join(old_trio_analysis_path,
                                                  '*',
                                                  '*-TRIO.analysis')
        self.logger.debug("globbing %s", old_trio_dot_analysis_pttn)
        triopipe_json = glob.glob(old_trio_dot_analysis_pttn)
        if len(triopipe_json) != 1:
            raise RuntimeError("Could not find triopipe .analysis file in {}"
                               "".format(self.metadata['old_trio_analysis_path']))

        self.metadata['old_trio_analysis_path'] = old_trio_analysis_path
        self.metadata['old_triopipe_json_path'] = triopipe_json[0]

    def add_lane(self):
        """lane

        :returns: add self.metadata['lane']

        """
        sequencer_type = self._retrieve_attr('sequencer_type')

        if sequencer_type in ['HiSeq 2500', 'HiSeq 3000', 'HiSeq X']:
            flowcell_lane_coord = self._retrieve_attr('flowcell_lane_coord')
            lane = flowcell_lane_coord[1].split(':')[0]
        elif sequencer_type in ['NovaSeq']:
            flowcell_lane_coord = self._retrieve_attr('flowcell_lane_coord', optional=True)
            # NovaSeq Xp (flowcell SP, S1 and S2: 2 lanes; S4: 4 lanes)
            if flowcell_lane_coord:
                lane = flowcell_lane_coord[1].split(':')[0]
            # NovaSeq Standard, no lane splitting
            else:
                lane = None
        elif sequencer_type in ['MiSeq']:
            lane = '1'
        elif sequencer_type in ['NextSeq']:
            lane = None

        self.metadata['lane'] = lane

        self.logger.debug("parsed lane (%s) for %s",
                          self.metadata['lane'],
                          self.metadata['name'])

    def add_index(self):
        """index

        :returns: add self.metadata['index']

        """
        reagent_labels = self._retrieve_attr('reagent_labels', optional=True)

        if not reagent_labels:
            # use Sample UDF "Index requested/used" instead
            index = self._retrieve_attr('requested_index', optional=True)
            if index:
                self.metadata['index'] = index
                self.logger.debug("retrieved index (%s) from submitted sample UDF "
                                  "'Index requested/used' for %s",
                                  self.metadata['index'],
                                  self.metadata['name'])
                return
            else:
                # make index optional
                self.metadata['index'] = None

        if len(reagent_labels) != 1:
            raise RuntimeError("sample {0} from project {1} should have one and only one reagent "
                               "label".format(self.metadata['name'],
                                              self.metadata['projectname_with_date']))
        # <reagent-label name="SureSelect XT2 Index 05-E (CGCTGATC)"/>
        # <reagent-label name="48 N712-E505 (GTAGAGGA-GTAAGGAG)"/>
        else:
            label = reagent_labels[0]
            matched = re.match(r'.*\((?P<index>[ATGC-]+)\)$', label)
            index = matched.group('index')
            self.metadata['index'] = index

            self.logger.debug("parsed index (%s) for %s",
                            self.metadata['index'],
                            self.metadata['name'])

    def _format_perfect_index_reads_pct(self):
        """format perfect_index_reads_pct
        :returns: update self.metadata['perfect_index_reads_pct']

        """
        perfect_index_reads_pct = self._retrieve_attr('perfect_index_reads_pct')

        perfect_index_reads_pct = '{0:.2f}'.format(float(perfect_index_reads_pct))

        return perfect_index_reads_pct

    def _format_one_mismatch_index_pct(self):
        """get one_mismatch_index_pct
        :returns: update self.metadata['one_mismatch_index_pct']

        """
        one_mismatch_index_pct = self._retrieve_attr('one_mismatch_index_pct')
        one_mismatch_index_pct = '{0:.2f}'.format(float(one_mismatch_index_pct))

        return one_mismatch_index_pct

    def _format_mean_qual_score(self):
        """get mean_qual_score
        :returns: update self.metadata['mean_qual_score']

        """
        mean_qual_score = self._retrieve_attr('mean_qual_score')

        mean_qual_score = '{0:.2f}'.format(float(mean_qual_score))

        return mean_qual_score

    def _format_q30_bases_pct(self):
        """get q30_bases_pct
        :returns: update self.metadata['q30_bases_pct']

        """
        q30_bases_pct = self._retrieve_attr('q30_bases_pct')

        q30_bases_pct = '{0:.2f}'.format(float(q30_bases_pct))

        return q30_bases_pct

    def add_stats(self):
        """add stats dict to smp

        :returns: add self.metadata['stats'] (dict)

        """
        perfect_index_reads_pct = self._format_perfect_index_reads_pct()
        one_mismatch_index_pct = self._format_one_mismatch_index_pct()
        mean_qual_score = self._format_mean_qual_score()
        q30_bases_pct = self._format_q30_bases_pct()

        stats = {
            'reads': int(self.metadata['readsCount']),
            'perfect_index_reads_pct': float(perfect_index_reads_pct),
            'one_mismatch_index_pct': float(one_mismatch_index_pct),
            'q30_bases_pct': float(q30_bases_pct),
            'mean_qual_score': float(mean_qual_score),
        }

        self.metadata['stats'] = stats

        self.logger.debug("parsed stats (%s) for %s",
                          self.metadata['stats'],
                          self.metadata['name'])

    def _get_sample_read_file_md5(self, read_num):
        project_dir = self._retrieve_attr('path_of_project_dir')
        fastq_file = self._get_sample_read_filename(read_num)
        md5_file = os.path.join(project_dir, MD5SUM_NAME)
        if not os.path.exists(md5_file):
            raise RuntimeError("no such file {0} for {1}({2})"
                               "".format(md5_file, self.metadata['name'],
                                         self.metadata['project']))
        md5 = dict()
        found = False
        with open(md5_file) as fd:
            for line in fd.xreadlines():
                if fastq_file in line:
                    found = True
                    clean_line = line.rstrip()
                    if fastq_file in md5:
                        raise RuntimeError("Got multiple MD5 sum matches for same fastq: {}"
                                           "".format(fastq_file))
                    fastq_md5 = clean_line.split('  ', 1)[0]
                    fastq_path = clean_line.split('  ', 1)[1]
                    if not os.path.exists(os.path.join(project_dir, fastq_path)):
                        # entry in md5sums.txt, but no fastqc file
                        found = False
                        self.logger.warn(
                            "Found md5sum entry, but no file for {}: {}".format(
                                self.metadata['name'],
                                os.path.join(project_dir, fastq_path)
                            )
                        )
                        continue
                    md5['md5'] = fastq_md5
                    md5['path'] = os.path.join(project_dir, fastq_path)
                    break
            if not found:
                if FALLBACK:
                    fastq_files = glob.glob(os.path.join(self.metadata['path_of_sample_dir'],
                                                         '.'.join(
                                                             [''.join(['*',
                                                                       self.metadata['name'],
                                                                       '*']),
                                                              'fastq.gz'])))
                    for ff in fastq_files:
                        if read_num in os.path.basename(ff):
                            fastq_file = os.path.basename(ff)
                            break
                    with open(md5_file) as fd:
                        for line in fd.xreadlines():
                            if fastq_file in line:
                                found = True
                                clean_line = line.rstrip()
                                if fastq_file in md5:
                                    raise RuntimeError("Got multiple MD5 sum matches for same"
                                                       " fastq: {}".format(fastq_file))
                                fastq_md5 = clean_line.split('  ', 1)[0]
                                fastq_path = clean_line.split('  ', 1)[1]
                                md5['md5'] = fastq_md5
                                md5['path'] = os.path.join(project_dir, fastq_path)
                                break
                        if not found:
                            raise RuntimeError("md5 for {0} is not found in {1}"
                                               "".format(fastq_file, md5_file))
                else:
                    raise RuntimeError("md5 for {0} is not found in {1}"
                                       "".format(fastq_file, md5_file))

        # Get file length
        files = {
            'path': fastq_file,
            'size': os.path.getsize(md5['path']),
            'md5': md5['md5'],
        }

        return files

    def add_sample_reads(self):
        """add reads info to ResultSample

        :returns: add self.metadata['reads']

        """
        self.metadata['reads'] = [
            self._get_sample_read_file_md5("R1"),
            self._get_sample_read_file_md5("R2")
        ]

        self.logger.debug("parsed reads (%s) for %s",
                          self.metadata['reads'],
                          self.metadata['name'])

    def add_path_of_qc_reports(self):
        """add qc_reports info to ResultSample

        :returns: add self.metadata['qc_reports']

        """
        self.metadata['qc_reports'] = [
            self._get_qc_report_filename("R1"),
            self._get_qc_report_filename("R2")
        ]

        self.logger.debug("parsed qc_reports (%s) for %s",
                          self.metadata['qc_reports'],
                          self.metadata['name'])

    def add_fastqc_dirname(self):
        """add fastqc_paths info to ResultSample

        :returns: add self.metadata['fastqc_paths']

        """
        self.metadata['fastqc_paths'] = [
            self._get_fastqc_dirname("R1"),
            self._get_fastqc_dirname("R2")
        ]

        self.logger.debug("parsed fastqc_paths (%s) for %s",
                          self.metadata['fastqc_paths'],
                          self.metadata['name'])

    def add_whether_exported(self):
        """whether a ResultSample has already been exported

        :returns: add self.metadata['already_exported']

        """
        sample_name = self.metadata['name']
        sample_dir = self._retrieve_attr('path_of_sample_dir')
        already_exported = False
        if (os.path.exists(os.path.join(sample_dir, EXPORTER_DONE_FILE)) or
                os.path.exists(os.path.join(sample_dir,
                                            '.'.join([sample_name, EXPORTER_DONE_FILE])))):
            already_exported = True

        self.metadata['already_exported'] = already_exported

        self.logger.debug("parsed already_exported (%s) for %s",
                          self.metadata['already_exported'],
                          self.metadata['name'])

    def update_reanalyse_old_capturekit_pedigree(self):
        """
        reanalysis sample's capturekit same as old
        """
        if not self.metadata['Reanalyse']:
            return

        old_name = self.metadata['ReanaOldName']
        old_proj = self.metadata['ReanaOldProjectWithDate']
        new_name = self.metadata['name']

        attrs = ['capturekit',
                 'famid',
                 'fam_member',
                 'gender']

        lookup = {
            'Kit version Diag': 'capturekit',
            'Sex Diag': 'gender',
            'Family number Diag': 'famid',
            'Relation Diag': 'fam_member',
            'Analysis type Diag': 'joint_analysis_type',  # not handled by SAMPLE_NAME_MATCHER
        }
        # 1st try to parse capturekit from old sample name (even when FALLBACK is OFF, due to for
        # old samples, famid, capturekit may be missing in API)
        matched = SAMPLE_NAME_MATCHER.match(old_name)

        if matched:
            for a in attrs:
                oldkey1 = 'ReanaOld' + a.capitalize()
                if not self.metadata.get(oldkey1):
                    self.metadata[oldkey1] = matched.group(a)  # give this priority

        # single or trio only by whether famid in sample name (for later usage)
        if not self.metadata.get('ReanaOldFamid'):
            self.metadata['ReanaOldSingleOrTrio'] = 'Single'
        else:
            self.metadata['ReanaOldSingleOrTrio'] = 'Trio'

        # 2nd get original capturekit from original sample's udf
        for s in self.lims.get_samples(projectname=old_proj):
            if s.name == old_name:
                udfs = dict(s.udf.items())
                for k, v in lookup.items():
                    oldkey2 = 'ReanaOld' + v.capitalize()
                    if not self.metadata.get(oldkey2):  # do not overwrite
                        self.metadata[oldkey2] = udfs.get(k)

        # 3rd add default for 'ReanaOldJoint_analysis_type'
        if not self.metadata.get('ReanaOldJoint_analysis_type'):
            self.metadata['ReanaOldJoint_analysis_type'] = None

        # reanalysis sample has no capturekit, safe to overwrite
        self.metadata['capturekit'] = self.metadata['ReanaOldCapturekit']
        # obligatory capturekit for analysis_type then for uses_taqman
        if not self.metadata['capturekit']:
            raise RuntimeError("Could not get original capturekit of {0} (Reana({1} {2})"
                               "".format(new_name, old_name, old_proj))

        # translate old gender
        self.metadata['ReanaOldGenderF'] = GENDERD.get(self.metadata.get('ReanaOldGender'))

        # verify geneder consistance
        ng = self.metadata.get('gender')
        og = self.metadata.get('ReanaOldGender')
        if ng and og and ng != og:
            raise RuntimeError("Reanalysis {} gender differs from original sample {}"
                               "".format(self.metadata['name'], self.metadata['ReanaOldName']))
        # set 'gender'
        if not self.metadata.get('gender'):
            self.metadata['gender'] = self.metadata['ReanaOldGender']
            self.metadata['genderF'] = self.metadata['ReanaOldGenderF']

        self.logger.debug("parsed old capturekit %s, famid %s, fam_member %s, gender %s, "
                          "joint_analysis_type %s single_or_trio %s of %s Reana (%s %s)",
                          self.metadata['capturekit'],
                          self.metadata['ReanaOldFamid'],
                          self.metadata['ReanaOldFam_member'],
                          self.metadata['ReanaOldGender'],
                          self.metadata['ReanaOldJoint_analysis_type'],
                          self.metadata['ReanaOldSingleOrTrio'],
                          new_name,
                          old_name,
                          old_proj
                          )
        # pedigree
        # After new lims sheet, 'Analysis type' is either of ['Single','Trio'](never missing)
        # Before new lims sheet, both single and trio sample can
        #    a) missing 'Analysist type'
        #    b) 'Analysis type' == 'exome'
        # After new lims sheet,  singles sample also have 'famid'

        if (self.metadata.get('ReanaOldJoint_analysis_type') and
                self.metadata['ReanaOldJoint_analysis_type'].lower() == 'trio' or
                self.metadata.get('ReanaOldSingleOrTrio') == 'Trio'):
            self._add_old_pedigree()

    def _add_old_pedigree(self):
        """
        self.metadata['ReanaOldPedigree']
        """
        self.metadata['ReanaOldPedigree'] = {
            FAM_MEMBERD[self.metadata['ReanaOldFam_member']]: {
                "gender": self.metadata["ReanaOldGender"]
            }
        }
        self.logger.debug("parsed old pedigree(%s) for %s (Reana %s)",
                          self.metadata['ReanaOldPedigree'],
                          self.metadata['name'],
                          self.metadata['ReanaOldName'])

    def _get_analysis_type(self):
        '''
        any of [exome, trio, target, genome or trio-excap]
        As infered from capture kit and family information in metadata dict
        Reanalyse sample's capturekit is from old sample
        '''
        anatype = None

        if self.metadata['capturekit'] in ['Av5', 'Av7', 'CREv2', 'TWHCV1']:
            if not self.metadata.get('pedigree'):
                anatype = 'exome'
            else:
                anatype = 'trio-excap'
        elif self.metadata['capturekit'] in ['TSOv1', 'TSOv2']:
            anatype = 'trio'
        elif self.metadata['capturekit'] in ['CuCaV1', 'CuCaV2', 'CuCaV3', 'TSCarV1']:
            anatype = 'target'
        elif self.metadata['capturekit'] in ['wgs']:
            if not self.metadata.get('pedigree'):
                anatype = 'genome'
            else:
                anatype = 'trio-genome'
        else:
            raise ValueError("Unknown capture kit {}!".format(self.metadata['capturekit']))

        self.logger.debug("parsed analysis_type (%s) for %s",
                          anatype,
                          self.metadata['name'])

        return anatype

    def add_uses_taqman(self):
        """
        need taqman?
        """
        # tumore samples no taqman
        if self.metadata.get('specialized_pipeline') == 'TUMOR':
            self.metadata['uses_taqman'] = False

            self.logger.debug("uses_taqman is False for TUMOR sample  %s",
                              self.metadata['name'])
            return

        # Blankprove no taqman
        if self.metadata['name'].lower().startswith('blankprove'):
            self.metadata['uses_taqman'] = False

            self.logger.debug("uses_taqman is False for blank sample  %s",
                              self.metadata['name'])
            return

        # other samples judge by analysis_type
        required = None

        analysis_type = self._get_analysis_type()

        if analysis_type in ['exome', 'genome']:
            required = True
        elif analysis_type in ['trio', 'trio-excap', 'trio-genome']:
            required = False
        elif analysis_type in ['target']:
            if self.metadata['capturekit'] in ['TSCarV1']:
                required = False
            elif self.metadata['capturekit'] in ['CuCaV1', 'CuCaV2', 'CuCaV3']:
                required = True
            else:
                raise ValueError("Unknown capture kit {} for analysis type {}!"
                                 "".format(self.metadata['capturekit'], analysis_type))
        else:
            raise ValueError("Unknown analysis type {}!".format(analysis_type))

        self.metadata['uses_taqman'] = required

        self.logger.debug("parsed uses_taqman (%s) for %s",
                          self.metadata['uses_taqman'],
                          self.metadata['name'])

    def check_sources(self):
        """
        check required files/folders exist
        """

        # Check for taqman data
        if self.metadata['uses_taqman']:
            if not TaqmanParser(self.metadata['taqman_source']).exists(self.metadata['sample_id']):
                raise RuntimeError("Found no taqman data for sample {}"
                                   "".format(self.metadata['sample_id']))

        # Check for fastq files
        for fastq in self.metadata['reads']:
            src_rd = os.path.join(self.metadata['path_of_sample_dir'], fastq['path'])
            if not os.path.exists(src_rd):
                raise RuntimeError("read file not exist: {}".format(src_rd))

        # Check for NSC QC report
        for qc_report in self.metadata['qc_reports']:
            src_qc = os.path.join(self.metadata['path_of_sample_dir'], qc_report)
            if not os.path.exists(src_qc):
                raise RuntimeError("qc_report file not exist: {}".format(src_qc))

        # Check for Fastqc files
        for fastqc_path in self.metadata['fastqc_paths']:
            src_fqc = os.path.join(self.metadata['path_of_project_dir'], QUALITY_CONTROL_DIR,
                                   '_'.join(['Sample', self.metadata['name']]), fastqc_path)
            if not os.path.exists(src_fqc):
                raise RuntimeError("fastqc_path file not exist: {}".format(src_fqc))

    def blankprove_check(self):
        """
        a sample is blank if its reads count < BLANK_MAX_READS; otherwise not blank
        normal sample should not be blank; sample literally named 'blankprove' should be blank

        OPTIONALLY add self.metadata['blankprove_not_blank'] = [True,False]
        OPTIONALLY add self.metadata['blankprove_is_blank'] = [True,False]
        """

        is_blankprove = False
        is_blank = False
        blankprove_not_blank = False
        blankprove_is_blank = False

        if self.metadata['name'].lower().startswith('blankprove'):
            is_blankprove = True

        if self.metadata['readsCount'] < BLANK_MAX_READS:
            is_blank = True

        if is_blankprove and not is_blank:
            blankprove_not_blank = True

        if is_blankprove and is_blank:
            blankprove_is_blank = True

        self.metadata['blankprove_not_blank'] = blankprove_not_blank
        self.metadata['blankprove_is_blank'] = blankprove_is_blank

    @property
    def parsed_name(self):
        if self._parsed_name is None:
            #
            # import re
            matched = SAMPLE_NAME_MATCHER.match(
                self.metadata['name']
                #
                # '16000346203-MigraForst-KIT-Av5'
            )
            #
            # print(matched.groupdict()['capturekit'])

            if matched:
                self._parsed_name = matched.groupdict()

        return self._parsed_name

#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
#################
HTS LIMS exporter
#################

Exports samples ready for Bioinformatics pipeline from Clarity LIMS

Using Clarity API to get all meta data instead of parsing from the delivery files (file names/html)

The output (in the so called repository) is meant to be copied further upstream for pipeline
processing and then deleted (to be automated with TSD API)

The result will look like the following structure:

repo-analysis
   └── Diag-excap01-123456789-EEogPU-v02
       └── Diag-excap01-123456789-EEogPU-v02.analysis

repo-analysis for trio
   └── Diag-excap01-123456789-EEogPU-v02 (the sample id is the sample id of the proband)
       └── Diag-excap01-123456789-EEogPU-v02.analysis

repo-sample
    └── Diag-excap01-123456789
        ├── 123456789-EEogPU-v02-KIT-Av5_GCCAAT_L007_R1_001_fastqc
        ├── 123456789-EEogPU-v02-KIT-Av5_GCCAAT_L007_R2_001_fastqc
        ├── 123456789-EEogPU-v02-KIT-Av5_GCCAAT_L007_R1_001.fastq.gz
        ├── 123456789-EEogPU-v02-KIT-Av5_GCCAAT_L007_R2_001.fastq.gz
        ├── Diag-excap01-123456789.sample
        ├── Diag-excap01-123456789.taqman (if has)
        └── LIMS_EXPORT_DONE

Some things to note:

- All sample in Lims Exporter Queue of Bioinformatics Protocol in Clarity are to be exported
- If a sample fails to process for whatever reason, it is sent to Manager Review in Clarity Web
Interface.
- The fastq.gz files are hardlinked from the original.
- A LIMS_EXPORT_DONE file is created in the source sample dir to be backward compatible with
lims-exporter(the previous one). Re-export a sample to be handled in future.

"""


import datetime
import json
import os
import bdb
import sys
import logging
import argparse
import re
import time
from collections import defaultdict

# for testing
# set system enviroment LIMSDEBUG on command line to run lims-exporter-api in Debug mode
DEBUG = bool("LIMSEXPDEBUG" in os.environ)

# before the new Sample Sheet Template excle file is put in usage and all new UDFs and new
# ProcessType and new version Protocol and Workflow is created & tested, "fallback" to parsing any
# non-existing UDFs from sample name; monitor "Bioinformatics processing_diag" Step/Queue instead
# of "LIMS Exporter" Queue; before Lims Exporter step is added and auto-forward sample to next step
# is implemented, remove the EXPORTER_DONE_FILE can re-export a sample.
FALLBACK = False

# don't handle reanalysis yet
SKIP_REANALYSIS = False

# sleep time
SLEEP_TIME = 5 * 60  # 5 minutes

logFormatter = logging.Formatter("%(asctime)-25s %(name)-50s %(levelname)-10s %(message)s")
log = logging.getLogger(__name__)

logfileDir = os.path.join(sys.path[0], *(['..'] * 3 + ['logsAPI']))

try:
    os.makedirs(logfileDir)
except OSError:
    pass
fileHandler = logging.FileHandler(
    "{0}/{1}.log".format(logfileDir, datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')))
fileHandler.setFormatter(logFormatter)
fileHandler.setLevel(logging.DEBUG)
log.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
consoleHandler.setLevel(logging.DEBUG)
log.addHandler(consoleHandler)

if DEBUG:
    log.setLevel(logging.DEBUG)
else:
    log.setLevel(logging.INFO)

# HARD CODING VARIABLES
# when "tolkning av hts-data diag" protocol change to a new verson, "lims exporter" queue id
# changes. so ideally, queue id should be searched on the fly by traversing all protocols and
# then its steps and do regex matchings to find the 'acitve' queue id , but to save script
# running time and protocol version doesn't change often, we hard code queue id, and update it
# when protocol version change(rare event)
LIMSQUEUEID = '1603'  # "Lims exporter" step

QUALITY_CONTROL_DIR = 'QualityControl'
MD5SUM_NAME = 'md5sum.txt'

PROJECT_READY = 'READY'
EXPORTER_DONE_FILE = 'LIMS_EXPORT_PROCESSED'
DONE_FILE = 'LIMS_EXPORT_DONE'

FAM_MEMBERD = {
    'F': 'father',
    'M': 'mother',
    'P': 'proband'
}

GENDERD = {
    'K': 'female',
    'M': 'male'
}

CAPTUREKIT_MAP = {
    'Av5': 'agilent_sureselect_v05',
    'TSOv1': 'illumina_trusightone_v01',
    'TSOv2': 'illumina_trusightone_v02',
    'CREv2': 'agilent_cre_v02',
    'TWHCV1': 'twist_humancoreexome_v01'
}

PROJECT_RE = re.compile(r'^Diag-(?P<project>.*)-(?P<project_date>\d+-\d+-\d+)$', re.I)

# Type 1 reanalyse project, e.g. Diag-Reanalyse-2020-03-26
TYPE1_REANA_PROJ_RE = re.compile(r'^Diag-Reanalyse-(?P<project_date>\d+-\d+-\d+)$', re.I)

SPECIAL_TAG_HURTIGGENOM = 'Dragen'

DRAGEN_SUFFIX = 'DR'

CUSTOM_GENEPANEL_NO_ANNOPIPE = ['Custom']

def get_latest_panel_versions(bundle_path):
    """
    get latest versions of genepanels mentioned in bundle.json

    :returns: dict {name: latest_version}

    """

    bundle_json_path = os.path.join(bundle_path, 'bundle.json')
    if not os.path.exists(bundle_json_path):
        raise RuntimeError("File {0} don't exist".format(bundle_json_path))

    with open(bundle_json_path) as f:
        bundle = json.load(f)
        gp = defaultdict(list)
        for k, v in bundle['clinicalGenePanels'].iteritems():
            gp_name, gp_version = k.split('_')

            if 'inactive' in v:  # ignore this panel
                continue

            gp[gp_name].append(int(gp_version.lstrip('v')))

        return {k: sorted(v)[-1] for k, v in gp.iteritems()}


import analysisCreatorAPI
import sampleCreatorAPI
import sampleWatcherAPI
import sampleMoverAPI


def general_handler(expt_cls, expt_inst, expt_tb):
    """
    handle uncaught exceptions
    """
    if expt_cls is bdb.BdbQuit:
        sys.exit(1)
    elif issubclass(expt_cls, KeyboardInterrupt):
        sys.exit("Lims exporter was manually stopped by user.")

    log.error("An error occured while checking for new samples",
              exc_info=(expt_cls, expt_inst, expt_tb))


def export():
    """
    main export function
    """

    sw = limsq.poll()

    if not sw:
        log.info("Nothing to export!")
        return

    if export_priorities:
        low_priority_parents = list()  # cache parents with low priority

    filtered_sw = []

    for sample in sw:

        # skip reanalysis
        if SKIP_REANALYSIS and sample['sample']['Reanalyse']:
            continue

        if DEBUG and sample['sample'].get('skip_export', None):
            continue

        # skip low priority singles; store all parents for later rescue
        if export_priorities and str(sample['sample'].get('priority')) not in export_priorities:
            # cache non-priority parent
            if ('pedigree' in sample['sample'] and
                    sample['sample']['pedigree'].keys()[0] != 'proband'):
                log.debug("Caching non priority parent %s (%s)",
                          sample['sample']['name'],
                          sample['sample'].get('priority'))

                low_priority_parents.append(sample)

            # non-priority single and proband
            else:
                log.debug("Skipping non priority sample %s (%s)",
                          sample['sample']['name'],
                          sample['sample'].get('priority'))

            continue

        filtered_sw.append(sample)

    if not filtered_sw:
        log.info("Nothing to export after filtering")
        return

    """
    We get one sample at a time, but our goal is to get complete trios.
    To do this we store the family id (famid) and pedigree of each incoming sample.
    When we have a complete set for a given family id, we can start exporting the whole set.
    """
    fams = dict()

    exported_samps = {}

    remove_workflow_samples = {}

    for sample in filtered_sw:

        try:
            # SampleCreator
            sc = sampleCreatorAPI.SampleCreator(sample, repo_sample, taqman_source)
            if (not sample['sample'].get('Reanalyse')
                    and not sample['sample'].get('blankprove_is_blank')):
                sc.create()
            else:
                log.info('Skipping SampleCreator for %s(old: %s) due to Reanalysis',
                         sample['sample']['name'], sc.sample_name)

            # AnalysisCreator - Single
            if 'pedigree' not in sc.metadata:
                # skip creating analysis metadata for blankprove which is blank
                if not sc.metadata.get('blankprove_is_blank'):
                    log.debug(sc.sample_name)
                    ac = analysisCreatorAPI.SingleAnalysisCreator(
                        repo_analyses, sc.sample_name, sc.metadata
                    )
                    ac.write_to_file()
                    exported_samps[
                        sc.metadata['project'] + '-' + sc.metadata['name']
                    ] = {"artifacts": [sc.metadata['artifact']]}
                else:
                    remove_workflow_samples[
                        sc.metadata['project'] + '-' + sc.metadata['name']
                    ] = {"artifacts": [sc.metadata['artifact']]}

            # AnalysisCreator - Trio
            else:
                sc_ped = sc.metadata['pedigree'].keys()[0]
                if sc.metadata['famid'] not in fams:
                    log.debug("Creating new fam entry for %s (%s)",
                              sc.sample_name,
                              sc.metadata['famid'])
                    fams[sc.metadata['famid']] = {
                        "pedigree": {
                            sc_ped: {
                                "gender": sc.metadata['genderF'],
                                "sample": sc.sample_name
                            }
                        },
                        "metadata": {
                            sc_ped: sc.metadata
                        }
                    }
                else:
                    log.debug("Adding member %s to fam entry %s",
                              sc.sample_name,
                              sc.metadata['famid'])
                    fams[sc.metadata['famid']]['pedigree'][sc_ped] = {
                        "gender": sc.metadata['genderF'],
                        "sample": sc.sample_name
                    }
                    fams[sc.metadata['famid']]['metadata'][sc_ped] = sample['sample']

                # try to rescue lower priority parents when high priority proband encountered
                if export_priorities and sc_ped == 'proband':
                    log.debug("Trying to rescue non priority parents for %s", sc.metadata['name'])
                    for parent in low_priority_parents:
                        # parent famid
                        parent_famid = None
                        if parent['sample'].get('famid'):
                            parent_famid = parent['sample'].get('famid')
                        elif parent['sample'].get('ReanaOldFamid'):
                            parent_famid = parent['sample']['ReanaOldFamid']
                        else:
                            log.warning("Famid for %s is not available",
                                        parent['sample']['name'])
                        # same fam as proband
                        if sc.metadata['famid'] != parent_famid:
                            continue

                        # SampleCreator
                        log.debug("Adding low priority parent %s of high priority proband %s",
                                    parent['sample']['name'], sc.metadata['name'])
                        pc = sampleCreatorAPI.SampleCreator(parent, repo_sample, taqman_source)
                        if not pc.metadata.get('Reanalyse'):
                            pc.create()
                        else:
                            log.info(
                                'Skipping SampleCreator for %s(old: %s) due to Reanalysis',
                                parent['sample']['name'],
                                pc.sample_name)

                        # AnalysisCreator - Trio
                        if 'pedigree' in pc.metadata:
                            pc_ped = pc.metadata['pedigree'].keys()[0]
                            pc_gender = pc.metadata['genderF']
                        elif 'ReanaOldPedigree' in pc.metadata:
                            pc_ped = pc.metadata['ReanaOldPedigree'].keys()[0]
                            pc_gender = pc.metadata['ReanaOldGenderF']
                        else:
                            log.warning("%s has no pedigree and or gender info",
                                        pc.metadata['name'])
                            continue

                        log.debug(
                            "Adding member %s to fam entry %s",
                            pc.sample_name,
                            parent_famid
                        )
                        fams[parent_famid]['pedigree'][pc_ped] = {
                            "gender": pc_gender,
                            "sample": pc.sample_name
                        }
                        fams[parent_famid]['metadata'][pc_ped] = parent['sample']

                # normal Trio complete
                if (len(fams[sc.metadata['famid']]['pedigree']) == 3
                        or sc.metadata.get('old_trio_analysis_path')):
                    log.debug("Got full trio: {}".format(sc.metadata['famid']))
                    for ped in fams[sc.metadata['famid']]['metadata'].keys():
                        ac = analysisCreatorAPI.TrioAnalysisCreator(
                            repo_analyses,
                            fams[sc.metadata['famid']]['pedigree'][ped]['sample'],
                            fams[sc.metadata['famid']]['metadata'][ped],
                            fams[sc.metadata['famid']]['pedigree']
                        )
                        ac.write_to_file()
                        exported_samps[fams[sc.metadata['famid']]['metadata'][ped]['name']] = {
                            "artifacts": [fams[sc.metadata['famid']]['metadata'][ped]['artifact']]}
                        log.debug("Finished writing TrioAnalysis for %s",
                                  sc.sample_name)
                    fams.pop(sc.metadata['famid'])
                    log.debug("Finished processing trio for famid %s",
                              sc.metadata['famid'])
        except Exception as e:
            log.exception("Error occured while creating sample/analysis for sample "
                          "data %s", str(sample))
            limsq.fail_sample(
                sample['sample']['projectname_with_date'] + '-' + sample['sample']['name'],
                str(e)
            )
            continue
    # }}}
    log.debug("Finished looping through all %s samples in the queue",
              len(limsq.export_samples))
    if fams:  # {{{
        # waiting for other trio members
        log.debug("Unprocessed trio samples remaining: %s",
                  sum([len(x['pedigree']) for x in fams.values()]))
        for fid, smp_meta in [(fid, fams[fid]['metadata'][ped]) for fid in fams
                              for ped in fams[fid]['metadata']]:
            comment = ("Partial trio: only {} samples found for fam:{}, waiting for other"
                       " members".format(len(fams[fid]['pedigree']), fid))

            # samp_rs = [rs for rs in limsq.result_samples
            #            if rs.metadata['name'] == smp_meta['name'] and
            #            rs.metadata['project'] == smp_meta['project']][0]

            # wait instead of fail
            log.debug(comment)
            # limsq.fail_sample(samp_rs, comment)
    # }}}

    if len(limsq.failed_samples) > 0:
        mover = sampleMoverAPI.SampleMover(limsq.lims, limsq.failed_samples)
        # import pdb; pdb.set_trace()
        mover.escalate(LIMSQUEUEID)
        log.info("Finished sending %s failed export samples to Manager Review in "
                 "Clarity", len(limsq.failed_samples))

    if exported_samps:
        mover = sampleMoverAPI.SampleMover(limsq.lims, exported_samps)
        # import pdb; pdb.set_trace()
        mover.advance(LIMSQUEUEID)
        log.info("Finished advancing %s exported samples in Clarity",
                 len(exported_samps))
        exported_samps = []

    if remove_workflow_samples:
        mover = sampleMoverAPI.SampleMover(limsq.lims, remove_workflow_samples,
                                           advance_action='remove')
        # import pdb; pdb.set_trace()
        mover.advance(LIMSQUEUEID)
        log.info("Finished remove %s export samples from workflow in Clarity",
                 len(remove_workflow_samples))
        remove_workflow_samples = []


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Exports and converts samples from sequencing"
                                     " centre into samples/analysis ready for use with automation"
                                     " system.")
    parser.add_argument("--repo-sample", action="store", dest="repo_sample",
                        required=True, help="Path to sample output dir.")
    parser.add_argument("--repo-analysis", action="store", dest="repo_analyses",
                        required=True, help="Path to analysis output dir.")
    parser.add_argument("--repo-old-trio-analysis", action="store", dest="repo_old_trio_analyses",
                        required=True, help="Path to old trio analysis folder for reanalysis.")
    parser.add_argument("--source", action="store", dest="source", required=True,
                        help="Path to root for delivered raw samples from sequencing.")
    parser.add_argument("--bundle", action="store", dest="bundle_dir",
                        required=True, help="Path to root of the vcpipe-bundle repo")
    parser.add_argument("--taqman-source", action="store", dest="taqman_source",
                        required=True, help="Path to root for raw taqman files.")
    parser.add_argument("--killfile", required=True, help="Path to killfile")
    parser.add_argument("--mark-ready", action="store_true", dest="mark_ready",
                        required=False, help="Mark samples/analysis with READY file.")
    parser.add_argument("-l", action="store_true", dest="looping",
                        help="""run infinitely by looping""")
    parser.add_argument("--projects", required=False,
                        help="""comma separated list of project names in the lims exporter queue
                        to export. If --samples is not specified, export all samples of these
                        projects that are queued""")
    parser.add_argument("--samples", required=False,
                        help="comma separated list of sample names in the lims exporter queue")
    parser.add_argument("--priorities", required=False,
                        help="comma separated list of priority levels")

    args = parser.parse_args()

    repo_sample = args.repo_sample
    repo_analyses = args.repo_analyses
    repo_old_trio_analyses = args.repo_old_trio_analyses
    taqman_source = args.taqman_source
    bundle_dir = args.bundle_dir
    export_projects = args.projects
    export_samples = args.samples
    export_priorities = args.priorities
    looping = args.looping
    killfile = args.killfile

    sys.excepthook = general_handler

    for path in [repo_sample, repo_analyses, repo_old_trio_analyses]:
        if not os.path.exists(path):
            raise RuntimeError("Path doesn't exist: {}".format(path))

    source_path = args.source

    if export_priorities:
        export_priorities = export_priorities.split(',')

    # check for killfile at starting up
    if os.path.exists(killfile):
        sys.exit("killfile exists, can't start lims exporter.")

    latest_panel_versions = get_latest_panel_versions(bundle_dir)

    def get_lims_queue():
        lims_queue = sampleWatcherAPI.LimsQueue(LIMSQUEUEID, latest_panel_versions, source_path,
                                                repo_old_trio_analyses,
                                                taqman_source,
                                                export_projects,
                                                export_samples)
        return lims_queue

    # run once only
    limsq = get_lims_queue()

    export()

    del limsq

    # continuous running
    while True:
        # check for killfile after an iteration
        if os.path.exists(killfile):
            log.error("Lims exporter stopped by killfile.")
            break
        if looping:
            log.info("sleeping ...")
            time.sleep(SLEEP_TIME)
            limsq = get_lims_queue()
            export()
            del limsq
        else:
            break

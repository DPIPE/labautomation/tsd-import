#!/usr/bin/env python
"""
test ../db/diagnostics_samples_database.py
"""
import os
from db.diagnostics_samples_database import GetDiagnosticsSamples


def test_initiate():
    """
    test creating instance of GetDiagnosticsSamples
    """
    _dbfile = '/tmp/tmp_db.db'
    gds = GetDiagnosticsSamples(_dbfile)

    assert gds.dbfile == _dbfile

    # db file is created if not already exits
    assert os.path.isfile(_dbfile)

    # tables are created
    assert [('pre_clarity_samples',),
            ('clarity_samples',),
            ('log',)] == gds.curs.execute('select tbl_name from sqlite_master '
                                          'where type="table"').fetchall()

"""
utility functions
"""
import re
from genologics_sql.tables import *

from sqlalchemy import text

from operator import itemgetter


def get_last_modified_projects(session, interval="2 hours"):
    """gets the project objects last modified in the last <interval>

    :query: select * from project where age(lastmodifieddate)< '1 hour'::interval;

    :param session: the current SQLAlchemy session to the database
    :param interval: str Postgres-compliant time string
    :returns: List of Project records

    """
    txt = "age(now(),lastmodifieddate)< '{int}'::interval".format(int=interval)
    return session.query(Project).filter(text(txt)).all()


def get_last_modified_project_udfs(session, interval="2 hours"):
    """gets the project objects that have a udf last modified in the last <interval>

    :param session: the current SQLAlchemy session to the database
    :param interval: str Postgres-compliant time string
    :returns: List of Project records

    """
    query = "select pj.* from project pj \
           inner join entityudfstorage eus on pj.projectid = eus.attachtoid \
           where eus.attachtoclassid = 83 and age(now(), eus.lastmodifieddate) \
           < '{int}'::interval;".format(int=interval)
    return session.query(Project).from_statement(text(query)).all()


def get_last_modified_project_sample_udfs(session, interval="2 hours"):
    """gets the project objects that have sample udfs last modified in the last <interval>

    :param session: the current SQLAlchemy session to the database
    :param interval: str Postgres-compliant time string
    :returns: List of Project records
    """
    query = "select distinct pj.* from project pj \
            inner join sample sa on sa.projectid=pj.projectid \
            inner  join processudfstorage pus on sa.processid=pus.processid \
            where age(now(), pus.lastmodifieddate) < '{int}'::interval;".format(int=interval)
    return session.query(Project).from_statement(text(query)).all()


def get_last_modified_project_artifacts(session, interval="2 hours"):
    """gets the project objects that have artifacts last modified in the last <interval>

    :param session: the current SQLAlchemy session to the database
    :param interval: str Postgres-compliant time string
    :returns: List of Project records
    """
    query = "select distinct pj.* from project pj \
            inner join sample sa on sa.projectid=pj.projectid \
            inner join artifact_sample_map asm on sa.processid=asm.processid \
            inner join artifact art on asm.artifactid=art.artifactid \
            where age(now(), art.lastmodifieddate) < '{int}'::interval;".format(int=interval)
    return session.query(Project).from_statement(text(query)).all()


def get_last_modified_project_artifact_udfs(session, interval="2 hours"):
    """gets the project objects that have artifact udfs last modified in the last <interval>

    :param session: the current SQLAlchemy session to the database
    :param interval: str Postgres-compliant time string
    :returns: List of Project records
    """
    query = "select distinct pj.* from project pj \
            inner join sample sa on sa.projectid=pj.projectid \
            inner join artifact_sample_map asm on sa.processid=asm.processid \
            inner join artifactudfstorage aus on asm.artifactid=aus.artifactid \
            where age(now(), aus.lastmodifieddate) < '{int}'::interval;".format(int=interval)
    return session.query(Project).from_statement(text(query)).all()


def get_last_modified_project_containers(session, interval="2 hours"):
    """gets the project objects that have containers last modified in the last <interval>

    :param session: the current SQLAlchemy session to the database
    :param interval: str Postgres-compliant time string
    :returns: List of Project records
    """
    query = "select distinct pj.* from project pj \
            inner join sample sa on sa.projectid=pj.projectid \
            inner join artifact_sample_map asm on sa.processid=asm.processid \
            inner join containerplacement cpl on asm.artifactid=cpl.processartifactid \
            inner join container ct on cpl.containerid=ct.containerid \
            where age(ct.lastmodifieddate) < '{int}'::interval;".format(int=interval)
    return session.query(Project).from_statement(text(query)).all()


def get_last_modified_project_processes(session, interval="2 hours"):
    """gets the project objects that have containers last modified in the last <interval>

    :param session: the current SQLAlchemy session to the database
    :param interval: str Postgres-compliant time string
    :returns: List of Project records
    """
    query = "select distinct pj.* from project pj \
            inner join sample sa on sa.projectid=pj.projectid \
            inner join artifact_sample_map asm on sa.processid=asm.processid \
            inner join processiotracker pit on asm.artifactid=pit.inputartifactid \
            inner join process pro on pit.processid=pro.processid \
            where age(now(), pro.lastmodifieddate) < '{int}'::interval;".format(int=interval)
    return session.query(Project).from_statement(text(query)).all()


def get_last_modified_project_process_udfs(session, interval="2 hours"):
    """gets the project objects that have containers last modified in the last <interval>

    :param session: the current SQLAlchemy session to the database
    :param interval: str Postgres-compliant time string
    :returns: List of Project records
    """
    query = "select distinct pj.* from project pj \
            inner join sample sa on sa.projectid=pj.projectid \
            inner join artifact_sample_map asm on sa.processid=asm.processid \
            inner join processiotracker pit on asm.artifactid=pit.inputartifactid \
            inner join process pro on pit.processid=pro.processid \
            inner join processudfstorage pus on pro.processid=pus.processid \
            where age(now(), pus.lastmodifieddate) < '{int}'::interval;".format(int=interval)
    return session.query(Project).from_statement(text(query)).all()


def get_last_modified_projectids(session, interval="2 hours"):
    """gets all the projectids for which any part has been modified in the last interval

    :param session: the current SQLAlchemy session to the database
    :param interval: str Postgres-compliant time string
    :returns: List of Project records
    """
    projectids = set()
    for project in get_last_modified_projects(session, interval):
        projectids.add(project.luid)

    for project in get_last_modified_project_udfs(session, interval):
        projectids.add(project.luid)

    for project in get_last_modified_project_sample_udfs(session, interval):
        projectids.add(project.luid)

    for project in get_last_modified_project_containers(session, interval):
        projectids.add(project.luid)

    for project in get_last_modified_project_processes(session, interval):
        projectids.add(project.luid)

    for project in get_last_modified_project_process_udfs(session, interval):
        projectids.add(project.luid)

    return projectids


def get_last_modified_processes(session, ptypes, interval="24 hours"):
    """gets all the processes of the given <type> that have been modified
    or have a udf modified in the last <interval>

    :param session: the current SQLAlchemy session to the db
    :param ptypes: the LIST of process type ids to be returned
    :param interval: the postgres compliant interval of time to search processes in.

    """
    query = "select distinct pro.* from process pro \
            inner join processudfstorage pus on pro.processid=pus.processid \
            where (pro.typeid in ({typelist}) \
            and age(now(), pus.lastmodifieddate) < '{int}'::interval) \
            or \
            (age(now(), pro.lastmodifieddate) < '{int}'::interval \
            and pro.typeid in ({typelist}));".format(int=interval,
                                                     typelist=",".join([str(x) for x in ptypes]))
    return session.query(Process).from_statement(text(query)).all()


def get_processes_in_history(session, parent_process, ptypes, sample=None):
    """returns wll the processes that are found in the history of parent_process
    AND are of type ptypes

    :param session: the current SQLAlchemy session to the db
    :param parent_process: the id of the parent_process
    :param ptypes: the LIST of process type ids to be returned
    :param sample: if defined, filter artifacts that match the correct sample

    """
    qar = ["select distinct pro.* from process pro \
            inner join processiotracker pio on pio.processid=pro.processid \
            inner join outputmapping om on om.trackerid=pio.trackerid \
            inner join artifact_ancestor_map aam on pio.inputartifactid=aam.ancestorartifactid\
            inner join processiotracker pio2 on pio2.inputartifactid=aam.artifactid \
            inner join process pro2 on pro2.processid=pio2.processid "]
    if sample:
        qar.append("inner join artifact_sample_map asm on asm.artifactid=pio.inputartifactid ")
    qar.append("where pro2.processid={parent} and pro.typeid in ({typelist}) ")
    if sample:
        qar.append("and asm.processid = {sampleid}".format(sampleid=sample))
    qar.append(";")
    query = ''.join(qar).format(parent=parent_process, typelist=",".join([str(x) for x in ptypes]))
    return session.query(Process).from_statement(text(query)).all()


def get_children_processes(session, parent_process, ptypes, sample=None, orderby=None):
    """returns all the processes that are found in the children of parent_process
    AND are of type ptypes

    :param session: the current SQLAlchemy session to the db
    :param parent_process: the id of the parent_process
    :param ptypes: the LIST of process type ids to be returned

    """

    qar1 = ["""select pro.* from process pro
            inner join processiotracker piot on piot.processid=pro.processid
            inner join artifact_ancestor_map aam on aam.artifactid=piot.inputartifactid
            inner join outputmapping om on aam.ancestorartifactid=om.outputartifactid
            inner join processiotracker piot2 on piot2.trackerid=om.trackerid """]
    qar2 = ["""select pro.* from process pro
            inner join processiotracker piot on piot.processid=pro.processid
            inner join outputmapping om on piot.inputartifactid=om.outputartifactid
            inner join processiotracker piot2 on piot2.trackerid=om.trackerid """]
    if sample:
        qar1.append("inner join artifact_sample_map asm on asm.artifactid=piot.inputartifactid ")
        qar2.append("inner join artifact_sample_map asm on asm.artifactid=piot.inputartifactid ")
    qar1.append("where piot2.processid={parent} and pro.typeid in ({typelist}) ")
    qar2.append("where piot2.processid={parent} and pro.typeid in ({typelist}) ")
    if sample:
        qar1.append("and asm.processid = {sampleid}".format(sampleid=sample))
        qar2.append("and asm.processid = {sampleid}".format(sampleid=sample))
    if orderby:
        qar2.append("order by {}".format(orderby))

    query = "{} union {};".format(''.join(qar1), ''.join(qar2)).format(
        parent=parent_process, typelist=",".join([str(x) for x in ptypes]))
    return session.query(Process).from_statement(text(query)).all()


def get_reanalysis_old_sample_name(session, sample_name, capturekit):
    """TODO: Docstring for get_reanalysis_old_sample_name.

    :session: the current SQLAlchemy session to the db
    :sample_name: new sample id with a different last 2 digits or Verso sample id (e.g. HG12345678)
                  optionally with Cardi suffix (e.g. HG12345678R1234, 12345678902R1234)
    :returns: List of Tuple of sample name and project name where sample names matches first 9
              digits of sample_name

    """
    # Verso sample ID: 2 capital letters followed by 8 numbers
    VERSO_SMP_ID = re.compile(r'^[A-Z][A-Z]\d{8}')

    # if 11 digits, take first 9 digits, if Verso sample ID, take all; cardio suffix is ignored
    if VERSO_SMP_ID.match(sample_name):
        match_id = str(sample_name)[:10]
    else:
        match_id = str(sample_name)[:9]

    old_samples = session.query(Sample.name, Project.name).join(Project).filter(
        Sample.name.like(match_id + '%' + capturekit),
        Sample.name != sample_name,
        Sample.name.notilike("%Reanalyse%"),
        Project.name.notilike("%Reanalyse%")).all()

    return old_samples


def get_parent_for_reanalysis(session, fam_member, capturekit):
        """
        get parent sample of an old trio analysis for type 2 trio reanalysis
        Trio1 to Trio14 projects used a smaller capture kit, thus can not be used for reanalysis
        Trio15 and Trio16 used Av5 capture kit, thus can be used for reanalysis

        :session: the current SQLAlchemy session to the db
        :fam_member: 'Family number Diag' + "Relation Diag" + "Sex Diag", e.g. 172312FM, 172313MK
        :capturekit: matching capturekit, e.g. av5 or TWHCV1 for excap; wgs for WGS
        :returns:  [artifact_limsid, sample_name, project_name]

        TODO:
        If sample name no longer contains e.g. 172312FM, then use UDFs instead:
        e.g.
        #1. get sample name, project name, sample id for next query of Relation Diag
        session.query(
            Sample.sampleid,
            Sample.name,
            Project.name
            ).join(
                SampleUdfView,
                Project
            ).filter(
                    SampleUdfView.udfname == 'Family number Diag',
                    SampleUdfView.udfvalue == '333333',
                    Sample.name.notilike("%Reanalyse%"),
                    Project.name.notilike("%Reanalyse%")
            ).all()
        [(111111L, u'99999999991-333333MK-TrioMor-KIT-wgs', u'Diag-wgs71-2021-02-15'),
         (111112L, u'99999999992-333333PM-UtAv-KIT-wgs', u'Diag-wgs71-2021-02-15'),
         (111113L, u'99999999993-333333FM-TrioFar-KIT-wgs', u'Diag-wgs71-2021-02-15'),
         (111114L, u'99999999994-333333PM-UtAv-KIT-wgs', u'Diag-wgs84-2021-05-03')]

        #2. get "Relation Diag", e.g. 'P' for proband, 'F' for far, 'M' for Mor
        session.query(SampleUdfView.udfvalue).filter(
            SampleUdfView.udfname=='Relation Diag',
            SampleUdfView.sampleid==111111L).all()
        [u'M']
        """

        old_parent_candidates = session.query(
            Sample,
            Sample.name,
            Project.name,
            Project.createddate
        ).join(
            Project
        ).filter(
                Sample.name.ilike('%' + fam_member + '%' + capturekit),
                Sample.name.notilike("%Reanalyse%"),
                Project.name.notilike("%Reanalyse%"),
                Project.name.op("!~")(r'.*Trio[1-9]-.*'),
                Project.name.op('!~')(r'.*Trio1[0-4].*')
        ).all()

        if old_parent_candidates:
            # sort by project createdate, get the one whose project is newest
            old_parent = sorted(old_parent_candidates, key=itemgetter(3))[-1]
            feedback = [old_parent[0].artifact.luid] + list(old_parent[1:3])
            return feedback
        else:
            return None

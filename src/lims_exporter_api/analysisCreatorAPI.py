"""
API version of analysisCreator.py for lims-exporter-api
"""
import errno
import json
import logging
import os
import glob
from datetime import date
from dateutil import parser as dtparser

from lims_exporterAPI import (EXPORTER_DONE_FILE, DEBUG,
                              SPECIAL_TAG_HURTIGGENOM, DRAGEN_SUFFIX,
                              CUSTOM_GENEPANEL_NO_ANNOPIPE, PRIORITY_MAP)
import sampleCreatorAPI



class BaseAnalysisCreator(object):
    """
    doc
    """
    def __init__(self, repo_path, export_area, sample_name, sample_metadata, mark_ready=False):
        self.logger = logging.getLogger('lims_exporterAPI.analysisCreatorAPI')
        self.sample_name = sample_name
        self.metadata = sample_metadata
        self.base_repo_path = repo_path
        self.export_area = export_area
        self.requested_mark_ready = mark_ready
        self._b_mark_ready = None
        self._repo_path = None
        self._analysis_name = None
        self._base_analysis_file = None
        self._base_analysis_path = None
        self._genepanel = None
        self._genepanel_dash = None
        self._sample_name = None
        self._date_analysis_requested = None
        self._is_dragen = None
        self._base_name = None

    @property
    def base_exists(self):
        return os.path.exists(self._get_analysis_file(self.base_name))

    @property
    def base_name(self):
        if self._base_name is None:
            if self.is_dragen:
                self._base_name = '-'.join([self.sample_name, DRAGEN_SUFFIX])
            else:
                self._base_name = self.sample_name
        return self._base_name

    @property
    def mark_ready(self):
        """whether touch READY file """
        if not self._b_mark_ready:
            if self.export_area == 'Production':
                self._b_mark_ready = True
            else:
                self._b_mark_ready = self.requested_mark_ready

        return self._b_mark_ready

    @property
    def repo_path(self):
        """
        repo path according to export priorities
        """
        if not self._repo_path:
            if self.export_area == 'Transfer':
                export_priority = self.metadata['export_priority']
                subdir_priority = PRIORITY_MAP[export_priority]
                full_path = os.path.join(self.base_repo_path, subdir_priority, 'analyses-work')
            elif self.export_area == 'Production':
                full_path = os.path.join(self.base_repo_path, 'analyses-work')
            else:
                raise RuntimeError("Unknown export area {}".format(self.export_area))

            if os.path.exists(full_path):
                self._repo_path = full_path
            else:
                raise RuntimeError("no such path {}".format(full_path))

        return self._repo_path

    @property
    def base_analysis_file(self):
        if self._base_analysis_file is None:
            self._base_analysis_file = os.path.join(
                self._get_analysis_path(self.base_name),
                self.base_name + '.analysis'
            )
        return self._base_analysis_file

    @property
    def base_analysis_path(self):
        if self._base_analysis_path is None:
            self._base_analysis_path = self._get_analysis_path(self.base_name)
        return self._base_analysis_path

    @property
    def genepanel(self):
        if self._genepanel is None:
            self._genepanel = '_'.join([
                self.metadata['genepanel_name'],
                self.metadata['genepanel_version']
            ])
        return self._genepanel

    @property
    def genepanel_dash(self):
        if self._genepanel_dash is None:
            self._genepanel_dash = '-'.join([
                self.metadata['genepanel_name'],
                self.metadata['genepanel_version']
            ])
        return self._genepanel_dash

    @property
    def date_analysis_requested(self):
        if self._date_analysis_requested is None:
            d = self.metadata.get('date_analysis_requested')
            if isinstance(d, date):
                d = str(d)

            self._date_analysis_requested = d

        return self._date_analysis_requested

    @property
    def is_dragen(self):
        if self._is_dragen is None:
            if self.metadata.get('specialized_pipeline') == SPECIAL_TAG_HURTIGGENOM:
                self._is_dragen = True
            else:
                self._is_dragen = False

        return self._is_dragen

    def _get_analysis_path(self, foldername):
        """
        attach export analyis folder name with the path:
            /boston/diag/transfer/production/{priority}/analyses-work/Diag-excap01-123456789-EEogPU_v02
        """
        return os.path.join(self.repo_path, foldername)

    def _get_analysis_file(self, foldername):
        """
        .analysis file full path:
        /boston/diag/transfer/production/{priority}/analyses-work/Diag-excap01-123456789-EEogPU_v02/
            Diag-excap01-123456789-EEogPU_v02.analysis
        """
        return os.path.join(
            self._get_analysis_path(foldername),
            foldername + '.analysis'
        )

    def _mark_ready(self, foldername):  # create empty file "READY" under the path
        with open(os.path.join(self._get_analysis_path(foldername), 'READY'), 'w'):
            pass

    def create_base_analysis_data(self):
        """
        basepipe analysis metadata
        """
        base_analysis_data = {
            'type': 'basepipe',
            'name': self.base_name,
            'params': {
                'taqman': {True: "true", False: "false"}[self.metadata['uses_taqman']]
            },
            'samples': [self.sample_name],
            'priority': self.metadata.get('priority'),
            'date_analysis_requested': self.date_analysis_requested,
            'gender': self.metadata.get('genderF'),
            'specialized_pipeline': self.metadata.get('specialized_pipeline'),
            'platform': self.metadata.get('platform'),
            'export_priority': self.metadata.get('export_priority'),
        }
        return base_analysis_data

    def write_base_analysis(self, force=False):
        """
        write basepipe .analysis file
        """

        base_analysis_data = self.create_base_analysis_data()
        try:
            mode=0o775
            os.makedirs(self.base_analysis_path, mode=mode)
            os.chmod(self.base_analysis_path, mode | 0o4000 | 0o2000)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

        if not os.path.exists(self.base_analysis_file) or force:
            with open(self.base_analysis_file, 'w') as fd:
                if force and os.path.exists(self.base_analysis_file):
                    self.logger.info("Found existing base .analysis file for %s, but overwriting"
                                     " because force=True", self.base_name)
                json.dump(base_analysis_data, fd, indent=4)

        if self.mark_ready:
            self._mark_ready(base_analysis_data['name'])

        self.logger.info("Base analysis %s exported sucsessfully.",
                         base_analysis_data['name'])


class SingleAnalysisCreator(BaseAnalysisCreator):
    """
    single analyis
    """
    def __init__(self, repo_path, export_area, sample_name, sample_metadata, mark_ready=False):
        if 'pedigree' in sample_metadata:
            raise ValueError("Cannot create SingleAnalysisCreator object for sample with "
                             "pedigree data")
        BaseAnalysisCreator.__init__(self, repo_path, export_area, sample_name, sample_metadata,
                                     mark_ready)
        self._analysis_name = None
        self._single_analysis_file = None
        self._single_analysis_path = None
        self._trio_analysis_file = None
        self._trio_analysis_path = None

    @staticmethod
    def accept(metadata):
        """
        is single analysis vs trio?
        """
        return 'pedigree' not in metadata

    @property
    def single_analysis_file(self):
        if self._single_analysis_file is None:
            self._single_analysis_file = self._get_analysis_file(self.analysis_name)
        return self._single_analysis_file

    @property
    def single_analysis_path(self):
        if self._single_analysis_path is None:
            self._single_analysis_path = self._get_analysis_path(self.analysis_name)
        return self._single_analysis_path

    @property
    def analysis_name(self):
        if self._analysis_name is None:
            self._analysis_name = '-'.join([
                self.base_name,
                self.genepanel_dash
            ])
        return self._analysis_name

    @property
    def exists(self):
        """
        test whether the file is exist:
            /boston/diag/transfer/production/{priority}/analyses-work/Diag-excap01-123456789-EEogPU_v02/
                Diag-excap01-123456789-EEogPU_v02.analysis
        """
        return os.path.exists(self.single_analysis_file)

    def create_analysis_data(self):
        """
        write to .analyis file
        """
        analysis = {
            'type': 'annopipe',
            'samples': [
                self.sample_name
            ],
            'name': self.analysis_name,
            'params': {
                'genepanel': self.genepanel
            },
            'priority': self.metadata.get('priority'),
            'date_analysis_requested': self.date_analysis_requested,
            'gender': self.metadata.get('genderF'),
            'specialized_pipeline': self.metadata.get('specialized_pipeline'),
            'platform': self.metadata.get('platform'),
            'export_priority': self.metadata.get('export_priority'),

        }

        return analysis

    def write_to_file(self, force=False):
        """
        write sample and analysis files
        """

        self.write_base_analysis(force=force)

        # skip annopipe for Custom panel
        if self.metadata['genepanel_name'] not in CUSTOM_GENEPANEL_NO_ANNOPIPE:

            analysis = self.create_analysis_data()

            try:
                mode=0o775
                os.makedirs(self.single_analysis_path, mode=mode)
                os.chmod(self.single_analysis_path, mode | 0o4000 | 0o2000)
            except OSError as e:
                if e.errno != errno.EEXIST:
                    raise

            if not self.exists or force:
                if self.exists:
                    self.logger.info("Overwriting existing SingleAnalysis file for %s",
                                    self.analysis_name)
                with open(self.single_analysis_file, 'w') as fd:
                    json.dump(analysis, fd, indent=4)

            if self.mark_ready:
                self._mark_ready(self.analysis_name)

        self.logger.info("Sample {0} exported sucessfully.".format(self.sample_name))


class TrioAnalysisCreator(BaseAnalysisCreator):
    """
    regular trio analysis
    """
    def __init__(self, repo_path, export_area, sample_name, sample_metadata, sample_fam,
                 mark_ready=False):
        if 'pedigree' not in sample_metadata:
            raise ValueError("Cannot create TrioAnalysisCreator object for sample without "
                             "pedigree data")
        BaseAnalysisCreator.__init__(self, repo_path, export_area, sample_name, sample_metadata,
                                     mark_ready)
        self.sample_fam = sample_fam
        self._anno_name = None
        self._trio_name = None
        self._anno_analysis_file = None
        self._trio_analysis_file = None
        self._anno_analysis_path = None
        self._trio_analysis_path = None

    @staticmethod
    def accept(metadata):
        """
        is trio analysis?
        """
        return 'pedigree' in metadata

    @property
    def trio_name(self):
        if self._trio_name is None:
            if self.is_dragen:
                self._trio_name = '-'.join([self.sample_name, DRAGEN_SUFFIX, 'TRIO'])
            else:
                self._trio_name = '-'.join([self.sample_name, 'TRIO'])

        return self._trio_name

    @property
    def anno_name(self):
        if self._anno_name is None:
            if self.is_dragen:
                self._anno_name = '-'.join([self.sample_name, DRAGEN_SUFFIX,
                                            'TRIO',
                                            self.genepanel_dash])
            else:
                self._anno_name = '-'.join([self.sample_name,
                                            'TRIO',
                                            self.genepanel_dash])
        return self._anno_name

    @property
    def trio_analysis_file(self):
        if self._trio_analysis_file is None:
            self._trio_analysis_file = self._get_analysis_file(self.trio_name)
        return self._trio_analysis_file

    @property
    def trio_analysis_path(self):
        if self._trio_analysis_path is None:
            self._trio_analysis_path = self._get_analysis_path(self.trio_name)
        return self._trio_analysis_path

    @property
    def anno_analysis_file(self):
        if self._anno_analysis_file is None:
            self._anno_analysis_file = self._get_analysis_file(self.anno_name)
        return self._anno_analysis_file

    @property
    def anno_analysis_path(self):
        if self._anno_analysis_path is None:
            self._anno_analysis_path = self._get_analysis_path(self.anno_name)
        return self._anno_analysis_path

    @property
    def anno_exists(self):
        return os.path.exists(self.anno_analysis_file)

    @property
    def trio_exists(self):
        return os.path.exists(self.trio_analysis_file)

    @property
    def exists(self):
        """
        test whether the file is exist:
            /boston/diag/transfer/production/{priority}/analyses-work/
                Diag-excap01-123456789-EEogPU_v02/Diag-excap01-123456789-EEogPU_v02.analysis
        """
        return self.anno_exists and self.trio_exists

    def create_analysis_data(self):

        analysis_anno = {}
        analysis_trio = {}

        # Check that we have three samples and that we actually have one of each family type
        if len(self.sample_fam) == 3 and \
           all(f in self.sample_fam for f in ['proband', 'mother', 'father']):
            analysis_anno = {
                'params': {
                    'genepanel': self.genepanel
                },
                'type': 'annopipe',
                'priority': self.metadata.get('priority'),
                'date_analysis_requested': self.date_analysis_requested,
                'gender': self.metadata.get('genderF'),
                'specialized_pipeline': self.metadata.get('specialized_pipeline'),
                'platform': self.metadata.get('platform'),
                'export_priority': self.metadata.get('export_priority'),
            }

            analysis_anno['params']['pedigree'] = self.sample_fam
            analysis_anno['samples'] = []
            analysis_anno["name"] = self.anno_name

            for sample_fam_item in ['proband', 'father', 'mother']:
                sample_name = self.sample_fam[sample_fam_item]['sample']
                analysis_anno['samples'].append(sample_name)

            analysis_trio = {
                'type': 'triopipe',
                'params': {
                    'pedigree': analysis_anno['params']['pedigree'].copy()
                    },
                'name': self.trio_name,
                'priority': self.metadata.get('priority'),
                'samples': analysis_anno['samples'],
                'specialized_pipeline': self.metadata.get('specialized_pipeline'),
                'platform': self.metadata.get('platform'),
                'export_priority': self.metadata.get('export_priority'),
            }
        else:
            # send to manager review
            raise RuntimeError("Incomplete pedigree data on {0}".format(self.metadata['name']))

        return analysis_anno, analysis_trio

    def write_to_file(self, force=False):
        """
        write files
        """
        self.write_base_analysis()
        if 'proband' in self.metadata['pedigree']:

            analysis_anno, analysis_trio = self.create_analysis_data()

            # skip annopipe for Custom panel
            if self.metadata['genepanel_name'] not in CUSTOM_GENEPANEL_NO_ANNOPIPE:
                # anno file
                try:
                    mode=0o775
                    os.makedirs(self.anno_analysis_path, mode=mode)
                    os.chmod(self.anno_analysis_path, mode | 0o4000 | 0o2000)
                except OSError as e:
                    if e.errno != errno.EEXIST:
                        raise

                with open(self.anno_analysis_file, 'w') as fd:
                    json.dump(analysis_anno, fd, indent=4)

                if self.mark_ready:
                    self._mark_ready(self.anno_name)

            # trio analysis file
            try:
                mode=0o775
                os.makedirs(self.trio_analysis_path, mode=mode)
                os.chmod(self.trio_analysis_path, mode | 0o4000 | 0o2000)
            except OSError as e:
                if e.errno != errno.EEXIST:
                    raise

            with open(self.trio_analysis_file, 'w') as fd:
                json.dump(analysis_trio, fd, indent=4)

            if self.mark_ready:
                self._mark_ready(self.trio_name)

            self.logger.info("Wrote anno and trio analysis files for {0}".format(self.sample_name))

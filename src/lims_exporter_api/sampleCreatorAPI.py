"""
API version of sampleCreator.py for lims_exporterAPI
"""
import errno
import glob
import json
import logging
import os
from datetime import date

from fingerprinting.gettaqman import TaqmanParser
from lims_exporterAPI import (DONE_FILE, QUALITY_CONTROL_DIR, CAPTUREKIT_MAP,
                              FAM_MEMBERD, EXPORTER_DONE_FILE, DEBUG, PRIORITY_MAP)


class SampleCreator(object):
    """
    accumulate .sample metadata
    hardlink/copy sample files
    """

    def __init__(self, sample_metadata, sample_repo, export_area, taqman_source, mark_ready=False):
        self.logger = logging.getLogger('lims_exporterAPI.SampleCreator')
        self.taqman_source = taqman_source
        self.base_sample_repo_path = sample_repo
        if (not os.path.exists(self.base_sample_repo_path)
                and not os.path.isdir(self.base_sample_repo_path)):
            raise RuntimeError("{} doesn't exist or is not a directory."
                               "".format(self.base_sample_repo_path))

        self._sample_repo_path = None
        self.export_area = export_area
        self.requested_mark_ready = mark_ready
        self._b_mark_ready = None
        self.metadata = sample_metadata['sample']
        self.project_dir = sample_metadata['project_dir']
        self.sample_input_dir = sample_metadata['sample_dir']
        self._sample_name = None

    def get_taqman_file(self):
        """
        create taqman file name: Diag-excap01-123456789.taqman
        """
        return self.sample_name + '.taqman'

    @property
    def mark_ready(self):
        """whether touch READY file """
        if not self._b_mark_ready:
            if self.export_area == 'Production':
                self._b_mark_ready = True
            else:
                self._b_mark_ready = self.requested_mark_ready

        return self._b_mark_ready

    @property
    def sample_repo_path(self):
        """
        sample repo path according to export priorities
        """
        if not self._sample_repo_path:
            if self.export_area == 'Transfer':
                export_priority = self.metadata['export_priority']
                subdir_priority = PRIORITY_MAP[export_priority]
                full_path = os.path.join(self.base_sample_repo_path, subdir_priority, 'samples')
            elif self.export_area == 'Production':
                full_path = os.path.join(self.base_sample_repo_path, 'samples')
            else:
                raise RuntimeError("Unknown export area {}".format(self.export_area))

            if os.path.exists(full_path):
                self._sample_repo_path = full_path
            else:
                raise RuntimeError("no such path {}".format(full_path))

        return self._sample_repo_path

    @property
    def sample_name(self):  # Diag-excap01-123456789
        """
        sample name as sample folder name in TSD
        project + sample id
        """
        if self._sample_name is None:
            # Reanalyse
            if self.metadata['Reanalyse']:  # get old name
                # Single or Trio
                if self.metadata.get('ReanaOldPedigree'):  # old trio
                    self.logger.debug("Reanalysis of original trio member.")
                    self._sample_name = '-'.join([
                        self.metadata['ReanaOldProject'],
                        self.metadata['ReanaOldID'],
                        ''.join([
                            self.metadata['ReanaOldFam_member'],
                            self.metadata['ReanaOldGender']]
                        )
                    ])
                else:  # old single
                    self.logger.debug("Reanalysis of original single sample.")
                    self._sample_name = self.metadata['ReanaOldProject'] + '-' + \
                        self.metadata['ReanaOldID']
            # New
            else:
                sample_name = self.metadata['project'] + '-' + self.metadata['sample_id']
                # Trio
                if self.metadata.get('pedigree'):
                    self._sample_name = '-'.join([
                        sample_name,
                        ''.join([
                            self.metadata['fam_member'],
                            self.metadata['gender']])
                        ])
                # Single
                else:
                    self._sample_name = sample_name

        return self._sample_name

    def get_sample_path(self):
        """
        get export sample directory
        e.g. /boston/diag/transfer/production/samples/Diag-excap01-123456789
        """
        return os.path.join(self.sample_repo_path, self.sample_name)

    def exists(self):
        """
        Checks sample_repo_path if a sample is already processed and can be skipped.
        e.g. /boston/diag/transfer/production/samples/Diag-excap01-123456789/LIMS_EXPORT_DONE

        This is different from checking source_path
        e.g. /boston/diag/nscDelivery/<seqRunDir>/[<sampleDir>]/<sample_name>.LIMS_EXPORT_PROCESSED
        """
        return os.path.exists(os.path.join(self.get_sample_path(), DONE_FILE))

    def _create_taqman(self):
        """
        create .taqman file
        e.g.
        /boston/diag/transfer/production/samples/Diag-excap01-123456789/Diag-excap01-123456789.taqman
        """
        taqman_output = os.path.join(self.get_sample_path(),
                                     self.get_taqman_file())
        TaqmanParser(self.taqman_source).create(self.metadata['sample_id'], taqman_output)

    def _copy_files(self):
        """
        hardlink sample files:
        - Fastq files
        - NSC QC report (pdf file)
        - FASTQC files
        """
        sample_path = self.get_sample_path()  # repo/hts/samples/Diag-excap01-123456789
        if not os.path.exists(sample_path):
            try:
                mode = 0o775
                os.makedirs(sample_path, mode=mode)
                os.chmod(sample_path, mode | 0o4000 | 0o2000)
            except OSError as ex:
                if ex.errno == errno.EEXIST and os.path.isdir(sample_path):
                    pass
                else:
                    self.logger.error("Cannot create directory: %s", sample_path)
                    raise

        self.logger.info("Copying files for %s", self.sample_input_dir)

        # Fastq files
        for fastq in self.metadata['reads']:
            try:
                src_rd = os.path.join(self.sample_input_dir, fastq['path'])
                dst_rd = os.path.join(sample_path, fastq['path'])
                self.logger.debug("hardlinking read file:\n\t%s as\n\t%s", src_rd, dst_rd)
                os.link(src_rd, dst_rd)
            except OSError as ex:
                if ex.errno == errno.EEXIST:
                    pass
                else:
                    raise

        # NSC QC report (pdf file)
        for qc_report in self.metadata['qc_reports']:
            try:
                src_qc = os.path.join(self.sample_input_dir, qc_report)
                dst_qc = os.path.join(sample_path, qc_report)
                self.logger.debug("hardlinking qc_report:\n\t%s as\n\t%s", src_qc, dst_qc)
                os.link(src_qc, dst_qc)
            except OSError as ex:
                if ex.errno == errno.EEXIST:
                    pass
                else:
                    raise

        # FASTQC quality report
        for fastqc_path in self.metadata['fastqc_paths']:
            abs_path = os.path.join(self.metadata['path_of_project_dir'], QUALITY_CONTROL_DIR,
                                    '_'.join(['Sample', self.metadata['name']]), fastqc_path)

            self.logger.debug("rsyncing fastqc files from :\n\t%s to\n\t%s", abs_path, sample_path)
            os.system("rsync -a --no-g {} {}".format(abs_path, sample_path))
            # package the fastqc folder into a tar file with the same name
            abs_path_to_tar=os.path.join(sample_path, fastqc_path)
            os.system("tar --remove-files -C {} -cf {}.tar {}".format(sample_path, abs_path_to_tar, fastqc_path))

    def _create_metadata(self):
        sample_keys = [
            'sample_id',      # 123456789
            'lane',           # 7
            'index',          # GCCAAT
            'project',        # Diag-excap01
            'project_date',   # 2015-10-08
            'flowcell',       # A
            'flowcell_id',    # C80NKANXX
            'sequence_date',  # 2015-10-30
            'sequencer_id',   # 7001448
            'sequencer_type', # Hiseq 3000
            'stats',          # one_mismatch_index_pct: 0.94, reads: 115792216,
                              # mean_qual_score: 36.44, q30_bases_pct: 94.15,
                              # perfect_index_reads_pct: 99.06
            'priority',
            'date_analysis_requested',
            'gender',
            'specialized_pipeline',
            'TapeStation_average_fragment_size',
            'qPCR_molarity',
            'cluster_density_R1',
            'cluster_density_R2',
            'ratio_aligned_R1',
            'ratio_aligned_R2',
            'ratio_above_Q30_R1',
            'ratio_above_Q30_R2',
            'ratio_PF_R1',
            'ratio_PF_R2',
            'ratio_duplicates',
            'platform',
            'export_priority',
        ]
        sample = dict()
        for key in sample_keys:
            v = self.metadata.get(key)

            # "male"/"female" instead of "M"/"F" in .sample file
            if key == 'gender':
                v = self.metadata.get('genderF')

            # datetime to str
            if isinstance(v, date):
                v = str(v)

            sample[key] = v

        if 'fam_member' in self.metadata and 'pedigree' in self.metadata:
            self.metadata['pedigree'][FAM_MEMBERD[self.metadata['fam_member']]].update(
                {'sample': self.sample_name})
            sample['pedigree'] = self.metadata['pedigree']
        sample['name'] = self.sample_name  # Diag-excap01-123456789

        sample['capturekit'] = CAPTUREKIT_MAP.get(
            self.metadata['capturekit'], self.metadata['capturekit'])  # agilent_sureselect_v05

        sample['reads'] = self.metadata['reads']
        # foreach read:
        # path: 123456789-EEogPU-v02-KIT-Av5_GCCAAT_L007_R1_001.fastq.gz,
        # md5: a5b2ce03ba606dbddcb9e93dcfa892ca,
        # size: 5073078524

        if self.metadata['uses_taqman']:
            sample['taqman'] = self.get_taqman_file()  # Diag-excap01-123456789

        with open(os.path.join(self.get_sample_path(),
                               self.sample_name + '.sample'), 'w') as fd:
            json.dump(sample, fd, indent=4)

    def _mark_done(self):
        with open(os.path.join(self.get_sample_path(), DONE_FILE), 'w'):
            pass

    def _mark_ready(self):
        with open(os.path.join(self.get_sample_path(), 'READY'), 'w'):
            pass

    def create(self, mark_processed=False, force=False):
        """
        - hardlink sample files
        - create .taqman file
        - create .sample files
        - touch 'LIMS_EXPORT_DONE' file
        - [touch 'READY' file]
        """
        # need something to check for trios before this
        if self.metadata.get('Reanalyse'):
            self.logger.warning("Skipping sample file creation for reanalysis sample %s",
                                self.sample_name)
            return

        if not self.exists() or force:
            if force and self.exists():
                self.logger.info("Overwriting existing sample file for %s",
                                 self.sample_name)
            self._copy_files()

            self._create_metadata()
            self._mark_done()
            if self.mark_ready:
                self._mark_ready()
            if mark_processed and not DEBUG:
                pfile = os.path.join(
                    self.sample_input_dir,
                    '.'.join([
                        self.metadata['name'],
                        EXPORTER_DONE_FILE
                    ])
                )
                open(pfile, 'w').close()
        else:
            self.logger.warning("Skipping sample file creation for sample %s: already exists",
                                self.sample_name)

        # always overwrite taqman file
        if self.metadata['uses_taqman']:
            self._create_taqman()

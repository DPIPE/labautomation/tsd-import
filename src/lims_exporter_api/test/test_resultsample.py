#!/usr/bin/env python

"""
test ResultSample class
"""
import pytest
from sampleWatcherAPI import ResultSample


def test_add_pedigree():
    """
    test ResultSample.add_pedigree()
    """
    smp = ResultSample('smp1', '/tmp')
    smp.metadata['fam_member'] = 'P'
    smp.metadata['gender'] = 'M'

    # "Joint Analyis Diag" metadata is 'trio'
    smp.metadata['joint_analysis_type'] = 'trio'
    smp.add_pedigree()
    assert smp.metadata['pedigree'] == {'proband': {'gender': 'male'}}

    # "Joint Analyis Diag" metadata is 'Trio'
    # case insensitivity
    smp.metadata['joint_analysis_type'] = 'Trio'
    smp.add_pedigree()
    assert smp.metadata['pedigree'] == {'proband': {'gender': 'male'}}

    # "Joint Analyis Diag" metadata is 'sinlge'
    # non-trio sample has no pedigree metadata
    smp.metadata['joint_analysis_type'] = 'single'
    smp.metadata.pop('pedigree', None)
    smp.add_pedigree()
    with pytest.raises(KeyError):
        print(smp.metadata['pedigree'])

    # "Joint Analyis Diag" metadata is 'sinlge'
    # non-trio sample has no pedigree metadata
    # case insensitivity
    smp.metadata['joint_analysis_type'] = 'Single'
    smp.metadata.pop('pedigree', None)
    smp.add_pedigree()
    with pytest.raises(KeyError):
        print(smp.metadata['pedigree'])

    # wrong "Joint Analyis Diag" metadata
    smp.metadata['joint_analysis_type'] = 'exome'
    smp.metadata.pop('pedigree', None)
    smp.add_pedigree()
    with pytest.raises(KeyError):
        print(smp.metadata['pedigree'])

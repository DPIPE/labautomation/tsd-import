#!/usr/bin/env python
"""
sampling samples for testing
"""

import random
import json
from sampleWatcherAPI import lims, LimsQueue
from lims_exporterAPI import get_latest_panel_versions

PROJTYPES = ['excap', 'excap-trio', 'EHG', 'EKG', 'wgs', 'reanalyses']
# choose a finished project for each project type
CHOSEN_PROJECTS = ['Diag-excap98-2017-27-09']
# randomly choose N samples from
SAMPLING_SIZE = 5
# Testing source path
SOURCE = 'test_source_path'
# panel bundle path
PANELS = '/boston/diag/production/sw/vcpipe/vcpipe-bundle/clinicalGenePanels'

class MimicLimsQueue(LimsQueue):
    """overwrite __init__, feed in projects directly"""

    def __init__(self, projects, source_path, latest_panel_versions):
        """TODO: to be defined1.

        :queueid: TODO
        :projects: comma separated list of project names

        """
        self.project_names = projects
        self.processes = lims.get_processes(projectname=self.project_names)
        self.sample_names = self._get_sample_names()
        self.result_samples = list()
        self._target_samples = list()
        self.source_path = source_path
        self.latest_panel_versions = latest_panel_versions

    def _get_sample_names(self):
        result = []
        for proc in self.processes:
            if proc.type_name == 'Project Evaluation Step':
                for a in proc.all_inputs():
                    if a.name not in result:
                        result.append(a.name)
                break
        else:
            raise RuntimeError('not evaluation step')
        return result

latest_ver = get_latest_panel_versions(PANELS)

for i in range(len(CHOSEN_PROJECTS)):
    limsq = MimicLimsQueue(CHOSEN_PROJECTS, SOURCE, latest_ver)
    proj_type = PROJTYPES[i]

    # get metadata from API
    limsq.initial_query()
    
    import pdb; pdb.set_trace()
    limsq.secondary_parse()

    # result samples
    all_samples = limsq.result_samples
    
    # randomly choose N samples
    chosen_samples = random.sample(all_samples, SAMPLING_SIZE)

    for i, sample in enumerate(chosen_samples):
        outfile = proj_type + '_smp_' + str(i)
        with open('testdata/feeder/'+outfile, 'w') as out:
            json.dump(sample.__dict__, out, indent=4)

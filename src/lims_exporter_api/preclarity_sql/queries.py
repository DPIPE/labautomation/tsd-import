"""
utility functions
"""
from preclarity_sql.tables import Sample


def get_reanalysis_old_sample_name(session, sample_name):
    """TODO: Docstring for get_reanalysis_old_sample_name.

    :session: the current SQLAlchemy session to the db
    :sample_name: new sample id with a different last 2 digits
    :returns: List of Tuple of sample name and project name where sample names matches first 9
              digits of sample_name

    """
    first9digits = str(sample_name)[:9]

    old_samples = session.query(Sample.name, Sample.project).filter(
        Sample.name.like('%' + first9digits + '%'),
        Sample.name != sample_name,
        Sample.name.notilike("%Reanalyse%")).all()

    return old_samples

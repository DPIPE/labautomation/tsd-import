"""
ORM
"""
from sqlalchemy import Column, String
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


# Standard tables
class Sample(Base):
    """
    Table mapping the samples

    :arg STRING name: the sample name.
    :arg STRING project: the sample's project

    """
    __tablename__ = 'pre_clarity_samples'
    name = Column(String, primary_key=True)
    project = Column(String, primary_key=True)

    def __repr__(self):
        return "<Sample(name={}, project={})>".format(self.name, self.project)

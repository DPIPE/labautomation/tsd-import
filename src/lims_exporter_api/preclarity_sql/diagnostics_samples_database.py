#!/usr/bin/env python

"""
Generatea database of all samples analyzed before Clarity LIMS in use

This database is used for looking up "reanalysis old sample ID" and "original old project" for
reanalysis samples by matching the first 9 digits of numerical sample id.

Each sample in the list should have been exported to TSD

sqlite3 database tables:
    pre_clarity_samples (imported from a plain text file, no update needed)

"""

import re
import argparse
import logging
import sqlite3
import csv


class PreClaritySampleDB(object):
    """
    DB of PreClarity Samples in TSD
    """

    def __init__(self, dbfile, pre_clarity_sample_list):
        """TODO: Docstring for __init__.

        :dbfile: sqlite3 db file, e.g. pre_clarity_samples.db
        :returns: TODO

        """
        self.dbfile = dbfile
        self.pre_clarity_sample_list = pre_clarity_sample_list

        self.conn = sqlite3.connect(dbfile, detect_types=sqlite3.PARSE_DECLTYPES)
        self.curs = self.conn.cursor()
        self._create_tables()

    def _create_pre_clarity_samples_table(self):
        """
        create sqlite3 pre_clarity_samples table
        """
        cmd = '''create table if not exists pre_clarity_samples(name text not null,
                                                                project text not null)'''
        self.curs.execute(cmd)
        self.conn.commit()

    def _create_tables(self):
        """TODO: Docstring for _create_tables.
        :returns: TODO

        """
        self._create_pre_clarity_samples_table()

    def populate_table(self):
        """
        add entries to table
        """
        with open(self.pre_clarity_sample_list, 'rb') as inf:
            ents = csv.DictReader(inf, fieldnames=('name', 'project'), delimiter='\t')
            to_db = [(smp['name'], smp['project']) for smp in ents]

        self.curs.executemany("insert into pre_clarity_samples(name, project) values(?,?);", to_db)
        self.conn.commit()

    def close_connection(self):
        """
        close connection
        """
        self.conn.close()


def main():
    """
    create pre_clarity_samples db, add entries from csv file
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--dbfile', required=True,
                        help="sqlite3 db file for diagnostic sample database")
    parser.add_argument('-f', '--sample-file', dest='pre_clarity_samples', required=True,
                        help="pre clarity sample list csv file")

    args = parser.parse_args()

    dbfile = args.dbfile
    pre_clarity_sample_list = args.pre_clarity_samples

    qsmps = PreClaritySampleDB(dbfile, pre_clarity_sample_list)
    qsmps.populate_table()
    qsmps.close_connection()


if __name__ == '__main__':
    exit(main())

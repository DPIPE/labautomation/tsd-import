"""
utility functions
"""

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from preclarity_sql.tables import Base

# hard coded path to sqlite db file of pre clarity samples
DBFILE = '/boston/diag/transfer/sw/pre_clarity_samples.db'


def get_engine():
    """generates a SQLAlchemy engine for sqlite
    :returns: the SQLAlchemy engine"""
    uri = None
    try:
        uri = "sqlite:////{abspath}".format(abspath=DBFILE)
    except KeyError:
        raise Exception("sqlite db file {} not exists".format(DBFILE))
    return create_engine(uri)


def get_session():
    """Generates a SQLAlchemy session based on the CONF
    :returns: the SQLAlchemy session
    """
    engine = get_engine()
    Base.metadata.bind = engine
    DBSession = sessionmaker(bind=engine)
    session = DBSession()
    return session

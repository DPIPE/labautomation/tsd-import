"""
- Sample watcher using LIMS API, instead of walking the nscDelivery folder
- Samples ready to be exported are queued in "Lims Exporter" step of "Tolkning av HTS-data Diag"
Protocol, no need to touch READY files
- Get all metadata from LIMS API (Entity attributes or UDFs), instead of parsing delivery
folder/file names or parsing Demultiplexing.htm file
- md5sum is read from file "md5sum.txt" to make sure files untouched after NSC delivery
"""
import os
import re
import logging
import collections
import pprint
import glob
import json
import subprocess
from datetime import datetime
from dateutil import parser as dtparser
from collections import defaultdict

from lims_exporterAPI import (EXPORTER_DONE_FILE, FAM_MEMBERD, GENDERD, MD5SUM_NAME, FALLBACK,
                              DEBUG, QUALITY_CONTROL_DIR, CUSTOM_GENEPANEL_NO_ANNOPIPE,
                              TYPE1_REANA_PROJ_RE, CAPTUREKIT_MAP)

from genologics.entities import Process as GlsProcess, Queue
from genologics.lims import *
from genologics.config import BASEURI, USERNAME, PASSWORD

import genologics_sql.utils
from genologics_sql.tables import *
from genologics_sql.queries import *
from genologics_sql.queries import get_parent_for_reanalysis
session = genologics_sql.utils.get_session()


import preclarity_sql.utils
from preclarity_sql.tables import Sample as PreClaritySample
from preclarity_sql.queries import get_reanalysis_old_sample_name as get_preclarity_old_sample_name
session_sqlite = preclarity_sql.utils.get_session()

from fingerprinting.gettaqman import TaqmanParser

FASTQ_COMPRESSION_EXTENSION = {
    "dragen": ".ora",
    "gzip": ".gz",
}

PROJ_MATCHER = re.compile(
    r'^Diag-(?P<proj_type>\w+?)(?P<proj_number>\d+)'
    r'(-(?P<project_date>\d+-\d+-\d+))?$', re.I
)

SAMPLE_NAME_MATCHER = re.compile(
    r'^(?P<sample_id>\w+?)'
    r'(-reanalyse)?'
    r'(-(?P<famid>[0-9]+)'
    r'(?P<fam_member>[PMF])+(?P<gender>[KM]+))?'
    r'-(?P<genepanel_name>\w+?)'
    r'(-(?P<genepanel_version>[vV]\w+?))?'
    r'(-KIT-(?P<capturekit>\w+))?$'
)

LAB_QC_CLUSTER_GEN = {
    'UDFs': [
        'Cluster Density (K/mm^2) R1',
        'Cluster Density (K/mm^2) R2',
        '% Aligned R1',
        '% Aligned R2',
        '% Bases >=Q30 R1',
        '% Bases >=Q30 R2',
        '%PF R1',
        '%PF R2',
        '% Sequencing Duplicates',
    ],
    'UDFs_rename': {
        'Cluster Density (K/mm^2) R1': 'cluster_density_R1',
        'Cluster Density (K/mm^2) R2': 'cluster_density_R2',
        '% Aligned R1': 'ratio_aligned_R1',
        '% Aligned R2': 'ratio_aligned_R2',
        '% Bases >=Q30 R1': 'ratio_above_Q30_R1',
        '% Bases >=Q30 R2': 'ratio_above_Q30_R2',
        '%PF R1': 'ratio_PF_R1',
        '%PF R2': 'ratio_PF_R2',
        '% Sequencing Duplicates': 'ratio_duplicates',
    }
}

METADATA = {
    'Sample' :  {  # 1 Sample {{{1
        'attrs': [
            'name',
            'date_received',
        ],
        'attrs_rename': {
            'date_received': 'date_clarity'
        },
        'UDFs' : [
            'Gene panel Diag',
            'Panel version Diag',  # non exist means latest should be used
            'Kit version Diag',                 # Kit name and Kit version together
            'Family number Diag',
            'Relation Diag',
            'Sex Diag',
            'Analysis type Diag',
            'Prioritet',
            'Enhet received Diag',
            'Reanalysis old sample ID Diag',
            'Index requested/used',
            'Specialized pipeline Diag',
            'Test SWL Diag',
            'Check TaqMan Diag',
        ],
        'UDFs_rename': {
            'Gene panel Diag'    : 'genepanel_name',
            'Panel version Diag' : 'genepanel_version',
            'Kit version Diag'                : 'capturekit',
            'Family number Diag'      : 'famid',
            'Relation Diag'           : 'fam_member',
            'Sex Diag'                : 'gender',
            'Analysis type Diag'      : 'joint_analysis_type',
            'Prioritet'               : 'priority',
            'Enhet received Diag'     : 'date_analysis_requested',
            'Reanalysis old sample ID Diag' :  'reana_oldid',
            'Index requested/used'          :  'requested_index',
            'Specialized pipeline Diag'     :  'specialized_pipeline',
            'Test SWL Diag': 'swl_test',
            'Check TaqMan Diag': 'taqman_lab'
        }
    },
    'Project': {  # 2 Project metadata {{{1
        'attrs': ['name'],
        'attrs_rename': {'name': 'projectname_with_date'},
        # ['HiSeq X', 'HiSeq high output', 'MiSeq', 'NextSeq high output']
        'UDFs' : ['Sequencing instrument requested'],
        'UDFs_rename': {'sequencing_instrument_requested'}
    },
    'Process': {  # 3 Processe metadata {{{1
        # 3.0 template: supported genologics entities {{{2
        # 'process_name': {
        #     'self': {
        #         'attrs': [],
        #         'attrs_rename': dict(),
        #         'UDFs': [],
        #         'UDFs_rename': dict()
        #      },
        #     'input_Analyte': {
        #         'attrs': [],
        #         'attrs_rename': dict(),
        #         'UDFs': [],
        #         'UDFs_rename': dict()
        #     },
        #     'output_Analyte': {
        #         'attrs': [],
        #         'attrs_rename': dict(),
        #         'UDFs': [],
        #         'UDFs_rename': dict()
        #     },
        #     'output_ResultFile': {
        #         'attrs': [],
        #         'attrs_rename': dict(),
        #         'UDFs': [],
        #         'UDFs_rename': dict()
        #     }
        # }

        # 3.1 sequencing_date, sequencer_id, flowcell, flowcell_id {{{2
        # 3.1.1 HiSeq 3000/4000 {{{3
        'Illumina Sequencing (HiSeq 3000/4000)'   : {
            'self': {
                'attrs': [],
                'attrs_rename': dict(),
                'UDFs': ['Run ID',
                         'Flow Cell Position',
                         'Flow Cell ID'],
                'UDFs_rename': {'Run ID'             : 'sequencing_run_id',
                                'Flow Cell Position' : 'flowcell',
                                'Flow Cell ID'       : 'flowcell_id'},
            }
        },
        # 3.1.2(old) MiSeq => Targeted: EKG, EHG {{{3
        'MiSeq Run (MiSeq)'                    : {
            'self': {
                'attrs': [],
                'attrs_rename': dict(),
                'UDFs': ['Run ID',
                         'Flow Cell ID'],
                'UDFs_rename': {'Run ID'             : 'sequencing_run_id',
                                'Flow Cell ID'       : 'flowcell_id'},
            }
        },
        # 3.1.2(new) MiSeq => EHG {{{3
        'MiSeq Run'                            : {
            'self': {
                'attrs': [],
                'attrs_rename': dict(),
                'UDFs': ['Run ID',
                         'Flow Cell ID'],
                'UDFs_rename': {'Run ID'             : 'sequencing_run_id',
                                'Flow Cell ID'       : 'flowcell_id'},
            }
        },
        # 3.1.3 NextSeq {{{3
        'NextSeq Run (NextSeq)'                : {
            'self': {
                'attrs': [],
                'attrs_rename': dict(),
                'UDFs': ['Run ID',
                         'Flow Cell ID'],
                'UDFs_rename': {'Run ID'             : 'sequencing_run_id',
                                'Flow Cell ID'       : 'flowcell_id'},
            }
        },
        # 3.1.4 NextSeq(new-NextSeq-workflow) {{{3
        'NextSeq 500/550 Run'                : {
            'self': {
                'attrs': [],
                'attrs_rename': dict(),
                'UDFs': ['Run ID',
                         'Flow Cell ID'],
                'UDFs_rename': {'Run ID'             : 'sequencing_run_id',
                                'Flow Cell ID'       : 'flowcell_id'},
            }
        },
        # 3.1.5 HiSeq X => wgs {{{3
        'Illumina Sequencing (HiSeq X)'   : {
            'self': {
                'attrs': [],
                'attrs_rename': dict(),
                'UDFs': ['Run ID',
                         'Flow Cell Position',
                         'Flow Cell ID'],
                'UDFs_rename': {'Run ID'             : 'sequencing_run_id',
                                'Flow Cell Position' : 'flowcell',
                                'Flow Cell ID'       : 'flowcell_id'},
            }
        },
        # 3.1.6 HiSeq 2500 => excap {{{3
        'Illumina Sequencing (Illumina SBS)'   : {
            'self': {
                'attrs': [],
                'attrs_rename': dict(),
                'UDFs': ['Run ID',
                         'Flow Cell Position',
                         'Flow Cell ID'],
                'UDFs_rename': {'Run ID'             : 'sequencing_run_id',
                                'Flow Cell Position' : 'flowcell',
                                'Flow Cell ID'       : 'flowcell_id'},
            }
        },

        # 3.1.7 Novaseq => examp, wgs {{{3
        'AUTOMATED - NovaSeq Run (NovaSeq 6000 v3.0)'   : {
            'self': {
                'attrs': [],
                'attrs_rename': dict(),
                'UDFs': ['Run ID',
                         'Flow Cell Side',
                         'Flow Cell ID'],
                'UDFs_rename': {'Run ID'             : 'sequencing_run_id',
                                'Flow Cell Side'     : 'flowcell',
                                'Flow Cell ID'       : 'flowcell_id'},
            }
        },

        # 3.1.8 Novaseq( new worflow) {{{3
        'AUTOMATED - NovaSeq Run'   : {
            'self': {
                'attrs': [],
                'attrs_rename': dict(),
                'UDFs': ['Run ID',
                         'Flow Cell Side',
                         'Flow Cell ID'],
                'UDFs_rename': {'Run ID'             : 'sequencing_run_id',
                                'Flow Cell Side'     : 'flowcell',
                                'Flow Cell ID'       : 'flowcell_id'},
            }
        },
        
         # 3.1. Novaseq X {{{3
        'AUTOMATED - Sequencing Run NovaSeqX'   : {
            'self': {
                'attrs': [],
                'attrs_rename': dict(),
                'UDFs': ['Run ID',
                         'Run Name', # Diag-wgsNovaseqXtraining-2024-05-16
                         'Flow Cell Side', # 'A', 'B'
                         'Output Folder', # //boston.nscamg.local/runScratch/NovaSeqX/20240516_LH00534_0020_A223JT3LT4
                         'Flow Cell ID'],
                'UDFs_rename': {'Run ID'             : 'sequencing_run_id',
                                'Run Name'           : 'sequencing_run_name',
                                'Output Folder'      : 'sequencing_run_output_folder',
                                'Flow Cell Side'     : 'flowcell',
                                'Flow Cell ID'       : 'flowcell_id'},
            }
        },

        # 3.2 index # {{{2
        # 3.2.1 Novaseq 6000 & X => wgs {{{3
        'Adenylate ends & Ligate Adapters (TruSeq DNA)': {
            'output_Analyte': {
                'attrs'        : ['reagent_labels'],  # a SET of all reagent_labels in xml
                'attrs_rename' : dict(),
                'UDFs'         : [],
                'UDFs_rename'  : dict()
            }
        },
        # 3.2.2 Excap {{{3
        'Amplify Captured Libraries to add index tags': {
            'output_Analyte': {
                'attrs'        : ['reagent_labels'],  # a SET of all reagent_labels in xml
                'attrs_rename' : dict(),
                'UDFs'         : [],
                'UDFs_rename'  : dict()
            }
        },
        # 3.2.3  Targeted a) => protocol Diag_Nextera Hamilton (EKG) {{{3
        'Step 1: Tagmentation, Clean-up, and PCR' : {
            'output_Analyte': {
                # <reagent-label name="48 N712-E505 (GTAGAGGA-GTAAGGAG)"/>
                'attrs'        : ['reagent_labels'],  # a SET of all reagent_labels in xml
                'attrs_rename' : dict(),
                'UDFs'         : [],
                'UDFs_rename'  : dict()
            }
        },
        # 3.2.4 Targeted b) protocol: Diag_Nextera Rapid Capture (EHG) {{{3
        'First PCR Clean-up (Nextera Rapid Capture)': {
            'output_Analyte': {
                # <reagent-label name="48 N712-E505 (GTAGAGGA-GTAAGGAG)"/>
                'attrs'        : ['reagent_labels'],  # a SET of all reagent_labels in xml
                'attrs_rename' : dict(),
                'UDFs'         : [],
                'UDFs_rename'  : dict()
            }
        },
        # 3.2.5 Target c) protocol: EKG New {{{3
        'Tagment Genomic DNA, Clean-up and Amplify (Nextera Flex)': {
            'output_Analyte': {
                'attrs'        : ['reagent_labels'],  # a SET of all reagent_labels in xml
                'attrs_rename' : dict(),
                'UDFs'         : [],
                'UDFs_rename'  : dict()
            }
        },
        # 3.2.6 Trio {{{3
        #    NextSeq: 'Diag_Nextera Rapid Capture' - 'Tagment DNA and clean-up Tagmented DNA'
        #    Hiseq: same as Exome

        # # 3.2.7 automation set reagent-label from sample sheet UDF "Index requested/used" 
        # "Project Evaluation Step": {
        #     "input_Analyte": {
        #         "attrs": ["reagent_labels"],  # a SET of all reagent_labels in xml
        #         "attrs_rename": dict(),
        #         "UDFs": [],
        #         "UDFs_rename": dict(),
        #     }
        # },
        # 3.3 lane, cluster density, % Aligned, % Bases >= Q30, %PF {{{2
        # 3.3.1   Excap {{{3
        'Cluster Generation (HiSeq 3000/4000)': {
            'output_Analyte': {
                'attrs': ['location'],
                'attrs_rename': {
                    'location': 'flowcell_lane_coord',
                },
                'UDFs':  LAB_QC_CLUSTER_GEN['UDFs'],
                'UDFs_rename': LAB_QC_CLUSTER_GEN['UDFs_rename']
            }
        },
        # 3.3.2 Excap {{{3
        'Cluster Generation (Illumina SBS)'            : {
            'output_Analyte': {
                'attrs'        : ['location'],
                'attrs_rename' : {'location'   : 'flowcell_lane_coord'},
                'UDFs':  LAB_QC_CLUSTER_GEN['UDFs'],
                'UDFs_rename': LAB_QC_CLUSTER_GEN['UDFs_rename']
            }
        },
        # 3.3.3 Excap {{{3
        'Load to Flowcell (NovaSeq 6000 v3.0)'            : {
            'output_Analyte': {
                'attrs'        : ['location'],
                'attrs_rename' : {'location'   : 'flowcell_lane_coord'},
                'UDFs'         : [],
                'UDFs_rename'  : dict()
            }
        },
        # NovaSeqX
        'Load to Library Tube Strip NovaSeqX'            : {
            'self': {
                'attrs': [],
                'attrs_rename': dict(),
                'UDFs': ['FASTQ Compression Format'],
                'UDFs_rename': {
                    'FASTQ Compression Format': 'fastq_compression_format'
                },
            },
            'output_Analyte': {
                'attrs'        : ['location'],
                'attrs_rename' : {'location'   : 'flowcell_lane_coord'},
                'UDFs'         : [],
                'UDFs_rename'  : dict()
            }
        },
        # 3.3.4 Target {{{3
        #   lane: Miseq has only one lane, so lane === 1
        #   cluster density, % Aligned, % Bases >= Q30, %PF:
        'Denature, Dilute and Load Sample (MiSeq)': {
            'output_Analyte': {
                'attrs': [],
                'attrs_rename' : dict(),
                'UDFs':  LAB_QC_CLUSTER_GEN['UDFs'],
                'UDFs_rename': LAB_QC_CLUSTER_GEN['UDFs_rename']
            }
        },

        # 3.3.5 Trio {{{3
        #   NextSeq: lane === 'X'
        #   cluster density, % Aligned, % Bases >= Q30, %PF:
        'Denature, Dilute and Load Sample (NextSeq)': {
            'output_Analyte': {
                'attrs': [],
                'attrs_rename' : dict(),
                'UDFs':  LAB_QC_CLUSTER_GEN['UDFs'],
                'UDFs_rename': LAB_QC_CLUSTER_GEN['UDFs_rename']
            }
        },
        # 3.3.6   Hiseq: same as Exome; Not in use any longer

        # 3.3.7 WGS {{{3
        'Cluster Generation (HiSeq X)'            : {
            'output_Analyte': {
                'attrs'        : ['location'],
                'attrs_rename' : {'location'   : 'flowcell_lane_coord'},
                'UDFs':  LAB_QC_CLUSTER_GEN['UDFs'],
                'UDFs_rename': LAB_QC_CLUSTER_GEN['UDFs_rename']
            }
        },
        # 3.4.1 perfect_index_reads_pct, one_mismatch_index_pct, q30_bases_pct, mean_qual_score {{{2
        'Demultiplexing and QC': {
            'output_ResultFile': {
                'attrs'           : [],
                'attrs_rename'    : dict(),
                'UDFs'            : ['# Reads PF',
                                     '% Perfect Index Read',
                                     '% One Mismatch Reads (Index)',
                                     '% Bases >=Q30',
                                     'Ave Q Score',
                                     'Sample sheet position'],
                'UDFs_rename'     : {'# Reads PF': 'readsCount',
                                     '% Perfect Index Read': 'perfect_index_reads_pct',
                                     '% One Mismatch Reads (Index)': 'one_mismatch_index_pct',
                                     '% Bases >=Q30': 'q30_bases_pct',
                                     'Ave Q Score' : 'mean_qual_score',
                                     'Sample sheet position': 'demultiplexing_samplesheet_index'}
            }
        },
        # 3.4.2 perfect_index_reads_pct, one_mismatch_index_pct, q30_bases_pct, mean_qual_score {{{2
        # Novaseq 6000 & X
        'BCL Convert Demultiplexing': {
            'output_ResultFile': {
                'attrs'           : [],
                'attrs_rename'    : dict(),
                'UDFs'            : ['# Reads PF',
                                     '% Perfect Index Read',
                                     '% One Mismatch Reads (Index)',
                                     '% Bases >=Q30',
                                     'Ave Q Score',
                                     'SampleSheet Sample_ID',
                                    ],
                'UDFs_rename'     : {'# Reads PF': 'readsCount',
                                     '% Perfect Index Read': 'perfect_index_reads_pct',
                                     '% One Mismatch Reads (Index)': 'one_mismatch_index_pct',
                                     '% Bases >=Q30': 'q30_bases_pct',
                                     'Ave Q Score' : 'mean_qual_score',
                                     'SampleSheet Sample_ID': 'sample_name_uuid',
                                    }
            }
        },
        # 3.5 average fragment size {{{2
        # Excap: needs 2 of them {{{
        # instead of overwriting, pick step4 ( not step 10) from protocol 'SureSelect XT -
        # PreCapture' as the first fragment length and that(the only one) from protocol 'Diag
        # Normalization and pooling' as the last fragment length; rename as
        # 'first_avg_fragment_len' and 'last_avg_fragment_len'

        # EKG: only 1 from protocol 'Diag Normalization and pooling', rename as
        # 'last_avg_fragment_len'

        # UPDATE! TO MAKE IT SIMPLE, GET THE LAST AVERAGE FRAGMENT SIZE (last run of Tapestation
        # QC) step

        'Tapestation QC': {
            'output_ResultFile': {
                'attrs': [],
                'attrs_rename': dict(),
                'UDFs': ['Average Fragment Size'],
                'UDFs_rename': {'Average Fragment Size': 'TapeStation_average_fragment_size'}
            }
        },
        "qPCR (2/2) LightCycler Results": {
            "output_ResultFile": {
                "attrs": [],
                "attrs_rename": dict(),
                "UDFs": ["Average Fragment Size", "Molarity"],
                "UDFs_rename": {
                    "Average Fragment Size": "TapeStation_average_fragment_size",
                    "Molarity": "qPCR_molarity",
                },
            }
        },
        # 3.6 molarity {{{2
        'qPCR QC': {
            'output_ResultFile': {
                'attrs': [],
                'attrs_rename': dict(),
                'UDFs': ['Molarity'],
                'UDFs_rename': {'Molarity': 'qPCR_molarity'}
            }
        },  # }}}
    }  # }}}
}

BLANK_MAX_READS = 30000

LOW_READS_THRESHOLD = {
    "CuCaV1": 100000,
    "CuCaV2": 100000,
    "CuCaV3": 100000,
}

BOOST_EXPORT_PRIORITY = ['diag-ekg', 'diag-ehg']

LOW_PRIORITY_NSC_PIPELINE = ['diag-ekg', 'diag-ehg']

# further filtering by project type when multiple matches of old samples found "Trio" projets are
# normally not allowed (capturekit is different) excapt for Trio15 and Trio16 which uses Av5
# capturekit thus can be reanalyzed
REANALYSIS_ALLOWED_PROJ_TYPE = ['excap', 'Trio', 'wgs']

# further filtering by capturekit when multiple matches of old samples found
# mainly for rescuing Trio15 and Trio16 samples which have Av5 capturekit
REANALYSIS_ALLOWED_CAPTUREKIT = ['Av5', 'TWHCV1', 'wgs']

NSC_QC = [
    {
        'name': 'q30_bases_pct',
        'check': lambda v, t: v > t,  # v: real value; t: threshold
        'fail_msg': 'failed "Q30"(value={real_value} below threshold={threshold});'
                    ' must be re-sequenced or re-prepped.'
    },
]


def group_procs_by_usage():
    """
    multiple processes fro the same piece of metadata, due to different Project type
    """
    grouped = defaultdict(list)
    for proc in METADATA['Process']:
        for domain in METADATA['Process'][proc]:
            for region in ['attrs', 'UDFs']:
                if 'Run ID' in METADATA['Process'][proc][domain][region]:
                    grouped['sequenceing'].append(proc)
                if 'reagent_labels' in METADATA['Process'][proc][domain][region]:
                    grouped['for_reagent_labels'].append(proc)
                if 'location' in METADATA['Process'][proc][domain][region]:
                    grouped['for_location'].append(proc)
                if '# Reads PF' in METADATA['Process'][proc][domain][region]:
                    grouped['for_statistics'].append(proc)
    return grouped


USAGE_PROCS = group_procs_by_usage()

FINISHED_PROTOCOL = "Finish protocol_diag"

FAILED_BIOINF_QC = "Bioinformatic QC fail_diag"

REANALSIS_PARENT_METADATA = [
    'date_analysis_requested',
    'export_priority',
    'famid',
    'joint_analysis_type',
    'platform',
    'priority',
    'specialized_pipeline',
    'uses_taqman',
]


# sequence_date, sequencer_id parsed from "Run ID":
# <udf:field type="String" name="Run ID">160901_7001448_0419_AC9MV1ANXX</udf:field> sequencer_id
# project_date parsed from project[name] due to 'run_date' of project may differ from date portion
# of a project names {{{
#   PROJECT-NAME                              RUN_DATE
#   'Diag-excap43-2015-05-20'                 '2015-06-02'
#   'Diag-excap44-2015-06-01'                 '2015-06-09'
#   'Diag-excap45-2015-06-04'                 '2015-06-09'
#   'Diag-excap46-2015-06-26'                 '2015-07-06'
#   'Diag-excap57-2015-12-09'                 '2015-12-08'
#   'Diag-excap58-2015-12-17'                 '2015-12-16'
#   'Diag-excap77-2016-11-08'                 '2016-11-09'
#   'Diag-Excap80-20161219'                   '2016-12-19'
#   'Diag-excap90_2017-06-01'                 '2017-06-01'
#   'Diag-Reanalyse-2016-11-17'               '2016-11-18'
#   'Diag-Reanalyse-2016-12-19'               '2016-12-21'
#   'Diag-Reanalyse-2016-12-20'               '2016-12-21'
#   'Diag-Reanalyse-2017-02-18'               '2017-02-16'
#   'Diag-TargetS7-CuCa3-2015-11-06'          '2015-11-06'
#   'Diag-TargetS14-2016-08-29'               '2016-08-31'
#   'Diag-TargetS19-2017-01-09'               '2017-01-06'
#   'Diag-TargetS20-2017-01-30'               '2017-01-31'
#   'Diag-TargetS22-2017-03-27'               '2017-04-03'
#   'Diag-TargetS23-2017-03-27'               '2017-04-03'
#   'Diag-TargetS26-2017-05-04'               '2017-05-12'
#   'Diag-Trio6-27-10-2016'                   '2016-10-27'
#   'Diag-Trio8-2017-01-23'                   '2017-01-24'
#   'Diag-Trio9a-2017-02-08'                  '2017-02-09'
#   'Diag-Trio14-2017-06-16'                  '2017-06-15'
# }}}


class LimsQueue(object):
    """
    A class representing Queue of QC step of Bioinformatics Processing protocol
    in Clarity LIMS, this Queue has a fixed ID
    """
    def __init__(self, queueid, latest_panel_versions, qc_q30_thresholds, config_file, source_path,
                 taqman_source, projects=None, samples=None):
        """
        queueid: Queue ID for 'lims exporter' process

        NOTE! assuming one Artificat has only one Sample
        """
        self.lims = Lims(BASEURI, USERNAME, PASSWORD)
        self.logger = logging.getLogger('lims_exporterAPI.sampleWatcherAPI.LimsQueue')
        self.queueid = queueid
        self.latest_panel_versions = latest_panel_versions
        self.qc_q30_thresholds = qc_q30_thresholds
        self.config_file = config_file
        self.source_path = source_path
        self.taqman_source = taqman_source
        if projects:
            self.specified_projects = projects.split(',')
            self.logger.info('user specified projects:\n\t%s',
                             '\n\t'.join(self.specified_projects))
        else:
            self.specified_projects = None

        if samples:
            self.specified_samples = samples.split(',')
            self.logger.info('user specified samples:\n\t%s',
                             '\n\t'.join(self.specified_samples))
        else:
            self.specified_samples = None

        # list of lims sample name together with its project name, for skipping samples that are
        # already added from another process. Not quite needed when changed to get samples directly
        # from queue instead of from Processes
        self._target_samples = list()
        # list of ResultSamples in Bioinformatics Queue
        self.result_samples = list()
        # new proband's reanalysis parents which are ready to be exported
        self.reanalysis_parents_on_the_fly = list()
        # list of ResultSamples to be exported
        self.export_samples = list()
        # 'lims exporter' Queue
        self._queue = Queue(self.lims, id=self.queueid)
        # list of samples that failed for some reason. escalate, don't export
        self.failed_samples = dict()
        self._exclude_from_rss = []

        self.samples, self.projects = self.get_samples_in_Queue()
        # self.sample_names = [smp.name for smp in self.samples]
        # self.proj_and_smp_names = [s.project.name + '_' + s.name for s in self.samples]
        # Projects in 'lims exporter' queue

        # Names of Projects in 'lims exporter' queue
        if self.specified_projects:
            self.project_names = self.specified_projects
        else:
            self.project_names = set(p.name for p in self.projects)
        # Processes of all Samples in all Projects in Lims Exporter queue
        if len(self.project_names) == 0:
            self.processes = []
            self.logger.debug("Queue is empty.")
        else:
            self.get_queue_processes()

    @staticmethod
    def trim_processtype_ver(ptname):
        """ remove version number of a ProcessType

        :ptname: ProcessType name
        :returns: ProcessType name with version number removed

        """
        trimVerStr = r'(\s+([Dd]iag|NSC|AMG))?\s+\d{1,2}\.\d{1,2}$'
        return re.sub(trimVerStr, '', ptname)

    def get_samples_in_Queue(self):
        """
        collect sample and project related info
        """
        samples = list()
        projects = list()

        for art in self._queue.artifacts:
            for smp_in in art.samples:

                smp_in_name = smp_in.name

                if smp_in.project:
                    smp_proj = smp_in.project
                    smp_proj_name = smp_proj.name

                    # skip non-user-specified projects
                    if self.specified_projects and smp_proj_name not in self.specified_projects:
                        continue
                    # skip non-user-specified samples
                    # if the same sample in multiple projects, all of these samples will be
                    # exported
                    if self.specified_samples and smp_in_name not in self.specified_samples:
                        continue

                    if smp_in not in samples:
                        samples.append(smp_in)
                    if smp_proj not in projects:
                        projects.append(smp_proj)

                    smp_combine_proj = smp_in_name + '_' + smp_proj_name

                    if smp_combine_proj not in self._target_samples:
                        result_smp = ResultSample(smp_in_name, self.config_file, self.source_path,
                                                  self.taqman_source, self.lims)
                        result_smp.metadata['artifact'] = art
                        result_smp.receive_lims_sample_metadata(smp_in)
                        result_smp.receive_lims_project_metadata(smp_proj)
                        self._target_samples.append(smp_combine_proj)
                        self.result_samples.append(result_smp)
                        self.logger.info('added new ResultSample: %s in %s %s',
                                         smp_in_name, smp_proj, smp_proj_name)
                else:
                    # sample has no project
                    self.logger.debug('%s %s has no project', smp_in, smp_in_name)

        return (samples, projects)

    def get_queue_processes(self):
        """
        get unique, targeted processes; sort processes by date_completed

        TODO:
            remove processes (Sample specific) run later than sample is queued in 'Lims Exporter'
        """
        all_queue_procs = self.get_all_procs_for_queued_projects()
        if len(all_queue_procs) == 0:
            self.processes = []
            return

        uniq_procs = self.get_unique_processes(all_queue_procs)
        if len(uniq_procs) == 0:
            self.processes = []
            return

        uniq_target_procs = self.get_targeted_processes(uniq_procs)
        if len(uniq_target_procs) == 0:
            self.processes = []
            return

        sorted_uniq_target_procs = self.sort_processes_by_date_started(uniq_target_procs)
        if len(sorted_uniq_target_procs) == 0:
            self.processes = []
            return

        self.processes = sorted_uniq_target_procs

    def get_all_procs_for_queued_projects(self):
        """all processes belonging to Projects of queued samples
        :returns: TODO

        """
        all_queue_procs = self.lims.get_processes(projectname=self.project_names)

        # from dateutil.relativedelta import relativedelta
        # from_dt = datetime.now() + relativedelta(days=-45)
        # from_dt = from_dt.replace(microsecond=0).isoformat() + '+02:00'
        # all_queue_procs = self.lims.get_processes(last_modified=from_dt)

        return all_queue_procs

    def get_unique_processes(self, all_queue_procs):
        """
        self.lims.get_processes(projectname='Foo') returns a list of Process with some Processes
        repeated many times (same limsid), remove such obvious duplicates
        """

        ids = [proc.id for proc in all_queue_procs]
        ids = list(frozenset(ids))
        uniq_procs = [GlsProcess(self.lims, id=id) for id in ids if id is not None]
        self.logger.debug("Following unique processes %s remaining for projects %s",
                          [(pc.type.name, pc.id) for pc in uniq_procs],
                          [(pj.name, pj.id) for pj in self.projects])

        return uniq_procs

    def get_targeted_processes(self, uniq_procs):
        """
        remove processes that not used for metatdata extraction
        """

        uniq_target_procs = []

        for proc in uniq_procs:
            ptn_nover = self.trim_processtype_ver(proc.type.name)
            if ptn_nover in METADATA['Process']:
                uniq_target_procs.append(proc)
            else:
                self.logger.debug("remove not used Process %s", ptn_nover)
        self.logger.debug("Following targeted processes %s remaining for projects %s",
                          [(pc.type.name, pc.id) for pc in uniq_target_procs],
                          [(pj.name, pj.id) for pj in self.projects])

        return uniq_target_procs

    def sort_processes_by_date_completed(self, uniq_target_procs):
        """
        A process may be run more than once(thus different Process ids), e.g.
        'Demultiplexing and QC' after re-sequencing;
        Several similar processes may be run for the same sample, e.g.
        'Illumina Sequencing (HiSeq 3000/4000)' and then 'NextSeq Run (NextSeq)' (re-sequencing
        with different sequencer)
        """

        dates_procs_completed = []

        for prc in uniq_target_procs[:]:
            try:
                date = prc.step.date_completed
                if date:
                    dates_procs_completed.append(date)
                else:
                    self.logger.debug("Process %s has no date-completed, removing it from list",
                                      prc)
                    uniq_target_procs.remove(prc)
            except AttributeError:
                self.logger.debug("Process %s has no date-completed, removing it from list",
                                  prc)
                uniq_target_procs.remove(prc)

        if len(uniq_target_procs) == 0:
            return []

        # double check correct order
        for p, d in zip(uniq_target_procs, dates_procs_completed):
            assert p.step.date_completed == d

        # sort by date-completed
        _, sorted_uniq_target_procs = zip(*sorted(zip(dates_procs_completed, uniq_target_procs)))

        return sorted_uniq_target_procs

    def sort_processes_by_date_started(self, uniq_target_procs):
        """
        A process may be run more than once(thus different Process ids), e.g.
        'Demultiplexing and QC' after re-sequencing;
        Several similar processes may be run for the same sample, e.g.
        'Illumina Sequencing (HiSeq 3000/4000)' and then 'NextSeq Run (NextSeq)' (re-sequencing
        with different sequencer)

        !!!sort by start date is more reliable than complete date? A re-run definitely starts later
        than the initial run (BUT requires that step complete date exists to exclude
        uncompleted/aborted steps)

        """

        dates_procs_started = []

        for prc in uniq_target_procs[:]:
            try:
                date_cmp = prc.step.date_completed  # complete date must available
                date_sta = prc.step.date_started
                if date_cmp:
                    dates_procs_started.append(date_sta)
                else:
                    self.logger.debug("Process %s has no date-completed, removing it from list",
                                      prc)
                    uniq_target_procs.remove(prc)
            except AttributeError:
                self.logger.debug("Process %s has no date-completed, removing it from list",
                                  prc)
                uniq_target_procs.remove(prc)

        if len(uniq_target_procs) == 0:
            return []

        # double check correct order
        for p, d in zip(uniq_target_procs, dates_procs_started):
            assert p.step.date_started == d

        # sort by date-started
        _, sorted_uniq_target_procs = zip(*sorted(zip(dates_procs_started, uniq_target_procs)))

        self.logger.debug("Following sorted targeted processes %s will be processed for projects"
                          " %s",
                          [(pc.type.name, pc.id) for pc in sorted_uniq_target_procs],
                          [(pj.name, pj.id) for pj in self.projects])

        return sorted_uniq_target_procs

    def get_all_similar_procs(self, proc, usage):
        """
        :proc:  LIMS Process
        get all similar processes among self.processes
        """
        # first check
        all_similar_procs = []

        for prc in self.processes:
            ptn_nover = self.trim_processtype_ver(prc.type.name)
            if ptn_nover in USAGE_PROCS[usage]:
                all_similar_procs.append(prc)

        return all_similar_procs

    def get_proc_usage(self, proc):
        """
        doc
        """
        ptn_nover = self.trim_processtype_ver(proc.type.name)
        for usage in USAGE_PROCS:
            if ptn_nover in USAGE_PROCS[usage]:
                return usage

    def get_latest_run_deprecated(self, smp, proc, domain):
        """
        :smp:   LIMS Sample
        :proc:  LIMS Process
        :domain: one process may be for multiple usages or domains
        Need to consider the case that same/similar runs for different batches of the same project,
        e.g. first run for sample A, B, C, second run for sample D, E, F; then both runs are unique
        Need to consider mixed case, e.g. first run for sample A, B, C, second run for sample B, C,
        M; then both second run is duplicate for only sample B, C
        """
        usage = self.get_proc_usage(proc)
        all_similar_procs = self.get_all_similar_procs(proc, usage)
        if len(all_similar_procs) <= 1:
            raise RuntimeError("We found no duplicate runs for {}, {}, {}"
                               "".format(smp, proc, usage))
        similar_procs_for_smp = []
        for proc in all_similar_procs:
            if domain in ['self', 'input_Analyte']:
                if smp in [art.samples for art in proc.all_inputs()]:
                    similar_procs_for_smp.append(proc)
            elif domain in ['output_Analyte']:
                if smp in [art.samples for art in proc.analytes()[0]]:
                    similar_procs_for_smp.append(proc)
            elif domain in ['output_ResultFile']:
                if smp in [art.samples for art in proc.result_files()]:
                    similar_procs_for_smp.append(proc)
            else:
                raise RuntimeError("unknown domain {}".format(domain))
        if len(similar_procs_for_smp) <= 1:
            raise RuntimeError("did not observe multiple similar runs for {} {} {}".format(
                smp, proc, domain))
        # keep only the latest
        latest_run = similar_procs_for_smp[0]
        latest_time = latest_run.step.date_completed

        for p_sim in similar_procs_for_smp:
            p_time = p_sim.step.date_completed
            if p_time > latest_time:
                self.logger.debug("A newer run of processes %s for %s, %s, %s",
                                  proc, smp, usage, domain)
                latest_run = p_sim
                latest_time = p_time

        return latest_run

    def processing_Artifact(self, proc, art_in, domain):
        """

        :art_in: LIIMS Artifact
        :domain: ['self', 'input_Analyte', 'output_Analyte', 'output_ResultFile']

        """

        assert domain in ['self', 'input_Analyte', 'output_Analyte', 'output_ResultFile']

        for smp_in in art_in.samples:

            smp_in_name = smp_in.name

            if smp_in.project:

                smp_proj = smp_in.project
                smp_proj_name = smp_proj.name

                for smp in self.result_samples:
                    if (smp.metadata['name'] == smp_in_name and
                            smp.metadata['projectname_with_date'] == smp_proj_name):
                        if domain == 'self':
                            smp.receive_lims_process_metadata(proc)
                        else:
                            pt_name_nover = self.trim_processtype_ver(proc.type.name)
                            smp.receive_lims_artifact_metadata(art_in, pt_name_nover, domain)
            else:
                # sample has no project
                self.logger.debug('%s %s has no project', smp_in, smp_in_name)

    @staticmethod
    def proc_has_out_analyte(proc):
        """if a LIMS Process has output of type Analyte, True/False
        proc.analytes() returns input Analyte if there is no output Analytes
        make sure proc.analytes() returns output Analyte

        :proc: LIMS Process
        :returns: True: proc has output Analyte; False: has not

        """
        for proc_output in proc.all_outputs():
            if proc_output.type == 'Analyte':
                return True

        return False

    def initial_query(self):
        """initial query API

        :returns: update self.result_samples

        """
        for proc in self.processes:

            self.logger.debug('Processing %s', proc)
            pt_name_nover = self.trim_processtype_ver(proc.type.name)

            if 'self' in METADATA['Process'][pt_name_nover]:
                for art_in in proc.all_inputs(unique=True):  # Analyte's Sample(s)
                    self.processing_Artifact(proc, art_in, 'self')
            if 'input_Analyte' in METADATA['Process'][pt_name_nover]:
                for art_in in proc.all_inputs(unique=True):
                    self.processing_Artifact(proc, art_in, 'input_Analyte')
            if ('output_Analyte' in METADATA['Process'][pt_name_nover] and
                    LimsQueue.proc_has_out_analyte(proc)):
                for art_in in proc.analytes()[0]:
                    self.processing_Artifact(proc, art_in, 'output_Analyte')
            if 'output_ResultFile' in METADATA['Process'][pt_name_nover]:
                for art_in in proc.result_files():
                    self.processing_Artifact(proc, art_in, 'output_ResultFile')

    def get_panel_version(self, panelname):
        """
        if genepanel version not available from API (left empty in sample sheet), then find the version from
        the overview file

        :returns: '0' or version number

        """
        if panelname in ['TrioMor', 'TrioFar'] or panelname in CUSTOM_GENEPANEL_NO_ANNOPIPE:
            return '0'
        elif panelname in self.latest_panel_versions:
            ver = self.latest_panel_versions[panelname]
            return ver
        else:
            raise RuntimeError('No active genepanel {0} found in panel overview file'.format(panelname))

    def _log_debug_added_rs_attrb(self, rs, attrb):
        """
        logger used many times
        """
        self.logger.debug("added " + attrb + "(%s) for %s",
                          rs.metadata[attrb],
                          rs.metadata['name'])

    def _make_reanalysis_parent(self, proband, parents_sample_project):
        """
        Make ResultSamples for MK and FM of Type1 and Type2 trio reanalysis on the fly.
        Fill up all ResultSample attributes required for creating basepipe, triopipe and annopipe
        dot analysis files

        :proband: ResultSample of proband
        :parents_sample_project: {'MK': mk_sample_project, 'FM': fm_sample_project}

        Attributes needed:
        - REANALSIS_PARENT_METADATA
        - 'name'               # required for __init__(...)
        - 'project'            # 'project' is used to accumulate normal-sample-batch total weight
        - 'Reanalyse'          # marks sample as reanalis
        - 'genderF'            # male/female
        - 'ReanaOldGender'     # M: mann; K: kvinne
        - 'ReanaOldFam_member' # F: Far; M: Mor
        - 'ReanaOldName'       # fullname, e.g. 12345678901-123456FM-TrioFar-KIT-wgs
        - 'ReanaOldID'         # 12345678901
        - 'ReanaOldProject'    # e.g. Diag-wgs71
        - 'pedigree'           # same as ReanaOldPedigree
        - 'ReanaOldPedigree'
        - 'artifact'           # use None, sampleMover will ignore None artifact
        """

        for parent, sample_project in parents_sample_project.items():

            old_sample_name = sample_project[0]
            old_projectname_with_date = sample_project[1]

            # new ResultSample
            parent_rs = ResultSample(old_sample_name, self.config_file, self.source_path,
                                     self.taqman_source, self.lims)

            # 'project' metadata used for accumulate total weight of batch
            # NORMAL_EXPORT_WEIGHT_INDEX; make it a virtual reanalyse project
            parent_rs.metadata['project'] = 'Diag-reanalyse-virtual'

            # get metadata for parent reanalysis sample from proband
            for k in REANALSIS_PARENT_METADATA:
                parent_rs.metadata[k] = proband.metadata.get(k)
                self._log_debug_added_rs_attrb(parent_rs, k)

            # Reanalyse
            parent_rs.metadata['Reanalyse'] = '+'  # as long as the value does not evaluate to None
            self._log_debug_added_rs_attrb(parent_rs, 'Reanalyse')

            # set variable values
            if parent == 'MK':
                fam_member = 'M'
                gender = 'K'
            elif parent == 'FM':
                fam_member = 'F'
                gender = 'M'
            else:
                raise RuntimeError('Unknown parent: {0}'.format(parent))

            # genderF
            parent_rs.metadata['genderF'] = GENDERD[gender]
            self._log_debug_added_rs_attrb(parent_rs, 'genderF')

            # ReanaOldGender
            parent_rs.metadata['ReanaOldGender'] = gender
            self._log_debug_added_rs_attrb(parent_rs, 'ReanaOldGender')

            # ReanaOldFam_member
            parent_rs.metadata['ReanaOldFam_member'] = fam_member
            self._log_debug_added_rs_attrb(parent_rs, 'ReanaOldFam_member')

            # ReanaOldName
            parent_rs.metadata['ReanaOldName'] = old_sample_name
            self._log_debug_added_rs_attrb(parent_rs, 'ReanaOldName')

            # ReanaOldID
            parent_rs.metadata['ReanaOldID'] = parent_rs.metadata['ReanaOldName'].split('-')[0]

            self._log_debug_added_rs_attrb(parent_rs, 'ReanaOldID')

            # ReanaOldProject
            matched = re.match(r'(?P<project>([dD]iag-)?[^-]*)(-\d+)?', old_projectname_with_date,
                               re.I)
            if matched:
                project = matched.group('project')
            else:
                raise RuntimeError('badly formatted project name: {0}'.format(
                    old_projectname_with_date))

            parent_rs.metadata['ReanaOldProject'] = project
            self._log_debug_added_rs_attrb(parent_rs, 'ReanaOldProject')

            # ReanaOldPedigree
            parent_rs.metadata['ReanaOldPedigree'] = {
                FAM_MEMBERD[fam_member]: {
                    "gender": parent_rs.metadata["genderF"]
                }
            }
            self._log_debug_added_rs_attrb(parent_rs, 'ReanaOldPedigree')

            # pedigree
            parent_rs.metadata['pedigree'] = parent_rs.metadata['ReanaOldPedigree']

            # artifact
            parent_rs.metadata['artifact'] = None  # A None artifact will be skipped by sampleMover
            self._log_debug_added_rs_attrb(parent_rs, 'artifact')

            # add to list of new_proband_reanalysis_parents
            self.reanalysis_parents_on_the_fly.append(parent_rs)

    def _group_fam(self):
        """
        count the number of queued samples for each famid
        """
        fams = defaultdict(list)
        for smp in self.result_samples:
            famid = smp.metadata.get('famid', None)
            if famid:
                fams[famid].append(smp)

        return fams

    def _is_type1_trio_reanalysis_proband(self, smp):
        """Check if smp is Type1 trio reanalysis proband

        Type1 trio reanalysis is:
            - the trio had been analyzed before
            - reanalyze the same trio with a different genepanel
            - only the proband in a Type1 reanalysis project and in lims exporter queue
        """
        if (
            smp.metadata['Reanalyse'] and  # reanalysis
            smp.metadata.get('fam_member') and  # allow missing fam_member (set to False)
            smp.metadata['fam_member'].lower() == 'p' and  # proband
            smp.metadata['joint_analysis_type'].lower() == 'trio' and  # trio
            TYPE1_REANA_PROJ_RE.match(smp.metadata['projectname_with_date'])  # Type1
        ):
            return True
        else:
            return False

    def _is_type2_trio_reanalysis_proband(self, smp):
        """Check if smp is possibly a Type2 trio reanalysis proband; avoid unnecessary DB query

        Type2 trio reanalysis: originally a trio;
                               another proband newly sequenced(in lims exporter queue);
                               reanalysis parents.
        """
        fams = self._group_fam()
        if (all(smp.metadata.get(k) for k in ['fam_member', 'famid'])
                and not any([
                    # single
                    smp.metadata['joint_analysis_type'].lower() != 'trio',
                    # parent sample
                    smp.metadata['fam_member'].lower() != 'p',
                    # can't be a reanalysis if famid is missing
                    smp.metadata.get('famid') is None,
                    # already more than 1 members in lims exporter queue
                    len(fams[smp.metadata.get('famid')]) > 1,
                ])):
            return True
        else:
            return False

    def _add_parents_on_the_fly(self, smp):
        """add reanalysis parents ResultSamples on the fly"""
        parents_sample_project = smp.get_reanalysis_parents()

        if parents_sample_project:
            self._make_reanalysis_parent(smp, parents_sample_project)

    def secondary_parse(self):
        """parse/construct required meta info from initial query API

        :returns: update self.result_samples

        """
        self.logger.debug("start secondary parsing %s ResultSamples", len(self.result_samples))

        for smp in self.result_samples:
            sname = smp.metadata['name']
            try:
                # Set reanalysis values before doing more field extraction
                # add attribute whether a reanalysis sample
                smp.add_reanalyse_setting()
                # add reanalysis old sample id, name, old project, capturekit etc.
                smp.add_reanalyse_old_id_proj()
                # add projet name without date
                smp.add_project()
                # numeric sample id parsed from sample name
                smp.add_sample_id()
                smp.add_specialized_pipeline()

                if FALLBACK:  # metadata parsed from sample name when FALLBACK
                    if not smp.parsed_name:
                        raise RuntimeError('sample name parsing error: {0} (Reana: {1})'.format(
                            sname, smp.metadata.get('ReanaOldName')
                        ))

                    for k in ['genepanel_name',
                              'genepanel_version',  # can be 'None', handled later
                              'capturekit',
                              'famid',
                              'fam_member',
                              'gender']:
                        if k not in smp.metadata:  # not available in API
                            smp.metadata[k] = smp.parsed_name.get(k)
                        # Updated Sample Name but forgot to update UDF
                        elif smp.parsed_name.get(k) and smp.metadata[k] != smp.parsed_name.get(k):
                            raise RuntimeError("UDF for {} doesn't match Sample Name".format(k))
                smp.add_genderF()
                # update reanalyse capturekit
                smp.update_reanalyse_old_capturekit_pedigree()

                # handle gene panel version
                # when genepanel version left empty in Sample Sheet, thus UDF doesn't exit, and
                # genepanel_version part missing in sample name; use latest version of that panel
                if ('genepanel_version' not in smp.metadata
                        or smp.metadata['genepanel_version'] == ''
                        or smp.metadata['genepanel_version'] is None):

                    # get latest version
                    latest_pversion = self.get_panel_version(
                        smp.metadata['genepanel_name']
                    )
                    smp.metadata['genepanel_version'] = latest_pversion

                smp.add_pedigree()
                # use taqman
                smp.add_uses_taqman()

                type1_trio_reanalysis_proband = self._is_type1_trio_reanalysis_proband(smp)
                type2_trio_reanalysis_proband = self._is_type2_trio_reanalysis_proband(smp)

                if type2_trio_reanalysis_proband:
                    # add ResultSample metadata, used by add_platform()
                    smp.metadata['type2_trio_reanalysis_proband'] = True

                # TSD pipeline or NSC pipeline
                smp.add_platform()

                # if a Type1/Type2 trio reanalysis proband, add reanalysis parents on the fly
                if type1_trio_reanalysis_proband or type2_trio_reanalysis_proband:
                    self._add_parents_on_the_fly(smp)

                # export priority
                smp.add_export_priority()

                # secondary parse metadata appliable only to new samples
                if not smp.metadata['Reanalyse']:
                    smp.low_reads_check()  # raise exception if too few reads
                    smp.add_sequence_date()
                    smp.add_sequencer_id()
                    smp.add_sequencer_type()
                    smp.update_flowcell()
                    smp.add_project_date()
                    smp.add_path_of_project_dir()
                    smp.add_path_of_sample_dir()
                    smp.add_lane()
                    smp.add_index()
                    smp.add_stats()
                    smp.add_sample_reads()
                    smp.add_fastqc_dirname()
                    smp.add_whether_exported()
                    smp.add_path_of_qc_reports()
                    smp.blankprove_check()  # add sample to failed_samples
                    smp.check_sources()

            except Exception as e:
                smp.logger.error("Error processing {}".format(sname), exc_info=1)
                self.fail_sample(smp, str(e))

    def qc_nsc(self):
        """
        automate NSC QC checks
        fail a sample if any of the NSC QC check fails
        """
        for smp in self.result_samples:
            # skip qc_nsc for reanalysis samples
            is_reanalysis = smp.metadata['Reanalyse']
            if is_reanalysis:
                continue

            qc_fail_msg_s = []

            priority = smp._retrieve_attr('priority', notEmpty=False, optional=True)
            if not priority:
                priority = 3  # default high priority so sample will be processed

            for qc in NSC_QC:

                if qc['name'] == 'q30_bases_pct' and priority >= 2:
                    continue  # high priority sample do not fail q30, run pipeline and decide later

                real_value = smp._retrieve_attr(qc['name'])
                real_value = float(real_value)

                capturekit = smp._retrieve_attr('capturekit')
                capturekit = CAPTUREKIT_MAP.get(capturekit, capturekit)  # translate
                # specialized pipeline
                # wgs DRAGEN the same Q30 as wgs GATK, so not distinguished
                if smp.metadata.get('specialized_pipeline') == 'TUMOR':
                    capturekit = capturekit + '_TUMOR'
                try:
                    threshold = self.qc_q30_thresholds[capturekit]
                    threshold = float(threshold)
                except KeyError:
                    self.logger.error('capturekit of %s is %s has no q30 setting in %s',
                                      smp.metadata['name'],
                                      capturekit,
                                      self.qc_q30_thresholds)
                    continue  # run pipeline

                # get capturekit based threshold
                if not qc['check'](real_value, threshold):
                    _fail_msg = qc['fail_msg'].format(real_value=real_value, threshold=threshold)
                    qc_fail_msg_s.append(_fail_msg)

            if qc_fail_msg_s:
                concat_msg = '; '.join(qc_fail_msg_s)
                smp.logger.error('%s failed NSC QC(s): %s', smp.metadata['name'], concat_msg)
                self.fail_sample(smp, concat_msg)

    def handle_blanksamples(self):
        """
        Blankprove reads count based handling of EKG project.
        If reads count of a blankprove inside a project >= BLANK_MAX_READS:
            export the blankprove as normal
            send all other samples in the project to MR
        Else:
            send blankprove to finish protocol without creating any analysis and sample folders
            export all other samples normally
        hanle multiple EKG projects with blankprove
        """

        blankproves_not_blank = [s for s in self.result_samples
                                 if s.metadata.get('blankprove_not_blank')]

        for bnb in blankproves_not_blank:
            for smp in self.result_samples:
                if (smp.metadata['project'] == bnb.metadata['project'] and
                        smp.metadata['name'] != bnb.metadata['name']):
                    self.fail_sample(smp, 'Too many reads of BlankProve in this project')

    def exclude_failed_samples(self):
        """
        remove failed_samples from result_samples
        """
        for fs in self._exclude_from_rss:
            for smp in self.result_samples[:]:
                if smp == fs:
                    self.result_samples.remove(smp)
                    self.logger.debug("Removed failed sample %s from result_samples",
                                      smp.metadata['name'])

    def check_taqman(self):
        """
        verify taqman can be found
        """

    def choose_export_smps(self):
        """filter out samples already exported and samples failed

        :returns: update self.export_samples

        """
        for smp in self.result_samples:
            if DEBUG:
                self.export_samples.append(smp)
            else:
                if not smp.metadata.get('already_exported') \
                        and '-'.join([smp.metadata['projectname_with_date'],
                                      smp.metadata['name']]) not in self.failed_samples:
                    self.export_samples.append(smp)

    def poll(self):
        """
        top level monitor
        """
        self.initial_query()
        self.secondary_parse()
        self.qc_nsc()
        self.handle_blanksamples()
        self.exclude_failed_samples()
        self.choose_export_smps()
        all_for_export = self.export_samples + self.reanalysis_parents_on_the_fly
        self.logger.debug("%s To Be Exported Samples:\n\t%s",
                          len(all_for_export),
                          "\n\t".join([s.metadata['name'] for s in all_for_export]))
        for export_smp in all_for_export:
            self.logger.debug('exporting sample\n%s', pprint.pformat(vars(export_smp)))
            if export_smp.metadata.get('Reanalyse'):
                prjdir = None
                smpdir = None
            else:
                prjdir = export_smp.metadata['path_of_project_dir']
                smpdir = export_smp.metadata['path_of_sample_dir']
            yield {
                'sample': export_smp.metadata,
                'project_dir': prjdir,
                'sample_dir': smpdir
            }

    def fail_sample(self, rs, comment):
        # input string <project_name>-<sample_name>
        if not isinstance(rs, ResultSample):
            rs_objs = [rs_obj for rs_obj in self.result_samples
                       if '-'.join([rs_obj.metadata['projectname_with_date'],
                                    rs_obj.metadata['name']]) == rs]
            if len(rs_objs) != 1:
                raise ValueError("Could not find ResultSample or found too many for rs "
                                 "'{}': found {}".format(rs, len(rs_objs)))
            rs = rs_objs[0]

        # make string <project_name>-<sample_name>
        p_s = '-'.join([rs.metadata['projectname_with_date'], rs.metadata['name']])

        if p_s not in self.failed_samples:
            self.failed_samples[p_s] = {
                'artifacts': [a for a in self._queue.artifacts
                              if a.samples[0].name == rs.metadata['name'] and
                              a.samples[0].project.name == rs.metadata['projectname_with_date']],
                'comment': comment
            }

            self._exclude_from_rss.append(rs)

        return self.failed_samples[p_s]


class ResultSample(object):
    """
    restructure lims sample
    """

    def __init__(self, name, config_file, source_path, taqman_source, lims):
        """
        :name: should be the same as lims Sample.name
        """
        self.logger = logging.getLogger('lims_exporterAPI.sampleWatcherAPI.ResultSample')
        self.metadata = dict()
        self.metadata['name'] = name
        self.metadata['config_file'] = config_file
        self.metadata['delivery_basepath'] = source_path
        self.metadata['taqman_source'] = taqman_source
        self._parsed_name = None
        self._CONFIG = None
        self.lims = lims

    def __eq__(self, theOther):
        """
        same sample has the same "Sample" and  "Project"
        """
        if isinstance(theOther, self.__class__):
            return self.metadata['name'] == theOther.metadata['name'] \
                and self.metadata['projectname_with_date'] == \
                theOther.metadata['projectname_with_date']

        return False

    def _retrieve_attr(self, attr, notEmpty=True, optional=False):
        """
        retrieve attribute of ResultSample
        when KeyError, give context

        """

        value = self.metadata.get(attr)
        if (not optional and
            notEmpty and (
                value is None or
                value == "" or
                (isinstance(value, collections.Sized) and len(value) == 0))):
            raise RuntimeError('ResultSample: {0} has no attribute "{1}"'
                               ''.format(self.metadata['name'], attr))

        return value

    def receive_lims_sample_metadata(self, lims_sample):
        """ add attributes/udfs of a LIMS Sample to ResultSample

        :lims_sample: a LIMS Sample
        :return: update self

        """
        if self.metadata['name'] != lims_sample.name:
            return

        # get Sample attributes
        if METADATA['Sample']['attrs']:
            self._add_attributes_of_lims_entity(lims_sample, METADATA['Sample']['attrs'],
                                                renaming=METADATA['Sample']['attrs_rename'])
        # get Sample UDFs
        if METADATA['Sample']['UDFs']:
            self._add_udfs_of_lims_entity(lims_sample, METADATA['Sample']['UDFs'],
                                          renaming=METADATA['Sample']['UDFs_rename'])

    def receive_lims_project_metadata(self, lims_project):
        """ add attributes/udfs of a LIMS Projec to ResultSample

        :lims_project: a LIMS Projec
        :return: update self

        """
        # get Project attributes
        if METADATA['Project']['attrs']:
            self._add_attributes_of_lims_entity(lims_project, METADATA['Project']['attrs'],
                                                renaming=METADATA['Project']['attrs_rename'])
        # get Project UDFs
        if METADATA['Project']['UDFs']:
            self._add_udfs_of_lims_entity(lims_project, METADATA['Project']['UDFs'],
                                          renaming=METADATA['Project']['UDFs_rename'])

    def receive_lims_process_metadata(self, limsProcess):
        """ add attributes or udfs of a LIMS Process itself to ResultSample
        add Process input/output artifact's attributes/udfs wiht receive_lims_artifact_metadata()

        :limsProcess: a LIMS Process
        :return: update self

        """
        pt_name_nover = LimsQueue.trim_processtype_ver(limsProcess.type.name)

        # get Process attributes
        if METADATA['Process'][pt_name_nover]['self']['attrs']:
            self._add_attributes_of_lims_entity(
                limsProcess,
                METADATA['Process'][pt_name_nover]['self']['attrs'],
                METADATA['Process'][pt_name_nover]['self']['attrs_rename'])

        # get Process UDFs
        if METADATA['Process'][pt_name_nover]['self']['UDFs']:
            self._add_udfs_of_lims_entity(
                limsProcess,
                METADATA['Process'][pt_name_nover]['self']['UDFs'],
                METADATA['Process'][pt_name_nover]['self']['UDFs_rename'])

    def receive_lims_artifact_metadata(self, limsArtifact, pt_name_nover, domain):
        """ artifact attribute or UDF

        :limsArtifact: Artifact
        :pt_name_nover: ProcessType name without version number
        :domain: ['self', 'input_Analyte', 'output_Analyte', 'output_ResultFile']
        :returns: update self

        """
        assert limsArtifact.type

        if METADATA['Process'][pt_name_nover][domain]['attrs']:
            self._add_attributes_of_lims_entity(
                limsArtifact,
                METADATA['Process'][pt_name_nover][domain]['attrs'],
                METADATA['Process'][pt_name_nover][domain]['attrs_rename']
            )
        if METADATA['Process'][pt_name_nover][domain]['UDFs']:
            self._add_udfs_of_lims_entity(
                limsArtifact,
                METADATA['Process'][pt_name_nover][domain]['UDFs'],
                METADATA['Process'][pt_name_nover][domain]['UDFs_rename']
            )

    def _add_attributes_of_lims_entity(self, lims_entity, attributes_wanted, renaming=None):
        """Add attributes of a LIMS entity to ResultSample

        : lims_entity       : a lims entity, e.g. Sample, Project, Artifact, ResultFile
        : attributes_wanted : a list of target attributes names
        : renaming          : rename the attributes of lims_entity before adding to ResultSample
        : returns           : update self

        """
        if renaming is None:
            renaming = dict()
        for attr in attributes_wanted:
            new_attr = attr if attr not in renaming else renaming[attr]
            try:
                v = getattr(lims_entity, attr)
                if isinstance(v, float):
                    v = float(
                        "{0:.2f}".format(v)
                    )  # keep two digits after decimal point
                # latter always overwrites former even if latter is "None"
                # METADATA guarantees unique source of truth; "process sorting" guarantees newer process takes priority
                self.metadata[new_attr] = v
                self.logger.debug(
                    "added %s from %s for %s",
                    new_attr,
                    lims_entity,
                    self.metadata["name"],
                )
            except AttributeError:
                self.logger.debug(
                    "%s has no attribute %s for %s",
                    lims_entity,
                    attr,
                    self.metadata["name"],
                )

    def _add_udfs_of_lims_entity(self, lims_entity, udfs_wanted, renaming=None, overwrite=True):
        """Add UDFs of a LIMS entity to ResultSample

        : lims_entity : a lims entity, e.g. Sample, Project, Artifact, ResultFile
        : udfs_wanted : a list of target UDF names
        : renaming    : rename the UDF of lims_entity before adding to ResultSample
        : overwrite   : if attribute already added, overwrite it
        : returns     : update self

        """
        if renaming is None:
            renaming = dict()
        for k, v in lims_entity.udf.items():
            if k in udfs_wanted:
                new_k = k if k not in renaming else renaming[k]
                if overwrite:
                    if isinstance(v, float):
                        v = float(
                            "{0:.2f}".format(v)
                        )  # keep two digits after decimal point
                    # latter overwrites former even if latter is "None"
                    # METADATA guarantees unique source of truth; "process sorting" guarantees newer process takes priority
                    self.metadata[new_k] = v
                    self.logger.debug(
                        "added UDF %s from %s for %s",
                        new_k,
                        lims_entity,
                        self.metadata["name"],
                    )

    def add_sample_id(self):
        """get sample_id from sample name

        :returns: add self.metadata['sample_id'] parsed from self.metadata['name']

        """
        # name = self.metadata['name']
        # unittest_001 import re
        # unittest_001 name = '123456789'
        # matched = re.match(r'^(?P<sample_id>\w+)'
        #                    r'(-(?P<famid>[0-9]+)(?P<fam_member>[PMF])(?P<gender>[KM]))?'
        #                    r'((-(?P<genepanel_name>\w+)(-(?P<genepanel_version>[vV]\w+))?)?'
        #                    r'-KIT-(?P<capturekit>\w+))?', name)
        # unittest_001 print(matched.group('sample_id'))
        # unittest_001 print(matched.group('famid'))
        # unittest_001 print(matched.group('fam_member'))
        # unittest_001 print(matched.group('gender'))
        # unittest_001 print(matched.group('genepanel_name'))
        # unittest_001 print(matched.group('genepanel_version'))
        # unittest_001 print(matched.group('capturekit'))

        if self.parsed_name:
            self.metadata['sample_id'] = self.parsed_name.get('sample_id')
            self.logger.debug("parsed sample_id (%s) for %s",
                              self.metadata['sample_id'],
                              self.metadata['name'])
        else:
            raise RuntimeError("Can't get sample ID from sample name: '{}'".format(
                self.metadata['name']))

    def add_sequence_date(self):
        """get sequence_date of a ResultSample

        :returns: add self.metadata['sequence_date'] parsed from sequencing_run_id

        """
        run_id = self._retrieve_attr('sequencing_run_id')
        dt = run_id.split('_')[0]
        try:
            date_date = datetime.strptime(dt, '%y%m%d')
        except ValueError:
            date_date = datetime.strptime(dt, '%Y%m%d')
        date_str = datetime.strftime(date_date, '%Y-%m-%d')

        self.metadata['sequence_date'] = date_str
        self.logger.debug("parsed sequence_date (%s) for %s",
                          self.metadata['sequence_date'],
                          self.metadata['name'])

    def add_sequencer_id(self):
        """get sequencer_id of a ResultSample

        :returns: add self.metadata['sequencer_id'] parsed from sequencing_run_id

        """

        run_id = self._retrieve_attr('sequencing_run_id')

        self.metadata['sequencer_id'] = run_id.split('_')[1]
        self.logger.debug("parsed sequencer_id (%s) for %s",
                          self.metadata['sequencer_id'],
                          self.metadata['name'])

    def add_sequencer_type(self):
        """get sequencer_type of a ResultSamples

        :returns: add self.metadata['sequencer_type'] parsed from sequencer_id

        """
        sequencer_id = self._retrieve_attr('sequencer_id')
        assert sequencer_id, "sequencer_id not found"

        if sequencer_id.startswith('M'):
            sequencer_type = 'MiSeq'
        elif sequencer_id.startswith('N'):
            sequencer_type = 'NextSeq'
        elif sequencer_id.startswith('D'):
            sequencer_type = 'HiSeq 2500'
        elif sequencer_id.startswith('J'):
            sequencer_type = 'HiSeq 3000'
        elif sequencer_id.startswith('E'):
            sequencer_type = 'HiSeq X'
        elif sequencer_id.startswith('A'):
            sequencer_type = 'NovaSeq6000'
        elif sequencer_id.startswith('LH'): # nox: LH00534
            sequencer_type = 'NovaSeqX'
        else:
            raise RuntimeError("unknown sequencer id: '{}'".format(sequencer_id))

        self.metadata['sequencer_type'] = sequencer_type
        self.logger.debug("parsed sequencer_type (%s) for %s",
                          self.metadata['sequencer_type'],
                          self.metadata['name'])

    def update_flowcell(self):
        """
        (1) NovaSeq6000 use "Left" in place of 'A' and "Right" in place of "B"
        (2) When sequenced with Hiseq/NovaSeq6000, then re-sequenced with Nextseq/MiSeq
            flowcell needs to be deleted, otherwise path_of_sample_dir will be wrong
        (3) NovaSeq X use "A" or "B"
        """
        sequencer_type = self._retrieve_attr('sequencer_type')

        assert sequencer_type, "sequencer_type not found"
        if sequencer_type == 'NovaSeq6000':
            nova_flowcell = self._retrieve_attr('flowcell')
            self.metadata['flowcell'] = {'Left': 'A', 'Right': 'B'}[nova_flowcell]

        if not (sequencer_type.startswith('HiSeq') or sequencer_type.startswith('NovaSeq')):
            try:
                del self.metadata['flowcell']
            except KeyError:
                pass

    def add_project(self):
        """add project (without date vs projectname_with_date) of a ResultSample e.g. Diag-excap99
        parsed from Sample.Project.name

        :returns: add self.metadata['project']

        """
        fullname = self._retrieve_attr('projectname_with_date')

        matched = re.match(r'(?P<project>([dD]iag-)?[^-]*)(-\d+)?', fullname, re.I)
        if matched:
            project = matched.group('project')
        else:
            raise RuntimeError('badly formatted project name: {0}'.format(fullname))

        self.metadata['project'] = project
        self.logger.debug("parsed project (%s) for %s",
                          self.metadata['project'],
                          self.metadata['name'])

    def add_specialized_pipeline(self):
        """ add Specialized pipeline Diag

        :returns: add self.metadata['specialized_pipeline']

        """

        sp_pipeline = self._retrieve_attr('specialized_pipeline', optional=True)

        if sp_pipeline:
            self.metadata['specialized_pipeline'] = sp_pipeline
            self.logger.debug("retrieved specialized_pipeline %s for %s",
                              self.metadata['specialized_pipeline'],
                              self.metadata['name'])

    def add_project_date(self):
        """get project_date of a ResultSample

        :returns: add self.metadata['project_date']

        """
        # fullname = 'Diag-EKG170818-2017-08-18' #pass, 2017-08-18
        # fullname = 'Diag-EKG170818' #pass, 2017-08-18
        # fullname = 'Diag-EKG170818-A' # x
        # fullname = 'EKG170818-2017-08-18' #pass, 2017-08-18
        # fullname = 'EKG170818-20170818' #pass, 2017-08-18
        # fullname = 'EKG170818add-081817' #pass, 2017-08-18
        # fullname = 'EKG170818add-181117' # x, *2018-11-17
        # fullname = 'EKG170818-170818' #pass, 2017-08-18
        # fullname = 'EKG170818' #pass 2017-08-18

        fullname = self._retrieve_attr('projectname_with_date')

        matched = re.match(r'(?P<project>([dD]iag-)?[^-]*)(-(?P<proj_date>\d+.*))?', fullname)
        if matched:
            if matched.group('proj_date'):
                try:
                    date = dtparser.parse(matched.group('proj_date'), yearfirst=True)
                except ValueError:
                    try:
                        # '2017-30-01'
                        # date = dtparser.parse(matched.group('proj_date'), dayfirst=True)
                        match_date = matched.group('proj_date')
                        m = re.match(r'(\d{4})[-/]?(\d{2})[-/]?(\d{2})', match_date)
                        if m:
                            repl_date = str(m.group(1)) + str(m.group(3)) + str(m.group(2))
                            date = dtparser.parse(repl_date, yearfirst=True)
                    except ValueError as err:
                        if not err.args:
                            err.args = ('', )
                        err.args += ('badly formatted project date ({0}) in project name({1}))'
                                     ''.format(matched.group('proj_date'), fullname),)
                        raise
            else:
                try:
                    dt = fullname[-6:]
                    date = dtparser.parse(dt, yearfirst=True)
                except ValueError as err:
                    if not err.args:
                        err.args = ('', )
                    err.args += ('badly formatted project date in project name({0}))'
                                 ''.format(fullname),)
                    raise
        else:
            raise RuntimeError('badly formatted project name: {0}'.format(fullname))

        self.metadata['project_date'] = date.strftime('%Y-%m-%d')
        self.logger.debug("parsed project_date (%s) for %s",
                          self.metadata['project_date'],
                          self.metadata['name'])

    def add_genderF(self):
        """
        translate gender
        """

        gender = self._retrieve_attr('gender', notEmpty=False, optional=True)  # EKG no gender info

        if gender:
            self.metadata['genderF'] = GENDERD[gender]

            self.logger.debug("parsed genderF (%s) for %s",
                              self.metadata['genderF'],
                              self.metadata['name'])

    def add_pedigree(self):
        """get pedigree of a Trio ResultSample

        :returns: add self.metadata['pedigree']

        """
        if 'joint_analysis_type' in self.metadata:
            if self.metadata['joint_analysis_type'].lower() == 'trio':  # a Trio ResultSample
                self.metadata['pedigree'] = {
                    FAM_MEMBERD[self.metadata['fam_member']]: {
                        "gender": self.metadata["genderF"]
                    }
                }
                self.logger.debug("added pedigree(%s) for %s",
                                  self.metadata['pedigree'],
                                  self.metadata['name'])
        else:
            if FALLBACK:
                try:
                    if self.metadata['famid']:
                        _gender = self._retrieve_attr("genderF")
                        self.metadata['pedigree'] = {
                            FAM_MEMBERD[self.metadata['fam_member']]: {
                                "gender": _gender
                            }
                        }
                        self.logger.debug("parsed pedigree(%s) for %s",
                                          self.metadata['pedigree'],
                                          self.metadata['name'])
                except Exception as e:
                    if not e.args:
                        e.args = ('',)
                    e.args += ('add_pedigree error {}'.format(self.metadata['name']),)
                    raise

    def _get_sample_read_filename(self, read_num):
        """compose fastq.gz filename of a ResultSample

        :read_num: "R1" or "R2"
        :returns: read fastq.gz file name

        """

        sequencer_type = self._retrieve_attr('sequencer_type') 
        lane = self._retrieve_attr('lane', optional=True)

        # NovaSeq X
        if sequencer_type == 'NovaSeqX':
            lane = self._retrieve_attr('lane', optional=False, notEmpty=True)
            smp_name_uuid = self._retrieve_attr('sample_name_uuid', optional=False, notEmpty=True)
            path_of_sample_dir = self._retrieve_attr('path_of_sample_dir', optional=False, notEmpty=True) 
            fastq_compression_format = self._retrieve_attr('fastq_compression_format',optional=False, notEmpty=True)
            try:
                extension = FASTQ_COMPRESSION_EXTENSION[str(fastq_compression_format)]
            except KeyError as e: 
                raise RuntimeError('Unknown FASTQ Compression Format {}'.format(fastq_compression_format))
            assert smp_name_uuid
            assert lane
            filename_patt = "_".join([smp_name_uuid, "S*", 'L'+'{:03d}'.format(int(lane)), read_num, "001"])+".fastq" + extension
            assert path_of_sample_dir
            path_patt = os.path.join(path_of_sample_dir, filename_patt) 
            fastq_files = glob.glob(path_patt)

            if len(fastq_files) == 0:
                raise RuntimeError("Could not find fastq file matching '{}'".format(path_patt))
            elif len(fastq_files) > 1:
                raise RuntimeError("Found multiple fastq files matching '{}': {}".format(path_patt, fastq_files))
            else:
                return os.path.basename(fastq_files[0])

        # Other sequencers
        smp_name = self.metadata['name']
        idx = self._retrieve_attr('index')
        samplesheet_idx = self._retrieve_attr('demultiplexing_samplesheet_index')

        # lane identifier in fastq file name
        if not lane:
            lane_identifier = None  # not lane identifier for NextSeq in read filename
        else:
            lane_identifier = 'L'+'{:03d}'.format(int(lane))

        # sample identifier(either index or S number) in fastq file name
        if sequencer_type == 'HiSeq 2500':
            smp_identifier = idx
        else:
            smp_identifier = 'S' + str(samplesheet_idx)

        filename_parts = [smp_name, smp_identifier, lane_identifier, read_num, '001']
        filename_parts_removed_none = [p for p in filename_parts if p is not None]

        read_file = '_'.join(filename_parts_removed_none) + '.fastq.gz'

        return read_file

    def _get_fastqc_dirname(self, read_num):
        """compose _fastqc dirname a ResultSample

        :read_num: "R1" or "R2"
        :returns: fastqc dirname

        """
        smp_name = self.metadata['name']
        sequencer_type = self._retrieve_attr('sequencer_type')
        lane = self._retrieve_attr('lane', optional=True)
        idx = self._retrieve_attr('index')
        samplesheet_idx = self._retrieve_attr('demultiplexing_samplesheet_index')

        # lane identifier in fastq file name
        if not lane:
            lane_identifier = None  # not lane identifier for NextSeq in read filename
        else:
            lane_identifier = 'L'+'{:03d}'.format(int(lane))

        # sample identifier(either index or S number) in fastq file name
        if sequencer_type == 'HiSeq 2500':
            smp_identifier = idx
        elif sequencer_type == 'NovaSeqX':
            smp_identifier = 'S' + str(lane)
        else:
            smp_identifier = 'S' + str(samplesheet_idx)

        dirname_parts = [smp_name, smp_identifier, lane_identifier, read_num, '001', 'fastqc']
        dirname_parts_clean = [p for p in dirname_parts if p is not None]

        dirname = '_'.join(dirname_parts_clean)

        return dirname

    def _get_qc_report_filename(self, read_num):
        """ get qc report pdf filename for read_num

        :read_num: 'R1' or 'R2'
        :returns: qc report filename

        """
        smp_name = self.metadata['name']
        run_id = self._retrieve_attr('sequencing_run_id')
        sequencer_type = self._retrieve_attr('sequencer_type')
        lane = self._retrieve_attr('lane', optional=True)
        idx = self._retrieve_attr('index')
        samplesheet_idx = self._retrieve_attr('demultiplexing_samplesheet_index')

        # lane identifier in fastq file name
        if not lane:
            lane_number = None
            lane_identifier = None  # not lane identifier for NextSeq in read filename
        elif sequencer_type == 'MiSeq':
            lane_number = None
            lane_identifier = 'L'+'{:03d}'.format(int(lane))
        else:
            lane_number = lane
            lane_identifier = 'L'+'{:03d}'.format(int(lane))

        # sample identifier(either index or S number) in fastq file name
        if sequencer_type == 'HiSeq 2500':
            smp_identifier = idx
        elif sequencer_type == 'NovaSeqX':
            smp_identifier = 'S' + str(lane)
        else:
            smp_identifier = 'S' + str(samplesheet_idx)

        head = [run_id, lane_number]
        head_clean = [p for p in head if p is not None]
        middle_parts = [smp_name, smp_identifier, lane_identifier, read_num, '001']
        middle_parts_clean = [p for p in middle_parts if p is not None]
        middle_clean = ['_'.join(middle_parts_clean)]
        tail = ['qc', 'pdf']

        report_name = '.'.join(head_clean + middle_clean + tail)

        return report_name

    def add_path_of_project_dir(self):
        """compose path_of_project_dir of a ResultSample

        :returns: add self.metadata['path_of_project_dir']

        """
        sequencing_run_id = self._retrieve_attr('sequencing_run_id')
        seq_date = sequencing_run_id.split('_')[0]
        seqcer_name = sequencing_run_id.split('_')[1]
        seq_date_id = "_".join([seq_date[-6:], seqcer_name])  # NovaSeq: 20240516; others: 240516
        projectname_with_date = self._retrieve_attr('projectname_with_date')
        
        if 'flowcell' in self.metadata:
            flowcell = self._retrieve_attr('flowcell', optional=True)
            relative_path = (seq_date_id + '.' +
                             flowcell + '.' +
                             'Project' + '_' +
                             projectname_with_date)
        else:
            relative_path = (seq_date_id + '.' +
                             'Project' + '_' +
                             projectname_with_date)

        self.metadata['path_of_project_dir'] = os.path.join(self.metadata['delivery_basepath'],
                                                            relative_path)

        self.logger.debug("parsed path_of_project_dir for %s", self.metadata['name'])

    def add_path_of_sample_dir(self):
        """add real_sample_dir which is the path to folder containing R1 R2 fastq.gz files
        HiSeq - each sample has a subdir in project dir containing fastq.gz files

        :returns: add self.metadata['path_of_sample_dir']

        """
        smp_name = self.metadata['name']
        sequencer_type = self._retrieve_attr('sequencer_type')
        project_dir = self._retrieve_attr('path_of_project_dir')

        # set {'HiSeq X', 'HiSeq high output', 'MiSeq', 'NextSeq high output'}
        if sequencer_type in ['HiSeq 2500', 'HiSeq 3000', 'HiSeq X', 'NovaSeq6000']:
            subdir = 'Sample_' + smp_name
            self.metadata['path_of_sample_dir'] = os.path.join(self.metadata['delivery_basepath'],
                                                               project_dir, subdir)
        elif sequencer_type in ['MiSeq', 'NextSeq']:
            self.metadata['path_of_sample_dir'] = os.path.join(self.metadata['delivery_basepath'],
                                                               project_dir)
        elif sequencer_type in ['NovaSeqX']:
            subdir = 'fastq'
            self.metadata['path_of_sample_dir'] = os.path.join(self.metadata['delivery_basepath'],
                                                               project_dir, subdir)

        self.logger.debug("parsed path_of_sample_dir (%s) for %s",
                          self.metadata['path_of_sample_dir'],
                          self.metadata['name'])

    def add_reanalyse_setting(self):
        """whether Reanalyse

        :returns: add self.metadata['Reanalyse']
        None             : not reanalysis sample ,
        *                : reanalysis sample     , search old id/name, old project automatically
                                                   by matching leading 9 digits of this sample id
        11 digits number : reanalysis sample     , search old id/name, old project automatically
                                                   by matching leading 9 digits of this 11 digits
                                                   number

        """
        if FALLBACK:
            projectname_with_date = self._retrieve_attr('projectname_with_date')

            matched = re.match(r'(diag[-_])?Reanalyse', projectname_with_date, re.I)

            self.metadata['Reanalyse'] = bool(matched)
        else:
            reanalysis_old_sampleid = self._retrieve_attr('reana_oldid', optional=True)
            self.metadata['Reanalyse'] = reanalysis_old_sampleid

        self.logger.debug("add Reanalyse (%s) for %s",
                          self.metadata['Reanalyse'],
                          self.metadata['name'])

    def _querydb_reana_oldname_oldproj(self):
        """look up Clarity database for reanalysis old sample ID and project """

        capturekit = self.get_capturekit(required=True)

        # get old sample id/name and project
        if FALLBACK:
            oldname_oldproj_clarity = get_reanalysis_old_sample_name(
                session,
                self.metadata['name'],
                capturekit
            )
            oldname_oldproj_preclarity = get_preclarity_old_sample_name(
                session_sqlite,
                self.metadata['name']
            )

            return oldname_oldproj_clarity + oldname_oldproj_preclarity

        else:
            _reana = self.metadata['Reanalyse']
            if _reana.strip() == '?':
                oldname_oldproj_clarity = get_reanalysis_old_sample_name(
                    session,
                    self.metadata['name'],
                    capturekit
                )
                oldname_oldproj_preclarity = get_preclarity_old_sample_name(
                    session_sqlite,
                    self.metadata['name']
                )

                return oldname_oldproj_clarity + oldname_oldproj_preclarity

            else:
                # 11 digits id/fullname/project given in excel sheet
                match = re.match(r'(?P<sample_name>(?P<sample_id>(\d{11}|[A-Z]{2}\d{8})([A-Z]\d+)?)(-[\w-]*)?)'
                                 r'([, ]+(?P<project>[\w-]+))?$',
                                 _reana.strip())
                if not match:
                    raise RuntimeError('"{}" is not a valid "Reanalysis old sample id".'
                                       ''.format(_reana))

                the_name, the_id, the_project = match.group('sample_name', 'sample_id', 'project')

                oldname_oldproj_clarity = get_reanalysis_old_sample_name(
                    session,
                    the_id,
                    capturekit
                )
                oldname_oldproj_preclarity = get_preclarity_old_sample_name(
                    session_sqlite, the_id
                )

                all_queried = oldname_oldproj_clarity + oldname_oldproj_preclarity
                match_specified = []

                if the_name == the_id and not the_project:  # only id is specified
                    match_specified = all_queried
                else:  # more filtering with sample_name and/or project
                    for smp, proj in all_queried:
                        if the_name != the_id:  # sample_name is given
                            if the_project:
                                if smp == the_name and proj == the_project:  # match both
                                    match_specified.append((smp, proj))
                            elif smp == the_name:  # match sample_name only
                                match_specified.append((smp, proj))
                        elif proj == the_project:  # "id,project" specified, match project
                            match_specified.append((smp, proj))

                return match_specified

    def add_reanalyse_old_id_proj(self):
        """
        If reanalysis sample, add old sample id/name, old project
        If multiple matches in db, do further filtering, e.g. filter out test projects, test
        samples etc.
        Add self.metadata['ReanaOldName']
        Add self.metadata['ReanaOldID']
        Add self.metadata['ReanaOldProject']
        """

        if not self.metadata['Reanalyse']:
            return

        oldname_oldproj = self._querydb_reana_oldname_oldproj()

        # update metadata
        if len(oldname_oldproj) == 0:   # no match
            # if DEBUG:
            #     self.logger.debug('Skip export reanalysis %s due to 0 old id found',
            #                       self.metadata['name'])
            #     self.metadata['skip_export'] = True
            # else:
            raise RuntimeError("Could not find reanalysis old sample id for {0}"
                               "".format(self.metadata['name']))

        elif len(oldname_oldproj) == 1:  # ideally single match
            self._update_reana_metadata(oldname_oldproj[0])

        else:  # multiple matches
            # try to rescue by further filtering
            diag_samples = []
            for samp, proj in oldname_oldproj:
                ProjMatch = PROJ_MATCHER.match(proj)
                SampleMatch = SAMPLE_NAME_MATCHER.match(samp)
                if not ProjMatch:  # not a properly named diagnostics project
                    continue
                if not SampleMatch:  # not a properly named sample
                    continue
                if ProjMatch.group('proj_type').lower() not in REANALYSIS_ALLOWED_PROJ_TYPE:
                    continue
                if not ProjMatch.group('proj_number'):
                    continue
                if SampleMatch.group('capturekit') not in REANALYSIS_ALLOWED_CAPTUREKIT:
                    continue

                diag_samples.append([int(ProjMatch.group('proj_number')), (samp, proj)])

            if len(diag_samples) == 0:
                raise RuntimeError(
                    "Found 0 old id from multiple candidate old sample ids:\n{}".format(
                        '\n'.join([', '.join([n, p]) for n, p in oldname_oldproj])
                    )
                )
            elif len(diag_samples) == 1:
                self._update_reana_metadata(diag_samples[0][1])
            else:
                latest_one = sorted(diag_samples, reverse=True)[0][1]
                self._update_reana_metadata(latest_one)

    def _update_reana_metadata(self, old_name_proj):
        """
        update reanalysis metadata
        :old_name_proj: tuple of old sample name, old project name
        """
        fullname_smp, fullname_prj = old_name_proj
        matched = re.match(r'(?P<project>([dD]iag-)?[^-]*)(-\d+)?', fullname_prj)
        if matched:
            project = matched.group('project')
        else:
            raise RuntimeError('badly formatted project name: {0}'.format(fullname_prj))

        self.metadata['ReanaOldName'] = fullname_smp
        self.metadata['ReanaOldID'] = self.metadata['ReanaOldName'].split('-')[0]
        self.metadata['ReanaOldProjectWithDate'] = fullname_prj
        self.metadata['ReanaOldProject'] = project

        self.logger.debug("parsed ReanaOldName (%s) for %s",
                          self.metadata['ReanaOldName'],
                          self.metadata['name'])
        self.logger.debug("parsed ReanaOldID (%s) for %s",
                          self.metadata['ReanaOldID'],
                          self.metadata['name'])
        self.logger.debug("parsed ReanaOldProjectWithDate(%s) for %s",
                          self.metadata['ReanaOldProjectWithDate'],
                          self.metadata['name'])
        self.logger.debug("parsed ReanaOldProject(%s) for %s",
                          self.metadata['ReanaOldProject'],
                          self.metadata['name'])

    def _validate_reanalysis_parent(self, artifact):
        """
        a reanalysis parent sample should have a process "Finish protocol_diag"
        a reanalysis parent sample should not have a process "Bioinformatic processing_diag"
        """
        self.logger.debug('artifactididid is %s', artifact)
        can_be_reanalysis = False

        # all processes of sample
        procs = self.lims.get_processes(inputartifactlimsid=artifact)
        self.logger.debug('procpros is %s', procs)
        for p in procs:
            self.logger.debug('namename is %s', p.type.name)

        finished_protocol = any(
            [p for p in procs if p.type.name.startswith(FINISHED_PROTOCOL)]
        )
        if artifact == 'KIR1544A24PA1':
            finished_protocol = True
        failed_qc = any(
            [p for p in procs if p.type.name.startswith(FAILED_BIOINF_QC)]
        )

        if not finished_protocol:
            print("{} not finished_protocol".format(artifact))
        if failed_qc:
            print("{} faild_qc".format(artifact))

        if finished_protocol and not failed_qc:
            can_be_reanalysis = True

        return can_be_reanalysis

    def get_capturekit(self, required=False):
        """Get capturekit which tells the project type, e.g. excap, wgs, etc.
        For new analysis:
            1. Get capturekit directly from the "Kit version Diag"("capturekit") sample UDF.
        For reanalysis:
            1. Try to get capturekit directly from the "Kit version Diag"("capturekit") sample UDF.
            2. Optionally infer capturekit from the "Test SWL Diag"("swl_test") sample UDF:
               <swl_test>      ->  <capturekit>
               "WGS_TrioUtAv"  ->  "wgs" (EGG wgs)
               "WGS_Cardio"    ->  "wgs" (EHG wgs)
               "WES_TrioUtAv"  ->  "Av5" ("Av7" is never used in production)

               FYI, currently, there is neither targeted EHG reanalysis nor EKG reanalysis,
               for new analysis:
               "HTS_Cardio" -> "TSCarV1"
               "MAMMA" -> NA (For EKG projects, "Test SWL Diag" is the same as genepanel, so not
                              possible to infer capturekit)
        """
        # get from "Kit version Diag"("capturekit") sample UDF
        capturekit = self._retrieve_attr("capturekit", notEmpty=False, optional=True)

        # for reanalysis, also infer capturekit from "Test SWL Diag"("swl_test") sample UDF
        if not capturekit and self.metadata['Reanalyse']:
            swl_test = self._retrieve_attr("swl_test", notEmpty=False, optional=True)
            if swl_test:
                if swl_test.startswith("WGS_"):
                    capturekit = "wgs"
                elif swl_test.startswith("WES_"):
                    capturekit = "Av5"

        if not capturekit and required:
            raise RuntimeError('The capturekit is unknown, add it to "Kit version Diag".')
        else:
            return capturekit

    def get_reanalysis_parents(self):
        """ Find type 1 and type 2 trio reanalysis parents in Clarity DB.
        Type1 trio reanalysis:
            originally a trio;
            reanalysis the same trio with new genepanel;
            only proband(reanalysis) in lims exporter queue.

        Type2 trio reanalysis:
            originally a trio;
            another proband newly sequenced(in lims exporter queue);
            reanalysis parents.

        For a proband sample of a trio analysis, no matter if it is reanalysis or not, search for
        parent samples in Clarity DB:
        1) If no FM and/or MK sample(s) found, send proband to MR with msg "missing parent(s)".
        2) If unique match for FM and unique match for MK; and both FM and MK are valid
           reanalysis parents(finished workflow, no qc fail), return a dict.
        3) If unique match for FM and unique match for MK, but one or both are not valid
           reanalysis parents, raise exception.
        """
        famid = self.metadata['famid']
        capturekit = self.get_capturekit(required=True)
        mk_query = famid + 'MK'
        fm_query = famid + 'FM'

        # same famid FM and MK samples in Clarity, can be either old trio or new trio
        mk_artifact_samp_proj = get_parent_for_reanalysis(session, mk_query, capturekit)
        fm_artifact_samp_proj = get_parent_for_reanalysis(session, fm_query, capturekit)

        if not mk_artifact_samp_proj or not fm_artifact_samp_proj:
            msg = []
            if not mk_artifact_samp_proj:
                msg.append("missing MK sample in Clarity")
            if not fm_artifact_samp_proj:
                msg.append("missing FM sample in Clarity")
            raise RuntimeError('; '.join(msg))

        # check if MK is old trio
        _mk_artifact_limsid = mk_artifact_samp_proj[0]
        mk_can_be_reanalysis = self._validate_reanalysis_parent(_mk_artifact_limsid)
        if not mk_can_be_reanalysis:
            self.logger.info("%s should be proband of a new regular trio whose MK is %s(%s)",
                             self.metadata['name'],
                             mk_artifact_samp_proj[1],
                             mk_artifact_samp_proj[2])

        # check if FM is old trio
        _fm_artifact_limsid = fm_artifact_samp_proj[0]
        fm_can_be_reanalysis = self._validate_reanalysis_parent(_fm_artifact_limsid)
        if not fm_can_be_reanalysis:
            self.logger.info("%s should be proband of a new regular trio whose FM is %s(%s)",
                             self.metadata['name'],
                             fm_artifact_samp_proj[1],
                             fm_artifact_samp_proj[2])

        # both parents valid
        if mk_can_be_reanalysis and fm_can_be_reanalysis:
            reanalysis_parents = {'MK': mk_artifact_samp_proj[1:], 'FM': fm_artifact_samp_proj[1:]}
            self.logger.info("Fount reanalysis parents for %s:\n%s",
                             self.metadata['name'],
                             pprint.pformat(reanalysis_parents))
            return reanalysis_parents
        else:
            msg = []
            if not mk_can_be_reanalysis:
                msg.append("could not find MK of {} for reanalysis".format(self.metadata['name']))
            if not fm_can_be_reanalysis:
                msg.append("could not find FM of {} for reanalysis".format(self.metadata['name']))
            self.logger.info('; '.join(msg))  # parents are sequencing

            return None

    def add_lane(self):
        """lane

        :returns: add self.metadata['lane']

        """
        sequencer_type = self._retrieve_attr('sequencer_type')

        if sequencer_type in ['HiSeq 2500', 'HiSeq 3000', 'HiSeq X']:
            flowcell_lane_coord = self._retrieve_attr('flowcell_lane_coord')
            lane = flowcell_lane_coord[1].split(':')[0]
        elif sequencer_type in ['NovaSeq6000', 'NovaSeqX']:
            flowcell_lane_coord = self._retrieve_attr('flowcell_lane_coord', optional=True)
            # NovaSeq6000 Xp (flowcell SP, S1 and S2: 2 lanes; S4: 4 lanes)
            if flowcell_lane_coord:
                lane = flowcell_lane_coord[1].split(':')[0]
            # NovaSeq6000 Standard, no lane splitting
            else:
                lane = None
        elif sequencer_type in ['MiSeq']:
            lane = '1'
        elif sequencer_type in ['NextSeq']:
            lane = None

        self.metadata['lane'] = lane

        self.logger.debug("parsed lane (%s) for %s",
                          self.metadata['lane'],
                          self.metadata['name'])

    def add_index(self):
        """index

        :returns: add self.metadata['index']

        """
        reagent_labels = self._retrieve_attr('reagent_labels', optional=True)

        if not reagent_labels:
            # use Sample UDF "Index requested/used" instead if available
            index = self._retrieve_attr("requested_index", optional=True)
            if index:
                self.metadata["index"] = index
                self.logger.debug(
                    "retrieved index (%s) from submitted sample UDF "
                    "'Index requested/used' for %s",
                    self.metadata["index"],
                    self.metadata["name"],
                )
            else:
                # make index optional
                self.metadata["index"] = None
                self.logger.debug(
                    "reagent-label not found and 'Index requested/used' not provided for %s",
                    self.metadata["name"],
                )

            return

        if len(reagent_labels) != 1:
            raise RuntimeError("sample {0} from project {1} should have one and only one reagent "
                               "label".format(self.metadata['name'],
                                              self.metadata['projectname_with_date']))
        # <reagent-label name="SureSelect XT2 Index 05-E (CGCTGATC)"/>
        # <reagent-label name="48 N712-E505 (GTAGAGGA-GTAAGGAG)"/>
        else:
            label = reagent_labels[0]
            matched = re.match(r'.*\((?P<index>[ATGC-]+)\)$', label)
            index = matched.group('index')
            self.metadata['index'] = index

            self.logger.debug("parsed index (%s) for %s",
                              self.metadata['index'],
                              self.metadata['name'])

    def _format_perfect_index_reads_pct(self):
        """format perfect_index_reads_pct
        :returns: update self.metadata['perfect_index_reads_pct']

        """
        perfect_index_reads_pct = self._retrieve_attr('perfect_index_reads_pct')

        perfect_index_reads_pct = '{0:.2f}'.format(float(perfect_index_reads_pct))

        return perfect_index_reads_pct

    def _format_one_mismatch_index_pct(self):
        """get one_mismatch_index_pct
        :returns: update self.metadata['one_mismatch_index_pct']

        """
        one_mismatch_index_pct = self._retrieve_attr('one_mismatch_index_pct')
        one_mismatch_index_pct = '{0:.2f}'.format(float(one_mismatch_index_pct))

        return one_mismatch_index_pct

    def _format_mean_qual_score(self):
        """get mean_qual_score
        :returns: update self.metadata['mean_qual_score']

        """
        mean_qual_score = self._retrieve_attr('mean_qual_score')

        mean_qual_score = '{0:.2f}'.format(float(mean_qual_score))

        return mean_qual_score

    def _format_q30_bases_pct(self):
        """get q30_bases_pct
        :returns: update self.metadata['q30_bases_pct']

        """
        q30_bases_pct = self._retrieve_attr('q30_bases_pct')

        q30_bases_pct = '{0:.2f}'.format(float(q30_bases_pct))

        return q30_bases_pct

    def add_stats(self):
        """add stats dict to smp

        :returns: add self.metadata['stats'] (dict)

        """
        perfect_index_reads_pct = self._format_perfect_index_reads_pct()
        one_mismatch_index_pct = self._format_one_mismatch_index_pct()
        mean_qual_score = self._format_mean_qual_score()
        q30_bases_pct = self._format_q30_bases_pct()

        stats = {
            'reads': int(self.metadata['readsCount']),
            'perfect_index_reads_pct': float(perfect_index_reads_pct),
            'one_mismatch_index_pct': float(one_mismatch_index_pct),
            'q30_bases_pct': float(q30_bases_pct),
            'mean_qual_score': float(mean_qual_score),
        }

        self.metadata['stats'] = stats

        self.logger.debug("parsed stats (%s) for %s",
                          self.metadata['stats'],
                          self.metadata['name'])

    def _get_sample_read_file_md5(self, read_num):
        sequencer_type = self._retrieve_attr("sequencer_type")
        dir_md5sum = self._retrieve_attr("path_of_project_dir")
        # NovaSeq X
        if sequencer_type == "NovaSeqX":
            dir_md5sum = self._retrieve_attr(
                "path_of_sample_dir", optional=False, notEmpty=True
            )
        fastq_file = self._get_sample_read_filename(read_num)
        md5_file = os.path.join(dir_md5sum, MD5SUM_NAME)
        if not os.path.exists(md5_file):
            raise RuntimeError(
                "no such file {0} for {1}({2})".format(
                    md5_file, self.metadata["name"], self.metadata["project"]
                )
            )
        md5 = dict()
        found_md5sum = False
        fastq_exists = True
        with open(md5_file) as fd:
            for line in fd.xreadlines():
                if fastq_file in line:
                    found_md5sum = True
                    clean_line = line.rstrip()
                    if fastq_file in md5:
                        raise RuntimeError("Got multiple MD5 sum matches for same fastq: {}"
                                           "".format(fastq_file))
                    fastq_md5 = clean_line.split('  ', 1)[0]
                    fastq_relative_path = clean_line.split('  ', 1)[1]
                    if not os.path.exists(os.path.join(dir_md5sum, fastq_relative_path)):
                        # entry in md5sums.txt, but no fastqc file
                        fastq_exists = False
                        self.logger.warn(
                            "Found md5sum entry, but no file for {}: {}".format(
                                self.metadata['name'],
                                os.path.join(dir_md5sum, fastq_relative_path)
                            )
                        )
                        continue
                    md5['md5'] = fastq_md5
                    md5['path'] = os.path.join(dir_md5sum, fastq_relative_path)
                    break

            if not found_md5sum and FALLBACK:
                fastq_files = glob.glob(
                    os.path.join(
                        self.metadata["path_of_sample_dir"],
                        ".".join(
                            ["".join(["*", self.metadata["name"], "*"]), "fastq.gz"]
                        ),
                    )
                )
                for ff in fastq_files:
                    if read_num in os.path.basename(ff):
                        fastq_file = os.path.basename(ff)
                        break
                with open(md5_file) as fd:
                    for line in fd.xreadlines():
                        if fastq_file in line:
                            found_md5sum = True
                            clean_line = line.rstrip()
                            if fastq_file in md5:
                                raise RuntimeError(
                                    "Got multiple MD5 sum matches for same"
                                    " fastq: {}".format(fastq_file)
                                )
                            fastq_md5 = clean_line.split("  ", 1)[0]
                            fastq_relative_path = clean_line.split("  ", 1)[1]
                            md5["md5"] = fastq_md5
                            md5["path"] = os.path.join(dir_md5sum, fastq_relative_path)
                            break

            if not found_md5sum:
                raise RuntimeError(
                    "md5 for {0} is not found in {1}; "
                    "is this a blank sample(no reads)".format(fastq_file, md5_file)
                )

            if not fastq_exists:
                raise RuntimeError(
                    "fastq file {0} doesn't exist; "
                    "is this a blank sample(no reads)?".format(os.path.join(dir_md5sum, fastq_relative_path))
                )

        # Get file length
        files = {
            'path': fastq_file,
            'size': os.path.getsize(md5['path']),
            'md5': md5['md5'],
        }

        return files

    def add_sample_reads(self):
        """add reads info to ResultSample

        :returns: add self.metadata['reads']

        """
        self.metadata['reads'] = [
            self._get_sample_read_file_md5("R1"),
            self._get_sample_read_file_md5("R2")
        ]

        self.logger.debug("parsed reads (%s) for %s",
                          self.metadata['reads'],
                          self.metadata['name'])

    def add_path_of_qc_reports(self):
        """add qc_reports info to ResultSample

        :returns: add self.metadata['qc_reports']

        """

        # NovaSeqX skip qc reports
        sequencer_type = self._retrieve_attr('sequencer_type')

        if sequencer_type == 'NovaSeqX':
            qc_reports = []
        else:
            qc_reports = [ self._get_qc_report_filename("R1"), self._get_qc_report_filename("R2") ]

        self.metadata['qc_reports'] = qc_reports
        self.logger.debug("parsed qc_reports (%s) for %s",
                          self.metadata['qc_reports'],
                          self.metadata['name'])

    def add_fastqc_dirname(self):
        """add fastqc_paths info to ResultSample

        :returns: add self.metadata['fastqc_paths']

        """
        sequencer_type = self._retrieve_attr("sequencer_type")

        if sequencer_type == "NovaSeqX":
            fastqc_path = []
        else:
            fastqc_path = [
                self._get_fastqc_dirname("R1"),
                self._get_fastqc_dirname("R2"),
            ]

        self.metadata["fastqc_paths"] = fastqc_path
        self.logger.debug(
            "parsed fastqc_paths (%s) for %s",
            self.metadata["fastqc_paths"],
            self.metadata["name"],
        )

    def add_whether_exported(self):
        """whether a ResultSample has already been exported

        :returns: add self.metadata['already_exported']

        """
        sample_name = self.metadata['name']
        sample_dir = self._retrieve_attr('path_of_sample_dir')
        already_exported = False
        if (os.path.exists(os.path.join(sample_dir, EXPORTER_DONE_FILE)) or
                os.path.exists(os.path.join(sample_dir,
                                            '.'.join([sample_name, EXPORTER_DONE_FILE])))):
            already_exported = True

        self.metadata['already_exported'] = already_exported

        self.logger.debug("parsed already_exported (%s) for %s",
                          self.metadata['already_exported'],
                          self.metadata['name'])

    def update_reanalyse_old_capturekit_pedigree(self):
        """
        reanalysis sample's capturekit same as old
        """
        if not self.metadata['Reanalyse']:
            return

        old_name = self.metadata['ReanaOldName']
        old_proj = self.metadata['ReanaOldProjectWithDate']
        new_name = self.metadata['name']

        attrs = ['capturekit',
                 'famid',
                 'fam_member',
                 'gender']

        lookup = {
            'Kit version Diag': 'capturekit',
            'Sex Diag': 'gender',
            'Family number Diag': 'famid',
            'Relation Diag': 'fam_member',
            'Analysis type Diag': 'joint_analysis_type',  # not handled by SAMPLE_NAME_MATCHER
        }
        # 1st try to parse capturekit from old sample name (even when FALLBACK is OFF, due to for
        # old samples, famid, capturekit may be missing in API)
        matched = SAMPLE_NAME_MATCHER.match(old_name)

        if matched:
            for a in attrs:
                oldkey1 = 'ReanaOld' + a.capitalize()
                if not self.metadata.get(oldkey1):
                    self.metadata[oldkey1] = matched.group(a)  # give this priority

        # single or trio only by whether famid in sample name (for later usage)
        if not self.metadata.get('ReanaOldFamid'):
            self.metadata['ReanaOldSingleOrTrio'] = 'Single'
        else:
            self.metadata['ReanaOldSingleOrTrio'] = 'Trio'

        # 2nd get original capturekit from original sample's udf
        for s in self.lims.get_samples(projectname=old_proj):
            if s.name == old_name:
                udfs = dict(s.udf.items())
                for k, v in lookup.items():
                    oldkey2 = 'ReanaOld' + v.capitalize()
                    if not self.metadata.get(oldkey2):  # do not overwrite
                        self.metadata[oldkey2] = udfs.get(k)

        # 3rd add default for 'ReanaOldJoint_analysis_type'
        if not self.metadata.get('ReanaOldJoint_analysis_type'):
            self.metadata['ReanaOldJoint_analysis_type'] = None

        # reanalysis sample has no capturekit, safe to overwrite
        self.metadata['capturekit'] = self.metadata['ReanaOldCapturekit']
        # obligatory capturekit for analysis_type then for uses_taqman
        if not self.metadata['capturekit']:
            raise RuntimeError("Could not get original capturekit of {0} (Reana({1} {2})"
                               "".format(new_name, old_name, old_proj))

        # translate old gender
        self.metadata['ReanaOldGenderF'] = GENDERD.get(self.metadata.get('ReanaOldGender'))

        # set 'gender'
        if not self.metadata.get('gender'):
            self.metadata['gender'] = self.metadata['ReanaOldGender']
            self.metadata['genderF'] = self.metadata['ReanaOldGenderF']

        self.logger.debug("parsed old capturekit %s, famid %s, fam_member %s, gender %s, "
                          "joint_analysis_type %s single_or_trio %s of %s Reana (%s %s)",
                          self.metadata['capturekit'],
                          self.metadata['ReanaOldFamid'],
                          self.metadata['ReanaOldFam_member'],
                          self.metadata['ReanaOldGender'],
                          self.metadata['ReanaOldJoint_analysis_type'],
                          self.metadata['ReanaOldSingleOrTrio'],
                          new_name,
                          old_name,
                          old_proj
                          )
        # pedigree
        # After new lims sheet, 'Analysis type' is either of ['Single','Trio'](never missing)
        # Before new lims sheet, both single and trio sample can
        #    a) missing 'Analysist type'
        #    b) 'Analysis type' == 'exome'
        # After new lims sheet,  singles sample also have 'famid'

        if (self.metadata.get('ReanaOldJoint_analysis_type') and
                self.metadata['ReanaOldJoint_analysis_type'].lower() == 'trio' or
                self.metadata.get('ReanaOldSingleOrTrio') == 'Trio'):
            self._add_old_pedigree()

    def _add_old_pedigree(self):
        """
        self.metadata['ReanaOldPedigree']
        """
        self.metadata['ReanaOldPedigree'] = {
            FAM_MEMBERD[self.metadata['ReanaOldFam_member']]: {
                "gender": self.metadata["ReanaOldGender"]
            }
        }
        self.logger.debug("parsed old pedigree(%s) for %s (Reana %s)",
                          self.metadata['ReanaOldPedigree'],
                          self.metadata['name'],
                          self.metadata['ReanaOldName'])

    def _get_analysis_type(self):
        '''
        any of [exome, trio, target, genome or trio-excap]
        As infered from capture kit and family information in metadata dict
        Reanalyse sample's capturekit is from old sample
        '''
        anatype = None

        if self.metadata['capturekit'] in ['Av5', 'Av7', 'CREv2', 'TWHCV1']:
            if not self.metadata.get('pedigree'):
                anatype = 'exome'
            else:
                anatype = 'trio-excap'
        elif self.metadata['capturekit'] in ['TSOv1', 'TSOv2']:
            anatype = 'trio'
        elif self.metadata['capturekit'] in ['CuCaV1', 'CuCaV2', 'CuCaV3', 'TSCarV1']:
            anatype = 'target'
        elif self.metadata['capturekit'] in ['wgs']:
            if not self.metadata.get('pedigree'):
                anatype = 'genome'
            else:
                anatype = 'trio-genome'
        else:
            raise ValueError("Unknown capture kit {}!".format(self.metadata['capturekit']))

        self.logger.debug("parsed analysis_type (%s) for %s",
                          anatype,
                          self.metadata['name'])

        return anatype

    def add_uses_taqman(self):
        """
        need taqman?
        """
        # tumore samples no taqman
        if self.metadata.get('specialized_pipeline') == 'TUMOR':
            self.metadata['uses_taqman'] = False

            self.logger.debug("uses_taqman is False for TUMOR sample  %s",
                              self.metadata['name'])
            return

        # Blankprove no taqman
        if self.metadata['name'].lower().startswith('blankprove'):
            self.metadata['uses_taqman'] = False

            self.logger.debug("uses_taqman is False for blank sample  %s",
                              self.metadata['name'])
            return

        # other samples judge by analysis_type
        required = None

        analysis_type = self._get_analysis_type()

        if analysis_type in ['exome', 'genome']:
            required = True
        elif analysis_type in ['trio', 'trio-excap', 'trio-genome']:
            required = False
        elif analysis_type in ['target']:
            if self.metadata['capturekit'] in ['TSCarV1']:
                required = False
            elif self.metadata['capturekit'] in ['CuCaV1', 'CuCaV2', 'CuCaV3']:
                required = True
            else:
                raise ValueError("Unknown capture kit {} for analysis type {}!"
                                 "".format(self.metadata['capturekit'], analysis_type))
        else:
            raise ValueError("Unknown analysis type {}!".format(analysis_type))

        # If the 'Check Taqman Diag' column in SampleSheet has 'no', force not checking SNP fingerprinting in the pipeline
        # if 'yes', force checking SNP fingerprinting in the pipeline
        # If blank, follow the rules above to decide whether to check SNP fingerprinting or not
        if self.metadata.get('taqman_lab'):
            if self.metadata.get('taqman_lab').lower() == 'no':
                required = False
            elif self.metadata.get('taqman_lab').lower() == 'yes':
                required = True

        self.metadata['uses_taqman'] = required

        self.logger.debug("parsed uses_taqman (%s) for %s",
                          self.metadata['uses_taqman'],
                          self.metadata['name'])

    def _whitelisted_by(self, whitelist_file):
        """check if a sample is whitelisted, returns boolean"""

        whitelisted = False

        with open(whitelist_file, 'r') as wl:
            for line in wl:
                # ignore comment lines
                if line.strip().startswith('#'):
                    continue
                try:
                    ps_pattern = re.compile(line.strip())
                    ps = self.metadata['project'] + '-' + self.metadata['sample_id']
                    if ps_pattern.match(ps):
                        whitelisted = True
                        self.logger.debug('%s is whitelisted by %s', ps, whitelist_file)
                except Exception as e:
                    self.logger.warning('whitelist pattern %s in %s not compilable with error: %s'
                                        '', line, whitelist_file, repr(e))
        return whitelisted

    def add_platform(self):
        """ Pipeline should be run on NSC or TSD
        check conditions with priority low to high with overwriting

        :returns: add self.metadata['platform']
        """
        # 1. platform defaults to the 'default_platform' in config_file
        default = self.CONFIG['default_platform']
        platform = default

        # 2. due to limited NSC capacity, priority 1 samples run on TSD anyways (exlcude EKG and EHG)
        _project = self._retrieve_attr('project', notEmpty=True, optional=False)
        not_low_priority_nsc = not any(_project.lower().startswith(p)
                                       for p in LOW_PRIORITY_NSC_PIPELINE)
        if platform == 'NSC':
            priority = self._retrieve_attr('priority', notEmpty=False, optional=True)
            if (not priority
                    or (int(self.metadata['priority']) == 1 and not_low_priority_nsc)):
                platform = 'TSD'

        # 3. whitelist file has highest priority
        white_tsd = self._whitelisted_by(self.CONFIG['platform_whitelist_tsd'])
        white_nsc = self._whitelisted_by(self.CONFIG['platform_whitelist_nsc'])
        if white_tsd and white_nsc:
            platform = 'NSC' if default == 'TSD' else 'TSD'
            self.logger.warning('Sample is whitelisted both TSD and NSC, use %s', platform)
        elif white_tsd:
            platform = 'TSD'
        elif white_nsc:
            platform = 'NSC'

        # 4.1 reanalysis always 'TSD' regardless; fastq.gz may only on TSD
        # 4.2 Type2 trio reanalysis proband itself is not reanalysis, i.e. newly sequenced
        #     but still, platform should be 'TSD' due to that parents are reanalyses
        if self.metadata['Reanalyse'] or self.metadata.get('type2_trio_reanalysis_proband'):
            platform = 'TSD'

        # add metadata
        self.metadata['platform'] = platform

    def add_export_priority(self):
        """
        add export priority of a sample
        boost EKG and EHG sample priority 1 to 2, 2 to 3

        :returns: add self.metadata['export_priority']
        """

        project = self._retrieve_attr('project', notEmpty=True, optional=False)
        priority = self._retrieve_attr('priority', notEmpty=False, optional=True)
        try:
            priority = int(priority)
        except(TypeError, ValueError):
            priority = 1

        # boost priority
        if any(project.lower().startswith(p) for p in BOOST_EXPORT_PRIORITY):
            exp_priority = priority + 1 if priority < 3 else priority
        else:
            exp_priority = priority

        self.metadata['export_priority'] = exp_priority

    def check_sources(self):
        """
        check required files/folders exist
        """

        # Check for taqman data
        if self.metadata['uses_taqman']:
            if not TaqmanParser(self.metadata['taqman_source']).exists(self.metadata['sample_id']):
                raise RuntimeError("Found no taqman data for sample {}"
                                   "".format(self.metadata['sample_id']))

        # Check for fastq files
        for fastq in self.metadata['reads']:
            src_rd = os.path.join(self.metadata['path_of_sample_dir'], fastq['path'])
            if not os.path.exists(src_rd):
                raise RuntimeError("read file not exist: {}".format(src_rd))

        # Check for NSC QC report
        for qc_report in self.metadata['qc_reports']:
            src_qc = os.path.join(self.metadata['path_of_sample_dir'], qc_report)
            if not os.path.exists(src_qc):
                raise RuntimeError("qc_report file not exist: {}".format(src_qc))

        # Check for Fastqc files if "enabled"
        for fastqc_path in self.metadata['fastqc_paths']:
            src_fqc = os.path.join(self.metadata['path_of_project_dir'], QUALITY_CONTROL_DIR,
                                   '_'.join(['Sample', self.metadata['name']]), fastqc_path)
            if not os.path.exists(src_fqc):
                raise RuntimeError("fastqc_path file not exist: {}".format(src_fqc))

    def blankprove_check(self):
        """
        a sample is blank if its reads count < BLANK_MAX_READS; otherwise not blank
        normal sample should not be blank; sample literally named 'blankprove' should be blank

        OPTIONALLY add self.metadata['blankprove_not_blank'] = [True,False]
        OPTIONALLY add self.metadata['blankprove_is_blank'] = [True,False]
        """

        is_blankprove = False
        is_blank = False
        blankprove_not_blank = False
        blankprove_is_blank = False

        if self.metadata['name'].lower().startswith('blankprove'):
            is_blankprove = True

        if self.metadata['readsCount'] < BLANK_MAX_READS:
            is_blank = True

        if is_blankprove and not is_blank:
            blankprove_not_blank = True

        if is_blankprove and is_blank:
            blankprove_is_blank = True

        self.metadata['blankprove_not_blank'] = blankprove_not_blank
        self.metadata['blankprove_is_blank'] = blankprove_is_blank

    def low_reads_check(self):
        """
        If a sample's reads is lower than the predefined threshold, send the sample to Manger Review.
        """
        
        # Blankprove has much lower read count threshold and is checked separately
        if self.metadata['name'].lower().startswith('blankprove'):
            return

        readsCount = self._retrieve_attr('readsCount', notEmpty=False, optional=False)
        capturekit = self._retrieve_attr('capturekit', notEmpty=False, optional=False)

        if readsCount and capturekit:
            reads_threshold = LOW_READS_THRESHOLD.get(capturekit)
            if reads_threshold and readsCount < reads_threshold:
                self.logger.warning("%s has %s reads < %s reads threshold", self.metadata['name'], readsCount, reads_threshold)
                raise RuntimeError("too low sequencing throughput, {} reads < {} reads threshold".format(readsCount, reads_threshold))

    @property
    def parsed_name(self):
        if self._parsed_name is None:
            #
            # import re
            matched = SAMPLE_NAME_MATCHER.match(
                self.metadata['name']
                #
                # '16000346203-MigraForst-KIT-Av5'
            )
            #
            # print(matched.groupdict()['capturekit'])

            if matched:
                self._parsed_name = matched.groupdict()

        return self._parsed_name

    @property
    def CONFIG(self):
        if self._CONFIG is None:
            with open(self.metadata['config_file'], 'r') as cf:
                self._CONFIG = json.load(cf)
            try:
                assert 'default_platform' in self._CONFIG, (
                        "'default_platform' not in {}".format(self.metadata['config_file'])
                        )
                assert 'platform_whitelist_tsd' in self._CONFIG, (
                        "'platform_whitelist_tsd' not in {}".format(self.metadata['config_file'])
                        )
                assert 'platform_whitelist_nsc' in self._CONFIG, (
                        "'platform_whitelist_nsc' not in {}".format(self.metadata['config_file'])
                        )
                assert self._CONFIG['default_platform'] in ['NSC', 'TSD'], (
                        "unknown platform {} in {}".format(
                            self._CONFIG['default_platform'],
                            self.metadata['config_file']
                            )
                        )
            except AssertionError as e:
                self.logger.error('%s', e.args[0])
                raise AssertionError(e.args[0])

        return self._CONFIG

#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
#################
HTS LIMS exporter
#################

Exports samples ready for Bioinformatics pipeline from Clarity LIMS

Using Clarity API to get all meta data instead of parsing from the delivery files (file names/html)

The output (in the so called repository) is meant to be copied further upstream for pipeline
processing and then deleted (to be automated with TSD API)

The result will look like the following structure:

repo-analysis
   └── Diag-excap01-123456789-EEogPU-v02
       └── Diag-excap01-123456789-EEogPU-v02.analysis

repo-analysis for trio
   └── Diag-excap01-123456789-EEogPU-v02 (the sample id is the sample id of the proband)
       └── Diag-excap01-123456789-EEogPU-v02.analysis

repo-sample
    └── Diag-excap01-123456789
        ├── 123456789-EEogPU-v02-KIT-Av5_GCCAAT_L007_R1_001_fastqc
        ├── 123456789-EEogPU-v02-KIT-Av5_GCCAAT_L007_R2_001_fastqc
        ├── 123456789-EEogPU-v02-KIT-Av5_GCCAAT_L007_R1_001.fastq.gz
        ├── 123456789-EEogPU-v02-KIT-Av5_GCCAAT_L007_R2_001.fastq.gz
        ├── Diag-excap01-123456789.sample
        ├── Diag-excap01-123456789.taqman (if has)
        └── LIMS_EXPORT_DONE

Some things to note:

- All sample in Lims Exporter Queue of Bioinformatics Protocol in Clarity are to be exported
- If a sample fails to process for whatever reason, it is sent to Manager Review in Clarity Web
Interface.
- The fastq.gz files are hardlinked from the original.
- A LIMS_EXPORT_DONE file is created in the source sample dir to be backward compatible with
lims-exporter(the previous one). Re-export a sample to be handled in future.

"""


import datetime
import json
import os
import bdb
import sys
import logging
import argparse
import re
import time
import glob
from collections import defaultdict
import requests

# for testing
# set system enviroment LIMS_EXPORTER_DEBUG on CLI to run lims-exporter-api in DEBUG mode
# DEBUG mode has lower logging level
DEBUG = bool("LIMS_EXPORTER_DEBUG" in os.environ)
# set system enviroment LIMS_EXPORTER_STAGING on CLI to run lims-exporter-api in STAGING mode
# STAGING mode will not move samples in Clarity
STAGING = bool("LIMS_EXPORTER_STAGING" in os.environ)

# before the new Sample Sheet Template excle file is put in usage and all new UDFs and new
# ProcessType and new version Protocol and Workflow is created & tested, "fallback" to parsing any
# non-existing UDFs from sample name; monitor "Bioinformatics processing_diag" Step/Queue instead
# of "LIMS Exporter" Queue; before Lims Exporter step is added and auto-forward sample to next step
# is implemented, remove the EXPORTER_DONE_FILE can re-export a sample.
FALLBACK = False

# don't handle reanalysis yet
SKIP_REANALYSIS = False

# sleep time
SLEEP_TIME = 15 * 60  # 15 minutes

logFormatter = logging.Formatter("%(asctime)-25s %(name)-50s %(levelname)-10s %(message)s")
log = logging.getLogger('lims_exporterAPI')

logfileDir = os.path.join(sys.path[0], *(['..'] * 3 + ['logsAPI']))

try:
    os.makedirs(logfileDir)
except OSError:
    pass

if not len(log.handlers):  # avoid adding handlers twice
    fileHandler = logging.FileHandler(
        "{0}/{1}.log".format(logfileDir, datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')))
    fileHandler.setFormatter(logFormatter)
    fileHandler.setLevel(logging.DEBUG)
    log.addHandler(fileHandler)

    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logFormatter)
    consoleHandler.setLevel(logging.DEBUG)
    log.addHandler(consoleHandler)

    if DEBUG:
        log.setLevel(logging.DEBUG)
    else:
        log.setLevel(logging.INFO)

# HARD CODING VARIABLES
# when "tolkning av hts-data diag" protocol change to a new verson, "lims exporter" queue id
# changes. so ideally, queue id should be searched on the fly by traversing all protocols and
# then its steps and do regex matchings to find the 'acitve' queue id , but to save script
# running time and protocol version doesn't change often, we hard code queue id, and update it
# when protocol version change(rare event)
LIMSQUEUEID = '1603'  # "Lims exporter" step

QUALITY_CONTROL_DIR = 'QualityControl'
MD5SUM_NAME = 'md5sum.txt'

PROJECT_READY = 'READY'
EXPORTER_DONE_FILE = 'LIMS_EXPORT_PROCESSED'
DONE_FILE = 'LIMS_EXPORT_DONE'

FAM_MEMBERD = {
    'F': 'father',
    'M': 'mother',
    'P': 'proband'
}

GENDERD = {
    'K': 'female',
    'M': 'male'
}

CAPTUREKIT_MAP = {
    'Av5': 'agilent_sureselect_v05',
    'TSOv1': 'illumina_trusightone_v01',
    'TSOv2': 'illumina_trusightone_v02',
    'CREv2': 'agilent_cre_v02',
    'TWHCV1': 'twist_humancoreexome_v01'
}

PROJECT_RE = re.compile(r'^Diag-(?P<project>.*)-(?P<project_date>\d+-\d+-\d+)$', re.I)

# Type 1 reanalyse project, e.g. Diag-Reanalyse-2020-03-26
TYPE1_REANA_PROJ_RE = re.compile(r'^Diag-Reanalyse-(?P<project_date>\d+-\d+-\d+)$', re.I)

SPECIAL_TAG_HURTIGGENOM = 'Dragen'

DRAGEN_SUFFIX = 'DR'

CUSTOM_GENEPANEL_NO_ANNOPIPE = ['Custom', 'Baerer']

PRIORITY_MAP = {3: 'urgent', 2: 'high', 1: 'normal'}

NSC_EXPORTER_ACTIVE = '/boston/diag/transfer/sw/NSC-EXPORTER-ACTIVE'
NSC_EXPORTER_STOPPED = '/boston/diag/transfer/sw/NSC-EXPORTER-STOPPED'

# throttled expoerting of normal-TSD-pipeline samples
NORMAL_EXPORT_WEIGHT_LIMIT = 42
NORMAL_EXPORT_WEIGHT_INDEX = {'wgs':10, 'excap': 2, 'reanalyse': 0}  # EKG,EHG have boosted priorities: 1 to 2 , 2 to 3

PLATFORMS = ['NSC', 'TSD']

def get_panel_versions(genepanels_path):
    """
    read overview file to find the version of all panels

    :returns: dict {name: version}

    """
    if not os.path.exists(genepanels_path):
        raise RuntimeError("File {0} don't exist".format(genepanels_path))

    with open(genepanels_path) as f:
        panels = json.load(f)
        gp = defaultdict(list)
        for k, v in panels.iteritems():
            gp_name, gp_version = k.split('_')

            if 'inactive' in v:  # ignore this panel
                continue

            gp[gp_name].append(gp_version)

        # should be only one entry, but we'll sort it anyway
        return {k: sorted(v)[-1] for k, v in gp.iteritems()}


def get_q30_threshold(vcpipe_path):
    """
    get capturekit based q30 QC thresholds
    """
    CAP_Q30 = dict()
    capture_config_s = glob.glob(os.path.join(vcpipe_path, 'config', 'analysis', '*.json'))
    for config in capture_config_s:
        capturekit = os.path.splitext(os.path.basename(config))[0]
        with open(config) as cf:
            conf_dict = json.load(cf)
            try:
                if 'basepipe' in conf_dict:
                    # wgs.json has two keys: 'qc' and 'qc_dragen', but both have the same Q30
                    # threshold
                    q30 = conf_dict['basepipe']['qc']['CollectQualityYieldMetrics']['PCT_Q30_BASES'][0]*100
                else:
                    q30 = conf_dict['qc']['CollectQualityYieldMetrics']['PCT_Q30_BASES'][0]*100

                CAP_Q30[capturekit] = q30
            except KeyError:
                pass

    return CAP_Q30


import analysisCreatorAPI
import sampleCreatorAPI
import sampleWatcherAPI
import sampleMoverAPI


def general_handler(expt_cls, expt_inst, expt_tb):
    """
    handle uncaught exceptions
    """
    if expt_cls is bdb.BdbQuit:
        sys.exit(1)
    elif issubclass(expt_cls, KeyboardInterrupt):
        sys.exit("Lims exporter was manually stopped by user.")

    log.error("An error occured while checking for new samples",
              exc_info=(expt_cls, expt_inst, expt_tb))


def export(filtered_sw, priority, target_area, repo_export, update_clarity=True):
    """
    We get one sample at a time, but our goal is to get complete trios.
    To do this we store the family id (famid) and pedigree of each incoming sample.
    When we have a complete set for a given family id, we can start exporting the whole set.
    """

    if not filtered_sw:
        return

    fams = dict()

    exported_samps = {}

    export_executed = False

    remove_workflow_samples = {}

    for sample in filtered_sw:

        try:
            # SampleCreator
            sc = sampleCreatorAPI.SampleCreator(sample, repo_export, target_area, taqman_source)
            if (not sample['sample'].get('Reanalyse')
                    and not sample['sample'].get('blankprove_is_blank')):
                sc.create()
            else:
                log.info('Skipping SampleCreator for %s(old: %s) due to Reanalysis',
                         sample['sample']['name'], sc.sample_name)

            # AnalysisCreator - Single
            if 'pedigree' not in sc.metadata:
                # skip creating analysis metadata for blankprove which is blank
                if not sc.metadata.get('blankprove_is_blank'):
                    log.debug(sc.sample_name)
                    ac = analysisCreatorAPI.SingleAnalysisCreator(
                        repo_export, target_area, sc.sample_name, sc.metadata
                    )
                    # skip creating analyses folder at Transfer area for NSC pipeline samples
                    platform = sc.metadata['platform']
                    if platform != 'NSC' or target_area != 'Transfer':
                        ac.write_to_file()
                    else:
                        log.info(
                            "Skip writing analyses-work folder for %s pipeline sample %s at %s area.",
                            platform, sc.sample_name, target_area)

                    exported_samps[
                        sc.metadata['project'] + '-' + sc.metadata['name']
                    ] = {"artifacts": [sc.metadata['artifact']]}
                else:
                    remove_workflow_samples[
                        sc.metadata['project'] + '-' + sc.metadata['name']
                    ] = {"artifacts": [sc.metadata['artifact']]}

            # AnalysisCreator - Trio
            else:
                sc_ped = sc.metadata['pedigree'].keys()[0]
                if sc.metadata['famid'] not in fams:
                    log.debug("Creating new fam entry for %s (%s)",
                              sc.sample_name,
                              sc.metadata['famid'])
                    fams[sc.metadata['famid']] = {
                        "pedigree": {
                            sc_ped: {
                                "gender": sc.metadata['genderF'],
                                "sample": sc.sample_name
                            }
                        },
                        "metadata": {
                            sc_ped: sc.metadata
                        }
                    }
                else:
                    log.debug("Adding member %s to fam entry %s",
                              sc.sample_name,
                              sc.metadata['famid'])
                    fams[sc.metadata['famid']]['pedigree'][sc_ped] = {
                        "gender": sc.metadata['genderF'],
                        "sample": sc.sample_name
                    }
                    fams[sc.metadata['famid']]['metadata'][sc_ped] = sample['sample']

                # normal Trio complete
                if (len(fams[sc.metadata['famid']]['pedigree']) == 3
                        or sc.metadata.get('old_trio_analysis_path')):
                    log.debug("Got full trio: {}".format(sc.metadata['famid']))
                    for ped in fams[sc.metadata['famid']]['metadata'].keys():
                        ac = analysisCreatorAPI.TrioAnalysisCreator(
                            repo_export,
                            target_area,
                            fams[sc.metadata['famid']]['pedigree'][ped]['sample'],
                            fams[sc.metadata['famid']]['metadata'][ped],
                            fams[sc.metadata['famid']]['pedigree']
                        )
                        # skip creating analyses folder at Transfer area for NSC pipeline samples
                        platform = fams[sc.metadata['famid']]['metadata'][ped]['platform']
                        if platform != 'NSC' or target_area != 'Transfer':
                            ac.write_to_file()
                        else:
                            log.info(
                                "Skip writing analyses-work folder(s) for %s pipeline sample %s at %s area.",
                                platform, sc.sample_name, target_area)

                        exported_samps[fams[sc.metadata['famid']]['metadata'][ped]['name']] = {
                            "artifacts": [fams[sc.metadata['famid']]['metadata'][ped]['artifact']]}

                    fams.pop(sc.metadata['famid'])
                    log.debug("Finished processing trio for famid %s",
                              sc.metadata['famid'])
        except Exception as e:
            log.exception("Error occured while creating sample/analysis for sample "
                          "data %s", str(sample))
            limsq.fail_sample(
                sample['sample']['projectname_with_date'] + '-' + sample['sample']['name'],
                str(e)
            )
            continue
    # }}}

    if fams:  # {{{
        # waiting for other trio members
        log.debug("Unprocessed trio samples remaining: %s",
                  sum([len(x['pedigree']) for x in fams.values()]))
        for fid, smp_meta in [(fid, fams[fid]['metadata'][ped]) for fid in fams
                              for ped in fams[fid]['metadata']]:
            comment = ("Partial trio in this batch: only {} samples found for fam:{},"
                       " waiting for other member(s)".format(len(fams[fid]['pedigree']), fid))

            # samp_rs = [rs for rs in limsq.result_samples
            #            if rs.metadata['name'] == smp_meta['name'] and
            #            rs.metadata['project'] == smp_meta['project']][0]

            # wait instead of fail
            log.debug(comment)
            # limsq.fail_sample(samp_rs, comment)
    # }}}

    # update export_executed
    if exported_samps:
        export_executed = True

    if exported_samps and update_clarity:
        mover = sampleMoverAPI.SampleMover(limsq.lims, exported_samps)
        # import pdb; pdb.set_trace()
        if not STAGING:
            mover.advance(LIMSQUEUEID)
        log.info("Finished advancing %s %s priority samples in Clarity:\n%s",
                 len(exported_samps),
                 priority,
                 '\n'.join(exported_samps.keys())
                 )
        exported_samps = []

    if remove_workflow_samples and update_clarity:
        mover = sampleMoverAPI.SampleMover(limsq.lims, remove_workflow_samples,
                                           advance_action='remove')
        # import pdb; pdb.set_trace()
        if not STAGING:
            mover.advance(LIMSQUEUEID)
        log.info("Finished remove %s export samples from workflow in Clarity:\n%s",
                 len(remove_workflow_samples),
                 '\n'.join(remove_workflow_samples.keys())
                 )
        remove_workflow_samples = []

    return export_executed


def manager_review_faild_samples():
    """
    send failed samples to MR
    """
    if len(limsq.failed_samples) > 0:
        mover = sampleMoverAPI.SampleMover(limsq.lims, limsq.failed_samples)
        if not STAGING:
            mover.escalate(LIMSQUEUEID)
        log.info("Finished sending %s failed export samples to Manager Review in Clarity:\n%s",
                 len(limsq.failed_samples),
                 '\n'.join(limsq.failed_samples.keys())
                 )


def export_all():
    """
    main export function
    """

    watcher_samples = limsq.poll()
    watcher_samples = list(watcher_samples)

    # nothing to export
    if not watcher_samples:
        # send failed samples to MR
        manager_review_faild_samples()

        log.info("Nothing to export!")
        return

    # filter out partial trios
    fams = defaultdict(int)
    for s in watcher_samples:
        if s['sample'].get('famid', None):
            fams[s['sample'].get('famid')] += 1

    # all partial trios, to be logged at the end
    partial_trios = defaultdict(list)

    for s in watcher_samples[:]:
        famid = s['sample'].get('famid', None)
        analysis_type = s['sample']['joint_analysis_type']
        if famid and analysis_type.lower() == 'trio' and fams[famid] < 3:
            partial_trios[famid].append(s['sample']['name'])
            watcher_samples.remove(s)

    # record probands' platform and priority
    proband_platform_priority = defaultdict(dict)

    for sample in watcher_samples:
        if ('pedigree' in sample['sample'] and
                sample['sample']['pedigree'].keys()[0] == 'proband'):  # a proband
            famid = sample['sample'].get('famid')
            oldfamid = sample['sample'].get('ReanaOldFamid')
            famid = famid if famid else oldfamid

            platform = sample['sample']['platform']
            expt_priority = sample['sample']['export_priority']

            proband_platform_priority[famid] = {
                    'platform': platform,
                    'export_priority': expt_priority
                    }

    # uniform parents platform and export_priority
    for sample in watcher_samples:
        if ('pedigree' in sample['sample'] and
                sample['sample']['pedigree'].keys()[0] != 'proband'):
            famid = sample['sample'].get('famid')
            oldfamid = sample['sample'].get('ReanaOldFamid')
            famid = famid if famid else oldfamid
            if famid and famid in proband_platform_priority:
                sample['sample']['platform'] = \
                    proband_platform_priority[famid]['platform']
                sample['sample']['export_priority'] = \
                    proband_platform_priority[famid]['export_priority']

    # grouping by platform (NSC/TSD) and priority(urgent/high/normal)
    ordered_export = defaultdict(lambda: defaultdict(list))
    for sample in watcher_samples:
        # skip reanalysis
        if SKIP_REANALYSIS and sample['sample']['Reanalyse']:
            continue

        # skip low priority samples
        if export_priorities:
            if ('pedigree' in sample['sample'] and
                    sample['sample']['pedigree'].keys()[0] != 'proband'):  # trio parents
                p = sample['sample']['export_priority']  # proband uniformed priority
            else:
                p = sample['sample']['priority']   # original priority

            if str(p) not in export_priorities:
                log.debug("Skipping non priority sample %s (%s)",
                          sample['sample']['name'],
                          sample['sample'].get('priority'))
                continue

        platform = sample['sample'].get('platform')
        expt_priority = sample['sample'].get('export_priority')
        ordered_export[platform][PRIORITY_MAP[expt_priority]].append(sample)

    log.debug("Finished looping through all %s samples in the queue", len(limsq.export_samples))

    def _count_queued(priorities):
        sum = 0
        for pl in PLATFORMS:
            if pl in ordered_export:
                for pr in priorities:
                    if pr in ordered_export[pl]:
                        sum += len(ordered_export[pl][pr])
        return sum

    # export NSC-urgent, NSC-high, NSC-normal to Production area
    for pr in ['urgent', 'high', 'normal']:
        export(ordered_export['NSC'][pr],
               pr,
               'Production',
               repo_nsc_pipeline,
               update_clarity=False)

    # export nsc-urgent, nsc-high, tsd-urgent, tsd-high samples to transfer area
    high_priority_export_executed = False
    for pl in PLATFORMS:
        for pr in ['urgent', 'high']:
            if export(ordered_export[pl][pr],
                      pr,
                      'Transfer',
                      repo_export,
                      update_clarity=True):
                high_priority_export_executed = True

    # throttled exporting of normal samples to transfer area when nsc-exporter idle and did not
    # export any high priority samples this run
    nsc_exporter_busy = os.path.exists(NSC_EXPORTER_ACTIVE)
    nsc_exporter_stopped = os.path.exists(NSC_EXPORTER_STOPPED)
    if not nsc_exporter_busy and not nsc_exporter_stopped and not high_priority_export_executed:
        sum_weight = 0
        # collect batch
        scheduled_samples = []
        for pl in PLATFORMS:
            if ordered_export[pl]['normal']:
                # sort by famid to export trio together
                sort_normals = sorted(ordered_export[pl]['normal'],
                                      key=lambda s : s['sample']['famid']
                                      if s['sample'].get('famid')
                                      else s['sample'].get('ReanaOldFamid', '99999'))
                for ns in sort_normals:
                    for k in NORMAL_EXPORT_WEIGHT_INDEX:
                        if ns['sample']['project'].lower().startswith('diag-' + k):
                            sum_weight += NORMAL_EXPORT_WEIGHT_INDEX[k]
                            if sum_weight > NORMAL_EXPORT_WEIGHT_LIMIT:
                                break
                            else:
                                scheduled_samples.append(ns)
                    else:
                        continue
                    break
                else:
                    continue
                break

        # export batch
        export(scheduled_samples,
               'normal',
               'Transfer',
               repo_export,
               update_clarity=True)
    else:
        _queued_normals = _count_queued(['normal'])
        if _queued_normals > 0:
            log.info("nsc-exporter busy or just queued high priority samples;"
                     " having %s low priority samples on hold.", _queued_normals)

    # send failed samples to MR
    manager_review_faild_samples()

    if partial_trios:
        msg = '\n\t'.join(
            ["{}(only {} members):".format(k, len(v)) + "\n\t\t" + '\n\t\t'.join(v)
             for k, v in partial_trios.items()])
        log.warning("skipped partial trios:\n\t{}".format(msg))


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Exports and converts samples from sequencing"
                                     " centre into samples/analysis ready for use with automation"
                                     " system.")
    parser.add_argument("--config-file", action="store", dest="config_file",
                        required=True, help="Path to configuration file.")
    parser.add_argument("--repo-export", action="store", dest="repo_export",
                        required=True, help="Path to output dir for samples and analyses-work.")
    parser.add_argument("--repo-nsc-pipeline", action="store", dest="repo_nsc_pipeline",
                        required=True, help="Path to NSC pipeline samples and analyses-work folders.")
    parser.add_argument("--source", action="store", dest="source", required=True,
                        help="Path to root for delivered raw samples from sequencing.")
    parser.add_argument("--genepanels", action="store", dest="genepanels",
                        required=True, help="Path to json file with genepanel overview")
    parser.add_argument("--vcpipe", action="store", dest="vcpipe_dir",
                        required=True, help="Path to root of the vcpipe repo")
    parser.add_argument("--taqman-source", action="store", dest="taqman_source",
                        required=True, help="Path to root for raw taqman files.")
    parser.add_argument("--killfile", required=True, help="Path to killfile")
    parser.add_argument("--mark-ready", action="store_true", dest="mark_ready",
                        required=False, help="Mark samples/analysis with READY file.")
    parser.add_argument("-l", action="store_true", dest="looping",
                        help="""run infinitely by looping""")
    parser.add_argument("--projects", required=False,
                        help="""comma separated list of project names in the lims exporter queue
                        to export. If --samples is not specified, export all samples of these
                        projects that are queued""")
    parser.add_argument("--samples", required=False,
                        help="comma separated list of sample names in the lims exporter queue")
    parser.add_argument("--priorities", required=False,
                        help="comma separated list of priority levels")

    args = parser.parse_args()

    config_file = args.config_file
    repo_export = args.repo_export
    repo_nsc_pipeline = args.repo_nsc_pipeline
    taqman_source = args.taqman_source
    genepanels = args.genepanels
    vcpipe_dir = args.vcpipe_dir
    export_projects = args.projects
    export_samples = args.samples
    export_priorities = args.priorities
    looping = args.looping
    killfile = args.killfile

    sys.excepthook = general_handler

    for path in [config_file, repo_export, repo_nsc_pipeline]:
        if not os.path.exists(path):
            raise RuntimeError("Path doesn't exist: {}".format(path))

    source_path = args.source

    if export_priorities:
        export_priorities = export_priorities.split(',')

    # check for killfile at starting up
    if os.path.exists(killfile):
        sys.exit("killfile exists, can't start lims exporter.")

    panel_versions = get_panel_versions(genepanels)
    qc_q30_thresholds = get_q30_threshold(vcpipe_dir)

    def get_lims_queue():
        lims_queue = sampleWatcherAPI.LimsQueue(
            LIMSQUEUEID,
            panel_versions,
            qc_q30_thresholds,
            config_file,
            source_path,
            taqman_source,
            export_projects,
            export_samples
         )
        return lims_queue

    # initialize limsq
    limsq = None

    # run once
    try:
        limsq = get_lims_queue()
        export_all()
    except requests.exceptions.Timeout as e:
        log.error("HTTP Timeout error: %s", e)
    except requests.exceptions.RequestException as e:
        log.error("Other HTTP error: %s", e)
    finally:
        del limsq

    # run continuously
    while True:
        # check for killfile after an iteration
        if os.path.exists(killfile):
            log.error("Lims exporter stopped by killfile.")
            break
        if looping:
            # initialize limsq
            limsq = None

            log.info("sleeping ...")
            time.sleep(SLEEP_TIME)
            try:
                limsq = get_lims_queue()
                export_all()
            except requests.exceptions.Timeout as e:
                log.error("HTTP Timeout error: %s", e)
            except requests.exceptions.RequestException as e:
                log.error("Other HTTP error: %s", e)
            finally:
                del limsq
        else:
            break

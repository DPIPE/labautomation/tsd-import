#!/usr/bin/env python

import argparse
import datetime
import json
import logging
import os
import subprocess
import sys

import requests

# Long lived api key. This key must be updated yearly according to procedure
with open("/boston/diag/transfer/sw/lims_exporter_api.config") as cff:
    config = json.load(cff)
    API_KEY_FILE = config["apikey_file"]
with open(API_KEY_FILE) as kf:
    API_LONG_LIVED_KEY = kf.readline().strip()

SETTINGS = {
    "api_end_point": "https://alt.api.tsd.usit.no/v1/p22/auth/basic/token",
    "headers": {"Authorization": " ".join(["Bearer", API_LONG_LIVED_KEY])},
    "params": {("type", "s3import")},
}


def setup_logger(logdir, debug):
    logFormatter = logging.Formatter("%(asctime)s %(name)s %(levelname)s %(message)s")
    log = logging.getLogger(__name__)

    try:
        os.makedirs(logdir)
    except OSError:
        pass

    # one log file per month
    fileHandler = logging.FileHandler(
        "{0}/{1}-{2}.log".format(
            logdir,
            "nsc-exporter",
            datetime.date.today().replace(day=1).strftime("%Y-%m-%d"),
        )
    )
    fileHandler.setFormatter(logFormatter)
    fileHandler.setLevel(logging.DEBUG)
    log.addHandler(fileHandler)

    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logFormatter)
    consoleHandler.setLevel(logging.DEBUG)
    log.addHandler(consoleHandler)

    if debug:
        log.setLevel(logging.DEBUG)
    else:
        log.setLevel(logging.INFO)

    return log


def getShortLivedToken(log):
    # call API
    response = requests.post(
        SETTINGS["api_end_point"],
        headers=SETTINGS["headers"],
        params=SETTINGS["params"],
    )
    # success
    if response.ok:
        try:
            token_json = response.json()
            token = token_json["token"]
            log.debug("get short lived token OK!")
        except KeyError:
            log.error("No short lived token is generated!")
            sys.exit(1)
    # fail
    else:
        log.error("API call to get short lived token FAILED!")
        sys.exit(1)

    return token


def main():
    parser = argparse.ArgumentParser(
        description="Using S3API to synchronize a source folder on NSC to a target bucket on TSD"
    )
    parser.add_argument(
        "--source-dir",
        action="store",
        dest="source_dir",
        required=False,
        help="Source dir to be synchronized to a target bucket",
    )
    parser.add_argument(
        "--source-file",
        action="store",
        dest="source_file",
        required=False,
        help="Source file to be put into a target bucket",
    )
    parser.add_argument(
        "--dest-bucket",
        action="store",
        dest="dest_bucket",
        required=False,
        help="Target bucket to synchronize folder or put file into.",
    )
    parser.add_argument(
        "--source-bucket",
        action="store",
        dest="source_bucket",
        required=False,
        help="Source bucket to get a file from",
    )
    parser.add_argument(
        "--action",
        action="store",
        required=True,
        help="Action, any of [get, put, sync]; also accept a special action: log",
    )
    parser.add_argument(
        "--log-level",
        dest="log_level",
        required=False,
        help="log level",
    )

    parser.add_argument(
        "--log-message",
        dest="log_message",
        required=False,
        help="log message",
    )

    parser.add_argument(
        "--logdir",
        required=True,
        help="Directory to store log files",
    )
    parser.add_argument(
        "--debug",
        action="store_true",
        required=False,
        help="debug mode",
    )

    args, extra_args = parser.parse_known_args()

    source_dir = args.source_dir
    source_file = args.source_file
    dest_bucket = args.dest_bucket
    source_bucket = args.source_bucket
    action = args.action
    log_level = args.log_level
    log_message = args.log_message
    logdir = args.logdir
    debug = args.debug

    log = setup_logger(logdir, debug)
    if action == "log":
        if log_level.lower() == "debug":
            log.debug(log_message)
        elif log_level.lower() == "warning":
            log.warning(log_message)
        elif log_level.lower() == "error":
            log.error(log_message)
        else:  # default to info, don't error out
            log.info(log_message)

        return

    elif action == "get":
        try:
            assert source_bucket
            assert source_file
            _cmd = ["get", "s3://" + source_bucket + source_file]
        except AssertionError:
            log.error(
                "missing argument(s) %s and/or %s %s action",
                "--source-bucket",
                "--source_file",
                action,
            )
            sys.exit(1)
    elif action == "sync":
        try:
            assert source_dir
            assert dest_bucket
            # _cmd = ['--cache-file=' + source_dir + '_CACHE_FILE', 'sync', source_dir, 's3://' + dest_bucket]
            _cmd = ["--no-check-md5", "sync", source_dir, "s3://" + dest_bucket]

        except AssertionError:
            log.error(
                "missing argument(s) %s and/or %s for %s action",
                "--source-dir",
                "--dest-bucket",
                action,
            )
            sys.exit(1)
    elif action == "put":
        try:
            assert source_file
            assert dest_bucket
            _cmd = ["put", source_file, "s3://" + dest_bucket]
        except AssertionError:
            log.error(
                "missing argument(s) %s and/or %s for %s action",
                "--source-file",
                "--dest-bucket",
                action,
            )
            sys.exit(1)
    elif action == "put-recursive":
        try:
            assert source_dir
            assert dest_bucket
            _cmd = ["put", "--recursive", source_dir, "s3://" + dest_bucket]
        except AssertionError:
            log.error(
                "missing argument(s) %s and/or %s for %s action",
                "--source-file",
                "--dest-bucket",
                action,
            )
            sys.exit(1)
    else:
        log.error("Unrecognized action %s", action)
        sys.exit(1)

    token = getShortLivedToken(log)

    base_cmd = [
        "s3cmd",
        "--multipart-chunk-size-mb=200",
        "--add-header=token:" + token,
        "--no-delete-removed",
        "--no-preserve",
        "--add-header=x-project-select:p22",
        "--stop-on-error",
    ]

    if debug:
        debug_cmd = ["--debug", "--verbose", "--stat"]
    else:
        debug_cmd = []

    cmd = base_cmd + debug_cmd + extra_args + _cmd

    log.debug("COMMAND: %s", cmd)

    try:
        sync_process = subprocess.Popen(
            cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            universal_newlines=True,
        )

        # stream to log file
        while True:
            line = sync_process.stdout.readline()
            if not line:
                break
            log.info(line.rstrip())

        output = sync_process.communicate()[0]

        if output:
            log.info("\n%s", output)
        rc = sync_process.returncode
        if rc == 64:
            log.warning("s3cmd SKIPPED %s", " ".join(_cmd))
        if rc != 0 and rc != 64:
            log.error("s3cmd FAILED %s", " ".join(_cmd))
            sys.exit(1)
    except (ValueError, OSError) as err:
        log.error("Popen error for:\n\t%s\ndue to:\n\t%s", " ".join(_cmd), err)
        sys.exit(1)


if __name__ == "__main__":
    main()

#!/bin/bash

cat > Dockerfile <<'EOF'
FROM ubuntu:bionic

RUN apt-get update \
    && apt-get install -y python3-pip python3-dev \
    && cd /usr/local/bin \
    && ln -s /usr/bin/python3 python \
    && pip3 install https://github.com/unioslo/tsd-s3cmd/archive/v0.1.0.zip

ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8
EOF

docker build -t local/tsd-s3cmd:latest .

singularity build tsd-s3cmd.sif docker-daemon://local/tsd-s3cmd:latest

rm Dockerfile

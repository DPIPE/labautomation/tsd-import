#!/usr/bin/env bash

set -euf -o pipefail

# Package project interpretations files (typically Excel and PDF) for transfer to Vali/Sleipnir

REDHAT_PLATFORM="7.7"

# ousamg-ENV-START
TARGET_HOST=tsd
SOURCE_FOLDER=/tsd/p22/data/durable/production/ella/ella-prod/data/analyses/incoming
TARGET_FOLDER=/tsd/p22/fx/export_alt
TIMESTAMP_FILE=/tmp/archive-time-window
# ousamg-ENV-END


function cleanup() {
	rm -f ${TIMESTAMP_FILE}
}

function usage() {
    echo ""
    echo "Usage:"
    echo "  $(basename ${BASH_SOURCE[0]}) [-l -h] <HHMM> [0123]"
    echo ""
    echo "Tar files created after a specific point in time."
    echo "0 (default) picks files created today at HHMM"
    echo "1 picks files created since yesterday at HHMM"
    echo ""
    echo "If option '-l' is given, only list the files that would be tar'ed; don't create a tar file"
    echo "If option '-h' is given, show help"
}

function self_test() {
	cat /etc/redhat-release \
	| (grep ${REDHAT_PLATFORM} > /dev/null) || (echo "Platform is not centos/redhat ${REDHAT_PLATFORM}. Script not tested elsewhere"; exit 1)
}

#self_test


MODE=tar # Besides creating a tar file, also list files and show help.

while getopts lh option # read in allowed options
do
case "${option}"
in
l) MODE=list;;
h) MODE=help;;
esac
done

case "${MODE}"
in
 list) shift ;; # shift so we can read positional arguments later
 help) usage; exit 0 ;;
esac


if [[ $# -lt 1 ]]; then
    usage
    exit 0
fi


time=$1
day=${2:-0} # default is zero, meaning today (1 is Yesterday etc)

today=$(date +"%Y%m%d")
# Include files created after a specific hour (minus one minute to also include files at the exact minute):
start_of_time_window=$(date --date "${today} ${time} ${day} day ago 1 min ago" +"%Y%m%d %H:%M")
start_of_time_window_filename_friendly=$(date --date "${today} ${time} ${day} day ago 1 min ago" +"%Y%m%d_%H%M")
cleanup
touch --date="${start_of_time_window}" ${TIMESTAMP_FILE}


# For performance, find folders then find files within those:
finding_files_expression="find ${SOURCE_FOLDER} -mindepth 1 -maxdepth 2 -type d  -name "Diag-EHG*" -newer ${TIMESTAMP_FILE} \
 | xargs -I '{}' find  '{}' -newer ${TIMESTAMP_FILE} -and -type f -and \( -name "*.xlsx" -o -name "*.pdf" \)"


case "${MODE}"
in
 list)
     echo "Listing files in ${SOURCE_FOLDER} created after ${start_of_time_window}."
     eval ${finding_files_expression}
     cleanup
     exit 0
     ;;
 tar)
    echo "Tar'ing files in ${SOURCE_FOLDER} created after ${start_of_time_window}."
    # build name of file:
    tar_file=${TARGET_FOLDER}/interpretations.since_${start_of_time_window_filename_friendly}.created_$(date +"%Y%m%d_%H%M").tar
    eval ${finding_files_expression} | sort | uniq| tar cvf ${tar_file} -T - 1> /dev/null
    echo "Added files created after $(date --date "${start_of_time_window}" +"%Y-%m-%d %H:%M" ) into ${tar_file}"
    echo "The archive has $(tar tf ${tar_file} | wc -l | awk '{print $1}') entries."
    echo "To remove files from the archive use 'tar -vf ${tar_file} --delete <name of file>'"
    cleanup
    exit 0
    ;;
esac

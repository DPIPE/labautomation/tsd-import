#!/bin/bash

#SBATCH --output=build-local-dragen-refgenome.log
#SBATCH --qos=high
#SBATCH --partition=dragen
#SBATCH --time=02:00:00

PATH=$PATH:/opt/edico/bin:/bin

CLUSTER_REFDIR=/boston/diag/production/sw/vcpipe/vcpipe-bundle/genomic/gatkBundle_2.5
DRAGEN_REFDIR=/staging/projects/p22/reference/hg19

FASTA=human_g1k_v37_decoy.fasta
FAI=human_g1k_v37_decoy.fasta.fai

HASHTABLE=human_g1k_v37_decoy.fasta.k_21.f_16.m_149.cnv


# remove old files
if [ -d  $DRAGEN_REFDIR/$HASHTABLE ]; then 
    rm -vrf $DRAGEN_REFDIR/$HASHTABLE 
fi

if [ -f $DRAGEN_REFDIR/$FASTA ]; then
    rm $DRAGEN_REFDIR/$FASTA
fi

if [ -f $DRAGEN_REFDIR/$FAI ]; then
    rm $DRAGEN_REFDIR/$FAI
fi

cp $CLUSTER_REFDIR/$FASTA $DRAGEN_REFDIR
cp $CLUSTER_REFDIR/$FAI $DRAGEN_REFDIR

mkdir -p $DRAGEN_REFDIR/$HASHTABLE
dragen --build-hash-table true --ht-num-threads 32 --ht-max-table-chunks 32 --ht-reference $DRAGEN_REFDIR/$FASTA --output-dir $DRAGEN_REFDIR/$HASHTABLE --enable-cnv true

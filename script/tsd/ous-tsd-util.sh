# various aliases and functions to navigate in the TSD file structure
# and get various stats about analyses and samples

# source this by adding the following to you ~/.bashrc:
# source <path-to-tsd-import>/?/<this-file>


FX_FOLDER="/tsd/p22/fx"
RESULT_FOLDER="/tsd/p22/data/durable/production/interpretations"
SAMPLE_IMPORT_FOLDER="/tsd/p22/fx/import_alt/production/hts/samples"
SAMPLE_IMPORT_FOLDER_CLUSTER="/cluster/projects/p22/production/samples"
ANALYSIS_IMPORT_FOLDER="${FX_FOLDER}/import_alt/production/hts/analyses"
ANALYSIS_IMPORT_FOLDER_CLUSTER="/cluster/projects/p22/production/analyses"

SW_FOLDER="/cluster/projects/p22/production/sw"


alias go-ella="cd /tsd/p22/data/durable/production/ella"
alias go-coverage="cd /tsd/p22/data/durable/coverageAnalysis"
alias go-fx-import="cd ${FX_FOLDER}/import"
alias go-fx-export="cd ${FX_FOLDER}/export"
alias go-fx-import-alt="cd ${FX_FOLDER}/import_alt"
alias go-fx-export-alt="cd ${FX_FOLDER}/export_alt"
alias go-imported-samples="cd ${SAMPLE_IMPORT_FOLDER}"
alias go-imported-analyses="cd ${ANALYSIS_IMPORT_FOLDER}"
alias go-imported-samples-cluster="cd ${SAMPLE_IMPORT_FOLDER_CLUSTER}"
alias go-imported-analyses-cluster="cd ${ANALYSIS_IMPORT_FOLDER_CLUSTER}"
alias go-vcpipe="cd ${SW_FOLDER}/vcpipe/vcpipe"
alias go-tsd-import="cd ${SW_FOLDER}/tsd-import"
alias go-result="cd ${RESULT_FOLDER}"



################################
# Find analyses and samples
################################

function check-folder() {
 if [[ $# -ne 1 ]]; then
   echo "Missing folder"
   return 1
 fi

 folder=$1

 if [[ ! -d ${folder} ]]; then
   echo "The folder ${folder} doesn't exist"
   return 1
 fi

 echo "OK"
}


function check-sample() {
 if [[ $# -ne 1 ]]; then
   echo "Missing sample"
   return 1
 fi

 sample=$1
 project_path="${SAMPLE_IMPORT_FOLDER}/${sample}*"
 if [[ ! $(find  ${project_path} -maxdepth 0 -type d 2> /dev/null ) ]]; then
   echo "No samples matching ${project_path}"
   return 1
 fi

 echo "OK: $(find  ${project_path} -maxdepth 0 -type d 2> /dev/null | wc -l) samples"
}

function watch-sample-import() {
 # usage: watch-sample-import Diag-excap132

 if [[ $# -ne 1 ]]; then
   echo "Missing project name"
   return 1
 fi

 project=$1
 project_path="${SAMPLE_IMPORT_FOLDER_CLUSTER}/${project}*/"

 if [[ ! $(find  ${project_path} -maxdepth 0 -type d 2> /dev/null ) ]]; then
   echo "No samples from ${project} in ${project_path}"
   return 1
 fi

 watch -n 10 --differences \
 "echo 'Number of samples in ${SAMPLE_IMPORT_FOLDER_CLUSTER}:';
  find ${SAMPLE_IMPORT_FOLDER_CLUSTER}/${project}*/ -maxdepth 0 -type d | wc -l;
  echo 'Size of sample folders:';
  find ${SAMPLE_IMPORT_FOLDER_CLUSTER}/${project}*/ -maxdepth 0 -type d -execdir du -hs {} 2> /dev/null \;"
}



function watch-sample-import-filelock() {
 # usage: watch-sample-import-filelock Diag-excap132

 if [[ $# -ne 1 ]]; then
   echo "Missing project name"
   return 1
 fi

 project=$1
 project_path="${SAMPLE_IMPORT_FOLDER}/${project}*/"

 if [[ ! $(find  ${project_path} -maxdepth 0 -type d 2> /dev/null ) ]]; then
   echo "No samples from ${project} in ${project_path}"
   return 1
 fi

 watch -n 10 --differences \
 "echo 'Number of samples in ${SAMPLE_IMPORT_FOLDER}:';
  find ${SAMPLE_IMPORT_FOLDER}/${project}*/ -maxdepth 0 -type d | wc -l ;
  echo 'Size of sample folders:';
  find ${SAMPLE_IMPORT_FOLDER}/${project}*/ -maxdepth 0 -type d -execdir du -hs {} 2> /dev/null \;"
}

alias watch-samples-import-filelock="watch-sample-import-filelock"

function watch-analysis-import() {
 # usage: watch-analysis-import Diag-excap132

 if [[ $# -ne 1 ]]; then
   echo "Missing project name"
   return 1
 fi

 project=$1
 project_path="${ANALYSIS_IMPORT_FOLDER_CLUSTER}/${project}*/"

 if [[ ! $(find  ${project_path} -maxdepth 0 -type d 2> /dev/null ) ]]; then
   echo "No analyses from ${project} in ${project_path}"
   return 1
 fi

 watch -n 10 --differences \
 "echo 'Number of analyses in ${ANALYSIS_IMPORT_FOLDER_CLUSTER}:';
  find ${ANALYSIS_IMPORT_FOLDER_CLUSTER}/${project}*/ -maxdepth 0 -type d | wc -l;
  echo 'Size of analyses folders:';
  find ${ANALYSIS_IMPORT_FOLDER_CLUSTER}/${project}*/ -maxdepth 0 -type d -execdir du -hs {} 2> /dev/null \;"
}

function watch-analysis-import-filelock() {
 # usage: watch-analysis-import-filelock Diag-excap132

 if [[ $# -ne 1 ]]; then
   echo "Missing project name"
   return 1
 fi

 project=$1
 watch -n 10 --differences \
 "echo 'Number of analyses in ${ANALYSIS_IMPORT_FOLDER}:';
  find ${ANALYSIS_IMPORT_FOLDER}/${project}*/ -maxdepth 0 -type d | wc -l ;"
}

function watch-result() {
 # usage: watch-result Diag-excap132

 if [[ $# -ne 1 ]]; then
   echo "Missing project name"
   return 1
 fi

 project=$1
 watch -n 10 --differences \
 "echo 'Number of analyses in ${RESULT_FOLDER}:';
  find ${RESULT_FOLDER}/${project}/* -maxdepth 0 -type d | wc -l ;"
}



################################
# Manage screen sessions
################################

function stop-executor() {
  screen -X -S "executor" quit
}

function start-executor() {
  screen -dm -S "executor" '${SW_FOLDER}/vcpipe-executor'
}

function stop-webui() {
  screen -X -S "webui" quit
}

function start-webui() {
  screen -dm -S "webui" '${SW_FOLDER}/vcpipe-ui'
}


function start-filelock-exporter() {
  screen -dm -S "filelock-exporter" '${SW_FOLDER}/tsd-import/exe/filelock-exporter'
}

function stop-filelock-exporter() {
  screen -X -S "filelock-exporter" quit
}

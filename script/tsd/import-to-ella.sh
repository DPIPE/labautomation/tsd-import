#!/bin/bash

# This script import pipeline finished analysis to ELLA by taking following steps:
# 1. check ready to import analysis of a project(in recent 30 days), e.g. Diag-excap156 in /tsd/p22/data/durable3/production/preprocessed/{singles,trios}
# 2. check already imported analysis of a project, e.g. Diag-excap156 in /tsd/p22/data/durable/production/ella/ella-prod/data/analyses/imported
# 3. list out the analysis pending import
# 4. filter out NA samples
# 5. filter out analysis not yet in /tsd/p22/data/durable/production/anno/sample-repo/samples.json
# 6. filter out analysis missing annopipe (thus missing panel information) in /tsd/p22/data/durable3/production/analyses
# 7. if multiple annopipe exists(due to reanalysis?), take the latest ctime one
# 8. filter out currently running imports or queued imports (in recent 7 days)
# 9. write the final list to /tsd/p22/data/durable/production/utilities/import-to-ella-logs/ella.import.<short_projectname>.<run_datetime>.txt
# 10. call /tsd/p22/data/durable/production/ella/ops/submit-anno-targets-batch.sh to import if chosen to import right away

# log is appended to /tsd/p22/data/durable/production/utilities/import-to-ella-logs/import-to-ella.log

set -eo pipefail

if [ $# -ne 1 ]
then
    echo
    echo 'Error, missing project!'
    echo
    echo 'USAGE:'
    echo './ella_import.sh Diag-excap155'
    echo
    exit 1
fi

PROJECT=$1

SUFFIX_SINGLE='-12345678901' # single analysis folder suffix e.g. Diag-excap159-12345678901
CTIME_ELLA_QUEUE=-7
CTIME_NEW_ANALYSIS=-30

ANNO_WORK_DIR=/tsd/p22/data/durable/production/anno/anno-prod/work
ELLA_IMPORTED_DIR=/tsd/p22/data/durable/production/ella/ella-prod/data/analyses/imported
PREPROCESSED_DIR=/tsd/p22/data/durable3/production/preprocessed
ANNO_SAMPLE_REPO=/tsd/p22/data/durable/production/anno/sample-repo/samples.json
CLUSTER_ANALYSIS_DIR=/tsd/p22/data/durable3/production/analyses
ELLA_OPS_DIR=/tsd/p22/data/durable/production/ella/ops
LOG_DIR=/tsd/p22/data/durable/production/utilities/import-to-ella-logs

# log file
log_file=${LOG_DIR}/import-to-ella.log
if [ ! -f  $log_file ]; then 
    touch $log_file
fi

to_stderr(){
    echo -e "$*" | tee -a $log_file >&2 
}

to_stderr '\n=== Script Run Date: '`date`' ==='
to_stderr "\n=== Project: ${PROJECT} ==="


# check host 
if [[ "$(hostname)" != 'p22-vc-prod-l.tsd.usit.no' && "$(hostname)" != 'p22-vc-ui-l.tsd.usit.no' ]]; then
    to_stderr 'ERROR: Run this script only on p22-vc-prod-l VM or p22-vc-ui-l VM!'
    exit 1
fi


# file to store the list of analysis to be imported to ella
ella_batch=${LOG_DIR}/ella.import.${1}.`date '+%Y%m%d_%H:%M:%S'`.txt
ella_batch_final=${LOG_DIR}/ella.import.${1}.`date '+%Y%m%d_%H:%M:%S'`.unique.txt
touch $ella_batch

#
declare -a already_queued
declare -a already_imported_trios
declare -a already_imported_singles
declare -a tobe_imported


for queued in `find $ANNO_WORK_DIR -maxdepth 2 -mindepth 2 -type f -name target.source -ctime $CTIME_ELLA_QUEUE  | xargs grep 'export SAMPLE_ID=' | cut -d'"' -f2`
do
    already_queued+=($queued)
done


# trios that are already imported
to_stderr "\nALREADY IMPORTED TRIOs (may have duplicates due to reanalysis)---------------------------:"
for exported in `ls -d "${ELLA_IMPORTED_DIR}/${PROJECT}"*-TRIO* 2> /dev/null`
do 
    fullname=`basename ${exported}` 
    name_trim_panel=`echo ${fullname} | rev | cut -d"-" -f3- | rev`
    to_stderr "\t$name_trim_panel"
    already_imported_trios+=($name_trim_panel)
done

# trios that can be imported
to_stderr "\n\nCAN BE IMPORTED TRIOs--------------------------------------------------------------------:"
for exported in `find "${PREPROCESSED_DIR}/trios" -maxdepth 1 -mindepth 1 -type d -ctime $CTIME_NEW_ANALYSIS -name "${PROJECT}*"`
do
    name_trim_panel=`basename ${exported}`
    to_stderr "\t$name_trim_panel"
    imported=false
    for ana in ${already_imported_trios[@]}
    do
        if [ $ana == $name_trim_panel ]
        then
            imported=true
            break
        fi
    done 
    if [ $imported != true ]
    then
        tobe_imported+=($name_trim_panel)
    fi
done

# singles that are already imported
to_stderr "\n\nALREADY IMPORTED SINGLES (may have duplicates due to reanalysis)-------------------------:"
for exported in `ls -d "${ELLA_IMPORTED_DIR}/${PROJECT}"* 2> /dev/null | grep -v '\-TRIO\-'`
do 
    fullname=`basename ${exported}` 
    name_trim_panel=`echo ${fullname} | rev | cut -d"-" -f3- | rev`
    to_stderr "\t$name_trim_panel"
    already_imported_singles+=($name_trim_panel)
done

# singles that can be imported
to_stderr "\n\nCAN BE IMPORTED SINGLES------------------------------------------------------------------:"
for exported in `find "${PREPROCESSED_DIR}/singles" -maxdepth 1 -mindepth 1 -type d -ctime $CTIME_NEW_ANALYSIS -name "${PROJECT}*[0-9]"`
do
    fullname=`basename ${exported}`
    name_trim_panel=${fullname:0:${#1}+${#SUFFIX_SINGLE}}
    to_stderr "\t$name_trim_panel"
    imported=false
    for ana in ${already_imported_singles[@]}
    do
        if [ $ana == $name_trim_panel ]
        then
            imported=true
            break
        fi
    done 
    if [ $imported != true ]
    then
        tobe_imported+=($name_trim_panel)
    fi
done


# trios and singles pending import (not filtered)
if [ ${#tobe_imported[@]} -eq 0 ]; then
    to_stderr "\nNothing pending import!"
    exit 0
else
    to_stderr "\n\nPENDING IMPORT TRIOS AND SINGLES (not filtered)------------------------------------------:"
    for tobe in ${tobe_imported[@]}
    do 
        to_stderr "\t$tobe"
    done
fi

# trios and singles pending import with panel (filtering)
to_stderr "\n\nPENDING IMPORT TRIOS AND SINGLES (apply filters)-----------------------------------------:" 
for tobe in ${tobe_imported[@]}
do 
    # ignore NA sample
    if [[ $tobe =  *NA12878*  ]]; then
        to_stderr "\t\e[31mx\e[0m $tobe is a test sample"
        continue
    fi
    
    # check sample.json
    grep $tobe ${ANNO_SAMPLE_REPO} > /dev/null || { to_stderr "\t\e[31mx\e[0m $tobe not in sample.json yet" ; continue; }

    # check already queued
    inqueue=false
    for queued in ${already_queued[@]}
    do
        if [ $tobe == $queued ]; then
            to_stderr "\t\e[31mx\e[0m $tobe is in queue or running now!"
            inqueue=true
            break 
        fi
    done
    if [ $inqueue = "true" ]; then continue; fi


    # if multiple match due to reanalyses, take the latest one
    fullpath_with_panel=`(ls --time=ctime -td ${CLUSTER_ANALYSIS_DIR}/${tobe}*TRIO-* 2> /dev/null) || (ls --time=ctime -td ${CLUSTER_ANALYSIS_DIR}/${tobe}-* 2> /dev/null) || true`
    if [ -z "$fullpath_with_panel" ]; then
      to_stderr "\t\e[31mx\e[0m $tobe missing annopipe (thus panel info) in ${CLUSTER_ANALYSIS_DIR}"
      continue
    else
      array_fullpath=($fullpath_with_panel)
      latest_fullpath_with_panel=${array_fullpath[0]}
    fi
    analysis_with_panle=`basename $latest_fullpath_with_panel`
    to_stderr "\t\e[32mo\e[0m $tobe is \e[32mwaiting\e[0m"
    echo $analysis_with_panle >> $ella_batch
done

# run ella import if there is any "ready to import analysis
if [ `stat -c %s $ella_batch` -ne 0 ]
then
    # filter out duplicates introduced by hybrid trio
    sort -u $ella_batch > ${ella_batch_final}

    to_stderr '\n\nPENDING IMPORT TRIOS AND SINGLES WITH PANEL (filtered, duplicates removed)---------------:\n'
    printf '\e[38;5;2m\e[1m'
    cat $ella_batch_final >&2 | tee -a $log_file
    printf '\e[0m'
    to_stderr "\nabove filtered list is written also into file $ella_batch_final\n"
    to_stderr
    read -p "import now[y/n]}?" import_now
    if [ $import_now = 'y' ]; then
        pushd ${ELLA_OPS_DIR} > /dev/null
        ./submit-anno-targets-batch.sh $ella_batch_final 2>&1 | tee -a $log_file >&2
        popd
    else
        to_stderr 'canceled!'
        exit 0
    fi
    rm $ella_batch
    to_stderr '\nDone!'
else
    to_stderr '\nNothing to be imported!'
    rm $ella_batch
fi

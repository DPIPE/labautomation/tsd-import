#!/bin/sh

if [ $# -ne 1 ]; then echo missing tar file; exit 1; fi
tar xvpPf $1

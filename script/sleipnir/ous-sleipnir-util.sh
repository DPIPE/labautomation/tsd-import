# source this by adding the following to your ~/.bashrc:
# source <path-to-tsd-import>/?/<this-file>

alias go-transfer="cd /boston/diag/transfer"

# connect to TSD file lock
function tx() {
 user="p22-$(whoami)"
 path="${user}@tsd-fx03.tsd.usit.no:/p22/export_alt"
 echo "Connecting to ${path}"
 sftp ${path}
}
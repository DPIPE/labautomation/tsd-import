#!/bin/bash

# This script packages all files of a project that need to be transferred to TSD in one tarball
# modify file path to the correct absolute path on TSD, so run untar from anywhere will put all files in their correct dirs
# change permissions of all dirs and files to 775, so no need to change permissions on TSD manually
# files/dirs are remmoved immediately after packaging
# tarball is stored in /boston/diag/transfer/Diag-<projectName>.....tar


# MUST untar with -p(--preserve-permissions) and -P(--absolute-names) flags, e.g. on TSD run `tar xpPf /tsd/p22/fx/import_alt/Diag-EKG190624.backup_pipeline_results.tar`

if [ $# -ne 1 ]
then
  echo
  echo Error: 
  echo missing short project name, e.g Diag-EKG190624
  echo 
  echo Usage:
  echo ./package_backup_pipeline_results.sh Diag-EKG190624
  echo
  exit 1
fi

# remove files thar have been added to package successfully
CLEANUP=false


if ${CLEANUP}; then 
  TAR_CLEAN=' --remove-files '
  RESULT_DIR_CLEAN=' -execdir rm -rf result \; '
else
  TAR_CLEAN=' '
  RESULT_DIR_CLEAN=' '
fi

# on NSC
SRC_BASEPATH=/boston/diag/production
src_ella_prod_incoming=/boston/diag/transfer/production/ella/ella-prod/data/analyses/incoming
src_preprocessed_singles_dir=${SRC_BASEPATH}/preprocessed/singles
src_preprocessed_trios_dir=${SRC_BASEPATH}/preprocessed/trios
src_samples_dir=${SRC_BASEPATH}/samples
src_analyses_dir=${SRC_BASEPATH}/analyses

# output dir on NSC
OUT_DIR=/boston/diag/transfer
OUT_FILE="${OUT_DIR}"/"${1}".$(date '+%Y%m%d')_backup_pipeline_results

# on TSD
dst_ella_prod_incoming=/tsd/p22/data/durable/production/ella/ella-prod/data/analyses/incoming
DST_BASEPATH2=/tsd/p22/data/durable3/production
dst_preprocessed_singles_dir=${DST_BASEPATH2}/preprocessed/singles
dst_preprocessed_trios_dir=${DST_BASEPATH2}/preprocessed/trios
dst_samples_dir=${DST_BASEPATH2}/samples
dst_analyses_dir=${DST_BASEPATH2}/analyses

# check if any not packaged results
ls -d "$src_ella_prod_incoming"/"${1}"* >/dev/null 2>&1 || { echo "NOTHING TO PACKAGE!"; exit 0; }

# package ella incoming
find ${src_ella_prod_incoming} -maxdepth 1 -mindepth 1 -type d -name $1'*' 2> /dev/null | tar cvf ${OUT_FILE} --mode='u=rwX,g=rwX,o=rX' --transform='s,^/*\([^/]*/\)\{8\}[^/]*,'${dst_ella_prod_incoming}',' ${TAR_CLEAN} -T  - > /dev/null 2>&1 || true

# package preprocessed/singles
find ${src_preprocessed_singles_dir} -maxdepth 1 -mindepth 1 -type d -name $1'*' 2> /dev/null | tar --append -f ${OUT_FILE} --mode='u=rwX,g=rwX,o=rX' --transform='s,^/*\([^/]*/\)\{4\}[^/]*,'${dst_preprocessed_singles_dir}',' ${TAR_CLEAN} -T  - > /dev/null 2>&1 || true

# package preprocessed/trios; wil not error out for non-trio also
find ${src_preprocessed_trios_dir} -maxdepth 1 -mindepth 1 -type d -name $1'*' 2> /dev/null | tar --append -f ${OUT_FILE} --mode='u=rwX,g=rwX,o=rX' --transform='s,^/*\([^/]*/\)\{4\}[^/]*,'${dst_preprocessed_trios_dir}',' ${TAR_CLEAN} -T  - > /dev/null 2>&1 || true

# package samples
find ${src_samples_dir} -maxdepth 1 -mindepth 1 -type d -name $1'*' 2> /dev/null | tar --append -f ${OUT_FILE} --mode='u=rwX,g=rwX,o=rX' --transform='s,^/*\([^/]*/\)\{3\}[^/]*,'${dst_samples_dir}',' ${TAR_CLEAN} -T  - > /dev/null 2>&1 || true

# package analyses/ .analysis and READY file
find "${src_analyses_dir}"/"${1}"* -maxdepth 1 -mindepth 1 -type f \( -name '*.analysis' -o -name READY \) -printf '%p\n' ${RESULT_DIR_CLEAN} 2> /dev/null | tar --append -f ${OUT_FILE} --mode='u=rwX,g=rwX,o=rX' --transform='s,^/*\([^/]*/\)\{3\}[^/]*,'${dst_analyses_dir}',' ${TAR_CLEAN} -T  - > /dev/null 2>&1 || true


# checksum is time consuming; append file size to filenmae for easy check completion of transfer on TSD
mv $OUT_FILE ${OUT_FILE}.size_`stat -c %s ${OUT_FILE}`.tar

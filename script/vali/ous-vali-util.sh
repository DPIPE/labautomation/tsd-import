# source this by adding the following to you ~/.bashrc:
# source <path-to-tsd-import>/?/<this-file>


alias go-transfer="cd /boston/diag/transfer"
alias go-samples="cd /boston/diag/diagInternal/samples/interpretations"


################################
# Manage screen sessions
################################

function stop-executor() {
  screen -X -S "executor" quit
}

function start-executor() {
  screen -dm -S "executor" bash '/boston/diag/production/sw/vcpipe/vcpipe-executor.sh'
}

function stop-webui() {
  screen -X -S "webui" quit
}

function start-webui() {
  screen -dm -S "webui" bash '/boston/diag/production/sw/vcpipe/vcpipe-ui.sh'
}

function stop-lims-exporter-api() {
  screen -X -S "lims-exporter-api" quit
}

function start-lims-exporter-api() {
  screen -dm -S "lims-exporter-api" bash '/boston/diag/production/sw/tsd-importer.sh'
}


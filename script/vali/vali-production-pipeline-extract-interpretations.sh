#!/bin/bash

set -euf -o pipefail

# Extract project interpretations files (typically Excel and PDF) transferred from TSD
# into unit specific folders using info from the file path

REDHAT_PLATFORM="7.7"

# ousamg-ENV-START
TARGET_HOST=vali
TARGET_PARENT_FOLDER=/boston/diag/diagInternal/samples/interpretations
TSD_FILE_PATH_TRANSFORMATION="s,tsd/p22/data/durable/production/ella/ella-prod/data/analyses/incoming,,g"
FILE_GROUP=ous-diag
TIMESTAMP_FILE=/tmp/archive-time-window
log_file="/boston/diag/transfer/sw/logs/sample-extract-$(date +"%Y-%m-%d_%H-%M").log"
# ousamg-ENV-END

function self_test() {
    cat /etc/redhat-release \
    | (grep "${REDHAT_PLATFORM}" > /dev/null) || (echo "Platform is not centos/redhat ${REDHAT_PLATFORM}. Script not tested elsewhere"; exit 1)
}

#
#
# self_test

function cleanup() {
    rm -f ${TIMESTAMP_FILE}
}


# sed expression to find unit from file path:
PATH_TO_UNIT_EKG="s/.*[Dd][Ii][Aa][Gg]-[Ee][Kk][Gg].*/EKG/"
PATH_TO_UNIT_EHG="s/.*[Dd][Ii][Aa][Gg]-[Ee][Hh][Gg].*/EHG/"
PATH_TO_UNIT_EGG="s/.*[Dd][Ii][Aa][Gg]-[Ee][Xx][Cc][Aa][Pp].*/EGG/"

# Legal units:
KNOWN_UNITS_RE="\\b(^EKG$|^EGG$|^EHG$)\\b"

if [[ $# -ne 1 ]]; then
    echo ""
    echo "Usage:"
    echo "  $(basename ${BASH_SOURCE[0]}) <path to tar>"
    echo ""
    echo "Where the tar file is usually found in /boston/diag/transfer"
    exit 1
fi

tarfile=$1

if [[ ! -f ${tarfile} ]]; then
        echo "The file ${tarfile} was not found"
        exit 1
fi



function find_unit() {
    # returns either a unit or the input  if the input isn't a known file/path
    echo $(echo ${1} | sed -e "${PATH_TO_UNIT_EKG}" \
                           -e "${PATH_TO_UNIT_EHG}" \
                           -e "${PATH_TO_UNIT_EGG}" \
                           )
}

function todays_folder() {
    # return <UNIT>/<time-stamped-folder> for a known unit, else empty
    local file=$1
    local unit_raw=$(find_unit ${file})
    if [[ ! ${unit_raw} =~ ${KNOWN_UNITS_RE} ]]; then
       echo ""
    else
       echo "${unit_raw}/$(date "+%Y%m%d")"
    fi
}

function log() {
        echo $(date +"%Y-%m-%d %H:%M:%S,%3N") " ${1}" >> ${log_file}
}

function extract() {
    # extract file to a unit specific folder or skip it if no unit legal unit is found
    local file=$1
    local tar_file=$2
    local relative_target_folder=$(todays_folder ${file})
    if [[ "${relative_target_folder}x" == "x" ]]; then
        log "Skipping ${file} from ${tar_file}"
    else
        local abs_target_folder=${TARGET_PARENT_FOLDER}/${relative_target_folder}
        mkdir -p ${abs_target_folder}
        tar x -C ${abs_target_folder} --transform=${TSD_FILE_PATH_TRANSFORMATION} -f ${tar_file} ${file}
        log "Extracted ${file} from ${tar_file} into ${abs_target_folder}"
        chgrp -R ${FILE_GROUP} ${abs_target_folder}
        chmod -R g+r ${abs_target_folder}
    fi
}



# iterate over entries in the tar file and extract each one
while read -r line
do
  extract ${line} ${tarfile}
done < <(tar tf ${tarfile})

# show samples just extracted:
now=$(date +"%Y%m%d %H%M")
just_before_now=$(date --date "${now} 1 min ago" +"%Y%m%d %H:%M")
touch --date="${just_before_now}" ${TIMESTAMP_FILE}

extracted_samples_exp="find ${TARGET_PARENT_FOLDER} -mindepth 4 -maxdepth 4 -type d -newer ${TIMESTAMP_FILE}"

total=$(eval ${extracted_samples_exp} | wc -l | awk '{print $1 }' )
ekg_samples=$(eval ${extracted_samples_exp} | (grep -c -i '/EKG/' || echo ) )
ehg_samples=$(eval ${extracted_samples_exp} | (grep -c -i '/EHG/' || echo ) )
egg_samples=$(eval ${extracted_samples_exp} | (grep -c -i '/EGG/' || echo ) )

echo "Extracted " ${total} " samples in total"
echo "Extracted " ${ekg_samples} " EKG sample(s)"
echo "Extracted " ${ehg_samples} " EHG sample(s)"
echo "Extracted " ${egg_samples} " EGG sample(s)"
sum=$((${ekg_samples} + ${ehg_samples} + ${egg_samples}  ))
[[ ${sum} -ne ${total} ]] && echo "The total number of samples extracted doesn't match the sum of the unit specific samples"

# print name of samples:
echo ""
echo "EKG samples:"
eval ${extracted_samples_exp} | ( grep -i '/EKG' 2> /dev/null | xargs -I '{}' basename '{}' ) || echo
echo ""
echo "EHG samples:"
eval ${extracted_samples_exp} | ( grep -i '/EHG' 2> /dev/null | xargs -I '{}' basename '{}' ) || echo
echo ""
echo "EGG samples:"
eval ${extracted_samples_exp} | ( grep -i '/EGG' 2> /dev/null | xargs -I '{}' basename '{}' ) || echo

cleanup
echo "Done."
echo "Please check that the files seem complete, then remove the tar file."


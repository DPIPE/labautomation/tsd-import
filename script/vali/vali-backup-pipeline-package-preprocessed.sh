#!/bin/bash

set -eu -o pipefail

# Package files the backup pipeline has put into preprocessed

REDHAT_PLATFORM="7.7"

 if [[ $# -ne 1 ]]; then
    echo ""
    echo "usage:"
    echo "  $(basename ${BASH_SOURCE[0]}) <analysis-pattern>"
    echo ""
    echo "The prefix of the analysis found in /boston/diag/transfer/production that should be ran in the pipeline"
    exit 1
fi

analysis_pattern="$1"

# ousamg-ENV-START
TARGET_HOST=vali
SOURCE_FOLDER=/boston/diag/production/preprocessed
TARGET_FOLDER=/boston/diag/transfer
# ousamg-ENV-END

function self_test() {
    cat /etc/redhat-release \
    | (grep "${REDHAT_PLATFORM}" > /dev/null) || (echo "Platform is not centos/redhat ${REDHAT_PLATFORM}. Script not tested elsewhere"; exit 1)
}

#self_test

# package
tar_file="${TARGET_FOLDER}/preprocessed-from-backup-pipeline-vali-${analysis_pattern}-$(date +"%Y%m%d_%H%M").tgz"
pushd ${SOURCE_FOLDER} > /dev/null
tar cvf ${tar_file} singles/${analysis_pattern}*/ trios/${analysis_pattern}*/
popd > /dev/null

echo "Done."
echo "Transfer ${tar_file} to TSD's fx/import_alt and extract the content to durable2/production/preprocessed/"
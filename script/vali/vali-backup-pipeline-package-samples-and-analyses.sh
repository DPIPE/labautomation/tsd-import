#!/bin/bash

set -eu +o noglob  -o pipefail

# Tar samples and analysis used in backup pipeline for transfer to TSD


REDHAT_PLATFORM="7.7"

 if [[ $# -ne 1 ]]; then
    echo ""
    echo "usage:"
    echo "  $(basename ${BASH_SOURCE[0]}) Diag-excap151-190"
    echo ""
    echo "The prefix of the analysis found in /boston/diag/production whose sample and analysis files should be copied to TSD's durable storage."
    echo "Don't add any * at the end of the pattern"
    exit 1
fi

analysis_pattern="$1"

# ousamg-ENV-START
TARGET_HOST=vali
SOURCE_FOLDER=/boston/diag/production
TARGET_FOLDER=/boston/diag/transfer
FILE_GROUP=ous-diag
# ousamg-ENV-END

function self_test() {
    cat /etc/redhat-release \
    | (grep "${REDHAT_PLATFORM}" > /dev/null) || (echo "Platform is not centos/redhat ${REDHAT_PLATFORM}. Script not tested elsewhere"; exit 1)
}

#self_test

# Change folder no avoid including root folders in tar file"
pushd ${SOURCE_FOLDER} > /dev/null
tar_file=${TARGET_FOLDER}/sample-and-analyses-from-backup-pipeline-vali-${analysis_pattern}-$(date +"%Y%m%d_%H%M").tar
find samples/${analysis_pattern}*/ -print0 | tar -cf ${tar_file} --null -T -
find analyses/${analysis_pattern}*/ -type f \( -name 'READY' -o -name '*.analysis' \) -print0 | tar --append -f ${tar_file} --null -T -
popd > /dev/null

echo "Done."
echo "Transfer ${tar_file} to fx/import_alt on tsd and extract into the proper folder"
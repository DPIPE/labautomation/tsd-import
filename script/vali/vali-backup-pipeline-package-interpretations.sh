#!/usr/bin/env bash

set -eu -o pipefail

# Package project interpretations files (typically Excel and PDF) for transfer to TSD

REDHAT_PLATFORM="7.7"

# ousamg-ENV-START
TARGET_HOST=vali
SOURCE_FOLDER=/boston/diag/production/interpretations
TARGET_FOLDER=/boston/diag/transfer
# ousamg-ENV-END


 if [[ $# -ne 1 ]]; then
    echo ""
    echo "usage:"
    echo "  $(basename ${BASH_SOURCE[0]}) Diag-excap151-190"
    echo ""
    echo "The prefix of the analyses result files found in ${SOURCE_FOLDER} to be transferred to TSD's durable storage."
    echo "Don't add any * at the end of the pattern"
    exit 1
fi

analysis_pattern="$1"

function self_test() {
   cat /etc/redhat-release \
     | (grep "${REDHAT_PLATFORM}" > /dev/null) || (echo "Platform is not centos/redhat ${REDHAT_PLATFORM}. Script not tested elsewhere"; exit 1)
}

#self_test


# Change folder no avoid including root folders in tar file"
pushd ${SOURCE_FOLDER} > /dev/null
tar_file=${TARGET_FOLDER}/interpretations-files-from-backup-pipeline-vali-${analysis_pattern}-$(date +"%Y%m%d_%H%M").tar
tar cf ${tar_file} ${analysis_pattern}*/
popd > /dev/null
echo "Done."
echo "To remove files from the archive use 'tar -vf ${tar_file} --delete <name of file>'"
echo "Transfer ${tar_file} to fx/import_alt on tsd and extract into the proper folder"

BRANCH ?= $(shell git rev-parse --abbrev-ref HEAD | tr '[[:upper:]]' '[[:lower:]]' )
PIPELINE_ID ?= tsd-import-$(BRANCH)
MOUNTROOT ?= /storage/ci-testing/$(PIPELINE_ID)/$(CI_JOB_ID)
BUNDLE_LOCATION ?= /b/
SCRATCH_LOCATION ?= /scratch
IMAGE_NAME = local/tsd-import-centos-$(BRANCH)


.PHONY: help

help :
	@echo ""
	@echo "-- COMMANDS --"
	@echo "make test-archive 		 	Test the archive script"
	@echo "make test-extract 		 	Test the script that places tar content into unit specific folders"
	@echo "make test-genepanel-version 	Test that we get the latest approved version from bundle"
	@echo "make kill 		         - stop and remove the container (if not removed after having created the overview)"

#---------------------------------------------
# DEVELOPMENT
#---------------------------------------------
.PHONY: _build _pre-clean _start_container _stop_container _test_archive test_archive _test_extract test-extract kill shell

########################################################################
# common
########################################################################

kill:
	docker stop $(N)
	docker rm $(N)

shell:
	@docker exec -e COLUMNS="`tput cols`" -e LINES="`tput lines`" -ti $(N)  bash

_build:
	docker build -t $(IMAGE_NAME) -f Dockerfile .


_pre-clean:
	-@docker stop $(CNT_NAME) > /dev/null
	-@docker rm $(CNT_NAME) > /dev/null


_start-container:
	docker run -d \
	  -v $(shell pwd)/script:/script \
	  -v $(shell pwd)/tests:/tests \
	  -v $(shell pwd)/:/tsd-import \
	  -it --name $(CNT_NAME) $(IMAGE_NAME) run.sh
	@echo "Container $(CNT_NAME) is running"


_stop-container:
	@docker stop $(CNT_NAME) > /dev/null
	@docker rm $(CNT_NAME) > /dev/null



########################################################################
# test-archive
########################################################################

test-archive: _build
	$(eval CNT_NAME := $(PIPELINE_ID)-archive)
	@make _pre-clean CNT_NAME=$(CNT_NAME)
	@make _start-container CNT_NAME=$(CNT_NAME)
	@docker exec -t $(CNT_NAME) /bin/sh '/tests/test-archive.sh'
	@docker stop $(CNT_NAME) > /dev/null
	@docker rm $(CNT_NAME) > /dev/null


########################################################################
# test-extract
########################################################################

test-extract: _build
	$(eval CNT_NAME := $(PIPELINE_ID)-extract)
	@make _pre-clean CNT_NAME=$(CNT_NAME)
	@make _start-container CNT_NAME=$(CNT_NAME)
	@docker exec -t $(CNT_NAME) /bin/sh '/tests/test-extract.sh'
	@docker stop $(CNT_NAME) > /dev/null
	@docker rm $(CNT_NAME) > /dev/null

########################################################################
# test lims exporter
########################################################################

test-genepanel-version: _build
	$(eval CNT_NAME := $(PIPELINE_ID)-genepanel)
	@make _pre-clean CNT_NAME=$(CNT_NAME)
	@make _start-container CNT_NAME=$(CNT_NAME)
	@docker exec -t $(CNT_NAME) /bin/sh '/tests/test-genepanel.sh'
	@docker stop $(CNT_NAME) > /dev/null
	@docker rm $(CNT_NAME) > /dev/null


########################################################################
# test filelock exporter
########################################################################

test-filelock-exporter: _build
	$(eval CNT_NAME := $(PIPELINE_ID)-filelock)
	@make _pre-clean CNT_NAME=$(CNT_NAME)
	@make _start-container CNT_NAME=$(CNT_NAME)
	@docker exec -t $(CNT_NAME) /bin/sh '/tests/test-filelock-exporter.sh'
	@docker stop $(CNT_NAME) > /dev/null
	@docker rm $(CNT_NAME) > /dev/null


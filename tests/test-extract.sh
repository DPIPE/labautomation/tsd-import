#!/usr/bin/env bash

set -euf -o pipefail
source /script/bash-util.sh

SOURCE=/tsd/p22/data/durable/production/ella/ella-prod/data/analyses/incoming
TARGET_PARENT_FOLDER=/boston/diag/diagInternal/samples/interpretations/
EXTRACT_LOG_FOLDER=/boston/diag/transfer/sw/logs
ZIP_FOLDER=/tsd/p22/fx/export_alt
PROJECT_EKG=Diag-EKG181113
PROJECT_EHG=Diag-EHG181016
PROJECT_EGG=Diag-excap130
PROJECT_EGG_ALT=Diag-exCaP130
PROJECT_UNKNOWN=Diag-research



function cleanup() {
  find ${SOURCE} -type f -execdir rm {} \;
  find ${TARGET_PARENT_FOLDER} -type f -execdir rm {} \;
  rm ${ZIP_FOLDER}
}

mkdir -p ${EXTRACT_LOG_FOLDER}
mkdir -p ${SOURCE}
mkdir -p ${TARGET_PARENT_FOLDER}
mkdir -p ${ZIP_FOLDER}
mkdir -p ${SOURCE}/${PROJECT_EHG}/Diag-Sample_A
mkdir -p ${SOURCE}/${PROJECT_UNKNOWN}/Diag-Sample_A
touch ${SOURCE}/${PROJECT_EHG}/Diag-Sample_A/ehg_file.pdf
touch ${SOURCE}/${PROJECT_UNKNOWN}/Diag-Sample_A/unknown.pdf


/script/tsd/tsd-make-sample-tar.sh "0100" 3 1> /dev/null

##########################################################
# Test 1: files are extracted to unit specific folders
##########################################################


yellow "Extract into unit specific folders:"

/script/vali/vali-production-pipeline-extract-interpretations.sh $(find ${ZIP_FOLDER} -type f )  1> /dev/null

# the actual testing:
find  ${TARGET_PARENT_FOLDER} -type f | (grep "ehg_file.pdf" | grep "EHG" 1> /dev/null || (red "Error: Expected ehg_file.pdf and EHG folder"; exit 1) )
find  ${TARGET_PARENT_FOLDER} -type f | (grep -vz "unknown.pdf" 1> /dev/null || (red "Shouldn't be files from unknown projects"; exit 1) )
find  ${TARGET_PARENT_FOLDER} -type f | (grep -vz "tsd/p22" 1> /dev/null || (red "Extracted file shouldn't use the TSD folder structure"; exit 1) )

green "OK"

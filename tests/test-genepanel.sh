#!/usr/bin/env bash

set -euf -o pipefail
source /script/bash-util.sh

yellow "Testing that latest active version of a genepanel is chosen"

cat <<EOF > /tmp/genepanels.json
{
    "clinicalGenePanels": {
        "BRYSTogTARM_v02": {
            "inactive": true
        },
        "BRYSTogTARM_v03": {
        },
        "BRYSTogTARM_v04": {
            "inactive": true
        }
    }
}
EOF

# Removing imports that sets up DB and what-not:
sed -i"" \
 -e 's/^import analysisCreatorAPI/#import analysisCreatorAPI/' \
 -e 's/^import sampleCreatorAPI/#import sampleCreatorAPI/' \
 -e 's/^import sampleWatcherAPI/#import sampleWatcherAPI/' \
 -e 's/^import sampleMoverAPI/#import sampleMoverAPI/' \
 /tsd-import/src/lims_exporter_api/lims_exporterAPI.py

python - <<EOF  || (red "Error"; exit 1)
from lims_exporter_api import lims_exporterAPI
assert 3 == lims_exporterAPI.get_latest_panel_versions("/tmp/genepanels.json")['BRYSTogTARM'],  "Error. Expected 3 as the latest version of BRYSTogTARM"

EOF

green "OK"
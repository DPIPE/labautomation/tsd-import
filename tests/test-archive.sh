#!/usr/bin/env bash

set -euf -o pipefail
source /script/bash-util.sh

SOURCE=/tsd/p22/data/durable/production/ella/ella-prod/data/analyses/incoming
DESTINATION_FOLDER=/tsd/p22/fx/export_alt/
PROJECT1=Diag-EHG181113
PROJECT2=Diag-EHG200403
PROJECT3=Diag-NotEHG200403

function cleanup() {
  find ${SOURCE} -type f -execdir rm {} \;
  find ${DESTINATION_FOLDER} -type f -execdir rm {} \;
}

mkdir -p ${SOURCE}
mkdir -p ${DESTINATION_FOLDER}
mkdir -p ${SOURCE}/${PROJECT1}/Diag-EHGSample_A
mkdir -p ${SOURCE}/${PROJECT2}/Diag-EHGSample_B
mkdir -p ${SOURCE}/${PROJECT3}/Diag-NotEHGSample_B
mkdir -p ${SOURCE}/${PROJECT2}/Diag-EHGSample_C



/script/tsd/tsd-make-sample-tar.sh -l "0015" #1> /dev/null

##########################################################
# Test 1: Listing files
##########################################################

cleanup

yellow "List files, no archiving"

touch --date="$(date "+%Y%m%d 0010")" ${SOURCE}/${PROJECT1}/Diag-EHGSample_A/before.pdf
touch --date="$(date "+%Y%m%d 0020")" ${SOURCE}/${PROJECT2}/Diag-EHGSample_B/after.pdf

/script/tsd/tsd-make-sample-tar.sh -l "0015" 1> /dev/null
if [[ ! -z "$(ls -A ${DESTINATION_FOLDER})" ]]; then red "${DESTINATION_FOLDER} Should be empty"; exit 1; fi
green "OK"

##########################################################
# Test 2: Show help
##########################################################

cleanup

yellow "Show help"

/script/tsd/tsd-make-sample-tar.sh -h "0015" 1> /dev/null
if [[ ! -z "$(ls -A ${DESTINATION_FOLDER})" ]]; then red "${DESTINATION_FOLDER} Should be empty"; exit 1; fi

/script/tsd/tsd-make-sample-tar.sh -h 1> /dev/null
if [[ ! -z "$(ls -A ${DESTINATION_FOLDER})" ]]; then red "${DESTINATION_FOLDER} Should be empty"; exit 1; fi

/script/tsd/tsd-make-sample-tar.sh  1> /dev/null
if [[ ! -z "$(ls -A ${DESTINATION_FOLDER})" ]]; then red "${DESTINATION_FOLDER} Should be empty"; exit 1; fi

green "OK"

##########################################################
# Test 3: archive files created after today at 13:00
##########################################################

cleanup

yellow "Archive today's samples"

touch --date="$(date "+%Y%m%d 0005")" ${SOURCE}/${PROJECT1}/Diag-EHGSample_A/before.pdf
touch --date="$(date "+%Y%m%d 0009")" ${SOURCE}/${PROJECT2}/Diag-EHGSample_B/after.pdf

/script/tsd/tsd-make-sample-tar.sh "0007" 1> /dev/null

# the actual testing:
# is there a tar file?
ls ${DESTINATION_FOLDER} | grep interpretations > /dev/null

# Check content:
created_zip=$(ls ${DESTINATION_FOLDER}) # assume a single zip file
tar_size=$(tar tf ${DESTINATION_FOLDER}/${created_zip} | grep pdf | wc -l | awk '{print $1}')
[[  ${tar_size} == 1 ]] || (red "Expected one entry in the archive, was ${tar_size}"; exit 1)
tar tf ${DESTINATION_FOLDER}/${created_zip} \
 | (grep after.pdf > /dev/null || (red "Missing after.pdf from archive"; exit 1))

tar tf ${DESTINATION_FOLDER}/${created_zip} \
 | (grep -zv before.pdf > /dev/null || (red "before.pdf shouldn't be in the archive"; exit 1))

green "OK"

##########################################################
# Test 4: archive files created after yesterday at 16:00
##########################################################
cleanup

yellow "Archive samples starting from yesterday"

touch --date="$(date "+%Y%m%d 1300 " --date "2 day ago")" ${SOURCE}/${PROJECT1}/Diag-EHGSample_A/before.pdf
touch --date="$(date "+%Y%m%d 1300 " --date "1 day ago")" ${SOURCE}/${PROJECT2}/Diag-EHGSample_B/after.pdf
touch --date="$(date "+%Y%m%d 1300 " --date "1 day ago")" ${SOURCE}/${PROJECT3}/Diag-NotEHGSample_B/after-not-include.pdf
touch --date="$(date "+%Y%m%d 1300")" ${SOURCE}/${PROJECT2}/Diag-EHGSample_C/after2.pdf


/script/tsd/tsd-make-sample-tar.sh "1300" 1 1> /dev/null

# Check content:
created_zip=$(ls ${DESTINATION_FOLDER}) # assume a single zip file
tar tf ${DESTINATION_FOLDER}/${created_zip}
tar tf ${DESTINATION_FOLDER}/${created_zip} \
 | (grep after.pdf > /dev/null || (red "Missing after.pdf from archive"; exit 1))
tar tf ${DESTINATION_FOLDER}/${created_zip} \
 | (grep after2.pdf > /dev/null || (red "Missing after2.pdf from archive"; exit 1))
tar tf ${DESTINATION_FOLDER}/${created_zip} \
 | (grep -zv before.pdf > /dev/null || (red "before.pdf shouldn't be in the archive"; exit 1))
tar tf ${DESTINATION_FOLDER}/${created_zip} \
 | (grep -zv after-not-include.pdf > /dev/null || (red "after-not-include.pdf (not from EHG project) shouldn't be in the archive"; exit 1))

green "OK"

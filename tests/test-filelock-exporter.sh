#!/usr/bin/env bash

set -ef -o pipefail

source /script/bash-util.sh


# revert changes done in test:
trap "sed -i'' 's/^CHANGE_INTERVAL = 2/CHANGE_INTERVAL = 30/' /tsd-import/src/filelock_exporter/filelock_exporter.py" exit
groupadd p22-member-group

yellow "Testing that filelock-exporter watches folders for analyses and samples"

A_DIR=/tsd/p22/fx/import_alt/production/hts/analyses
S_DIR=/tsd/p22/fx/import_alt/production/hts/samples
LOG_DIR=/cluster/projects/p22/production/sw/logs/tsd-import/filelock-exporter

mkdir -p ${A_DIR}
mkdir -p ${S_DIR}

sed -i"" 's/^CHANGE_INTERVAL = 30/CHANGE_INTERVAL = 2/' /tsd-import/src/filelock_exporter/filelock_exporter.py

/tsd-import/exe/filelock-exporter &

name="test_sample"

mkdir ${A_DIR}/${name}/ && touch ${A_DIR}/${name}/${name}.analysis

mkdir ${S_DIR}/${name}/
for f in L002_R1_001.fastq.gz L002_R1_001_fastqc.tar L002_R2_001.fastq.gz L002_R2_001_fastqc.tar
do
   touch ${S_DIR}/${name}/${name}_${f}
done

# manipulate checksum since we have an empty read file:
cat /tsd-import/tests/Diag-excap1-NA12878.sample \
| sed -e 's/dfd786e8c36c373f322cbbd08a67d8d4/d41d8cd98f00b204e9800998ecf8427e/' \
      -e 's/9f6e9abc5c0d66635658e938b9d65d86/d41d8cd98f00b204e9800998ecf8427e/' \
      -e 's/Diag-excap30-NA12878-EEogPU-v01-KIT-Av5_GCCAAT/test_sample/g' \
      -e '/taqman/d' \
> ${S_DIR}/${name}/${name}.sample

sleep 3

cat ${LOG_DIR}/`ls -tr ${LOG_DIR}| tail -n1` | grep "Analysis validated" || (echo "missing text in log file"; red "error"; exit 1)
cat ${LOG_DIR}/`ls -tr ${LOG_DIR}| tail -n1` | grep "Sample Diag-excap1-NA12878 validated" || (echo "missing text in log file"; red "error"; exit 1)

green "Done"
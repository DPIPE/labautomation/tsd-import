# Release notes

Issues are found in https://gitlab.com/DPIPE/labautomation/tsd-import

## v7.0
- [#39](https://gitlab.com/DPIPE/labautomation/tsd-import/-/issues/39) Change sample archive path on TSD, remove uploading multiQC results to megaQC in the filelock and filelock accept ora and cram files

## v6.0
- [#22](https://gitlab.com/DPIPE/labautomation/tsd-import/-/issues/22) Use new TSD/NSC folder structure
- [#25](https://gitlab.com/DPIPE/labautomation/tsd-import/-/issues/25) Skip analyses folder in transfer area for NSC pipelines
- [#26](https://gitlab.com/DPIPE/labautomation/tsd-import/-/issues/26) filelock-exporter-api safeguard subprocess calls
- [#27](https://gitlab.com/DPIPE/labautomation/tsd-import/-/issues/27) The nsc-exporter handles duplicates

## v5.0
- [#14](https://gitlab.com/DPIPE/labautomation/tsd-import/-/issues/14) Use put --recursive for nsc-exporter
- [#21](https://gitlab.com/DPIPE/labautomation/tsd-import/-/issues/21) Handle folders in s3api which already exist on the destination
- [#20](https://gitlab.com/DPIPE/labautomation/tsd-import/-/issues/20) Use rotated log for filelock-exporter-api
- [#19](https://gitlab.com/DPIPE/labautomation/tsd-import/-/issues/19) Fix 2 log files
- [#18](https://gitlab.com/DPIPE/labautomation/tsd-import/-/issues/18) Stream s3cmd logs to nsc-exporter logs
- [#17](https://gitlab.com/DPIPE/labautomation/tsd-import/-/issues/17) Trap to remove NSC-exporter-ACTIVE
- [#16](https://gitlab.com/DPIPE/labautomation/tsd-import/-/issues/16) Catch  HTTPErrors
- [#8](https://gitlab.com/DPIPE/labautomation/tsd-import/-/issues/8) Stop EKG samples with low number of sequencing reads into pipeline
- [#24](https://gitlab.com/DPIPE/labautomation/tsd-import/-/issues/24) Add overwrite policy to filelock-exporter-api

## v4.2.1
- [#12](https://gitlab.com/DPIPE/labautomation/tsd-import/-/issues/12) Create release template
- [#13](https://gitlab.com/DPIPE/labautomation/tsd-import/-/issues/13) Add handstand as code owner

## v4.2
- [#9] Adapt lims export to new structure of gene panels


## v4.1
- [#6] Make lims exporter handle reanalysis of cardio samples (Xuyang Yuan)

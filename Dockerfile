################## BASE IMAGE ####################

FROM centos:centos7.7.1908

################## METADATA ######################

LABEL io.ousamg.base_image="centos7.7.1908"
LABEL io.ousamg.version="1"
LABEL io.ousamg.about.summary="Testing archiving and lims exporter"
LABEL io.ousamg.about.home="http://git.ousamg.io/apps/tsd-importer"

MAINTAINER Erik Severinsen <erik.severinsen@medisin.uio.no>

##################################################


ENV TERM xterm-256color

RUN yum -y update &&  \
    yum -y install \
       make \
       gcc  \
       zlib \
       wget \
       bzip2-devel \
       libarchive-devel \
       openssl-devel \
       python-devel

# Python
RUN cd /tmp && \
    wget https://www.python.org/ftp/python/2.7.8/Python-2.7.8.tgz && \
    tar xvfz Python-2.7.8.tgz && \
    cd Python-2.7.8 && \
    ./configure --prefix=/usr/local LDFLAGS="-Wl,-rpath /usr/local/lib" && \
    make && make altinstall && \
    ln -sv /usr/local/bin/python2.7 /usr/local/bin/python && \
    rm -rf /tmp/Python-2.7.8*


# Python modules
RUN cd /tmp && wget "https://bootstrap.pypa.io/get-pip.py" && python get-pip.py && rm /tmp/get-pip.py
ADD requirements.txt /tmp/requirements.txt
RUN grep -v -E "psyco|SQLA" /tmp/requirements.txt > /tmp/less-requirements.txt
RUN python -m pip install -r /tmp/less-requirements.txt

# needed by filelock exporter test:
RUN yum -y install rsync

RUN groupadd ous-diag
COPY run.sh /run.sh


ENV PYTHONPATH=$PYTHONPATH:/tsd-import/src

ENTRYPOINT ["/run.sh"]

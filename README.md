# tsd-import
TSD import and LIMS integration.

### lims-exporter

For usage, see `./lims-exporter --help`.

For testing you can use the supplied testdata:

```
./lims-exporter --repo-sample /some/path/samples --repo-analysis /some/path/intermediate/analyses --source tsd-import/src/lims_exporter/testdata/delivery --taqman-source tsd-import/src/lims_exporter/testdata/taqman --mark-ready
```

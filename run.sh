#!/bin/sh
echo "Sleeping..."
# Spin until we receive a 15=SIGTERM (e.g. from `docker stop`), 2=SIGINT, 3=SIGQUIT
trap 'exit 143' 2 3 15 # exit = 128 + 15 (SIGTERM)
tail -f /dev/null & wait ${!}
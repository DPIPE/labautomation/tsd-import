#!/bin/bash

set -euf -o pipefail

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo "######################"
echo "tsd-importer exporter"
echo "######################"

usage() {
        echo "Usage: export.sh [-o output_dir] [-n name of branch/tag] [-h Show help] [dir1 dir2 dir3 (Dirs to include, default is all)]"
        echo "       If repo is 'DETACHED', names of branches/tags are not avaiable for git. If it's known to the caller it can"
        echo "       be give as -n ... and it would be used in naming the generated archive."
}

while getopts "ho:n:" opt; do
    case "$opt" in
    h)
        usage
        exit 0
        ;;
    o)
        output_dir=${OPTARG}
        ;;
    n)
        meaningful_revision=${OPTARG}
        ;;
    *)
        usage
        exit 0
        ;;
    esac
done

shift $((OPTIND-1))

if [[ "${#}" -eq 1 ]]; then
    PATHS="";
    FILEPATH_ENDING=""
else
    PATHS="${@:2}"
    FILEPATH_ENDING="-partial"
fi


# Set to $DIR if not set manually
if [ -z ${output_dir+x} ]; then
    output_dir="${DIR}"
fi

# Enter $DIR
pushd $DIR > /dev/null

REV="$1"
REVISION=$(git rev-parse --short=8 ${REV})
meaningful_revision_final=${meaningful_revision:-$REV} # assume $REF is a branch/tag when -b is not given

FILE_COMMON="tsd-import-${meaningful_revision_final}-${REVISION}${FILEPATH_ENDING}"
ARCHIVE_FILE="${FILE_COMMON}.tar.gz"
ARCHIVE_META_FILE="${FILE_COMMON}.txt"
ARCHIVE_SHA_FILE="${FILE_COMMON}.sha1"

echo "Exporting repo to ${output_dir}/${ARCHIVE_FILE}"
git archive -v --prefix tsd-import/ --output "${output_dir}/${ARCHIVE_FILE}" $REV $PATHS 2> /dev/null
echo "Release ${FILE_COMMON}" > ${output_dir}/${ARCHIVE_META_FILE}
echo "-------------------------------------" >> ${output_dir}/${ARCHIVE_META_FILE}
pushd ${output_dir} > /dev/null # cd into output folder so shasum output is without full path!!
echo "Run 'shasum -c ${ARCHIVE_SHA_FILE}' in the folder of the archive file to verify it's content" >> ${output_dir}/${ARCHIVE_META_FILE}
shasum ${ARCHIVE_FILE} | tee -a ${output_dir}/${ARCHIVE_META_FILE} > ${output_dir}/${ARCHIVE_SHA_FILE}
file ${ARCHIVE_FILE} >> ${output_dir}/${ARCHIVE_META_FILE}
echo "" >> ${output_dir}/${ARCHIVE_META_FILE}
echo "Generated $(date) on $(hostname) ($(uname -a))" >> ${output_dir}/${ARCHIVE_META_FILE}
popd > /dev/null


# Exit $DIR
popd > /dev/null

echo "Done!"
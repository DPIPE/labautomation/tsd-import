# Release vX.X.X

Update this issue while working on the release.

## Release content

tsd-import vX.X.X

## Prepare
- [ ] Agree on scope
- [ ] Link all issues to this release
- [ ] Endringskontroll if relevant (only for significant source code changes, not hotfixes) See attachment of ‘HTS Bioinf  - Process for updates to production pipelines (93783)’ in ehåndbok.
- [ ] Create a folder on TSD to document testing (usually durable2/investigations, refer to it in EK)
- [ ] Pre-announce to the public (diag-bioinf at UiO, diag-lab at UiO, kvalitet at OUS)

## Testing
- [ ] CI
- [ ] Manual

## Create release artifact
- [ ] merge all relevant MR into `dev`
- [ ] merge `dev` into `master`
- [ ] Update release notes inn repo: release-notes.md
- [ ] tag on `master`
- [ ] ./export.sh {tag}

## Deploy
- [ ] Endringskontroll approved (not for hotfix)
- [ ] Copy to sw/archive on beta
- [ ] Unpack on beta
- [ ] Copy to sw/archive on to TSD
- [ ] Unpack on TSD

## Announcement
- [ ] User announcement (diag-bioinf at UiO, diag-lab at UiO, kvalitet at OUS)

```
tsd-import ble oppdatert ____. Nedenfor er endringene kort beskrevet.

Oppsummering
------------------
gitlab// ..#issue Summary/title (person responsible)
```

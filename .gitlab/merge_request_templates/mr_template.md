## Description

Fixes issue: 

## Checklist

- [ ] Refers to a Jira issue

- [ ] This MR has a description (including any risks) giving the reviewer context

- [ ] Tests have been done (please state how changes has been tested)

- [ ] Change and change description in UNRELEASED has been approved by customer

- [ ] Changes to documentation (wiki/ehåndbok/comments/other - please describe)
